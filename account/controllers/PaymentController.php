<?php

namespace frontend\account\controllers;

use yii;
use yii\web\Controller;
use yii\helpers\Url;
use common\models\User;
use frontend\account\models\UserPayment;

class PaymentController extends Controller
{
    private $user_id;

    public function init()
    {
        $this->user_id = Yii::$app->user->identity->id;

    }

    public function actionPaymentShare()
    {
        $this->setUserPaidStatus($this->user_id, 1);
    }

    private function setUserPaidStatus($user_id, $paid_status){
        $payment = Yii::$app->request->post('payment', false);
        if($payment){
            if(!isset($_SESSION['payment_share_2b']))
                $_SESSION['payment_share_2b'] = 0;

            $_SESSION['payment_share_2b'] = $_SESSION['payment_share_2b'] + 1;
            if($_SESSION['payment_share_2b'] == 3){
                unset($_SESSION['payment_share_2b']);
                $user = User::find()->where(['id' => $user_id])->one();
                $user->paid_status = $paid_status;
                $user->update();
                UserPayment::updatePaymentData($user_id, 20);
                UserPayment::activateDesignerPortfolio($user_id, 1);
                echo 3;
            }

        }

    }

}
