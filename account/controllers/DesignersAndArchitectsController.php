<?php

namespace frontend\account\controllers;

use yii;
use frontend\models\ProfileReviews;
use frontend\account\models\User;
use frontend\models\UserHonours;
use frontend\models\UserVideos;
use frontend\models\UserLicenses;
use frontend\account\models\Portfolio;
use frontend\account\models\ItemDescription;
use frontend\account\models\AgentDesigners;
use frontend\models\PortfolioCollections;
use frontend\models\Language;
use yii\web\Controller;
use yii\data\Pagination;
use frontend\account\models\AgentProjectDescription;

class DesignersAndArchitectsController extends Controller
{
    private $user_id;
    private $language = "ru";


    public function init()
    {
        $this->user_id = Yii::$app->user->identity->id;
        $this->language = Language::getCurrent()->url;
    }

    public function actionIndex()
    {
        $my_designers_id = AgentDesigners::getDesignersIdByGroup($this->user_id);
        $users_data = false;

        if($my_designers_id){
            $q = Yii::$app->request->get('q', '');
            $column = Yii::$app->request->get('column', '');
            if ($q && $column) {
                $users_data = $this->getMyDesignersAndArchitects($q, $column, $my_designers_id);
            } else {
                $users_data = $this->getMyDesignersAndArchitects($q, $column, $my_designers_id);
            }
        }


        return $this->render('index', [
            'users_data' => $users_data,
        ]);
    }

    public function actionAddDesigner()
    {
        $request = Yii::$app->request;
        if(AgentDesigners::getDesignersCountByAgentId($this->user_id) <= 30 && !AgentDesigners::findOne(['id_designer' => $request->post('id_designer'), 'id_agent' => $this->user_id])){
            if (AgentDesigners::addDesigner($this->user_id, $request->post('id_designer')))
                return 'done!';
        }

    }

    public function actionRemoveDesigner()
    {
        $request = Yii::$app->request;
        if (AgentDesigners::removeDesigner($this->user_id, $request->post('id_designer')))
            return 'done!';
    }

    public function actionAll()
    {
        $q = Yii::$app->request->get('q', '');
        $column = Yii::$app->request->get('column', '');
        if ($q && $column) {
            $users_data = $this->getAllDesignersAndArchitects($q, $column);
        } else {
            $users_data = $this->getAllDesignersAndArchitects();
        }
        $my_designers_id = AgentDesigners::getDesignersIdByAgentId($this->user_id);
        return $this->render('all', [
            'my_designers_id'=> $my_designers_id,
            'users_data' => $users_data,
        ]);
    }

    private function getAllDesignersAndArchitects($q = '', $column = '')
    {
        if ($q) {
            $pagination = new Pagination([
                'defaultPageSize' => 6,
                'totalCount' => User::find()->where(['roles' => 'designer'])->andWhere(['LIKE', $column, $q])->count()
            ]);

            $users = User::allDesignersAndArchitects($pagination->offset, $pagination->limit, $q, $column);
        } else {
            $pagination = new Pagination([
                'defaultPageSize' => 6,
                'totalCount' => User::find()->where(['roles' => 'designer'])->count()
            ]);

            $users = User::allDesignersAndArchitects($pagination->offset, $pagination->limit);
        }

        return ['users' => $users, 'pagination' => $pagination];
    }

    private function getMyDesignersAndArchitects($q = '', $column = '', $id_designers)
    {
        if ($q) {
            $pagination = new Pagination([
                'defaultPageSize' => 6,
                'totalCount' => User::find()->where(['roles' => 'designer'])->andWhere(['in', 'id', $id_designers])->andWhere(['LIKE', $column, $q])->count()
            ]);

            $users = User::myDesignersAndArchitects($pagination->offset, $pagination->limit, $q, $column, implode(',',$id_designers));
        } else {
            $pagination = new Pagination([
                'defaultPageSize' => 6,
                'totalCount' => User::find()->where(['roles' => 'designer'])->andWhere(['in', 'id', implode(',',$id_designers)])->count()
            ]);

            $users = User::myDesignersAndArchitects($pagination->offset, $pagination->limit, $q, $column, implode(',',$id_designers));
        }

        return ['users' => $users, 'pagination' => $pagination];
    }


}