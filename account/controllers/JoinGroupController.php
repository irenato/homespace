<?php

namespace frontend\account\controllers;

use frontend\account\models\User;
use yii;
use yii\web\Controller;
use frontend\account\models\Groups;
use frontend\account\models\ProductsCollections;
use frontend\account\models\Likes;
use frontend\account\models\AgentDesigners;
use frontend\account\models\CountGroupsForm;
use frontend\models\CatalogCategories;
use frontend\models\Language;
use yii\data\Pagination;
use yii\helpers\Json;


class JoinGroupController extends \yii\web\Controller
{
    private $language;
    private $user_id;

    public function init()
    {
        session_start();
        $this->user_id = Yii::$app->user->identity->id;
        $this->language = Language::getCurrent()->url;
        if (!isset($_SESSION['group'])) {
            $_SESSION['group'] = ['sort' => 'views',
                'tags' => "",
                'count' => 6,
            ];
        }
    }

    public function actionIndex()
    {

        if (isset($_SESSION['group'])) {
            $class = isset($_POST['parent_class']) ? $_POST['parent_class'] : "";
            $value = isset($_POST['value']) ? $_POST['value'] : "";

            if ($class == 'count') {
                $_SESSION['group']['count'] = $value;
            } else if ($class == 'sort') {

                $_SESSION['group']['sort'] = $value;
            } else if ($class == 'group_tags') {

                $_SESSION['group']['tags'] = $value;
            }


        }
        $alias = Yii::$app->getUrlManager()->getBaseUrl();
        $lang = Language::getCurrent()->url;
        $select = new Groups;
        $get_url = Yii::$app->urlManager->createUrl(['account/join-group']);
        $sort = $_SESSION['group']['sort'];
        $countGroups = $_SESSION['group']['count'];
        $pagination = new Pagination([
            'defaultPageSize' => $countGroups,
            'totalCount' => Groups::find()->count(),
        ]);


        if (empty($_SESSION['group']['tags'])) {
            $groups = Groups::find()
                ->where(['paid_status' => 0])
                ->orderBy($sort . ' DESC')
                ->offset($pagination->offset)
                ->limit($pagination->limit)
                ->all();


        } else {

            $groups = Groups::find()
                ->where(['and', ['paid_status' => 0], ['like', 'group_tags', $_SESSION['group']['tags']]])
                ->orderBy($sort . ' DESC')
                ->offset($pagination->offset)
                ->limit($pagination->limit)
                ->all();

        }

        $paid = Groups::find()
            ->where(['paid_status' => 1])
            ->all();

        return $this->render('index', [
            'alias' => $alias,
            'pagination' => $pagination,
            'groups' => $groups,
            'paid' => $paid,
            'get_url' => $get_url,
            'tags' => $_SESSION['group']['tags'],
        ]);

    }

    public function actionAll()
    {
        $agents_id = array_column(AgentDesigners::selectAllTeasersForDesigner($this->user_id), 'id_agent');
        $new_agents = User::selectAgentsById($agents_id);
        if ($new_agents) {
            $i = -1;
            foreach ($new_agents as $new_agent) {
                $i++;
                $new_agents[$i]['teaser'] = Groups::selectMainTeasersByAgentId($new_agent['id']);
                if ($new_agents[$i]['teaser']) {
                    $new_agents[$i]['teaser']['cities_names'] = $this->selectCityNameByPlaceId($new_agents[$i]['teaser']['cities_id']);
                    $new_agents[$i]['teaser']['teaser_collections'] = ProductsCollections::getCollectionsById(explode(',', $new_agents[$i]['teaser']['colections']), $this->language);
                    $new_agents[$i]['teaser']['teaser_categories'] = CatalogCategories::selectCategoriesByCategoryId(explode(',', $new_agents[$i]['teaser']['categories']), $this->language);
                }
            }
            return $this->render('all', [
                'new_agents' => $new_agents,
                'language' => $this->language,
            ]);
        } else {
            return $this->goHome();
        }
    }

    public function actionNew()
    {
        $agents_id = array_column(AgentDesigners::selectNewTeasersForDesigner($this->user_id), 'id_agent');
        $new_agents = User::selectAgentsById($agents_id);
        if ($new_agents) {
            $i = -1;
            foreach ($new_agents as $new_agent) {
                $i++;
                $new_agents[$i]['teaser'] = Groups::selectMainTeasersByAgentId($new_agent['id']);
                if ($new_agents[$i]['teaser']) {
                    $new_agents[$i]['teaser']['cities_names'] = $this->selectCityNameByPlaceId($new_agents[$i]['teaser']['cities_id']);
                    $new_agents[$i]['teaser']['teaser_collections'] = ProductsCollections::getCollectionsById(explode(',', $new_agents[$i]['teaser']['colections']), $this->language);
                    $new_agents[$i]['teaser']['teaser_categories'] = CatalogCategories::selectCategoriesByCategoryId(explode(',', $new_agents[$i]['teaser']['categories']), $this->language);
                }
            }
            return $this->render('new', [
                'new_agents' => $new_agents,
                'language' => $this->language,
            ]);
        } else {
            return $this->goHome();
        }
    }

    public function actionGroup()
    {
        $alias = Yii::$app->getUrlManager()->getBaseUrl();
        $get_url = Yii::$app->urlManager->createUrl(['account/join-group']);
        $group = Groups::find()
            ->where(['id' => Yii::$app->request->getQueryString()])
            ->one();

        $count_likes = count(Likes::find()->where(['object_id' => $group['id']])->all());
        $view = Groups::findOne($group['id']);
        $view->views = $group['views'] + 1;
        $view->save();

        return $this->render('group', [
            //'items' => $this->menuItems(),
            'alias' => $alias,
            'group' => $group,
            'likes' => $count_likes,
            'get_url' => $get_url,
        ]);
    }

    public function actionLikes()
    {
        if (Likes::find()->where(['object_id' => $_POST['id'], 'category' => 1, 'user_id' => \Yii::$app->user->identity->id])->one()) {

            Likes::deleteAll(['object_id' => $_POST['id'], 'category' => 1, 'user_id' => \Yii::$app->user->identity->id]);
            $like_group = Groups::findOne(['id' => $_POST['id']]);
            $like_group->likes = $like_group->likes - 1;
            $like_group->update();

        } else {
            $like = new Likes;
            $like->object_id = $_POST['id'];
            $like->user_id = \Yii::$app->user->identity->id;
            $like->category = 1;
            $like->save();
            $like_group = Groups::findOne(['id' => $_POST['id']]);
            $like_group->likes = $like_group->likes + 1;
            $like_group->update();
        }
        $count = Groups::findOne(['id' => $_POST['id']]);
        echo $count->likes;
    }


    public static function textCutter($str, $length)
    {

        return substr($str, 0, $length) . "...";
    }

    public function actionSort($orderby)
    {
        $pagination = new Pagination([
            'defaultPageSize' => $this->selectGroups,
            'totalCount' => Groups::find()->count(),
        ]);
        //if (Yii::$app->request->isGet){
        if ($orderby == 0)
            $groups = Groups::find()
                ->orderBy(['views' => SORT_DESC])
                ->offset($pagination->offset)
                ->limit($pagination->limit)
                ->where(['paid_status' => 0])
                ->all();
        else
            $groups = Groups::find()
                ->orderBy('views')
                ->offset($pagination->offset)
                ->limit($pagination->limit)
                ->where(['paid_status' => 0])
                ->all();
        //}
        $groups = Json::encode($groups);
        echo $groups;
    }


    public function actionAddToGroup()
    {
        $request = Yii::$app->request;
        if (AgentDesigners::confirmInvitation($request->post('id_agent'), Yii::$app->user->identity->id))
            return 'done!';
    }

    public function actionEscapeFromGroup()
    {
        $request = Yii::$app->request;
        if (AgentDesigners::removeDesigner($request->post('id_agent'), Yii::$app->user->identity->id))
            return 'done!';
    }

    protected function selectCityNameByPlaceId($place_id)
    {
        $place_id = explode(',', $place_id);
        $city_name = [];
        $i = -1;
        foreach ($place_id as $single_place_id) {
            $i++;
            if ($single_place_id) {
                $data = json_decode(file_get_contents("https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyBZLWGV4klfZp4MjxvXtnljM-qsxyOdzrE&place_id=" . $single_place_id . "&sensor=false&language=" . Language::getCurrent()->url . ""));
                if ($i + 1 != count($place_id))
                    $city_name[] = $data->results[0]->address_components[0]->long_name;
            }
        }
        return implode(',', $city_name);
    }
}
