<?php

namespace frontend\account\controllers;

use frontend\account\models\User;
use yii\data\ActiveDataProvider;
use Yii;
use yii\web\UploadedFile;
use frontend\account\models\Application;
use frontend\account\models\Groups;
use frontend\account\models\OrderFolder;
use frontend\account\models\ProductsCollections;
use frontend\models\Language;
use frontend\account\models\LikeIt;
use frontend\account\models\LikeItItems;

class OrderController extends \yii\web\Controller
{
    public $currentUserId;
    private $language = "ru";

    public function init()
    {
        $this->currentUserId = Yii::$app->user->identity->id;
        $this->language = Language::getCurrent()->url;
    }

    public function actionIndex()
    {
        $model = new Application();
        $collections = array_column(ProductsCollections::selectAllCollections(), $this->language);
        $orders = (new \yii\db\Query())
            ->select('*')
            ->from(['p' => 'products_list'])
            ->innerJoin('orders o', 'p.id_prod=o.id_prod')
            ->where(['id_user' => $this->currentUserId, 'id_folder' => 0])
            ->orderBy('id_order DESC');
        $dataProvider = new ActiveDataProvider([
            'query' => $orders,
            'pagination' => ['pageSize' => 6,
                'pageParam' => 'order-page',
                'pageSizeParam' => 'per-order-page'
            ],
        ]);
//        $folders = (new \yii\db\Query())
//            ->select('*')
//            ->from('order_folder')
//            ->where(['id_user' => $this->currentUserId])
//            ->andWhere('parent_status = 0')
//            ->orderBy('id_folder DESC');
//
//        $info_folder = new ActiveDataProvider([
//            'query' => $folders,
//            'pagination' => ['pageSize' => 4,
//                'pageParam' => 'folder-page',
//                'pageSizeParam' => 'per-folder-page',
//            ],
//        ]);
//        $folder = (new \yii\db\Query())->select('*')->from('order_folder')->where(['id_user' => $this->currentUserId])->all();
        $url = array(
            'delete' => '/account/order/delete',
            'rename' => '/account/order/rename',
        );
        if ($model->load(Yii::$app->request->post()) && $model->uploadApp()) {
            return $this->redirect(['index']);
        }
        $sendTo = Groups::find()->select('user_id', 'title')->column();

        //$sendTo = ['24'=>'grup'];
        return $this->render('index', [
            'collections' => $collections,
            'data' => $dataProvider,
//            'info_folder' => $info_folder,// отдает выборку с лимитом для пагинации
            'url' => $url,
            'model' => $model,
//            'folder_list' => $folder,// отдает полный список папок
            'sendTo' => $sendTo,
            'products_folders' => LikeIt::selectFoldersIds(2, $this->currentUserId),
            'projects_folders' => LikeIt::selectFoldersIds(1, $this->currentUserId),
            'products' => LikeIt::rootDesignerProducts(2, $this->currentUserId),
            'projects' => LikeIt::rootDesignerItems(1, $this->currentUserId, $this->language),
        ]);
    }

//удаление заказа или папки
    private function delete_folder($id_folder)
    {
        $del_folder_in = (new \yii\db\Query())
            ->select('*')
            ->from('order_folder')
            ->where(['parent_status' => $id_folder])->all();
        if ($del_folder_in) {
            foreach ($del_folder_in as $del_folder) {
                $this->delete_folder($del_folder['id_folder']);
            }
        }
        $del_img_in = (new \yii\db\Query())->select('*')->from('orders')->where(['id_folder' => $id_folder])->all();
        if ($del_img_in) {
            $del = Yii::$app->db->createCommand()->delete('orders', ['id_folder' => $id_folder])->execute();
        }
        Yii::$app->db->createCommand()->delete('order_folder', ['id_folder' => $id_folder])->execute();


    }

    public function actionDelete()
    {
        if (isset($_POST[id_order])) {
            $id = $_POST['id_order'];
            Yii::$app->db->createCommand()->delete('orders', ['id_order' => $id])->execute();
            return 1;
        }
        if (isset($_POST['id_folder'])) {
            $id_folder = $_POST['id_folder'];
            $id_folder = trim($id_folder);
            $id_folder = htmlspecialchars($id_folder);
            $this->delete_folder($id_folder);
            return 1;
        }
    }

    // создание новой папки на странице complete order, для создания папки из вне account/catalog/addtofolder
    public function actionNewfolder()
    {

        $title = trim($_POST['title']);
        $query = OrderFolder::find()->where(['title' => $title, 'id_user' => $this->currentUserId])->one();
        if (!empty($query)) {
            return 0;
        } else {
            $folder = new OrderFolder();
            $parent = $_POST['folder'] ? $_POST['folder'] : 0;
            $folder->title = $title;
            $folder->id_user = $this->currentUserId;
            $folder->parent_status = $parent;
            $folder->save();
            if ($folder->save()) {
                return 1;
            }
        }
    }

// переименование папки на странице complete order и в папке
    public function actionRename()
    {
        $title = trim($_POST['title']);
        $query = OrderFolder::find()->where(['title' => $title, 'id_user' => $this->currentUserId])->one();
        if (!empty($query)) {
            return 0;
        } else {
            $id_folder = $_POST['id_folder'];
            $folder = OrderFolder::findOne((int)$id_folder);
            var_dump($folder);
            die();
            $folder->title = $title;
            if ($folder->update()) {
                return 1;
            }
        }
    }

// миниатюры на папке
    public static function onFolder($id_folder)
    {
        $mini_img = (new \yii\db\Query())
            ->select(['p.main_image'])
            ->from(['p' => 'products_list', 'o' => 'orders'])
            ->where('p.id_prod=o.id_prod')->andWhere(['o.id_folder' => $id_folder])->limit(4)->all();
        $folder = (new \yii\db\Query())
            ->select(['folder_img'])
            ->from('order_folder')
            ->where(['parent_status' => $id_folder])->limit(4)->all();
        foreach ($mini_img as $item) {
            $images = array_push($folder, $item);
        }
        $img = array_slice($folder, 0, 4);
        return $img;
    }
    // страничка в папке
    // $id - идентификатор папки
    public function actionFolder($id)
    {
        $order = (new \yii\db\Query())
            ->select('*')
            ->from(['p' => 'products_list'])
            ->innerJoin('orders o', 'p.id_prod=o.id_prod')
            ->where(['id_user' => $this->currentUserId, 'id_folder' => $id])
            ->orderBy('id_order DESC');
        $dataProvider = new ActiveDataProvider([
            'query' => $order,
            'pagination' => ['pageSize' => 6,
                'pageParam' => 'order-page',
                'pageSizeParam' => 'per-order-page',
            ],
        ]);
//        $parent_count = 0;
//        $status = (new \yii\db\Query())->select('parent_status')->from('order_folder')->where(['id_user' => $this->currentUserId, 'id_folder' => $id])->one();
//        while ($status>0){
//            $parent_count++;
//            $status = (new \yii\db\Query())->select('parent_status')->from('order_folder')->where(['id_user' => $this->currentUserId, 'id_folder' => $status])->one();
//        }
        $folders = (new \yii\db\Query())
            ->select('*')
            ->from('order_folder')
            ->where(['id_user' => $this->currentUserId])
            ->andWhere(['parent_status' => $id])
            ->orderBy('id_folder DESC');
        $info_folder = new ActiveDataProvider([
            'query' => $folders,
            'pagination' => ['pageSize' => 4,
                'pageParam' => 'folder-page',
                'pageSizeParam' => 'per-folder-page',
            ],
        ]);
        $folder = (new \yii\db\Query())->select(['title', 'id_folder'])->from('order_folder')->where(['id_user' => $this->currentUserId, 'id_folder' => $id])->one();


        return $this->render('folder', [
            'data' => $dataProvider,
            'info_folder' => $info_folder,// отдает выборку с лимитом для пагинации
            'folder' => $folder,// отдает полный список папок
            //'parent_count'=>$parent_count,
        ]);

    }

    public function actionSearchRecipients()
    {
        $request = Yii::$app->request;
        $recipients = User::searchUsers($request->post('search_recipients'));
        $result = array_map(function ($recipient) {
            return $array[] = ['id' => $recipient['id'], 'user_data' => $recipient['first_name'] . ' ' . $recipient['last_name'] . '(' . $recipient['roles'] . ')',];
        }, $recipients);
        return json_encode($result);
    }

    public function actionMoveTo()
    {
        $id_folder = trim($_POST['folder']);
        $id_order = trim($_POST['id_order']);
        $move = Yii::$app->db->createCommand()->update("orders", ["id_folder" => $id_folder], ['id_order' => $id_order])->execute();
        if ($move) {
            return 1;
        }
    }

    private function useLikeIt()
    {
        $products_folders = LikeIt::selectFoldersIds(1, $this->currentUserId);
        $projects_folders = LikeIt::selectFoldersIds(2, $this->currentUserId);
//        $products_ids_out_of_folder = LikeItItems::selectItemsIds(1);
//        $projects_ids_out_of_folder = LikeItItems::selectItemsIds(2);
        $products_out_of_folder = LikeIt::rootDesignerProducts(1, $this->currentUserId);
        $projects_out_of_folder = LikeIt::rootDesignerItems(2, $this->currentUserId, $this->language);
        return array(
            'products_folders' => $products_folders,
            'projects_folders' => $projects_folders,
            'products' => $products_out_of_folder,
            'projects' => $projects_out_of_folder
        );
    }
}
