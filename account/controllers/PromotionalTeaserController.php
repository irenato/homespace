<?php

namespace frontend\account\controllers;

use frontend\account\models\User;
use yii;
use yii\web\Controller;
use yii\data\Pagination;
use frontend\models\Language;
use frontend\account\models\ProductsTags;
use frontend\account\models\ProductsCollections;
use frontend\models\CatalogCategories;
use frontend\account\models\ProductsTagsUnite;
use frontend\account\models\ProductsList;
use frontend\account\models\AgentProjectDescription;
use frontend\account\models\Groups;
use yii\helpers\ArrayHelper;

class PromotionalTeaserController extends Controller
{
    private $user_id;
    private $language;

    public function init()
    {
        $this->user_id = Yii::$app->user->identity->id;
        $this->language = Language::getCurrent()->url;
    }

    public function actionIndex()
    {
        $model = new Groups();
        $user_data = Yii::$app->user->identity;
        $q = Yii::$app->request->get('q', '');
        $column = Yii::$app->request->get('column', '');
        if ($q && $column) {
            if ($column == 'city') {
                $place = $this->selectPlaceIdByCityName($q);
                $column = 'cities_id';
                $pagination = new Pagination([
                    'defaultPageSize' => 6,
                    'totalCount' => Groups::countTeasersByAgentId($this->user_id, $place, $column),
                ]);
                $promotional_teasers = Groups::selectTeasersByAgentId($this->user_id, $pagination->offset, $pagination->limit, $place, $column);
                $i = -1;
                foreach ($promotional_teasers as $teaser) {
                    $i++;
                    $promotional_teasers[$i]['city_name'] = $this->selectCityNameByPlaceId($teaser['cities_id']);
                    $promotional_teasers[$i]['teaser_collections'] = ProductsCollections::getCollectionsById(explode(',', $teaser['colections']), $this->language);
                    $promotional_teasers[$i]['teaser_categories'] = CatalogCategories::selectCategoriesByCategoryId(explode(',', $teaser['categories']), $this->language);
                }
            } elseif ($column == 'category') {
                $id_category = ProductsCollections::selectCollectionIdByColectionName($q, $this->language);
                $column = 'colections';
                $pagination = new Pagination([
                    'defaultPageSize' => 6,
                    'totalCount' => Groups::countTeasersByAgentId($this->user_id, $id_category, $column),
                ]);
                $promotional_teasers = Groups::selectTeasersByAgentId($this->user_id, $pagination->offset, $pagination->limit, $id_category, $column);
                $i = -1;
                foreach ($promotional_teasers as $teaser) {
                    $i++;
                    $promotional_teasers[$i]['city_name'] = $this->selectCityNameByPlaceId($teaser['cities_id']);
                    $promotional_teasers[$i]['teaser_collections'] = ProductsCollections::getCollectionsById(explode(',', $teaser['colections']), $this->language);
                    $promotional_teasers[$i]['teaser_categories'] = CatalogCategories::selectCategoriesByCategoryId(explode(',', $teaser['categories']), $this->language);
                }
            } else {
                $pagination = new Pagination([
                    'defaultPageSize' => 6,
                    'totalCount' => Groups::countTeasersByAgentId($this->user_id, $q, $column),
                ]);
                $promotional_teasers = Groups::selectTeasersByAgentId($this->user_id, $pagination->offset, $pagination->limit, $q, $column);
                $i = -1;
                foreach ($promotional_teasers as $teaser) {
                    $i++;
                    $promotional_teasers[$i]['city_name'] = $this->selectCityNameByPlaceId($teaser['cities_id']);
                    $promotional_teasers[$i]['teaser_collections'] = ProductsCollections::getCollectionsById(explode(',', $teaser['colections']), $this->language);
                    $promotional_teasers[$i]['teaser_categories'] = CatalogCategories::selectCategoriesByCategoryId(explode(',', $teaser['categories']), $this->language);
                }
            }
        } else {
            $pagination = new Pagination([
                'defaultPageSize' => 6,
                'totalCount' => Groups::countTeasersByAgentId($this->user_id),
            ]);
            $promotional_teasers = Groups::selectTeasersByAgentId($this->user_id, $pagination->offset, $pagination->limit);
            $i = -1;
            foreach ($promotional_teasers as $teaser) {
                $i++;
                $promotional_teasers[$i]['city_name'] = $this->selectCityNameByPlaceId($teaser['cities_id']);
                $promotional_teasers[$i]['teaser_collections'] = ProductsCollections::getCollectionsById(explode(',', $teaser['colections']), $this->language);
                $promotional_teasers[$i]['teaser_categories'] = CatalogCategories::selectCategoriesByCategoryId(explode(',', $teaser['categories']), $this->language);
            }
        }
        $collections = ArrayHelper::map(ProductsCollections::selectAllCollections(), 'id', $this->language);
        $categories = ArrayHelper::map(CatalogCategories::selectAllCategoriesByLanguage(), 'id', 'title_' . $this->language);
        if ($model->load(Yii::$app->request->post())) {
            $model->saveNewGroup($model);
            return $this->refresh();
        }
        return $this->render('index', [
            'promotional_teasers' => $promotional_teasers,
            'user_data' => $user_data,
            'pagination' => $pagination,
            'language' => $this->language,
            'collections' => $collections,
            'categories' => $categories,
            'model' => new Groups(),
        ]);
    }

    public function actionEdit()
    {
        $request = Yii::$app->request;
        $teaser_cities = $request->post('teaser_cities');
        if(is_array($teaser_cities))
            $teaser_cities = implode(',', $teaser_cities);
        $categories = ProductsCollections::selectCollectionsIdByColectionName($request->post('teaser_categories'), $this->language);
        $collections = ProductsCollections::selectCollectionsIdByColectionName($request->post('teaser_collections'), $this->language);
        $prom_teaser = new Groups();
        if($prom_teaser->updatePromotionalTeaser($request, $teaser_cities, $collections, $categories)){
            return 'Done!';
        }
    }

    protected function selectCityNameByPlaceId($place_id)
    {
        $place_id = explode(',', $place_id);
        $city_name = [];
        $i = -1;
        foreach ($place_id as $single_place_id) {
            $i++;
            if ($single_place_id) {
                $data = json_decode(file_get_contents("https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyBZLWGV4klfZp4MjxvXtnljM-qsxyOdzrE&place_id=" . $single_place_id . "&sensor=false&language=" . Language::getCurrent()->url . ""));
                if ($i + 1 != count($place_id))
                    $city_name[] = $data->results[0]->address_components[0]->long_name;
            }
        }
        return implode(',', $city_name);
    }

    protected function selectPlaceIdByCityName($city)
    {
        $data = json_decode(file_get_contents("https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyBZLWGV4klfZp4MjxvXtnljM-qsxyOdzrE&address=" . $city . "&sensor=false&language=" . Language::getCurrent()->url . ""));
        return $data->results[0]->place_id;
    }

}