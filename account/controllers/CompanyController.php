<?php

namespace frontend\account\controllers;

use frontend\account\models\ProductsList;
use frontend\account\models\ProductsTagsUnite;
use frontend\models\UserVideos;
use yii;
use yii\web\Controller;
use yii\data\Pagination;
use yii\helpers\Url;
use frontend\models\Language;
use frontend\account\models\ProductsCollections;
use frontend\account\models\User;
use frontend\account\models\UserCompanies;
use frontend\account\models\UserStatistics;
use frontend\account\models\CompaniesCollections;
use frontend\account\models\UserPayment;
use common\models\LoginForm;

class CompanyController extends Controller
{
    private $user_id;
    private $language = "ru";

    public function init()
    {
        $this->user_id = Yii::$app->user->identity->id;
        $this->language = Language::getCurrent()->url;
    }


    //------------------------------------------------
    // search categories for autocomplete(add company)
    //------------------------------------------------
    public function actionSearchCategories()
    {
        $request = Yii::$app->request;
        $collections = ProductsCollections::find()->select(['id', $this->language])->where(['like', $this->language, $request->post('search_categories')])->asArray()->all();
        $i = -1;
        foreach ($collections as $item) {
            $i++;
            $data[$i]['id'] = $item['id'];
            $data[$i]['category'] = $item[$this->language];
        }
        $data = json_encode($data);
        return $data;
    }
    //------------------
    // create new company
    //------------------
    public function actionCreateNewCompany()
    {
        $data = [];
        $request = Yii::$app->request;
        $data['first_name'] = $request->post('company_first_name');
        $data['last_name'] = $request->post('company_last_name');
        $data['phone'] = $request->post('company_phone', '');
        $data['country'] = $request->post('company_country', '');
        $data['city'] = $request->post('company_city', '');
        $data['address'] = $request->post('company_address', '');
        $data['message'] = $request->post('company_message', '');
        $data['link_user_site'] = $request->post('company_site', '');
        $data['company'] = $request->post('company_name');
        $data['brand'] = $request->post('brand_name');
        $data['position'] = $request->post('company_position');
        $login = new LoginForm();
        if($login->validatePasswordForAgent($request->post('company_login'), $request->post('company_password'))){
            $company = new User;
            $id_company = $company->saveNewCompany($data);
            $user_company = new UserCompanies();
            $company_collections = new CompaniesCollections();
            if ($request->post('company_collections') != null) $company_collections->saveCompanyCollections($id_company, $request->post('company_collections'));
            if ($user_company->uniteIds( $id_company)) {
                $result = array();
                $result['company_name'] = $data['company'];
                $result['company_id'] = $id_company;
                $result['result'] = 'done!';
                echo json_encode($result);
                die();
            } else {
                $result['result'] = 'failed!';
                echo json_encode($result);
                die();
            }
        }else{
            $result['result'] = 'done!';
            echo json_encode($result);
            echo "Incorrect username or password.";
            die();
        }


    }

    //------------------
    // check agent
    //------------------
//    public function validatePasswordForAgent($login, $password)
//    {
//        $user = User::findByUsername($login);
//
//        if (!$user || !$user->validatePassword($password)) {
//            return false;
//        } else {
//            return true;
//        }
//    }


    //------------------
    // edit company
    //------------------
    public function actionEditCompany()
    {
        $request = Yii::$app->request;
        $login = new LoginForm();
        if($login->validatePasswordForAgent($request->post('company_login'), $request->post('company_password'))) {
            if (User::editCompanyById($request)) {
//            if (UserVideos::findOne(['user_id' => $request->post('id')])) {
//                UserVideos::updateUserVideo($request->post('id'), $request->post('company_video_link'), $request->post('company_video_name'));
//            } else {
//                $company_video = new UserVideos();
//                $company_video->addVideo($request->post('id'), $request->post('company_video_link'), $request->post('company_video_name'));
//            }
                if (CompaniesCollections::findOne(['id_company' => $request->post('id')])) {
                    CompaniesCollections::updateCompanyCollections($request->post('id'), $request->post('company_collections'));
                } else {
                    $company_collect = new CompaniesCollections();
                    $company_collect->saveCompanyCollections($request->post('id'), $request->post('company_collections'));
                }
                $result['result'] = 'done';
                $result['company_name'] = $request->post('company_name');
                $result['company_id'] = $request->post('id');
                echo json_encode($result);
                die();
            } else {
                $result['result'] = 'failed';
                echo json_encode($result);
                die();
            }
        }else{
            $result['result'] = 'Incorrect username or password.';
            echo json_encode($result);
            die();
        }
    }

}
