<?php

namespace frontend\account\controllers;

use frontend\account\models\AppRecipients;
use yii;
use frontend\account\models\Application;
use common\models\User;
use yii\helpers\ArrayHelper;


class ApplicationController extends \yii\web\Controller
{

    public function actionIndex()
    {
        $create_app = Yii::$app->urlManager->createUrl(['/account/application/create_app']);
        $model = new Application();
        if ($model->load(Yii::$app->request->post()) && $model->uploadApp()) {
            return $this->redirect(['index']);
        }
        $user_id = \Yii::$app->user->identity->id;
        $app = (new \yii\db\Query())
            ->select(['a.id_app', 'a.title_app', 'a.url_app', 'a.id_folder_projects', 'a.description_app', 'a.id_folder', 'u.id', 'u.first_name', 'u.last_name', 'u.specialization', 'u.avatar', 'r.status', 'r.id', 'r.message', 'r.files', 'r.date'])
            ->from(['a' => 'application', 'r' => 'app_recipients' , 'u' => 'user'])
            ->where(['r.to' => $user_id])->andWhere('r.from=u.id')->andWhere('r.app_id = a.id_app')
            ->orderBy('id_app DESC')->all();
        $curent_user = User::find()->select(['first_name', 'last_name','specialization', 'avatar'])->where(['id' => $user_id])->asArray()->one();
        return $this->render('index', [
            'model' => $model,
            'create_url' => $create_app,
            'app' => $app,
            'curent_user' =>$curent_user,

        ]);
    }

    public function actionAppsend()
    {
        $user_id = \Yii::$app->user->identity->id;
        $models = Application::find()->where(['user_id' => $user_id])->orderBy('id_app DESC')->all();
        $user = User::find()->select(['first_name', 'last_name', 'avatar', 'specialization'])->where(['id' => $user_id])->one();
        if (!$user->avatar) {
            $user->avatar = 'camera3.png';
        }
        return $this->render('appsend', [
            'models' => $models,
            'user' => $user,
        ]);

    }

    protected function findModel($id)
    {
        if (($model = Application::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
// удаление созданой заявки
    public function actionDelete($id)
    {
        $model = (new \yii\db\Query())->select(['url_app'])->from(['application'])->where(['id_app' => $id])->one();
        if ($model[url_app] != null) {
            $file = explode('|', $model[url_app]);
            foreach ($file as $each_file) {
                unlink(Yii::getAlias('@media') . '/application/new_app/' . $each_file);
            }
        }
        $this->findModel($id)->delete();
        return $this->redirect(['appsend']);

    }

    public function actionRead()
    {
       $app_id = $_POST['app_id'];
       $user_id = \Yii::$app->user->identity->id;
       Yii::$app->db->createCommand()->update("app_recipients", ["status" => 1], ['to'=>$user_id, 'app_id'=>$app_id])->execute();
    }
    // удаление входящей заявки
    public function actionRemove($id)
    {
        AppRecipients::findOne($id)->delete();
        return $this->redirect(['index']);
    }

    public function actionSend()
    {
        $user_id = \Yii::$app->user->identity->id;
        $currentUser  = User::findOne($user_id);
        $path1 = Yii::getAlias('@media') . '/temp/';
        $path2 = Yii::getAlias('@media') . '/application/send_app/';
        $i = -1;
        $files = "";
        if (isset($_FILES)) {
            foreach( $_FILES as $file ){
                $temp = explode("/", $file['type']);

                if($temp[1] == 'doc' || $temp[1] == 'txt'|| $temp[1] == 'rtf'|| $temp[1] == 'jpg'|| $temp[1] == 'png' || $temp[1] == 'rtf' || $temp[1] == 'jpeg') {
                    $i++;
                    $ext = '.' . $temp[1];
                    $name = time().rand(111,999);

                    if( stristr ($_REQUEST['fileNames'], $file['name']) != false ){
                        $file_name[$i] = rand(111, 999)."_".$file['name'];
                        move_uploaded_file($file['tmp_name'] , $path1.$name.$ext );
                        copy( $path1.$name.$ext , $path2.$file_name[$i] );
                        $files .= $file_name[$i];
                        $files .= "|";
                    }

                }
                if (!empty($files)){
                    $add = substr($files, 0, -1);
                    Yii::$app->db->createCommand()->update("app_recipients", ["message"=>$_REQUEST['text'],"files" => $add ],['to'=>$user_id, 'app_id'=>$_REQUEST['app_id']])->execute();
                }else{
                    Yii::$app->db->createCommand()->update("app_recipients", ["message"=>$_REQUEST['text']],['to'=>$user_id, 'app_id'=>$_REQUEST['app_id']])->execute();
                }

            }

            $data['sender_avatar'] = $currentUser->avatar;
            $data['sender_specialization'] = $currentUser->specialization;
            $data['sender_username'] = $currentUser->first_name . " " . $currentUser->last_name;
            $data['url_avatar'] = Yii::getAlias('@avatar/'.$data['sender_avatar']);
            $data['file_name'] = $file_name;
            $data['path'] = $path2;
            $data['message'] = $_REQUEST['text'];
            $data['created_at'] = date("Y-m-d H:m:s");
            $data = json_encode($data);

            return $data;
        }

    }

    public function actionText()
    {
        $user_id = \Yii::$app->user->identity->id;
        $currentUser  = User::findOne($user_id);
        Yii::$app->db->createCommand()->update("app_recipients", ["message"=>$_REQUEST['text']],['to'=>$user_id, 'app_id'=>$_REQUEST['app_id']])->execute();
        $data['sender_avatar'] = $currentUser->avatar;
        $data['sender_username'] = $currentUser->first_name ." ". $currentUser->last_name;
        $data['url_avatar'] = Yii::getAlias('@avatar/'.$data['sender_avatar']);
        $data['message'] = $_REQUEST['text'];
        $data['sender_specialization'] = $currentUser->specialization;
        $data['created_at'] = date("Y-m-d H:m:s");
        $data = json_encode($data);

        return $data;
    }


}
