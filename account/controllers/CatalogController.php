<?php

namespace frontend\account\controllers;

use Yii;
use frontend\account\models\Catalog;
use frontend\account\models\ProductsList;
use frontend\account\models\Likes;
use frontend\account\models\OrderFolder;




class CatalogController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $model = new Catalog;
        $dataProvider = $model->search(\Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $model,
            'data' => $dataProvider
        ]);
    }

    public function actionCatalog($id)
    {
        $products_list = new ProductsList();
        $catalog = Catalog::findOne($id);
        $dataProvider = $products_list->sort(\Yii::$app->request->queryParams);
        return $this->render('catalog',[
            'catalog' => $catalog,
            'products_list' => $products_list,
            'data' => $dataProvider
        ]);

    }
     public function actionAddorder(){
         $db = Yii::$app->db;
         $id = isset($_POST['id_prod']) ? $_POST['id_prod'] : 0;
         $user_id = Yii::$app->user->identity->id;
         return $db->createCommand()->insert('orders', [
             'id_prod' => $id,
             'id_user' => $user_id,
             'id_folder' =>0,
         ])->execute();

     }
    //создание папки и добавление в нее товара
    public function actionCreateaddfolder()
    {
        $db = Yii::$app->db;
        $title = $_POST['title'];
        $id_prod = $_POST['id_prod'];
        $user_id = Yii::$app->user->identity->id;
        $folder = new OrderFolder();
        $folder->title = trim($title);
        $folder->id_user = $user_id;
        $folder->save();
        if($folder->save()) {
            $id_folder = OrderFolder::find()->select('id_folder')->where(['title'=>$folder->title, 'id_user'=>$user_id])->one();
            $db->createCommand()->insert('orders', [
                'id_prod' => $id_prod,
                'id_user' => $user_id,
                'id_folder' =>$id_folder['id_folder'],
            ])->execute();
            return 1;
        }
    }
    // добавление товара в существующие папки
    public function actionAddtofolder()
    {
        $db = Yii::$app->db;
        $id_folder = $_POST['id_folder'];
        $id_prod = $_POST['id_prod'];
        $user_id = Yii::$app->user->identity->id;
        $db->createCommand()->insert('orders', [
            'id_prod' => $id_prod,
            'id_user' => $user_id,
            'id_folder' =>$id_folder,
        ])->execute();
        return 1;

    }


    public function actionLikes(){

        if (Likes::find()->where(['object_id' => $_REQUEST['id'] , 'category' => 4 ,'user_id' => \Yii::$app->user->identity->id ])->one()) {

            Likes::deleteAll(['object_id' => $_REQUEST['id'] , 'category' => 4 , 'user_id' => \Yii::$app->user->identity->id]);
            $product= ProductsList::findOne(['id_prod' => $_REQUEST['id']]);
            $product->likes = $product->likes - 1;
            $product->update();

        }else{
            $like = new Likes;
            $like->object_id = $_REQUEST['id'];
            $like->user_id = \Yii::$app->user->identity->id;
            $like->category = 4;
            $like->save();
            $product = ProductsList::findOne(['id_prod' => $_REQUEST['id']]);
            $product->likes = $product->likes + 1;
            $product->update();
        }
        return  Likes::find()->where(['object_id' => $_REQUEST['id'] , 'category' => 4])->count();
    }





}
