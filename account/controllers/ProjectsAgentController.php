<?php

namespace frontend\account\controllers;

use frontend\account\models\ItemDescription;
use frontend\account\models\ProductsCollections;
use frontend\account\models\User;
use frontend\account\models\UserCompanies;
use yii;
use yii\web\Controller;
use yii\data\Pagination;
use frontend\models\Language;
use frontend\account\models\UserStatistics;
use frontend\account\models\ProductsTags;
use frontend\account\models\ProductsTagsUnite;
use frontend\account\models\ProductsList;
use frontend\account\models\Portfolio;
use frontend\account\models\AgentProjectDescription;
use frontend\account\models\Messages;
use yii\helpers\Url;

class ProjectsAgentController extends Controller
{
    private $user_id;
    private $language = "en";

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function init()
    {
        $this->user_id = Yii::$app->user->identity->id;
        $this->language = Language::getCurrent()->url;
    }

    public function actionIndex()
    {
        $viewed_all = isset($_SESSION['agent-projects-viewed-all']) ? $_SESSION['agent-projects-viewed-all'] : 3;
        $confirmation_view = isset($_SESSION['agent-projects-confirmation-view']) ? $_SESSION['agent-projects-confirmation-view'] : 3;
        $approved_view = isset($_SESSION['agent-projects-approved-view']) ? $_SESSION['agent-projects-approved-view'] : 3;
        $products = ProductsList::find()->where(['user_id' => $this->user_id])->all();
        $user_stat = new UserStatistics();
        $user_stat_res = $user_stat->getStatistics($this->user_id);
        $user_stat_to_string = implode(' ,', $user_stat_res['per_month']);
        $q = Yii::$app->request->get('q', false);
        $tag = Yii::$app->request->get('tag', false);
        $tag1 = Yii::$app->request->get('tag1', false);
        $tag2 = Yii::$app->request->get('tag2', false);
        return $this->render('index', [
            'products' => $products,
            'user_stat' => $user_stat_res,
            'user_stat_to_string' => $user_stat_to_string,
            'all_projects' => $this->getAllProjects($viewed_all, $q, $tag),
            'view_all' => $viewed_all,
            'confirmation_projects' => $this->getApprovedConfirmationProjects($confirmation_view, 0, $tag1),
            'confirmation_view' => $confirmation_view,
            'approved_view' => $approved_view,
            'approved_projects' => $this->getApprovedConfirmationProjects($approved_view, 1, $tag2),
        ]);
    }

    public function actionSelect()
    {
        $request = Yii::$app->request;
        $session = Yii::$app->session;
        $id = Yii::$app->request->get('id', 0);
        $collection = $session['sp_collection'] ? $session['sp_collection'] : 0;
//        $companies_id_by_current_agent = UserCompanies::getCompaniesByAgent($this->user_id);
//        $products_from_user_companies = ProductsList::selectProductsByAgentCompany($companies_id_by_current_agent, 1);
//        $products_from_other_companies = ProductsList::selectProductsFromOtherCompanies($companies_id_by_current_agent, 0);
//        if (is_array($products_from_user_companies) && is_array($products_from_other_companies)) {
//            $products_result = array_merge($products_from_user_companies, $products_from_other_companies);
//        } else if (is_array($products_from_user_companies)) {
//            $products_result = $products_from_user_companies;
//        } else {
//            $products_result = is_array($products_from_other_companies) ? $products_from_other_companies : array();
//        }
        $companies_id = ProductsList::selectCompaniesByAgentProducts();
        $pagination_product = new Pagination([
            'defaultPageSize' => $session['size_for_products'] ? $session['size_for_products'] : 2,
            'totalCount' => ProductsList::countProductsByCompanyId($companies_id),
        ]);
        $products = ProductsList::selectProductsByCompanyId($companies_id, $pagination_product, $collection);
//        $products = $this->paginationForProducts($products_result, $pagination_product->offset, $pagination_product->limit);
        $user_stat = new UserStatistics();
        $user_stat_res = $user_stat->getStatistics($this->user_id);
        $user_stat_to_string = implode(' ,', $user_stat_res['per_month']);
        list($controller) = Yii::$app->createController('about');
        $item = $controller->getDesignerItem($id, $this->language);
        if ($item) {
            $descriptions = AgentProjectDescription::find()->where(['id_project' => $item['id'], 'approved' => 1])->all();
        } else
            $descriptions = false;
        $collections = ProductsCollections::selectAllCollections();
        return $this->render('select', [
            'products_per_page' => $session['size_for_products'],
            'products' => $products,
            'pagination_product' => $pagination_product,
            'user_stat' => $user_stat_res,
            'user_stat_to_string' => $user_stat_to_string,
            'item' => $item,
            'descriptions' => $descriptions,
            'new_projects' => $this->getAllProjects(3),
            'my_projects' => $this->getApprovedConfirmationProjects(3, 1),
            'collections' => $collections,
            'current_collection' => $collection,
            'language' => $this->language
        ]);
    }

    // End Views

    function actionChangePageSize()
    {
        $request = Yii::$app->request;
        $session = Yii::$app->session;
        if ($request->post('page_size') != null) {
            $session['size_for_products'] = $request->post('page_size');
        }
    }

    function actionChangeCollection()
    {
        $request = Yii::$app->request;
        $session = Yii::$app->session;
        if ($request->post('collection') != null) {
            $session['sp_collection'] = $request->post('collection');
        }
    }
    //------------------------------------------------
    // Add Agent own Description to Project
    //------------------------------------------------

    public function actionAddDescription()
    {
        $id = Yii::$app->request->post('id', 0);
        $agent = "<p>" . Yii::$app->user->identity->first_name . " " . Yii::$app->user->identity->last_name . "<p>";
//        $agent = "<p><a href='" . Yii::$app->urlManager->createUrl(['manufacturers/profile', 'id' => $this->user_id]) . "'>" . Yii::$app->user->identity->first_name . " " . Yii::$app->user->identity->last_name . "</a><p>";
        $description = (string)Yii::$app->request->post('description', '');
        $description = $agent . $description;
        $description = htmlentities($description);
        $message = new Messages();
        $recipient = Portfolio::findOne(['item_id' => $id]);
        $portfolio = ItemDescription::findOne(['item_id' => $id, 'language' => 'en']);
        $notification = '';
        $notification .= Yii::$app->user->identity->first_name . " " . Yii::$app->user->identity->last_name;
        $notification .= ' added new description to ' . $portfolio->title . '. ';
        $notification .= Yii::$app->request->post('description', '');
        $notification .= ' <br>';
        $notification .= ' <br>';
        $notification .= ' <button class="button confirm-it-now" data-project="' . $id . '" data-id-agent="' . Yii::$app->user->identity->id . '" >Confirm</button>';
        $notification .= ' <button class="button delete-it-now" data-project="' . $id . '" data-id-agent="' . Yii::$app->user->identity->id . '" >Delete</button>';
        echo $this->sendNotificationByMail($recipient->owner_id);
//        $notification .= ' To confirm click here <a href="' . Url::to(['confirm/profile', 'id_progect' => $id, 'id_agent' => Yii::$app->user->identity->id], true) . '">Confirm</a>';
        if ($message->createNotificationAboutPortfolioDescription($recipient->owner_id, $notification) && AgentProjectDescription::agentAddDescription($id, Yii::$app->user->identity->id, $description) && $this->sendNotificationByMail($recipient->owner_id))
            return 'Done!';

    }

    private function sendNotificationByMail($user_id)
    {
        $user = User::findOne($user_id);
        $to = $user->email;
        $subject = "Home Space";

        $message = "
            <html>
                <head>
                    <title>Home Space Today</title>
                </head>
                <body>
                    <p>New description for your project to confirm or reject click here: </p>
                    <a href=" . Yii::$app->urlManager->createAbsoluteUrl(['account/messages']) . ">Home Space Today</a>                    
                </body>
            </html>";

        $headers = "Content-type: text/html; charset=utf-8 \r\n";
        $headers .= "From: homespace \r\n";

        return mail($to, $subject, $message, $headers);
    }

    //------------------------------------------------
    // Get and save view count
    //------------------------------------------------

    public function actionViewed()
    {
        if (isset($_POST['all_view'])) {
            $viewed_all = Yii::$app->request->post('all_view', 3);
            $_SESSION['agent-projects-viewed-all'] = $viewed_all;
        } elseif (isset($_POST['confirmation_view'])) {
            $confirmation_view = Yii::$app->request->post('confirmation_view', 3);
            $_SESSION['agent-projects-confirmation-view'] = $confirmation_view;
        } elseif (isset($_POST['approved_view'])) {
            $approved_view = Yii::$app->request->post('approved_view', 3);
            $_SESSION['agent-projects-approved-view'] = $approved_view;
        }

    }

    public function actionUpload()
    {
        if (isset($_FILES['file'])) {
            $file = $_FILES['file'];
            $temp = explode("/", $file['type']);
            if ($temp[1] == 'png' || $temp[1] == 'jpeg' || $temp[1] == 'jpg' || $temp[1] == 'bmp' || $temp[1] == 'gif') {
                $path = Yii::getAlias('@media') . '/products/';
                $ext = '.' . $temp[1];
                $name = time();
                move_uploaded_file($file['tmp_name'], $path . $name . $ext);
                echo '/media/products/' . $name . $ext;
            }
        }

    }
//------------------------------------------------
// search tags for autocomplete(add product)
//------------------------------------------------
    public function actionSearchTags()
    {
        $tags = ProductsTags::find()->select(['id', 'title'])->where(['like', 'title', $_REQUEST['search_tags']])->asArray()->all();
        $i = -1;
        $data = [];
        foreach ($tags as $item) {
            $i++;
            $data[$i]['id'] = $item['id'];
            $data[$i]['tag'] = $item['title'];
        }
        $data = json_encode($data);
        return $data;
    }

//------------------------------------------------
// search company name for autocomplete(add product)
//------------------------------------------------
    public function actionSearchCompanyName()
    {
        $request = Yii::$app->request;
        $companies = User::searchCompanyName($request->post('search_company'));
        return json_encode($companies);
    }

//------------------------------------------------
// search company name for autocomplete(add product)
//------------------------------------------------
    public function actionSearchProductName()
    {
        $request = Yii::$app->request;
        $products = ProductsList::searchProductsNames($request->post('search_product'), $request->post('company_id'));
        return json_encode($products);
    }
//------------------------------------------------
// create new product(modal)
//------------------------------------------------
    public function actionAddNewProduct()
    {
        $request = Yii::$app->request;
        if (ProductsList::findOne(['product_name' => $request->post('product_name'), 'user_id' => \Yii::$app->user->identity->id, 'user_id' => $request->post('company_id')])) {
            $data['response'] = Yii::t('account', 'product_name_exists');
            return json_encode($data);
        } elseif ($request->post('product_name') == null || $request->post('price') == null
            || $request->post('description') == null || $request->post('image1') == null || $request->post('image2') == null
            || $request->post('image3') == null || $request->post('tags') == null
        ) {
            if ($request->post('product_name') == null)
                $data['response'] = 'Fill in the name of the product!';
            elseif ($request->post('price') == null)
                $data['response'] = 'Fill in the price of the product!';
            elseif ($request->post('description') == null)
                $data['response'] = 'Fill in the description of the product!';
            elseif ($request->post('tags') == null)
                $data['response'] = 'Fill in the tags of the product!';
            elseif ($request->post('image1') == null || $request->post('image2') == null || $request->post('image3') == null)
                $data['response'] = 'Add product images!';
            return json_encode($data);
        } elseif ($request->post('special_offer') == 1 && $request->post('discount') == NULL
        ) {
            $data['response'] = Yii::t('account', 'fill_discount_field');
            return json_encode($data);
        } elseif ($request->post('company_id') == NULL) {
            $data['response'] = Yii::t('account', 'choose_company');
            return json_encode($data);
        } else {
            $product = new ProductsList();
            $image1 = explode('/', $request->post('image1'));
            $image2 = explode('/', $request->post('image2'));
            $image3 = explode('/', $request->post('image3'));
            $tags = implode(',', $request->post('tags'));
            $id_product = $product->saveNewProduct($request->post('company_id'), $request->post('product_name'), $tags,
                $request->post('price'), $request->post('description'), $image1[count($image1) - 1], $image2[count($image2) - 1], $image3[count($image3) - 1], $request->post('special_offer'), $request->post('discount'), $request->post('product_collection'));
            if ($id_product) {
//            $tags = [];
//            foreach ($request->post('tags_id') as $tag_id) {
//                array_push($tags, [(int)$product_id, (int)$tag_id]);
//            }
//            if (Yii::$app->db->createCommand()->batchInsert('products_tags_unite', ['id_product', 'id_tag'], $tags)->execute()) {
                $data['id_product'] = $id_product;
                $data['response'] = 'done!';
                return json_encode($data);
            } else {
                $data['response'] = Yii::t('account', 'error_please_try_again_later');
                return json_encode($data);
            }
        }

    }

    public
    function actionSearchProducts()
    {
        $request = Yii::$app->request;
        $company_ids = array_column(User::searchManufacturerByName($request->post('company')), 'id');
        if (!$company_ids)
            return 'not found!';
        elseif ($request->post('product_name') == null) {
            return json_encode(array());
        }
        return json_encode(ProductsList::searchProducts($request->post('product_name'), $company_ids, $request->post('collection_id')));
    }

//------------------------------------------------
// Get Projects List and pagination
//------------------------------------------------

    public
    function getAllProjects($view_by, $q = '', $tag = '')
    {
        if ($tag == '') {
            if ($q != '') {
                $cnt = Portfolio::agentAllProjectsCount($this->language, $q);
                $pagination = new Pagination([
                    'defaultPageSize' => $view_by,
                    'totalCount' => $cnt[0]['COUNT(*)']
                ]);

                $projects = Portfolio::agentAllProjects($this->language, $pagination->limit, $pagination->offset, $q);
            } else {
                $pagination = new Pagination([
                    'defaultPageSize' => $view_by,
                    'totalCount' => Portfolio::find()->where(['active' => 1, 'moderation' => 1, 'catalog_id' => 0])->count()
                ]);

                $projects = Portfolio::agentAllProjects($this->language, $pagination->limit, $pagination->offset);
            }

        } else {
            $cnt = Portfolio::agentAllProjectsCount($this->language, '', $tag);
            $pagination = new Pagination([
                'defaultPageSize' => $view_by,
                'totalCount' => $cnt[0]['COUNT(*)']
            ]);

            $projects = Portfolio::agentAllProjects($this->language, $pagination->limit, $pagination->offset, '', $tag);

        }

        $tags_list = Portfolio::agentAllProjectsTags($this->language);
        $tags = [];
        foreach ($tags_list as $tag) {
            foreach (explode(',', $tag['tags']) as $value)
                array_push($tags, $value);
        }

        $result = array_unique($tags);


        return ['projects' => $projects, 'pagination' => $pagination, 'tags' => $result];

    }

    public
    function getApprovedConfirmationProjects($view_by, $approved, $tag = '')
    {
        if (!$tag) {
            $cnt = AgentProjectDescription::onConfirmationProjectsCount($this->language, $this->user_id, $approved);
            $pagination = new Pagination([
                'defaultPageSize' => $view_by,
                'totalCount' => $cnt[0]['COUNT(*)']
            ]);

            $projects = AgentProjectDescription::onConfirmationProjects($this->language, $this->user_id, $approved, $pagination->limit, $pagination->offset);
        } else {
            $cnt = AgentProjectDescription::onConfirmationProjectsCount($this->language, $this->user_id, $approved, $tag);
            $pagination = new Pagination([
                'defaultPageSize' => $view_by,
                'totalCount' => $cnt[0]['COUNT(*)']
            ]);

            $projects = AgentProjectDescription::onConfirmationProjects($this->language, $this->user_id, $approved, $pagination->limit, $pagination->offset, $tag);
        }


        $tags_list = AgentProjectDescription::onConfirmationProjectsTags($this->language, $this->user_id, $approved);
        $tags = [];
        foreach ($tags_list as $tag) {
            foreach (explode(',', $tag['tags']) as $value)
                array_push($tags, $value);
        }

        $result = array_unique($tags);

        return ['projects' => $projects, 'pagination' => $pagination, 'tags' => $result];

    }

    private
    function paginationForProducts($products, $offset, $limit)
    {
        $result = array();
        for ($i = $offset; $i < $limit + $offset; $i++) {
            array_push($result, $products[$i]);
        }
        return $result;
    }
}