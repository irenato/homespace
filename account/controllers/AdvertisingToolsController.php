<?php

namespace frontend\account\controllers;

use frontend\account\models\ProductsList;
use frontend\account\models\ProductsTagsUnite;
use frontend\models\UserVideos;
use yii;
use yii\web\Controller;
use yii\data\Pagination;
use yii\helpers\Url;
use frontend\models\Language;
use frontend\account\models\ProductsCollections;
use frontend\account\models\User;
use frontend\account\models\UserCompanies;
use frontend\account\models\UserStatistics;
use frontend\account\models\CompaniesCollections;
use frontend\account\models\UserPayment;

class AdvertisingToolsController extends Controller
{
    private $user_id;
    private $language = "ru";

    public function init()
    {
        $this->user_id = Yii::$app->user->identity->id;
        $this->language = Language::getCurrent()->url;
    }

    public function actionIndex()
    {
        $this->getPaymentList();

//        $companies_id_by_current_agent = UserCompanies::getCompaniesByAgent($this->user_id) ? UserCompanies::getCompaniesByAgent($this->user_id) : array();
//        $products_from_user_companies = ProductsList::selectProductsByAgentCompany($companies_id_by_current_agent, 1);
//        $products_from_other_companies = ProductsList::selectProductsFromOtherCompanies($companies_id_by_current_agent, 0);
//        if (is_array($products_from_user_companies) && is_array($products_from_other_companies)) {
//            $products_result = array_merge($products_from_user_companies, $products_from_other_companies);
//        } else if (is_array($products_from_user_companies)) {
//            $products_result = $products_from_user_companies;
//        } else {
//            $products_result = is_array($products_from_other_companies) ? $products_from_other_companies : array();
//        }
//        $pagination_product = new Pagination([
//            'defaultPageSize' => 4,
//            'totalCount' => count($products_result),
//        ]);
//        $products = $this->paginationForProducts($products_result, $pagination_product->offset, $pagination_product->limit);

        $pagination_product = new Pagination([
            'defaultPageSize' => 4,
            'totalCount' => ProductsList::countSpecialOfferByAgent($this->user_id),
        ]);

        $products = ProductsList::selectSpecialOfferByAgent($this->user_id, $pagination_product);
        $user_stat = new UserStatistics();
        $companies_id = UserCompanies::getCompaniesByAgent($this->user_id);
        $user_stat_res = $user_stat->getStatistics($this->user_id);
        $pagination = new Pagination([
            'defaultPageSize' => '4',
            'totalCount' => count($companies_id),
        ]);
        $user_stat_to_string = implode(' ,', $user_stat_res['per_month']);
        $companies = User::getCompaniesById($companies_id ? $companies_id : array(), $pagination);
        $i = -1;
        foreach ($companies as $company) {
            $i++;
            $companies[$i]['products_count'] = ProductsList::countProductsByCompanyId($company['id']);
            $companies[$i]['collections'] = CompaniesCollections::selectCollectionsByCompanyId($company['id']);
            $companies[$i]['so_count'] = ProductsList::countStockByCompanyId($company['id']);
            $companies[$i]['new_products'] = ProductsList::countNewProducts($company['id']);
            $companies[$i]['discounts'] = ProductsList::selectMaxDiscount($company['id']);
        }
        return $this->render('index', [
            'special_offers' => $products,
            'pagination_product' => $pagination_product,
            'user_stat_res' => $user_stat_res,
            'user_stat_to_string' => $user_stat_to_string,
            'companies' => $companies,
            'language' => $this->language,
            'pagination' => $pagination,
            'payment_users' => $this->getPaymentList()
        ]);
    }

    public function actionCreate()
    {
        return $this->render('create');
    }

    public function actionManufacturer()
    {
        $request = Yii::$app->request;
        $manufacturer = User::selectCompanyById($request->get('id'));
        $categories = CompaniesCollections::selectCollectionsByCompanyId($request->get('id'));
        $manufacturers_id_by_agent_id = UserCompanies::getCompaniesByAgent($this->user_id);
        $products = ProductsList::selectThreeRandomProduct();
        $pagination_product = new Pagination([
            'defaultPageSize' => 4,
            'totalCount' => ProductsList::countProductsByCompanyId($request->get('id')),
        ]);
        $products_list = ProductsList::selectProductsByCompanyId($request->get('id'), $pagination_product);
        if (in_array($request->get('id'), $manufacturers_id_by_agent_id))
            return $this->render('manufacturer', [
                'manufacturer' => $manufacturer,
                'products' => $products,
                'categories' => $categories,
                'language' => $this->language,
                'pagination_product' => $pagination_product,
                'products_list' => $products_list,
            ]);
        else {
            return $this->goHome();
        }
    }

    public function actionEdit()
    {
        $request = Yii::$app->request;
        $manufacturer = User::selectCompanyById($request->get('id'));
        $categories = CompaniesCollections::selectCollectionsByCompanyId($request->get('id'));
        $manufacturers_id_by_agent_id = UserCompanies::getCompaniesByAgent($this->user_id);
//        $video = UserVideos::selectUserVideo($request->get('id'));
        if (in_array($request->get('id'), $manufacturers_id_by_agent_id))
            return $this->render('edit', [
                'manufacturer' => $manufacturer,
                'categories' => $categories,
//                'video' => $video,
                'language' => $this->language,
            ]);
        else {
            return $this->goHome();
        }
    }


    //------------------------------------------------
    // search categories for autocomplete(add company)
    //------------------------------------------------
    public function actionSearchCategories()
    {
        $request = Yii::$app->request;
        $collections = ProductsCollections::find()->select(['id', $this->language])->where(['like', $this->language, $request->post('search_categories')])->asArray()->all();
        $i = -1;
        foreach ($collections as $item) {
            $i++;
            $data[$i]['id'] = $item['id'];
            $data[$i]['category'] = $item[$this->language];
        }
        $data = json_encode($data);
        return $data;
    }
    //------------------
    // create new company
    //------------------
//    public function actionCreateNewCompany()
//    {
//        $data = [];
//        $request = Yii::$app->request;
////        $company_avatar = $request->post('company_avatar');
////        $company_avatar = explode('/', $company_avatar);
////        $data['avatar'] = array_pop($company_avatar);
////        if ($request->post('company_bg') != null) {
////            $company_background = $request->post('company_bg');
////            $company_background = explode('/', $company_background);
////            $data['background'] = array_pop($company_background);
////        } else {
////            $data['background'] = 'bg-agents.png';
////        }
////        $company_logo = $request->post('company_logo');
////        $company_logo = explode('/', $company_logo);
////        $data['logo'] = array_pop($company_logo);
//        $data['first_name'] = $request->post('company_first_name');
//        $data['last_name'] = $request->post('company_last_name');
//        $data['phone'] = $request->post('company_phone', '');
//        $data['country'] = $request->post('company_country', '');
//        $data['city'] = $request->post('company_city', '');
//        $data['address'] = $request->post('company_address', '');
//        $data['message'] = $request->post('company_message', '');
//        $data['link_user_site'] = $request->post('company_site', '');
////        $data['link_fb'] = $request->post('link_fb', '');
////        $data['link_tw'] = $request->post('link_twitter', '');
////        $data['link_inst'] = $request->post('link_instagram', '');
////        $data['link_behance'] = $request->post('link_behance', '');
////        $data['link_google'] = $request->post('link_google', '');
////        $data['link_pint'] = $request->post('link_pinterest', '');
////        $data['link_tumblr'] = $request->post('link_tumblr', '');
////        $data['link_linkedin'] = $request->post('link_linkedin', '');
////        $data['link_blg'] = $request->post('link_blogger', '');
//        $data['avatar'] = !empty($data['avatar']) ? $data['avatar'] : "noava.png";
////        $data['email'] = $request->post('company_email');
////        $data['username'] = $request->post('company_login');
//        $data['company'] = $request->post('company_name');
//        $data['brand'] = $request->post('brand_name');
//        $data['position'] = $request->post('company_position');
////        $data['password_hash'] = Yii::$app->security->generatePasswordHash($request->post('company_password'));
//        $company = new User;
//        $id_company = $company->saveNewCompany($data);
//        $user_company = new UserCompanies();
//        $company_video = new UserVideos();
////        $company_video->addVideo($id_company, $request->post('company_video_link'), $request->post('company_video_name'));
//        $company_collections = new CompaniesCollections();
//        if ($request->post('company_collections') != null) $company_collections->saveCompanyCollections($id_company, $request->post('company_collections'));
//        if ($user_company->uniteIds($id_company)) {
//            echo "done";
//            die();
//        } else {
//            echo "failed";
//            die();
//        }
//    }

    //------------------
    // edit company
    //------------------
    public function actionEditCompany()
    {
        $request = Yii::$app->request;
        if (User::editCompanyById($request)) {
            if (UserVideos::findOne(['user_id' => $request->post('id')])) {
                UserVideos::updateUserVideo($request->post('id'), $request->post('company_video_link'), $request->post('company_video_name'));
            } else {
                $company_video = new UserVideos();
                $company_video->addVideo($request->post('id'), $request->post('company_video_link'), $request->post('company_video_name'));
            }
            if (CompaniesCollections::findOne(['id_company' => $request->post('id')])) {
                CompaniesCollections::updateCompanyCollections($request->post('id'), $request->post('company_collections'));
            } else {
                $company_collect = new CompaniesCollections();
                $company_collect->saveCompanyCollections($request->post('id'), $request->post('company_collections'));
            }
            echo "done";
            die();
        } else {
            echo "failed";
            die();
        }
    }

//    protected function CompanyCollectionsUnite($id_company, $collections)
//    {
//        $tags = [];
//        if (is_array($collections)) {
//            foreach ($collections as $tag_id) {
//                array_push($tags, [(int)$id_company, (int)$tag_id]);
//            }
//            Yii::$app->db->createCommand()->batchInsert('companies_collections', ['id_company', 'id_collection'], $tags)->execute();
//            return true;
//            die();
//        } else {
//            return false;
//            die();
//        }
//    }

    private function getPaymentList()
    {
        $users = User::find()->select(['id', 'paid_status', 'first_name', 'avatar', 'last_name', 'date_finish'])->leftJoin('user_payment', '`user_payment`.`user_id` = `user`.`id`')->where(['roles' => 'designer'])->orderBy(['rand()' => 'id'])->limit(10)->asArray()->all();
        return $users;
    }

    private
    function paginationForProducts($products, $offset, $limit)
    {
        $result = array();
        for ($i = $offset; $i < $limit + $offset; $i++) {
            array_push($result, $products[$i]);
        }
        return $result;
    }

}
