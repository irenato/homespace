<?php

namespace frontend\account\controllers;

use yii;
use frontend\models\ProfileReviews;
use frontend\account\models\User;
use frontend\models\UserHonours;
use frontend\models\UserVideos;
use frontend\models\UserLicenses;
use frontend\account\models\Portfolio;
use frontend\account\models\ItemDescription;
use frontend\models\PortfolioCollections;
use frontend\models\Language;
use yii\web\Controller;
use yii\data\Pagination;

class ProfileController extends Controller
{
    private $user_id;
    private $moderation = 1;
    private $language = "ru";
    private $paid_status = 2;


    public function init()
    {
        $this->user_id = Yii::$app->user->identity->id;
        $this->language = Language::getCurrent()->url;
        $this->paid_status = Yii::$app->user->identity->paid_status;
    }

    public function actionIndex()
    {
        if($this->paid_status == 0) {
            return $this->render('index', [
                'portfolio' => $this->getUserItems($this->user_id, $this->language, $this->moderation),
                'followers' => 0,
                'following' => 0,
                'tariff' => 'Basic',
                'user_information' => User::find()->where(['id' => $this->user_id])->one(),
                'honours' => UserHonours::find()->where(['user_id' => $this->user_id, 'moderation' => $this->moderation])->all(),
                'licenses' => UserLicenses::find()->where(['user_id' => $this->user_id, 'moderation' => $this->moderation])->all(),
                'reviews' => $this->getReviews($this->user_id),
                'videos' => UserVideos::find()->where(['user_id' => $this->user_id])->all(),
            ]);
        }else if($this->paid_status == 1){
            return $this->render('index2b', [
                'portfolio' => $this->getUserItems($this->user_id, $this->language, $this->moderation),
                'followers' => 0,
                'following' => 0,
                'tariff' => 'Basic',
                'user_information' => User::find()->where(['id' => $this->user_id])->one(),
                'honours' => UserHonours::find()->where(['user_id' => $this->user_id, 'moderation' => $this->moderation])->all(),
                'licenses' => UserLicenses::find()->where(['user_id' => $this->user_id, 'moderation' => $this->moderation])->all(),
                'reviews' => $this->getReviews($this->user_id),
                'best_items' => $this->getBestItems($this->user_id, 2, $this->language),
                'videos' => UserVideos::find()->where(['user_id' => $this->user_id])->all(),
            ]);
        }else if($this->paid_status == 2){
            return $this->render('index3b', [
                'portfolio' => $this->getUserItems($this->user_id, $this->language, $this->moderation),
                'followers' => 0,
                'following' => 0,
                'tariff' => 'Basic',
                'user_information' => User::find()->where(['id' => $this->user_id])->one(),
                'honours' => UserHonours::find()->where(['user_id' => $this->user_id, 'moderation' => $this->moderation])->all(),
                'licenses' => UserLicenses::find()->where(['user_id' => $this->user_id, 'moderation' => $this->moderation])->all(),
                'reviews' => $this->getReviews($this->user_id),
                'best_items' => $this->getBestItems($this->user_id, 2, $this->language),
                'videos' => UserVideos::find()->where(['user_id' => $this->user_id])->all(),
                'style' => $this->getStyle($this->user_id, 3, $this->language),
            ]);
        }
    }

    // Other functions

    public function getUserItems($user_id, $lang, $moderation)
    {
        $portfolio = array();
        $temp = Portfolio::find()->where(['owner_id' => $user_id, 'moderation' => $moderation])->all();
        foreach ($temp as $value) {

            $images = explode("|", $value['images']);
            $description = ItemDescription::find()->where(['item_id' => $value['item_id'], 'language' => $lang])->one();
            $short_d = substr($description['short_description'], 0, 378);
            $short_d = htmlspecialchars($short_d);

            $portfolio[] = [
                'id' => $value['item_id'],
                'image' => Yii::getAlias('@portfolio/'.$images[0]),
                'title' => $description['title'],
                'short_description' => $short_d,
                'url' => Yii::$app->urlManager->createUrl(['account/portfolio/item', 'id' => $value['item_id']])

            ];

        }


        return $portfolio;
    }

    // Tariff 2b
    private function getBestItems($owner_id, $status, $lang)
    {
        $data = array();
        $items = Portfolio::find()->where(['owner_id' => $owner_id, 'status' => $status])->all();
        if($items) {
            $count = 0;
            foreach ($items as $item) {
                $description = ItemDescription::find()->where(['item_id' => $item['item_id'], 'language' => $lang])->one();
                $temp_images = explode("|", $item['images']);
                $count++;
                $data[] = [
                    'id' => $item['item_id'],
                    'image' => Yii::getAlias('@portfolio/'.$temp_images[0]),
                    'class' => $count,
                    'title' => $description['title']
                ];

            }

            if ($count < 3) {

                for ($i = $count; $i < F3; $i++) {
                    $data[$i] = [
                        'id' => '',
                        'image' => '/images/no-img.png',
                        'class' => $i + 1,
                        'title' => ''
                    ];
                }
            }
        }

        return $data;
    }

    public function getReviews($to)
    {
        $data = array();

        $pagination = new Pagination([
            'defaultPageSize' => 5,
            'totalCount' => ProfileReviews::find()->where(['to' => $to])->count(),
        ]);

        $reviews = ProfileReviews::find()->where(['to' => $to])->offset($pagination->offset)->orderBy(['id' => SORT_DESC])->limit($pagination->limit)->all();
        foreach ($reviews as $review) {
            $profile = User::find()->where(['id' => $review['from']])->one();
            if($profile){
                $data[] = [
                    'link' => Yii::$app->urlManager->createUrl(['about/user', 'id' => $review['from']]),
                    'author' => $profile->first_name.' '.$profile->last_name,
                    'image' => Yii::getAlias('@avatar/'.$profile['avatar']),
                    'comment' => $review['review'],
                    'date' => date('j.m.y; H:i',$review['date']),
                ];
            }else{

                $data[] = [
                    'link' => '',
                    'author' => ' ',
                    'image' => Yii::getAlias('@avatar/noava.png'),
                    'comment' => $review['review'],
                    'date' => date('j.m.y; i:H',$review['date']),
                ];

            }
        }

        $new_data = [
            'reviews' => $data,
            'pagination' => $pagination
        ];

        return $new_data;
    }

    private function getCollections($lang, $user_id)
    {
        $data = array();
        $collections = PortfolioCollections::find()->where(['user_id' => $user_id])->all();
        foreach ($collections as $value) {
            $items = array();
            $portfolio = Portfolio::find()->where(['item_id' => $value['main']])->one();
            $temp_img = explode("|", $portfolio['images']);
            $items[0]['image'] = Yii::getAlias('@portfolio/'.$temp_img[0]);
            $description = ItemDescription::find()->where(['item_id' => $value['main'], 'language' => $lang])->one();
            $items[0]['title'] = $description['title'];
            $items[0]['id'] = $value['main'];
            $items[0]['url'] = Yii::$app->urlManager->createUrl(['account/portfolio/item', 'id' => $value['main']]);

            $items_id = explode("|", $value['items']);
            $i = 1;
            if($items_id){
                foreach ($items_id as $item) {
                    if($item != "" && $item != $value['main'] && $item != " "){
                        $portfolio = Portfolio::find()->where(['item_id' => $item])->one();
                        $temp_img = explode("|", $portfolio['images']);
                        $items[$i]['image'] = Yii::getAlias('@portfolio/'.$temp_img[0]);
                        $description = ItemDescription::find()->where(['item_id' => $item, 'language' => $lang])->one();
                        $items[$i]['title'] = $description['title'];
                        $items[$i]['id'] = $item;
                        $items[$i]['url'] = Yii::$app->urlManager->createUrl(['account/portfolio/item', 'id' => $item]);
                        $i++;
                    }
                }
            }

            $data[] = [
                'id' => $value['id'],
                'name' => $value['name'],
                'main' => $value['main'],
                'about' => $value['about'],
                'items' => $items
            ];


        }


        return $data;
    }

    private function getStyle($user_id, $status, $lang){
        $data = [];
        $item = Portfolio::find()->where(['owner_id' => $user_id, 'status' => $status, 'moderation' => $this->moderation])->one();
        if($item) {
            $description = ItemDescription::find()->where(['item_id' => $item['item_id'], 'language' => $lang])->one();
            $image = explode('|', $item['images']);
            $data = [
                'id' => $item['item_id'],
                'image' => Yii::getAlias('@portfolio/'.$image[0]),
                'title' => $description['title'],
                'likes' => 0,
                'link' => Yii::$app->urlManager->createUrl(['account/portfolio/item', 'id' => $item['item_id']]),
                'share_link' => Yii::$app->urlManager->createUrl(['about/item', 'id' => $item['item_id']]),
            ];
        }

        return $data;
    }


}