<?php

namespace frontend\account\controllers;

use yii;
use yii\web\Controller;
use yii\data\Pagination;
use yii\helpers\Url;
use frontend\account\models\ProductsList;
use frontend\models\Language;

class OffersController extends \yii\web\Controller
{
    private $user_id;
    private $language = "ru";

    public function init()
    {
        $this->user_id = Yii::$app->user->identity->id;
        $this->language = Language::getCurrent()->url;
    }

    public function actionIndex()
    {
        $session = Yii::$app->session;
        $tags = (isset($_GET['tags'])) ? $_GET['tags'] : "";
        $pagination = new Pagination([
            'defaultPageSize' => $session->get('view_special_offers', 4),
            'totalCount' => ProductsList::find()
                ->where(['agent_id' => $this->user_id])
                ->andWhere(['stock' => 1])
                ->andFilterWhere(['like', 'meta_tags', $tags])
                ->count(),
        ]);
        $special_offers = ProductsList::selectSpecialOfferWhithFiltersByAgent($this->user_id, $pagination, $tags);
        return $this->render('index', [
            'special_offers' => $special_offers,
            'pagination' => $pagination,
        ]);

    }

    public function actionSort()
    {
        $request = Yii::$app->request;
        $session = Yii::$app->session;
        if ($request->post('value') == 'likes' || $request->post('value') == 'price' || $request->post('value') == 'price1') {
            $session['sort_special_offers'] = $request->post('value');
        } else {
            $session['view_special_offers'] = $request->post('value');
        }
    }

}
