<?php

namespace frontend\account\controllers;


use yii;
use yii\web\Controller;
use frontend\account\models\User;
use frontend\account\models\Messages;
use frontend\account\models\Groups;
use frontend\account\models\Likes;
use frontend\account\models\CountGroupsForm;
use frontend\models\Language;
use yii\data\Pagination;
use yii\helpers\Json;


class MessagesController extends \yii\web\Controller
{
    public function init()
    {

    }

    public function actionIndex()
    {
        $user = \Yii::$app->user->identity->id;
        $i = -1;
        $interlocutors = Messages::getAllInterlocutors($user);
        $message = array();
        $pagination = new Pagination([
            'defaultPageSize' => 5,
            'totalCount' => count($interlocutors),
        ]);

        if(!empty($interlocutors)){

            foreach ($interlocutors as $interlocutor) {
                $i++;
                $messages[$i] = Messages::showLastMessage($user, $interlocutor);
                $messages[$i]['about']  = $this->getUserInformation($interlocutor);

                if($messages[$i]['file_app'] != 'NULL'){

                    $messages[$i]['files'] = explode('|',$messages[$i]['file_app']);

                }else{

                    $messages[$i]['files'] = NULL;
                }

            }

            $count = $pagination->offset + $pagination->limit;
            $n = $pagination->offset;

            while($n<$count){

                array_push($message, $messages[$n]);
                $n++;

            }

            return $this->render('index', [

                'messages' => $message,
                'pagination' => $pagination,

            ]);

        }else{

            return $this->render('index', [

                'messages' => false,
                'pagination' => false,

            ]);

        }

    }


    public function actionSearchinterlocutors(){

        $interlocutors = User::find()->select(['id', 'first_name', 'last_name', 'avatar', 'specialization'])->where(['or', ['like', 'username', $_REQUEST['search']], ['like', 'first_name', $_REQUEST['search']], ['like', 'last_name', $_REQUEST['search']]])->all();
        $all_recipients = Messages::getAllInterlocutors(\Yii::$app->user->identity->id);
        $i = -1;

        foreach ($interlocutors as $user) {

            $recipients = in_array($user->id, $all_recipients);
            $i++;

            if($recipients != "") {

                $recipient[$i]['id'] = $user->id;
                $recipient[$i]['first_name'] = $user->first_name;
                $recipient[$i]['last_name'] = $user->last_name;

            }

        }

        $recipient = json_encode($recipient);

        return($recipient);
    }



    private function getUserInformation($id){

        $userInformation = User::find()->select(['id', 'first_name', 'last_name', 'avatar', 'specialization'])->where(['id'=>$id])->asArray()->one();

        return $userInformation;
    }

}