<?php

namespace frontend\account\models;
use Yii;
use yii\data\ActiveDataProvider;


class ProductsTags extends \yii\db\ActiveRecord
{
    public $id;
    public $title;

    public static function tableName()
    {
        return 'products_tags';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['title'], 'string'],
        ];
    }

}