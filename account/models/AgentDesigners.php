<?php

namespace frontend\account\models;

use Yii;

/**
 * This is the model class for table "agent_designers".
 *
 * @property integer $id
 * @property integer $id_agent
 * @property integer $id_designer
 * @property integer $status
 */
class AgentDesigners extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'agent_designers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_agent', 'id_designer', 'status'], 'required'],
            [['id_agent', 'id_designer', 'status'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_agent' => 'Id Agent',
            'id_designer' => 'Id Designer',
            'status' => 'Status',
        ];
    }

//-----------------------------------------------
// adding a new designer in the group
//-----------------------------------------------
    public static function addDesigner($id_agent, $id_designer)
    {
        $new_designer = new AgentDesigners();
        $new_designer->id_agent = intval($id_agent);
        $new_designer->id_designer = intval($id_designer);
        $new_designer->status = 0;
        return $new_designer->save();
    }

//-----------------------------------------------
// adding a new designer in the group whithout confirmation
//-----------------------------------------------
    public static function addDesignerWhithoutConfirmation($id_agent, $id_designer)
    {
        if(!AgentDesigners::findOne(['id_designer' => $id_designer, 'id_agent' => $id_agent])){
            $new_designer = new AgentDesigners();
            $new_designer->id_agent = intval($id_agent);
            $new_designer->id_designer = intval($id_designer);
            $new_designer->status = 1;
            return $new_designer->save();
        }
        return true;
    }

//-----------------------------------------------
// remove designer from group
//-----------------------------------------------
    public static function removeDesigner($id_agent, $id_designer)
    {
        return static::deleteAll(['id_agent' => $id_agent, 'id_designer' => $id_designer]);
    }

//-----------------------------------------------
// get designers by agent
//-----------------------------------------------
    public static function getDesignersIdByAgentId($id_agent)
    {
        return array_column(static::find()
            ->select(['id_designer'])
            ->where(['id_agent' => intval($id_agent)])
            ->orderBy('id_designer DESC')
            ->asArray()
            ->all(), "id_designer");
    }

//-----------------------------------------------
// get designers by group
//-----------------------------------------------
    public static function getDesignersIdByGroup($id_agent)
    {
        return array_column(static::find()
            ->select(['id_designer'])
            ->where(['id_agent' => intval($id_agent)])
            ->andWhere(['status' => 1])->orderBy('id_designer DESC')
            ->asArray()
            ->all(), "id_designer");
    }

//-----------------------------------------------
// get two last designers by group
//-----------------------------------------------
    public static function getTwoDesignersIdByGroup($id_agent)
    {
        return array_column(static::find()
            ->select(['id_designer'])
            ->where(['id_agent' => intval($id_agent)])
            ->andWhere(['status' => 1])->orderBy('id_designer DESC')
            ->orderBy('id DESC')
            ->limit('2')
            ->asArray()
            ->all(), "id_designer");
    }

    public static function getDesignersCountByAgentId($id_agent)
    {
        return static::find()
            ->select(['id_designer'])
            ->where(['id_agent' => intval($id_agent)])
            ->count();
    }

//-----------------------------------------------
// count of proposal for a current designer
//-----------------------------------------------
    public static function selectInvitationsForDesigner($id_designer)
    {
        return self::find()
            ->where(['id_designer' => $id_designer])
            ->andWhere(['status' => 0])
            ->groupBy(['id_agent'])
            ->count();
    }

//-----------------------------------------------
// select new proposals for a current designer
//-----------------------------------------------
    public static function selectNewTeasersForDesigner($id_designer)
    {
        return self::find()
            ->select(['id_agent'])
            ->where(['id_designer' => $id_designer])
            ->andWhere(['status' => 0])
            ->asArray()
            ->all();
    }

//-----------------------------------------------
// select new proposals for a current designer
//-----------------------------------------------
    public static function selectAllTeasersForDesigner($id_designer)
    {
        return self::find()
            ->select(['id_agent'])
            ->where(['id_designer' => $id_designer])
            ->andWhere(['status' => 1])
            ->asArray()
            ->all();
    }

 //-----------------------------------------------
// select two last groups for a current designer
//-----------------------------------------------
    public static function selectTwoLastGroupsForDesigner($id_designer)
    {
        return array_column(self::find()
            ->select(['id_agent'])
            ->where(['id_designer' => $id_designer])
            ->andWhere(['status' => 1])
            ->orderBy('id DESC')
            ->limit('2')
            ->asArray()
            ->all(), 'id_agent');
    }

//-----------------------------------------------
// confirmation invitation for current designer
//-----------------------------------------------
    public static function confirmInvitation($id_agent, $id_designer)
    {
        $group = static::find()
            ->where(['id_designer' => $id_designer])
            ->andWhere(['id_agent' => $id_agent])
            ->andWhere(['status' => 0])
            ->one();
        $group->status = 1;
        return $group->update();
    }

}



