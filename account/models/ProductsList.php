<?php
namespace frontend\account\models;

use Yii;
use yii\data\ActiveDataProvider;


class ProductsList extends \yii\db\ActiveRecord
{
    public $count = 12;
    public $sort;
//    public $id_prod;
//    public $user_id;
//    public $product_name;
//    public $company_name;
//    public $meta_tags;
//    public $price;
//    public $images;
//    public $main_image;
//    public $miniature_img;
//    public $prod_desc;


    public static function tableName()
    {
        return 'products_list';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_prod', 'user_id', 'agent_id'], 'integer'],
            [['stock'], 'integer', 'max' => 1],
            [['discount'], 'integer',],
//            [['meta_tags', 'sort'], 'string'],
            [['product_name', 'collection', 'meta_tags', 'price', 'images', 'miniature_img', 'main_image', 'prod_desc', 'sort'], 'string'],
            [['created_at'], 'string', 'max' => 64]
////            [['price'], 'number'],
////            [['prod_desc'], 'string'],
////            [['likes', 'id_catalog'], 'integer'],
//            [['product_name', 'meta_tegs', 'images', 'main_image'], 'string', 'max' => 255]
        ];
    }

    public function sort($params)
    {
        $query = ProductsList::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => ['pageSize' => $this->count],
        ]);
        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }
        $meta_tags = explode(',', $this->meta_tags);
        $trimmed_tags = array_map("trim", $meta_tags);
        $dataProvider->pagination->pageSize = $this->count;
        if ($this->sort === 'like') {
            $query->orderBy(['likes' => SORT_DESC]);
        } elseif ($this->sort === 'chip') {
            $query->orderBy(['price' => SORT_ASC]);
        } elseif ($this->sort === 'expensive') {
            $query->orderBy(['price' => SORT_DESC]);
        }
        foreach ($trimmed_tags as $tag) {
            $query->orFilterWhere([
                'like', 'meta_tags', $tag
            ]);
        }
        return $dataProvider;
    }

    public function saveNewProduct($company_id, $product_name, $meta_tag, $price, $description, $image1, $image2, $image3, $special_offer, $discount, $collection)
    {
        $this->product_name = $product_name;
        $this->meta_tags = $meta_tag;
        $this->price = $price;
        $this->user_id = (int)$company_id;
        $this->agent_id = (int)Yii::$app->user->identity->id;
        $this->prod_desc = $description;
        $this->main_image = $image1;
        $this->images = $image1 . (($image2 == null) ? '' : ' ,' . $image2) . (($image3 == null) ? '' : ' ,' . $image3);
        $this->miniature_img = $image3;
        $this->created_at = (string)time();
        $this->collection = $collection;
        if ($special_offer == 1) {
            $this->stock = (int)$special_offer;
            $this->discount = (int)$discount;
        }
        if ($this->save()) return $this->id_prod;
//        if ($this->save())
//            return $this->id_prod;
//        else {
//            $error = $this->getErrors();
//            die(var_dump($error));
//        };
//        else die(var_dump($this->getErrors()));
    }

    public function getCompanies()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public static function countProductsByCompanyId($company_id)
    {
        if (is_array($company_id))
            return $products = static::find()->where(['in', 'user_id', $company_id])->count();
        return $products = static::find()->where(['user_id' => $company_id])->count();

    }

//    ----------------------------------------------------
//    for select new products for last 30 days
//    ----------------------------------------------------

    public static function countNewProducts($company_id)
    {
        $thirty_days = (int)time() - 86400;
        if (is_array($company_id))
            return $products = static::find()
                ->where(['in', 'user_id', $company_id])
                ->andWhere(['>', 'created_at', $thirty_days])
                ->count();
        return $products = static::find()
            ->where(['user_id' => $company_id])
            ->andWhere(['>', 'created_at', $thirty_days])
            ->count();

    }

    public static function countStockByCompanyId($company_id)
    {
        if (is_array($company_id))
            return $products = static::find()
                ->where(['in', 'user_id', $company_id])
                ->andWhere(['stock' => 1])
                ->count();
        return $products = static::find()
            ->where(['user_id' => $company_id])
            ->andWhere(['stock' => 1])
            ->count();

    }

    public static function selectProducts($offset, $limit, $stock = 0)
    {
        return static::find()
            ->select(['product_name', 'main_image', 'price'])
            ->where(['and', ['stock' => $stock], ['agent_id' => (int)Yii::$app->user->identity->id], ['not in', 'user_id', $companies_id]])
            ->andWhere(['agent_id' => (int)Yii::$app->user->identity->id])
            ->offset($offset)
            ->limit($limit)
            ->orderBy('id_prod DESC')
            ->asArray()
            ->all();
    }

    public static function selectProductsByAgentCompany($companies_id)
    {
        return static::find()
            ->select(['id_prod', 'user_id', 'product_name', 'prod_desc', 'main_image', 'price', 'discount', 'stock'])
            ->where(['and', ['agent_id' => (int)Yii::$app->user->identity->id], ['in', 'user_id', $companies_id]])
            ->andWhere(['agent_id' => (int)Yii::$app->user->identity->id])
            ->orderBy('id_prod DESC')
            ->asArray()
            ->all();
    }

    public static function selectSpecialOfferByAgent($id_agent, $pagination)
    {
        return static::find()
            ->where(['agent_id' => $id_agent])
            ->andWhere(['stock' => 1])
            ->orderBy('id_prod DESC')
            ->offset($pagination->offset)
            ->limit($pagination->limit)
            ->asArray()
            ->all();
    }

    public static function selectSpecialOfferWhithFiltersByAgent($id_agent, $pagination, $tags)
    {
        $session = Yii::$app->session;
        switch ($session['sort_special_offers']) {
            case 'likes':
                $sort = 'likes DESC';
                break;
            case 'price':
                $sort = 'price DESC';
                break;
            case 'price1':
                $sort = 'price';
                break;
            default:
                $sort = 'id_prod';
                break;
        }
        if ($tags) {
            return static::find()
                ->where(['agent_id' => $id_agent])
                ->andWhere(['stock' => 1])
                ->andFilterWhere(['like', 'meta_tags', $tags])
                ->orderBy($sort)
                ->offset($pagination->offset)
                ->limit($pagination->limit)
                ->asArray()
                ->all();
        }
        return static::find()
            ->where(['agent_id' => $id_agent])
            ->andWhere(['stock' => 1])
            ->orderBy($sort)
            ->offset($pagination->offset)
            ->limit($pagination->limit)
            ->asArray()
            ->all();
    }

    public static function countSpecialOfferByAgent($id_agent)
    {
        return static::find()
            ->where(['agent_id' => $id_agent])
            ->andWhere(['stock' => 1])
            ->count();
    }

    public static function selectProductsByCompanyId($companies_id, $pagination, $collection)
    {
        if ($collection != 0)
            return static::find()
                ->select(['id_prod', 'user_id', 'product_name', 'prod_desc', 'main_image', 'price', 'discount', 'stock'])
                ->where(['in', 'user_id', $companies_id])
                ->andWhere(['collection' => (int)$collection])
                ->orderBy('id_prod DESC')
                ->offset($pagination->offset)
                ->limit($pagination->limit)
                ->asArray()
                ->all();
        else
            return static::find()
                ->select(['id_prod', 'user_id', 'product_name', 'prod_desc', 'main_image', 'price', 'discount', 'stock'])
                ->where(['in', 'user_id', $companies_id])
                ->orderBy('id_prod DESC')
                ->offset($pagination->offset)
                ->limit($pagination->limit)
                ->asArray()
                ->all();
    }

    public static function selectCompaniesByAgentProducts()
    {
        return array_column(static::find()
            ->select(['user_id'])
            ->where(['agent_id' => (int)Yii::$app->user->identity->id])
            ->orderBy('id_prod DESC')
            ->asArray()
            ->all(), 'user_id');
    }

//    public static function selectProductsByAgent($companies_id, $colection)
//    {
//        return static::find()
//            ->select(['id_prod', 'user_id', 'product_name', 'prod_desc', 'main_image', 'price', 'discount', 'stock'])
//            ->where(['and', ['agent_id' => (int)Yii::$app->user->identity->id], ['in', 'user_id', $companies_id]])
//            ->andWhere(['agent_id' => (int)Yii::$app->user->identity->id])
//            ->orderBy('id_prod DESC')
//            ->asArray()
//            ->all();
//    }

    public static function countProducts($stock = 0, $companies_id)
    {
        return static::find()
            ->where(['and', ['stock' => 0], ['agent_id' => (int)Yii::$app->user->identity->id], ['in', 'user_id', $companies_id]])
            ->orWhere(['agent_id' => (int)Yii::$app->user->identity->id])
            ->count();
    }

    public static function selectProductsFromOtherCompanies($companies_id, $stock = 0)
    {
        $companies_id_array = array_count_values(array_column(self::selectOtherCompaniesId($companies_id, 0), 'user_id'));
        $companies_id_result = array();
        foreach ($companies_id_array as $k => $v) {
            if ($v >= 3)
                array_push($companies_id_result, $k);
        }
        if (count($companies_id_result) > 0) {
            return static::find()
                ->select(['id_prod', 'user_id', 'product_name', 'prod_desc', 'main_image', 'price', 'discount', 'stock'])
                ->where(['and', ['agent_id' => (int)Yii::$app->user->identity->id], ['in', 'user_id', $companies_id_result]])
//                ->where(['and', ['stock' => $stock], ['agent_id' => (int)Yii::$app->user->identity->id], ['in', 'user_id', $companies_id_result]])
                ->orderBy('user_id')
                ->asArray()
                ->all();
        }
    }

    private static function selectOtherCompaniesId($companies_id, $stock = 0)
    {
        return static::find()
            ->select(['user_id'])
            ->where(['and', ['agent_id' => (int)Yii::$app->user->identity->id], ['not in', 'user_id', $companies_id]])
//            ->where(['and', ['stock' => $stock], ['agent_id' => (int)Yii::$app->user->identity->id], ['not in', 'user_id', $companies_id]])
            ->orderBy('user_id')
            ->asArray()
            ->all();
    }

    public static function searchProducts($product_name, $company, $collection_id)
    {

        return static::find()
            ->select(['id_prod', 'product_name', 'main_image'])
            ->where(['and', ['like', 'product_name', $product_name], ['in', 'user_id', $company], ['collection' => $collection_id]])
//            ->where(['and', ['product_name' => $product_name], ['user_id' => $company_id], ['collection' => $collection_id]])
            ->asArray()
            ->all();
    }

    public static function searchProductsNames($text, $id)
    {
        $products = static::find()->select(['id_prod', 'product_name'])->where(['and', ['like', 'product_name', $text], ['user_id' => $id]])->orderBy('id_prod DESC')->asArray()->all();
        $product_names = array_map(function ($product) {
            return $array[] = ['id_prod' => $product['id_prod'], 'product_name' => $product['product_name'],];
        }, $products);
        return $product_names;
    }

    public static function selectMaxDiscount($user_id)
    {
        return static::find()
            ->select(['discount'])
            ->where(['user_id' => (int)$user_id])
            ->andWhere(['stock' => 1])
            ->orderBy('discount DESC')
            ->asArray()
            ->one();
    }

    public static function makeSpecialOffer($id, $discount)
    {
        $special_offers = static::findOne(['id_prod' => $id]);
        $special_offers->stock = 1;
        $special_offers->discount = $discount;
        return $special_offers->update();
    }

    public static function selectProductById($id)
    {
        return static::findOne(['id_prod' => $id]);
    }

    public static function selectThreeRandomProduct()
    {
        return static::find()
            ->select(['id_prod', 'product_name', 'main_image'])
            ->limit(3)
            ->orderBy('RAND()')
            ->asArray()
            ->all();
    }

//        $result = array();
//        $i = -1;
//        foreach ($companies_id as $company_id) {
//            $i++;
//            if ($this->find()
//                    ->select(['id_prod'])
//                    ->where(['agent_id' => (int)Yii::$app->user->identity->id])
//                    ->andWhere(['user_id' => $company_id])
//                    ->andWhere(['stock' => $stock])
//                    ->count() >= 3
//            ) ;
//            $result[$i] = $company_id;
//        }
//
//        return $result;
//    }
}