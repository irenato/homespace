<?php


namespace frontend\account\models;

use Yii;
use yii\data\ActiveDataProvider;


class UserCompanies extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'user_companies';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_user', 'id_company'], 'integer'],
        ];
    }

    public function uniteIds($id_company)
    {
        $this->id_user = Yii::$app->user->identity->id;
        $this->id_company = $id_company;
        return $this->save();
    }
//---------------------------------------------------
//select companies id
//---------------------------------------------------
    public static function getCompaniesByAgent($id_user)
    {
        $id_companies = static::find()->select(['id_company'])->where(['id_user' => $id_user])->orderBy('id_company DESC')->asArray()->all();
        if ($id_companies)
            return array_column($id_companies, 'id_company');
    }
//---------------------------------------------------
//select count fo companies by agent
//---------------------------------------------------
    public static function selectCountCompaniesByAgent($id_user)
    {
        return static::find()->select(['id_company'])->where(['id_user' => $id_user])->count();
    }

    public function getAgent()
    {
        return $this->hasOne(User::className(), ['id' => 'id_user']);
    }
}