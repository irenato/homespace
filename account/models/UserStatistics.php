<?php

namespace frontend\account\models;

use Yii;

/**
 * This is the model class for table "user_statistics".
 *
 * @property integer $id
 * @property integer $id_user
 * @property string $guests
 * @property string $homeowners
 * @property string $designers
 * @property string $agents
 * @property string $manufacturer
 * @property string $professionals
 * @property integer $month
 */
class UserStatistics extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_statistics';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user', 'month'], 'required'],
            [['id_user', 'month'], 'integer'],
            [['guest', 'homeowner', 'designer', 'agent', 'manufacturer', 'professional'], 'string', 'max' => 12]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_user' => 'Id User',
            'guest' => 'Guests',
            'homeowner' => 'Homeowners',
            'designer' => 'Designers',
            'agent' => 'Agents',
            'manufacturer' => 'Manufacturer',
            'professional' => 'Professionals',
            'month' => 'Month',
        ];
    }

    public function getStatistics($id_user)
    {
        $user_stat = static::find()->where(['id_user' => $id_user])->asArray()->one();
        return $this->parseUserStatistic($user_stat);
        die();
    }

    public static function createNewUserRow($id)
    {
        $month = date("n", time());
        Yii::$app->db->createCommand("INSERT INTO `user_statistics` (`id_user`, `guest`, `homeowner`, `designer`, `agent`, `manufacturer`, `professional`, `month`) VALUES ($id, '1,1', '1,1', '1,1', '1,1', '1,1', '1,1', $month);")->execute();
    }

    //------------------------------------------------
    // user profile view statistics
    //------------------------------------------------
    protected function parseUserStatistic($data = false)
    {
        $res = [];
        if ($data) {
            $guests = ($data['guest'] != null) ? explode(',', $data['guest']) : 0;
            $res['all']['guest'] = $guests[0];
            $res['per_month']['guest'] = $guests[1];
            $agents = ($data['agent'] != null) ? explode(',', $data['agent']) : 0;
            $res['all']['agent'] = $agents[0];
            $res['per_month']['agent'] = $agents[1];
            $homeowners = ($data['homeowner'] != null) ? explode(',', $data['homeowner']) : 0;
            $res['all']['homeowner'] = $homeowners[0];
            $res['per_month']['homeowner'] = $homeowners[1];
            $manufacturer = ($data['manufacturer'] != null) ? explode(',', $data['manufacturer']) : 0;
            $res['all']['manufacturer'] = $manufacturer[0];
            $res['per_month']['manufacturer'] = $manufacturer[1];
            $designers = ($data['designer'] != null) ? explode(',', $data['designer']) : 0;
            $res['all']['designer'] = $designers[0];
            $res['per_month']['designer'] = $designers[1];
            $professionals = ($data['professional'] != null) ? explode(',', $data['professional']) : 0;
            $res['all']['professional'] = $professionals[0];
            $res['per_month']['professional'] = $professionals[1];
        } else {
            $res['all']['guest'] = 0;
            $res['per_month']['guest'] = 0;
            $res['all']['agent'] = 0;
            $res['per_month']['agent'] = 0;
            $res['all']['homeowner'] = 0;
            $res['per_month']['homeowner'] = 0;
            $res['all']['manufacturer'] = 0;
            $res['per_month']['manufacturer'] = 0;
            $res['all']['designer'] = 0;
            $res['per_month']['designer'] = 0;
            $res['all']['professional'] = 0;
            $res['per_month']['professional'] = 0;
        }
        return $res;
    }
}
