<?php

namespace frontend\account\models;

use Yii;

/**
 * This is the model class for table "groups".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $title
 * @property string $text
 * @property string $cities_id
 * @property string $colections
 * @property string $categories
 * @property string $images
 * @property integer $likes
 * @property integer $views
 * @property string $followers
 * @property integer $followers_count
 * @property integer $paid_status
 * @property string $group_tags
 * @property string $created_at
 * @property string $updated_at
 * @property integer $flag
 */
class Groups extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'groups';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'text', 'colections', 'categories',], 'required'],
            [['user_id', 'likes', 'views', 'followers_count', 'paid_status', 'flag'], 'integer'],
            [['text'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['title', 'images', 'followers', 'group_tags', 'cities_id',], 'string', 'max' => 255],
//            [['categories'], 'string', 'max' => 64],
//            [['colections'], 'each', 'rule' => ['integer']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'title' => 'Title',
            'text' => 'Text',
            'cities_id' => 'Cities ID',
            'colections' => 'Colections',
            'categories' => 'Categories',
            'images' => 'Images',
            'likes' => 'Likes',
            'views' => 'Views',
            'followers' => 'Followers',
            'followers_count' => 'Followers Count',
            'paid_status' => 'Paid Status',
            'group_tags' => 'Group Tags',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'flag' => 'Flag',
        ];
    }

    public static function selectTeasersByAgentId($id_agent, $offset, $limit, $q = false, $column = false)
    {
        if ($q && $column) {
            return static::find()
                ->where(['user_id' => $id_agent])
                ->andWhere(['LIKE', $column, $q])
                ->offset($offset)
                ->limit($limit)
                ->orderBy('id DESC')
                ->asArray()
                ->all();
        } else {
            return static::find()
                ->where(['user_id' => $id_agent])
                ->offset($offset)
                ->limit($limit)
                ->orderBy('id DESC')
                ->asArray()
                ->all();
        }

    }

    public static function countTeasersByAgentId($id_agent, $q = false, $column = false)
    {
        if ($q && $column) {
            return static::find()
                ->where(['user_id' => $id_agent])
                ->andWhere(['LIKE', $column, $q])
                ->count();
        } else {
            return static::find()
                ->where(['user_id' => $id_agent])
                ->count();
        }

    }

    public function saveNewGroup($data)
    {
//        var_dump($data['title']);
//        $group = new Groups();
//        if (Yii::$app->request->isPost) {
        $this->user_id = (int)Yii::$app->user->identity->id;
        $this->title = $data['title'];
        if ($data['flag'] !== 0 && $this->checkFlag((int)Yii::$app->user->identity->id, $data['flag'])) {
            $this->flag = $data['flag'];
        }
        $this->text = $data['text'];
        $this->cities_id = $data['cities_id'];
        $this->created_at = time();
        $this->colections = implode(',', $data['colections']);
        $this->categories = $data['categories'];
        return $this->save();
//        }
    }

    public static function selectMainTeasersByAgentId($id_agent)
    {
        return static::find()
            ->where(['user_id' => $id_agent])
            ->andWhere(['flag' => 1])
            ->asArray()
            ->one();
    }

    public function updatePromotionalTeaser($request, $teaser_cities, $collection, $categories)
    {
        if (is_array($collection))
            $collection = implode(',', $collection);
        if (is_array($categories))
            $categories = implode(',', $categories);
        $editible_promotional_teaser = static::findOne(['id' => $request->post('teaser_id')]);
        $editible_promotional_teaser->title = $request->post('teaser_name');
        $editible_promotional_teaser->text = $request->post('teaser_description');
        $editible_promotional_teaser->cities_id = $teaser_cities;
        $editible_promotional_teaser->categories = $categories;
        $editible_promotional_teaser->colections = $collection;
        if($request->post('teaser_status') == 1 && $this->checkFlag((int)Yii::$app->user->identity->id, $request->post('teaser_status'))){
            $editible_promotional_teaser->flag = 1;
        }
        return $editible_promotional_teaser->update();
    }

    private function checkFlag($id_agent, $flag)
    {
        $teaser_whith_flag = Groups::find()
            ->where(['user_id' => $id_agent])
            ->andWhere(['flag' => 1])
            ->one();
        if (!$teaser_whith_flag)
            return true;
        $teaser_whith_flag->flag = 0;
        return $teaser_whith_flag->update();
    }

}
