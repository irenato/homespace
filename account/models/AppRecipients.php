<?php

namespace frontend\account\models;

use Yii;
use frontend\account\models\User;

/**
 * This is the model class for table "app_recipients".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $app_id
 * @property integer $status
 */
class AppRecipients extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'app_recipients';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['from', 'to', 'app_id', 'status'], 'required'],
            [['from', 'to', 'app_id', 'status'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */

    public static function selectRecipients($app_id){
        return static::find()
            ->select('to')
            ->where(['app_id' => $app_id])
            ->all();
    }
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'to']);
    }
    /*public function getUser()
    {
        return $this->hasOne(User::className(), ['to' => 'id']);
    }*/
}