<?php

namespace frontend\account\models;

use Yii;

/**
 * This is the model class for table "profile_reviews".
 *
 * @property integer $id
 * @property integer $to
 * @property integer $from
 * @property integer $date
 * @property string $review
 */
class ProductsReviews extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'products_reviews';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'from', 'date', 'review_text'], 'required'],
            [['product_id', 'from', 'date'], 'integer',],
            [['review_text'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'ProductId',
            'from' => 'From',
            'date' => 'Date',
            'review_text' => 'ReviewText',
        ];
    }

    public function addComment($request)
    {
        $this->product_id = (int)$request->post('id');
        $this->from = (int)Yii::$app->user->identity->id;;
        $this->review_text = $request->post('text');
        $this->date = (int)time();
        return $this->save();
    }

    public static function showComments($id, $pagination)
    {
        return static::find()
            ->where(['product_id' => $id])
            ->offset($pagination->offset)
            ->limit($pagination->limit)
            ->orderBy('date DESC')
            ->all();
    }

    public static function countReviews($id)
    {
        return static::find()
            ->where(['product_id' => $id])
            ->count();
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'from']);
    }
}