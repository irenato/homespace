<?php

namespace frontend\account\models;

use Yii;

/**
 * This is the model class for table "agent_project_description".
 *
 * @property integer $id
 * @property integer $id_project
 * @property integer $id_agent
 * @property string $description
 * @property integer $approved
 */
class AgentProjectDescription extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'agent_project_description';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_project', 'id_agent', 'description'], 'required'],
            [['id_project', 'id_agent', 'approved'], 'integer'],
            [['description'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_project' => 'Id Project',
            'id_agent' => 'Id Agent',
            'description' => 'Description',
            'approved' => 'Approved',
        ];
    }

    public static function agentAddDescription($id, $user_id, $desc){
        $temp = self::findOne(['id_project' => $id, 'id_agent' => $user_id]);
        if ($temp) {
            $temp['description'] = $desc;
            $temp['approved'] = 0;
            return $temp->update();
        }else
            return Yii::$app->db->createCommand()->insert('agent_project_description', [
                'id_project' => $id,
                'id_agent' => $user_id,
                'description' => $desc,
            ])->execute();
    }

    public static function onConfirmationProjects($lang, $user_id, $approved, $limit, $offset, $tag = ''){
        if(!$tag)
            return \Yii::$app->db->createCommand("SELECT portfolio.item_id, portfolio.images, item_description.title
                                                FROM  `portfolio`
                                                LEFT JOIN `item_description` ON (item_description.item_id = portfolio.item_id)
                                                WHERE portfolio.item_id IN (SELECT `id_project` FROM `agent_project_description` WHERE `id_agent` = $user_id AND `approved` = $approved)
                                                AND item_description.language = '$lang'
                                                AND portfolio.active = 1
                                                AND portfolio.moderation = 1
                                                LIMIT $limit
                                                OFFSET $offset;")->queryAll();
        else
            return \Yii::$app->db->createCommand("SELECT portfolio.item_id, portfolio.images, item_description.title
                                                FROM  `portfolio`
                                                LEFT JOIN `item_description` ON (item_description.item_id = portfolio.item_id)
                                                WHERE portfolio.item_id IN (SELECT `id_project` FROM `agent_project_description` WHERE `id_agent` = $user_id AND `approved` = $approved)
                                                AND item_description.language = '$lang'
                                                AND item_description.tags LIKE '%$tag%'
                                                AND portfolio.active = 1
                                                AND portfolio.moderation = 1
                                                LIMIT $limit
                                                OFFSET $offset;")->queryAll();
    }

    public static function onConfirmationProjectsTags($lang, $user_id, $approved){
        return \Yii::$app->db->createCommand("SELECT item_description.tags
                                                FROM  `portfolio`
                                                LEFT JOIN `item_description` ON (item_description.item_id = portfolio.item_id)
                                                WHERE portfolio.item_id IN (SELECT `id_project` FROM `agent_project_description` WHERE `id_agent` = $user_id AND `approved` = $approved)
                                                AND item_description.language = '$lang'
                                                AND portfolio.active = 1
                                                AND portfolio.moderation = 1;")->queryAll();
    }

    public static function onConfirmationProjectsCount($lang, $user_id, $approved, $tag = ""){
        if($tag)
            return \Yii::$app->db->createCommand("SELECT COUNT(*)
                                                FROM  `portfolio`
                                                LEFT JOIN `item_description` ON (item_description.item_id = portfolio.item_id)
                                                WHERE portfolio.item_id IN (SELECT `id_project` FROM `agent_project_description` WHERE `id_agent` = $user_id AND `approved` = $approved)
                                                AND item_description.language = '$lang'
                                                AND portfolio.active = 1
                                                AND portfolio.moderation = 1
                                                AND item_description.tags LIKE '%$tag%';")->queryAll();
        else
            return \Yii::$app->db->createCommand("SELECT COUNT(*)
                                                FROM  `portfolio`
                                                WHERE portfolio.item_id IN (SELECT `id_project` FROM `agent_project_description` WHERE `id_agent` = $user_id AND `approved` = $approved)
                                                AND portfolio.active = 1
                                                AND portfolio.moderation = 1;")->queryAll();
    }
}
