<?php
namespace frontend\account\models;

use Yii;
use yii\data\ActiveDataProvider;


class ProductsCollections extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'products_collections';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['ru', 'en'], 'string'],
        ];
    }

    public static function getCollectionsById($collections_id, $lang)
    {
        if (is_array($collections_id))
            $collections = static::find()->where(['in', 'id', $collections_id])->select(['id', $lang])->orderBy('id DESC')->asArray()->all();
        else
            $collections = static::find()->where(['id' => $collections_id])->select(['id', $lang])->orderBy('id DESC')->asArray()->all();
        return $collections;
        die();
    }

    public static function selectCollectionIdByColectionName($collection, $lang)
    {
            return array_column(static::find()
                ->where(['like', $lang, $collection])
                ->select(['id'])
                ->orderBy('id DESC')
                ->asArray()
                ->all(), 'id');
    }

    public static function selectCollectionsIdByColectionName($collections, $lang)
    {
        $collections = explode(',', $collections);
        return array_column(static::find()
            ->where(['in', $lang, $collections])
            ->select(['id'])
            ->orderBy('id DESC')
            ->asArray()
            ->all(), 'id');
    }

    public static function selectAllCollections()
    {
        return static::find()
            ->asArray()
            ->all();
    }
}