<?php

namespace frontend\account\models;

use Yii;

/**
 * This is the model class for table "companies_collections".
 *
 * @property integer $id
 * @property integer $id_company
 * @property string $collection
 */
class CompaniesCollections extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'companies_collections';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_company', 'collection'], 'required'],
            [['id_company'], 'integer'],
            [['collection'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_company' => 'Id Company',
            'collection' => 'Collection',
        ];
    }

    public static function selectCollectionsByCompanyId($id_company)
    {
        return static::find()->where(['id_company' => $id_company])->select(['collection'])->asArray()->one();
    }

    public function saveCompanyCollections($id_company, $collections)
    {
        $this->id_company = $id_company;
        $this->collection = $collections;
        return $this->save();
    }

    public static function updateCompanyCollections($id_company, $collections)
    {
        $collection = static::findOne(['id_company' => $id_company]);
        $collection->collection = ($collections != null) ? $collections : $collection->collection;
        return $collection->update();
    }

    public static function selectCompaniesIdByCategory($category)
    {
        return array_column(static::find()
            ->select(['id_company'])
            ->where(['like', 'collection', $category])
            ->asArray()
            ->all(), 'id_company');
    }
}
