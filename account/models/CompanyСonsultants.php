<?php
namespace frontend\account\models;
use Yii;
use yii\data\ActiveDataProvider;


class CompanyConsultants extends \yii\db\ActiveRecord
{
//    public $id;
//    public $title;

    public static function tableName()
    {
        return 'products_tags';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_user', 'send_message'], 'integer'],
            [['first_name', 'last_name', 'position', 'avatar', 'phone'], 'string'],
        ];
    }

}