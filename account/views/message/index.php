<?php

use yii\widgets\LinkPager;
use yii\helpers\Url;

$this->title = Yii::t('titles', 'account').Yii::t('titles', 'messages');
$this->params['breadcrumbs'][] = $this->title;

?>

<!--START CENTER-->
<div class="col-lg-7 central-content centralScroll" xmlns="http://www.w3.org/1999/html">

    <div class="content page-catalog page-in-catalog in-order applications message-from-moon">
        <div class="row">
            <div class="reviews aplication new-messages">
                <h4 class="promote  promote-messages">New message</h4>
                <div class="clearfix"></div>
                <form class="forma-input">
                    <div class="input-edit">
                        <div class="input-container">
                            <input class="text-input floating-label" type="text" name="sample1" />
                            <label for="sample1">Recipient</label>
                        </div>
                    </div>

                    <textarea class='message-to-moon' name="" id="" cols="30" rows="10" placeholder="Your message..."></textarea>
                    <div class="uploading-files-button button-for-change">
                        <input type="file" class="files-to-moon" name="file" accept=".txt, .pdf, .rtf, .doc, .jpg, .png" multiple="">
                        <i class="icon-symbols"></i>
                    </div>
          
                    <div class="block-button">
                        <input type="submit" id="message-to-moon" class="button" value="Send">
                    </div>
                    
                </form>

                <div class="uploading-files-list">
                </div>
            </div>
        </div>
    </div>

    <div class="content page-catalog page-in-catalog in-order applications">
        <div class="row">
            <div class="reviews aplication">
                <h4 class="promote  promote-messages"><?= Yii::t('account', 'messages') ?></h4>
                <p class="upload-portfolio download-portfolio write"><?= Yii::t('account', 'write') ?><a href="#"></a></p>
                <div class="clearfix"></div>

                <div class="all-correspondence">

                    <?php foreach($correspondence as $message) : ?>

                        <?php if($message['type'] == 'get') : ?>

                            <div class="reviews-comment recipient" id="<?= $interlocutor['id'] ?>">
                                <div class="reviews-photo">
                                    
                                    <a href="<?= Url::to(['/about/user/', 'id' => $interlocutor['id']]); ?>">

                                        <img src="<?= Yii::getAlias('@avatar/'.$interlocutor['avatar']); ?>" alt="">

                                        <p class="name"><?= $interlocutor['first_name']." ".$interlocutor['last_name'] ?><span><?php if($interlocutor['specialization'] != 'NULL') echo $interlocutor['specialization'] ?></span></p>

                                    </a>

                                </div>

                                <div class="my-content-messages">

                                    <?php if(!empty($message['message'])) : ?>

                                        <p></p><?= $message['message'] ?></p>

                                    <?php endif; ?>

                                    <?php if (!empty($message['files'])) : ?>

                                        <ul class="uploading-files">

                                            <?php foreach ($message['files'] as $file) : ?>

                                                <li><a href="/media/upload/<?= $file ?>" download="/media/upload/<?= $file ?>"><?= substr($file, 4, strlen($file)) ?></a></li>

                                            <?php endforeach; ?>

                                        </ul>

                                    <?php endif; ?>

                                </div>

                                <p class="data"><?= $message['created_at'] ?></p>
                            </div>
                            <hr class="sline">

                        <?php else : ?>

                                <div class="reviews-comment my-reviews-comment">
                                    <div class="from-content-messages">
                                       <?php if(!empty($message['message'])) : ?>

                                           <?= $message['message'] ?>

                                       <?php endif; ?>

                                       <?php if ($message['files'] != 0) : ?>

                                           <ul class="uploading-files">

                                           <?php foreach ($message['files'] as $file) : ?>

                                               <li><a href="/media/upload/<?= $file ?>" download="/media/upload/<?= $file ?>"><?= substr($file, 4, strlen($file)) ?></a></li>

                                           <?php endforeach; ?>

                                           </ul>

                                       <?php endif; ?>
                                    </div>
                                <div class="reviews-photo">
                                    <img src="<?= Yii::getAlias('@avatar/'.$currentUser['avatar']); ?>" alt="">
                                    <p class="name"><?= $currentUser['first_name']." ".$currentUser['last_name'] ?><span><?php if($currentUser['specialization'] != 'NULL') echo $currentUser['specialization'] ?></span></p>
                                </div>
                                <p class="data"><?= $message['created_at'] ?></p>
                            </div>
                            <hr class="sline">

                        <?php endif; ?>

                    <?php endforeach;?>

                    <div class="bread-crumbs">
                        <?= LinkPager::widget([
                            'pagination' => $pagination,
                            'disabledPageCssClass' => false,
                            'nextPageLabel' => '',
                            'prevPageLabel' => '',
                            'options'=>['class'=>'hvr-radial-out'], ]) ?>
                    </div>

                    <div class="clearfix"></div>

                </div>


                <form class="new-messages" action="">
                    <textarea name="message" class="better-call-soul"  id="" cols="30" rows="10" data-file-names=""></textarea>

                    <div class="uploading-files-main">

                    </div>

                    <div class="uploading-files-button">
                        <input type="file" class="" id="file-message-input" name="file" accept=".txt, .pdf, .rtf, .doc, .jpg, .png" multiple="">
                        <i class="icon-symbols"></i>
                    </div>
                    <div class="block-button">
                        <input type="submit" class="button" id="main-send" value="<?= Yii::t('account', 'send') ?>">
                    </div>
                </form>

            </div>
        </div>
    </div>
    <div class="row">
        <?= frontend\widgets\Banner::widget(['position' => 'bottom']);?>
    </div>


</div>
<!--END CENTER-->


<?php
//$this->registerJsFile('js/jquery-2.1.4.min.js', ['depends'=>'frontend\assets\AppAsset']);
//$this->registerJsFile('js/owl.carousel.min.js', ['depends'=>'frontend\assets\AppAsset']);
//$this->registerJsFile('js/bootstrap.min.js', ['depends'=>'frontend\assets\AppAsset']);
//$this->registerJsFile('js/browser.js', ['depends'=>'frontend\assets\AppAsset']);
////$this->registerJsFile('js/jquery.uploadThumbs.js', ['depends'=>'frontend\ass    ets\AppAsset']);
//$this->registerJsFile('js/common.js', ['depends'=>'frontend\assets\AppAsset']);
//$this->registerJsFile('js/jscript.js', ['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('https://code.jquery.com/ui/1.11.4/jquery-ui.js', ['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('js/messages.js', ['depends'=>'frontend\assets\AppAsset']);
?>