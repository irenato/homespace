<?php
use yii\widgets\LinkPager;
use frontend\widgets\Banner;
use frontend\widgets\SideBar;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\Html;

?>

    <div class="col-lg-7 profile-content profile-agent-comps agents centralScroll archdes promteaser promotional">
        <div class="row">
            <div class="content">
                <div class="portfolio-top profile-conten-me ">
                    <div class="catalog-box special-offers-catalog page-in-catalog">
                        <div class="clearfix"></div>
                        <h4 class="promote promote-bottom">Promotional teaser</h4>
                        <p class=" upload-portfolio download-portfolio join add-new-teaser">
                            Add<a href="#"></a></p>
                        <!--                        <p class="upload-portfolio download-portfolio join" data-toggle="modal"-->
                        <!--                           data-target=".group-modal">-->
                        <!--                            Add<a href="#"></a></p>-->

                        <div class="clearfix"></div>
                        <form class="forma-input">
                            <div class="search">
                                <div class="input-edit">
                                    <div class="input-container">
                                        <input class="text-input floating-label" name="q" type="text" value="">
                                        <label for="sample1">Search</label>
                                    </div>
                                </div>
                            </div>
                            <div class="selects">
                                <select name="column">
                                    <option value="title" selected>Search by</option>
                                    <option value="title">Title</option>
                                    <option value="city">City</option>
                                    <option value="category">Category</option>
                                </select>
                            </div>
                        </form>
                        <div class="companys">

                            <div class="preloader">
                                <img src="/images/preloader.gif" alt="">
                            </div>
                            <div>
                                <!-- Form for add new teaser(start) -->
                                <div id="add-new-teaser" class="company agents edite"
                                     style="background-image: url('/images/pic_50.png'); display: none;">
                                    <?php $form = ActiveForm::begin(['id' => 'group']); ?>
                                    <div class="bg teaser-block">
                                        <div class="checkbox">
                                            <p>
                                                <input type="hidden" name="Groups[flag]" value="0">
                                                <input id="groups-flag" name="Groups[flag]" value="1"
                                                       type="checkbox">
                                                <label for="groups-flag">to make major</label>
                                            </p>
                                        </div>

                                        <div class="form-group field-groups-title required ">
                                            <div class="input-container">
                                                <input type="text" id="groups-title"
                                                       class="form-control text-input floating-label"
                                                       name="Groups[title]" label="Title" id="pt-title">
                                            </div>
                                            <div class="help-block"></div>
                                        </div>
                                        <!--                                        <input type="text" value="">-->

                                        <ul class="buttons">
                                            <li><input type="submit" id="create-new-teaser" class="button" value="Save">
                                            </li>
                                        </ul>
                                        <div class="clearfix"></div>
                                        <div class="author"><img
                                                src="<?= Yii::getAlias('@avatar/' . $user_data->logo) ?>" alt="#">
                                            <p class="name"><?= $user_data->company ?></p>
                                        </div>
                                        <div class="author-description">
                                            <img class="company-img"
                                                 src="<?= Yii::getAlias('@avatar/' . $user_data->avatar) ?>"
                                                 alt="#">
                                            <div class="name-company">
                                                <h5><?= $user_data->first_name ?>
                                                    <span><?= $user_data->last_name ?></span></h5>
                                            </div>
                                        </div>
                                        <div class="city">
                                            <?php echo $form->field($model, 'cities_id')->hiddenInput()->label(false); ?>
                                            <ul>
                                                <li>
                                                    <input id="google-search-cities" type="search" value="">
                                                </li>
                                            </ul>


                                            <div class="showRooms">
											<span class="showRoom">
												show <br> room
											</span>
                                                <span class="showRoom">
												show <br> room
											</span>
                                            </div>
                                            <div class="search-result">
                                                <ul id="search-result-items"></ul>
                                            </div>
                                        </div>
                                        <textarea name="Groups[text]" class="description" id="pm-desc"></textarea>
                                        <ul class="categories">
                                            <!--                                            <li><a href="#">Categories:</a></li>-->
                                            <li> <?= $form->field($model, 'colections[]')->widget(Select2::classname(), [
                                                    'data' => $collections,
                                                    'options' => [
                                                        'multiple' => true,
                                                        'placeholder' => 'Categories',
                                                        'id' => 'pm_categories'
                                                    ],
                                                    'showToggleAll' => false,
                                                    'pluginOptions' => [
                                                        'maximumSelectionLength' => 4,
                                                        'allowClear' => true,
                                                    ],
                                                ])->label(false); ?></li>

                                        </ul>
                                        <ul class="description">
                                            <li>Added products: <span>56</span></li>
                                            <li>Identified objects: <span>16</span></li>
                                            <li>Special offers: <span>16</span></li>

                                        </ul>
                                        <ul class="categories best">
                                            <li><a href="#">Best seller for:</a></li>
                                            <li> <?= $form->field($model, 'categories[]')->widget(Select2::classname(), [
                                                    'data' => $categories,
                                                    'options' => [
                                                        'multiple' => true,
                                                        'id' => 'pm_collections'
                                                    ],
                                                    'showToggleAll' => false,
                                                    'pluginOptions' => [
                                                        'maximumSelectionLength' => 4,
                                                        'allowClear' => true,
                                                    ],
                                                ])->label(false); ?></li>
                                        </ul>
                                    </div>
                                    <?php ActiveForm::end() ?>
                                </div>
                                <!-- Form for add new teaser(end) -->


                                <!-- Form for edit current teaser(start) -->
                                <div class="company agents use-it-for-edit-teaser"
                                     style="background-image: url('/images/pic_50.png'); display: none;">
                                    <div class="main-block">
                                        <div class="bg">
                                            <div class="checkbox">
                                                <p>
                                                    <input id="pt-status" name="pt-status" type="checkbox" value="1">
                                                    <label for="pt-status">To make major</label>
                                                </p>
                                            </div>
                                            <input type="text" class="teaser-title-edit">
                                            <ul class="buttons">
                                                <li><input type="submit" class="button current-teaser-edit"
                                                           value="Save"></li>
                                            </ul>
                                            <div class="clearfix"></div>
                                            <div class="author"><img class="company-img-edit" src="" alt="#">
                                                <p class="name teaser-company-name-edit"></p>
                                            </div>
                                            <div class="author-description">
                                                <img class="company-img company-avatar-edit" src="#" alt="#">
                                                <div class="name-company">
                                                    <h5 class="teaser-first-name-edit"><span
                                                            class="teaser-last-name-edit"></span></h5>
                                                </div>
                                            </div>
                                            <div class="city">
                                                <ul>
                                                    <li><input class="search-cities" type="search"></li>
                                                </ul>
                                                <div class="search-result">
                                                    <ul class="search-result-items"></ul>
                                                </div>
                                                <div class="showRooms">
											<span class="showRoom">
												show <br> room
											</span>
                                                    <span class="showRoom">
												show <br> room
											</span>
                                                </div>

                                            </div>
                                            <textarea name="description" class="description"></textarea>
                                            <ul class="categories">
                                                <li><a href="#">Categories:</a></li>
                                                <li><input class="teaser-categories-edit" name="categories"
                                                           type="text" value=""></li>

                                            </ul>
                                            <ul class="description">
                                                <li>Added products: <span>56</span></li>
                                                <li>Identified objects: <span>16</span></li>
                                                <li>Special offers: <span>16</span></li>

                                            </ul>
                                            <ul class="categories best">
                                                <li><a href="#">Best seller for:</a></li>
                                                <li><textarea class="teaser-collections-edit" name="#"></textarea>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <!-- Form for edit current teaser(end) -->
                                <?php if ($promotional_teasers): ?>
                                <?php while ($promotional_teasers) : ?>
                                    <?php $teaser = array_pop($promotional_teasers); ?>
                                    <div class="company agents"
                                         style="background-image: url('/images/pic_50.png');">
                                        <div class="main-block">
                                            <div class="bg teaser-block">
                                                <!--                                            <ul class="buttons">-->
                                                <h4 class="promote promote-bottom teaser-title"><?= $teaser['title'] ?></h4>
                                                <p class="upload-portfolio download-portfolio edit edit-teaser"
                                                   data-id="<?= $teaser['id'] ?>">Edit<a href="#"></a></p>
                                                <!--                                            </ul>-->
                                                <div class="clearfix"></div>
                                                <div class="author"><img class="agent-teaser-logo-img"
                                                                         src="<?= Yii::getAlias('@avatar/' . $user_data->logo) ?>"
                                                                         alt="#">
                                                    <p class="name teaser-company-name"><?= $user_data->company ?></p>
                                                </div>
                                                <div class="author-description">
                                                    <img class="company-img"
                                                         src="<?= Yii::getAlias('@avatar/' . $user_data->avatar) ?>"
                                                         alt="#">
                                                    <div class="name-company">
                                                        <h5 class="teaser-first-name"><?= $user_data->first_name ?>
                                                            <span
                                                                class="teaser-last-name"><?= $user_data->last_name ?></span>
                                                        </h5>
                                                    </div>
                                                </div>
                                                <div class="city" data-cities="<?= $teaser['city_name'] ?>"
                                                     data-cities-id="<?= $teaser['cities_id'] ?>">
                                                    <ul>
                                                        <?php $cities = explode(',', $teaser['city_name']);
                                                        $place_id = explode(',', $teaser['cities_id']);
                                                        $i = -1;
                                                        foreach ($cities as $city) :
                                                            $i++;
                                                            ?>
                                                            <li data-id="<?= $place_id[$i] ?>"><?= $city ?></li>
                                                        <?php endforeach; ?>
                                                    </ul>
                                                    <div class="showRooms">
                                    <span class="showRoom">
                                        show <br> room
                                    </span>
                                                        <span class="showRoom">
                                        show <br> room
                                    </span>
                                                    </div>
                                                    <div class="cities-list">
                                                        <p><?= $user_data->city ?></p>
                                                    </div>
                                                </div>
                                                <div class="description-block">
                                                    <p class="descriptions">
                                                        <?= substr($teaser['text'], 0, 120) . '...' ?>
                                                    </p>
                                                </div>
                                                <ul class="categories"
                                                    data-categories-id="<?= implode(',', array_column($teaser['teaser_categories'], 'id')) ?>"
                                                    data-categories-list="<?= implode(',', array_column($teaser['teaser_categories'], 'title_' . $language)) ?>">
                                                    <li><a href="#">Categories:</a></li>
                                                    <?php foreach ($teaser['teaser_categories'] as $category) : ?>
                                                        <li><a data-id="<?= $category['id'] ?>"
                                                               href="#"><?= $category['title_' . $language] ?></a>
                                                        </li>
                                                    <?php endforeach; ?>
                                                </ul>
                                                <ul class="description">
                                                    <li>Added products: <span>56</span></li>
                                                    <li>Identified objects: <span>16</span></li>
                                                    <li>Special offers: <span>16</span></li>
                                                </ul>
                                                <ul class="categories best"
                                                    data-collections-id="<?= implode(',', array_column($teaser['teaser_collections'], 'id')) ?>"
                                                    data-collections-list="<?= implode(',', array_column($teaser['teaser_collections'], $language)) ?>">
                                                    <li><a href="#">Best seller for:</a></li>
                                                    <?php foreach ($teaser['teaser_collections'] as $collection) : ?>
                                                        <li><a href="#">-<?= $collection[$language] ?></a></li>
                                                    <?php endforeach; ?>
                                                </ul>
                                                <!--                                        <ul class="buttons">-->
                                                <!--                                            <li><a href="#" class="button">View</a></li>-->
                                                <!--                                        </ul>-->
                                                <!--                                        <a href="#" class="messages"><i class="icon-black218"></i>Message</a>-->
                                            </div>
                                        </div>
                                    </div>
                                <?php endwhile; ?>

                                <div class="clearfix"></div>
                                <div class="bread-crumbs">
                                    <?= LinkPager::widget(['pagination' => $pagination,
                                        'disabledPageCssClass' => false,
                                        'nextPageLabel' => '',
                                        'prevPageLabel' => '',
                                        'options' => ['class' => 'hvr-radial-out1, teaser-pagination'],
                                    ]); ?>
                                </div>
                            </div>
                        </div>

                        <?php endif; ?>
                        <?= frontend\widgets\Banner::widget(['position' => 'bottom']); ?>
                    </div>
                </div>
            </div>
        </div>
        <!--END CENTER-->
    </div>
<?php
$this->registerJsFile('agent_js/agents.js', ['depends' => 'frontend\assets\AppAsset']);
$this->registerJsFile('agent_js/action_agent.js', ['depends' => 'frontend\assets\AppAsset']);
$this->registerJsFile('https://maps.googleapis.com/maps/api/js?key=AIzaSyBZLWGV4klfZp4MjxvXtnljM-qsxyOdzrE&libraries=places&language=' . $language, ['depends' => 'frontend\assets\AppAsset']);
