<?php
use frontend\widgets\Banner;
use yii\widgets\LinkPager;

use yii\widgets\Pjax;
use yii\widgets\ActiveForm;
use frontend\account\models\Likes;

//use Yii;
$this->title = Yii::t('titles', 'account') . Yii::t('titles', 'group');
$this->params['breadcrumbs'][] = $this->title;

?>


<!--START CENTER-->
<div class="col-lg-7 profile-content profile-agent-comps agents centralScroll archdes promteaser">
    <div class="row">
        <div class="content">
            <div class="portfolio-top profile-conten-me ">
                <div class="catalog-box special-offers-catalog page-in-catalog">
                    <div class="clearfix"></div>
                    <h4 class="promote promote-bottom">Promotional teaser</h4>
                    <div class="clearfix"></div>
                    <!--                    <form class="forma-input">-->
                    <!--                        <div class="search">-->
                    <!--                            <div class="input-edit">-->
                    <!--                                <div class="input-container">-->
                    <!--                                    <input class="text-input floating-label" name="sample1" type="text">-->
                    <!--                                    <label for="sample1">Search</label>-->
                    <!--                                </div>-->
                    <!--                            </div>-->
                    <!--                        </div>-->
                    <!--                        <div class="selects">-->
                    <!--                            <select name="#">-->
                    <!--                                <option value="#">Search by</option>-->
                    <!--                                <option value="#">Search by</option>-->
                    <!--                                <option value="#">Search by</option>-->
                    <!--                                <option value="#">Search by</option>-->
                    <!--                            </select>-->
                    <!--                        </div>-->
                    <!--                    </form>-->
                    <div class="companys cfrj">
                        <?php if ($new_agents) : ?>
                            <?php foreach ($new_agents as $new_agent) : ?>
                                <div class="company agents pf-new new-groups-for-des" style="background-image: url('/images/pic_50.png');" data-user="<?= $new_agent['id'] ?>">
                                    <div class="bg teaser-block">
                                        <h4><?= $teaser['title'] ?></h4>
                                        <ul class="buttons">
                                            <li><a class="accept-invitation" href="#" data-id="<?= $new_agent['id'] ?>">Confirm</a></li>
                                            <li><a class="cancel-invitation" href="#" data-id="<?= $new_agent['id'] ?>">Reject</a></li>
                                        </ul>
                                        <div class="author"><img
                                                src="<?= Yii::getAlias('@avatar/' . $new_agent['logo']) ?>" alt="#">
                                            <p class="name"><?= $new_agent['company']; ?></p>
                                        </div>
                                        <div class="author-description">
                                            <img class="company-img"
                                                 src="<?= Yii::getAlias('@avatar/' . $new_agent['avatar']) ?>" alt="#">
                                            <div class="name-company">
                                                <h5><?= $new_agent['first_name'] ?>
                                                    <span><?= $new_agent['last_name'] ?></span>
                                                </h5>
                                            </div>
                                        </div>
                                        <div class="city">
                                            <ul>
                                                <?php $cities = explode(',', $new_agent['teaser']['cities_names']);
                                                $place_id = explode(',', $new_agent['teaser']['cities_id']);
                                                $i = -1;
                                                foreach ($cities as $city) :
                                                    $i++;
                                                    ?>
                                                    <li data-id="<?= $place_id[$i] ?>"><?= $city ?></li>
                                                <?php endforeach; ?>
                                            </ul>
                                            <div class="showRooms">
												<span class="showRoom">
													show <br> room
												</span>
												<span class="showRoom">
													show <br> room
												</span>
                                            </div>
                                            <p><?= $new_agent['city'] ?></p>
                                        </div>
                                        <p class="descriptions">
                                            <?= substr($new_agent['teaser']['text'], 0, 120) . '...' ?>
                                        </p>
                                        <ul class="categories">
                                            <li><a href="#">Categories:</a></li>
                                            <?php foreach($new_agent['teaser']['teaser_categories'] as $category) : ?>
                                                <li><a data-id="<?= $category['id'] ?>" href="#"><?= $category['title_' . $language] ?></a></li>
                                            <?php endforeach; ?>
                                        </ul>
                                        <ul class="description">
                                            <li>Added products: <span>56</span></li>
                                            <li>Identified objects: <span>16</span></li>
                                            <li>Special offers: <span>16</span></li>

                                        </ul>
                                        <ul class="categories best">
                                            <li><a href="#">Best seller for:</a></li>
                                            <?php foreach($new_agent['teaser']['teaser_collections'] as $collection) : ?>
                                                <li><a href="#">-<?= $collection[$language] ?></a></li>
                                            <?php endforeach; ?>
                                        </ul>
                                        <ul class="buttons">
                                            <li><a href="<?= Yii::$app->urlManager->createUrl(['agents/profile', 'id' => $new_agent['id']]) ?>" class="button">View</a></li>
                                        </ul>
                                        <a href="#" class="messages send-chat-message"><i class="icon-black218"></i>Message</a>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        <?php endif; ?>
                        <div class="clearfix"></div>
<!--                        <div class="bread-crumbs">-->
<!--                            <ul class="hvr-radial-out">-->
<!--                                <li>-->
<!--                                    <a href="#"></a>-->
<!--                                </li>-->
<!--                                <li class="active"><a href="#">1</a></li>-->
<!--                                <li><a href="#">2</a></li>-->
<!--                                <li><a href="#">3</a></li>-->
<!--                                <li><a href="#">4</a></li>-->
<!--                                <li><a href="#">5</a></li>-->
<!--                                <li><a href="#">6</a></li>-->
<!--                                <li><a href="#">7</a></li>-->
<!--                                <li>-->
<!--                                    <a href="#"></a>-->
<!--                                </li>-->
<!--                            </ul>-->
<!--                        </div>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--END CENTER-->
</div>


<?php
$this->registerJsFile('js/new_group.js', ['depends' => 'frontend\assets\AppAsset']);
$this->registerJsFile('js/chat2.js', ['depends' => 'frontend\assets\AppAsset']);
?>
