<?php

use frontend\widgets\Banner;
use yii\widgets\LinkPager;

use yii\widgets\Pjax;
use yii\widgets\ActiveForm;
use frontend\account\models\Likes;

//use Yii;
$this->title = Yii::t('titles', 'account') . Yii::t('titles', 'group');
$this->params['breadcrumbs'][] = $this->title;

?>


<!--START CENTER-->
<div class="col-lg-7 central-content centralScroll">
    <div class="content group">

        <!-- Groups -->
        <?= frontend\widgets\PGroups::widget(); ?>
        <!-- End Groups -->

        <div class="row groups-instruments-column">
            <div class="all-grup">
                <h4 class="promote"><?= Yii::t('account', 'all_groups') ?></h4>
                <div class="sel group-sort-instruments">
                    <div class="selects" name="sort">
                        <p><?= Yii::t('account', 'sort_by') ?></p>
                        <i class="icon-sort"></i>
                        <select class="sort sort-groups">
                            <option class="hiden"></option>
                            <option value="likes"><?= Yii::t('account', 'most_liked') ?></option>
                            <option value="views"><?= Yii::t('account', 'most_viewed') ?></option>
                        </select>
                    </div>
                    <div class="selects" name="count">
                        <p><?= Yii::t('account', 'view_by') ?></p>
                        <i class="icon-view"></i>
                        <select class="view">
                            <option value="6">6</option>
                            <option value="8">8</option>
                            <option value="10">10</option>
                            <option value="12">12</option>
                        </select>
                    </div>
                    <form class="forma-input">
                        <i class="icon-tag"></i>
                        <div class="input-container">
                            <input class="text-input floating-label group-tags" type="text" name="sample"
                                   value="<?= $tags ?>"/>
                            <label for="sample"><?= Yii::t('account', 'tags') ?></label>
                        </div>
                    </form>
                </div>
                <div class="clearfix"></div>
                <?php foreach ($groups as $group) : ?>

                    <a href="<?= $get_url ?>/group?<?= $group['id'] ?>">
                        <div class="item">
                            <div class="portfolio-item white">
                                <a href="<?= Yii::$app->urlManager->createUrl(['account/join-group/group?' . $group['id']]); ?>"><img
                                        src="<?= $alias ?>/images/<?= $group['images'] ?>" alt=""></a>
                                <div class="like-ico">
                                    <i class="icon-eye110"><?= $group['views'] ?></i>
                                    <i class="icon-user168"><?= $group['followers_count'] ?></i>
                                    <i class="icon-heart2971"><?= $group['likes'] ?></i>
                                </div>
                                <a href="#" class="like"><i class="icon-heart297"></i></a>
                                <p><?= \frontend\account\controllers\JoinGroupController::textCutter($group['text'], 200) ?></p>
                            </div>
                        </div>
                    </a>

                <?php endforeach; ?>


            </div>
            <div class="clearfix"></div>
            <!--div class="bread-crumbs">-->
            <div class="bread-crumbs">
                <!--                --><? //= LinkPager::widget(['pagination' => $pagination,'disabledPageCssClass'=>false,'options'=>['class'=>'hvr-radial-out'], ]) ?>
                <?= LinkPager::widget(['pagination' => $pagination,
                    'disabledPageCssClass' => false,
                    'nextPageLabel' => '',
                    'prevPageLabel' => '',
                    'options' => ['class' => 'hvr-radial-out'],
                ]); ?>

            </div>
        </div>
        <div class="row">
            <?= frontend\widgets\Banner::widget(['position' => 'bottom']); ?>
        </div>
    </div>
</div>
<!--END CENTER-->


<?php
$this->registerJsFile('js/new-js.js', ['depends' => 'frontend\assets\AppAsset']);
$this->registerJsFile('js/common.js', ['depends' => 'frontend\assets\AppAsset']);
$this->registerJsFile('js/owl.carousel.min.js', ['depends' => 'frontend\assets\AppAsset']);
//$this->registerJsFile('js/fblikes.js', ['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('js/action.js', ['depends' => 'frontend\assets\AppAsset']);
$this->registerJsFile('js/join_group.js', ['depends' => 'frontend\assets\AppAsset']);
?>
