<?php
use yii\widgets\LinkPager;


$this->title = Yii::t('titles', 'account').Yii::t('titles', 'portfolio');
$this->params['breadcrumbs'][] = $this->title;

?>

<!--START CENTER-->
<div class="col-lg-7 central-content centralScroll">
	<div class="content">

		<!--Items -->
		<div class="row">
			<p class="upload-portfolio download-portfolio join"><?= Yii::t('account', 'add_item') ?><a href="<?= $create_url ?>"></a></p>
			<h4>Projects</h4>
			<div class="clearfix"></div>
			<?php if($items){ ?>
				<?php foreach ($items as $item) {?>
						<div class="portfolio-item white">
							<img src="<?= $item['image'] ?>" alt="<?= $item['title'] ?>">
							<p><?= $item['title'] ?></p>
							<a href="<?= $item['url'] ?>" class="hvr-radial-out"></a>
						</div>
				<?php } ?>
			<?php } ?>
			<div class="clearfix"></div>
			<div class="bread-crumbs">
				<?= LinkPager::widget([
					'pagination' => $pagination,
					'disabledPageCssClass' => false,
					'maxButtonCount' => 10,
					'nextPageLabel' => '',
					'prevPageLabel' => '',
					'options'=>['class'=>'hvr-radial-out'], ]) ?>
			</div>

		</div>
		<hr class="sline">
		<div class="row">
			<h4>Design&Decor</h4>
			<div class="clearfix"></div>
			<?php if($decor){ ?>
				<?php $item = null; ?>
				<?php foreach ($decor as $item) {?>
						<div class="portfolio-item white">
							<img src="<?= $item['image'] ?>" alt="<?= $item['title'] ?>">
							<p><?= $item['title'] ?></p>
							<a href="<?= $item['url'] ?>" class="hvr-radial-out"></a>
						</div>
				<?php } ?>
			<?php } ?>

			<div class="clearfix"></div>
			<div class="bread-crumbs">
				<?= LinkPager::widget([
					'pagination' => $pagination2,
					'disabledPageCssClass' => false,
					'maxButtonCount' => 10,
					'nextPageLabel' => '',
					'prevPageLabel' => '',
					'options'=>['class'=>'hvr-radial-out'], ]) ?>
			</div>
		</div>
		<!-- End Items -->
		<!-- Best 3 items -->

		<div class="row row-2b">
			<div class="sidebar-caption"><?= Yii::t('account', 'tariff_plan_2B') ?><br></div>
			<p class="upload-portfolio download-portfolio" id="set-best-3-works"><?= Yii::t('account', 'edit_best') ?><a href="#"></a></p>
			<div class="clearfix"></div>
			<div class="tariff-plan_block">
				<?php $count = 1;?>
				<?php foreach ($best_items as $item) {?>
					<div id="work-<?= $count ?>" class="work" data-toggle="modal" data-target="#best_3_works">
						<img src="<?= $item['image'] ?>" data-item="<?= $item['id'] ?>" data-link="<?= $item['url'] ?>">
					</div>
					<?php $count++;?>
				<?php } ?>
			</div>
		</div>

		<!-- End Best 3 items -->
        <br>
		<hr class="sline">

		<!-- Groups -->
		<?= frontend\widgets\PGroups::widget();?>
		<!-- End Groups -->

		<div class="row">
			<?= frontend\widgets\Banner::widget(['position' => 'bottom']);?>
		</div>
	</div>
</div>
<!--END CENTER-->



<?php
$this->registerJsFile('js/bootstrap.min.js', ['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('js/common.js', ['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('js/owl.carousel.min.js', ['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('scripts/portfolio.js', ['depends'=>'frontend\assets\AppAsset']);
?>
