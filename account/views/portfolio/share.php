<?php
$this->title = Yii::t('titles', 'account').Yii::t('titles', 'share');
$this->params['breadcrumbs'][] = $this->title;
?>

<!--START CENTER-->
<div class="col-lg-7  central-content centralScroll">
    <div class="content group share">
        <div class="row">
            <h4 class="promote promote-bottom">Share</h4>
            <p><?= Yii::t('modal', 'profile_share_text') ?></p>
            <?php if($items['items']){?>
                <?php foreach ($items['items'] as $item) {?>
                    <div class="share-item">
                        <a href="<?= $item['url'] ?>"><img src="<?= $item['image'] ?>" alt="<?= $item['title'] ?>"></a>
                        <div class="share-link">
                            <p><?= $item['title'] ?></p>
                            <a href="<?= $item['share_link'] ?>" class="share-link-text"><?= $item['share_link'] ?></a>
                            <ul>
                                <li><a href="http://www.facebook.com/sharer.php?u=<?= $item['share_link'] ?>" target="_blank"><i class="icon-facebook55"></i></a></li>
                                <li><a href="https://twitter.com/intent/tweet?text=<?= $item['title'] ?>&url=<?= $item['share_link'] ?>" target="_blank"><i class="icon-twitter1"></i></a></li>
<!--                                <a href="https://twitter.com/intent/tweet?text=--><?//= $item['title'] ?><!--&url=--><?//= $item['share_link'] ?><!--">Share on Twitter</a>-->
                            </ul>
                        </div>
                    </div>
                <?php } ?>
            <?php } ?>
        </div>
    </div>

    <div class="row">
        <?= frontend\widgets\Banner::widget(['position' => 'bottom']);?>
    </div>
</div>
<!--END CENTER-->

<?php
$this->registerJsFile('http://connect.facebook.net/en_US/all.js', ['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('scripts/common.js', ['depends'=>'frontend\assets\AppAsset']);
?>