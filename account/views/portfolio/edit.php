<?php
use frontend\models\Language;
use mihaildev\ckeditor\CKEditor;

$this->title = Yii::t('titles', 'account').Yii::t('titles', 'portfolio_edit').$item['description_ru']['title'].')';
$this->params['breadcrumbs'][] = $this->title;

?>

				
<!--START CENTER-->			
			<div class="col-lg-7 central-content about-content centralScroll">
				<div class="content">
				<?php if($item){ ?>
					<div class="row">
						<h4 class="promote drop"><?= Yii::t('account', 'header_edit') ?></h4>
						<input type="file" accept="image/*;capture=camera" style="display : none;" id="open_browse" onchange="fileChange(this);" />
						<?php $count = 1;?>
			  			<?php foreach ($item['images'] as $image) {?>
			  				<div class="dropzone" id="dropzone<?= $count ?>" style="background: url('<?= $image ?>');"><?= Yii::t('account', 'drop_zone') ?></div>
			  				<input type="hidden" id="img_hidden<?= $count ?>" value="<?= $image ?>">
			  				<?php $count++;?>
			  			<?php } ?> 
					</div>

					<?php if($paid_status > 0): ?>
						<div class="catalog-block">
							<label>Catalog Design&Decor</label>
							<?php if($item['catalog_id'] == 0): ?>
								<input type="checkbox" id="catalog1" name="catalog">
							<?php else: ?>
								<input type="checkbox" id="catalog1" name="catalog" checked>
							<?php endif; ?>
							<label for="catalog1"></label>
						</div>
					<?php endif; ?>

					<div class="category-block">
						<label>Choose categories</label>
						<select class="item-catalog-category">
							<?php foreach($categories as $category): ?>
								<option value="<?= $category['id'] ?>"><?= ($lang == 'en') ? $category['title_en'] : $category['title_ru'] ?></option>
							<?php endforeach; ?>
						</select>
					</div>
					<div class="clearfix"></div>
					<br>
						<input type="hidden" id="item_id_hidden" value="<?= $item_id ?>">
						<ul class="nav nav-tabs myTab">
						  <li class="active"><a href="#ru" data-toggle="tab">Rus</a></li>
						  <li><a href="#en" data-toggle="tab">Eng</a></li>
						</ul>
						<div class="tab-content fade in drop">
							<div class="tab-pane active" id="ru">
								<label><?= Yii::t('account', 'item_title') ?></label>
								<input type="text" placeholder="Enter Title" class="input_item_title" value="<?= $item['description_ru']['title'] ?>">
								<label><?= Yii::t('account', 'item_sd') ?></label>
								<textarea class="short_description"><?= $item['description_ru']['short_description'] ?></textarea>
								<label class="desc-drop"><?= Yii::t('account', 'item_fd') ?></label>
								<div class="editor">
									<?= CKEditor::widget([
										'name' => 'description_ru',
										'value' => $item['description_ru']['description'],
										'editorOptions' => [
											'preset' => 'full',
											'inline' => false,
											'language' => Language::getCurrent()->url,
										]
									]);?>
								</div>

								<label><?= Yii::t('account', 'item_tags') ?></label>
								<input type="text" placeholder="<?= Yii::t('account', 'item_placeholder_tags') ?>" class="input_item_tags" value="<?= $item['description_ru']['tags'] ?>">
							</div>

							<div class="tab-pane fade" id="en">
								<label><?= Yii::t('account', 'item_title') ?></label>
								<input type="text" placeholder="Enter Title" class="input_item_title" value="<?= $item['description_en']['title'] ?>">
								<label><?= Yii::t('account', 'item_sd') ?></label>
								<textarea class="short_description"><?= $item['description_en']['short_description'] ?></textarea>
								<label class="desc-drop"><?= Yii::t('account', 'item_fd') ?></label>
								<div class="editor">
									<?= CKEditor::widget([
									'name' => 'description_en',
									'value' => $item['description_en']['description'],
									'editorOptions' => [
										'preset' => 'full',
										'inline' => false,
										'language' => Language::getCurrent()->url,
									]
									]);?>
								</div>

								<label><?= Yii::t('account', 'item_tags') ?></label>
								<input type="text" placeholder="<?= Yii::t('account', 'item_placeholder_tags') ?>" class="input_item_tags" value="<?= $item['description_en']['tags'] ?>">
							</div>
						</div>
					<p class="valid"><?= Yii::t('account', 'enter_valid') ?></p>
							<p class="error" id="portfolio-save-error"><?= Yii::t('account', 'enter_fields') ?></p>
					        <button class="save_item"><?= Yii::t('account', 'button_save') ?></button>
					        <div class="clearfix"></div>
					        <div class="row">
					        	<?= frontend\widgets\Banner::widget(['position' => 'bottom']);?>
					        </div>
					        <div class="clearfix"></div>
						<?php }else{ ?>
		                    <div class="row">
		                        <h2><?= Yii::t('account', 'not_found') ?></h2>
								<br>
								<?= frontend\widgets\Banner::widget(['position' => 'bottom']);?>
		                    </div>
		                <?php } ?>
				</div>
			</div>
<!--END CENTER-->			

<?php
$this->registerJsFile('js/bootstrap.min.js', ['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('scripts/edit.js', ['depends'=>'frontend\assets\AppAsset']);
?>