<?php

$this->title = Yii::t('titles', 'account').Yii::t('titles', 'portfolio');
$this->params['breadcrumbs'][] = $this->title;

?>
		
<!--START CENTER-->			
			<div class="col-lg-7 central-content centralScroll">
				<div class="content">

					<!--Items -->
					<div class="row">
						<p class="upload-portfolio download-portfolio join"><?= Yii::t('account', 'add_item') ?><a href="<?= $create_url ?>"></a></p>
						<div class="clearfix"></div>
						<?php if($items){ ?>
							<?php $count = 0; ?>
							<?php foreach ($items as $item) {?>
								<?php if($count < $max_item_count){ ?>
									<div class="portfolio-item white">
										<img src="<?= $item['image'] ?>" alt="<?= $item['title'] ?>">
										<p><?= $item['title'] ?></p>
										<a href="<?= $item['url'] ?>" class="hvr-radial-out"></a>
									</div>
									<?php $count++; ?>
								<?php }else break; ?>
							<?php } ?> 
						<?php }else{ ?>
							<h3>All works are moderated!</h3>
						<?php } ?>
						<?php if($count == $max_item_count) {?>
							<div class="portfolio-text">
								<p><?= Yii::t('modal', 'portfolio_tariff_text') ?></p>
								<a href="#" class="button" data-toggle="modal" data-target="#modal-pay" ><?= Yii::t('account', 'move_on') ?></a>
							</div>
						<?php } ?> 
					</div>
					<!-- End Items -->
	
					<hr class="separation_line">

					<!-- Groups -->
					<?= frontend\widgets\PGroups::widget();?>
					<!-- End Groups -->

	
					<div class="row">
						<?= frontend\widgets\Banner::widget(['position' => 'bottom']);?>
					</div>
				</div>
			</div>
<!--END CENTER-->						


<?php
$this->registerJsFile('js/bootstrap.min.js', ['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('js/common.js', ['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('js/owl.carousel.min.js', ['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('scripts/portfolio.js', ['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('scripts/payment_share.js', ['depends'=>'frontend\assets\AppAsset']);
?>
