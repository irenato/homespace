<?php
use mihaildev\ckeditor\CKEditor;
use frontend\models\Language;

$this->title = Yii::t('titles', 'account').Yii::t('titles', 'portfolio_create');
$this->params['breadcrumbs'][] = $this->title;

?>

			
<!--START CENTER-->
			<div class="col-lg-7 central-content about-content centralScroll">
				<div class="content">
					<div class="row">
						<h4 class="promote drop"><?= Yii::t('account', 'header_create') ?></h4>
						<input type="file" accept="image/*;capture=camera" style="display : none;" id="open_browse" onchange="fileChange(this);" />
						<div class="dropzone" id="dropzone1"><?= Yii::t('account', 'drop_zone') ?></div>
					    <div class="dropzone" id="dropzone2"><?= Yii::t('account', 'drop_zone') ?></div>
					    <div class="dropzone" id="dropzone3"><?= Yii::t('account', 'drop_zone') ?></div>
					</div>

					<?php if($paid_status > 0): ?>
						<div class="catalog-block">
							<label>Catalog Design&Decor</label>
							<input type="checkbox" id="catalog1" name="catalog">
							<label for="catalog1"></label>
						</div>
					<?php endif; ?>

					<div class="category-block">
						<label>Choose categories</label>
						<select class="item-catalog-category">
							<?php foreach($categories as $category): ?>
								<option value="<?= $category['id'] ?>"><?= ($lang == 'en') ? $category['title_en'] : $category['title_ru'] ?></option>
							<?php endforeach; ?>
						</select>
					</div>

					<div class="tab-content fade in drop">
						<div class="tab-pane active" id="ru">
							<label><?= Yii::t('account', 'item_title') ?></label>
							<input type="text" class="input_item_title">
							<label><?= Yii::t('account', 'item_sd') ?></label>
							<textarea class="short_description"></textarea>
							<label class="desc-drop"><?= Yii::t('account', 'item_fd') ?></label>
							<div class="editor">
								<?= CKEditor::widget([
									'name' => 'description',
									'editorOptions' => [
										'preset' => 'full',
										'inline' => false,
										'language' => Language::getCurrent()->url,
									]
								]);?>
							</div>

							<label><?= Yii::t('account', 'item_tags') ?></label>
							<input type="text" placeholder="<?= Yii::t('account', 'item_placeholder_tags') ?>" class="input_item_tags">
						</div>
					</div>
					<div class="clearfix"></div>
					<p class="valid"><?= Yii::t('account', 'enter_valid') ?></p>
					<p class="error" id="portfolio-save-error"><?= Yii::t('account', 'enter_fields') ?></p>
					<button class="save_item"><?= Yii::t('account', 'button_save') ?></button>


					<div class="clearfix"></div>
					<div class="row">
						<?= frontend\widgets\Banner::widget(['position' => 'bottom']);?>
					</div>

					<div class="clearfix"></div>

				</div>

			</div>
<!--END CENTER-->			


<?php
$this->registerJsFile('js/bootstrap.min.js', ['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('scripts/create.js', ['depends'=>'frontend\assets\AppAsset']);
?>

