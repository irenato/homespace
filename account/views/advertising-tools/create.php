<?php
use yii\widgets\LinkPager;

//$this->title = Yii::t('titles', 'account') . Yii::t('titles', 'account_profile');
$this->params['breadcrumbs'][] = $this->title;
?>


    <div class="col-lg-7 central-content profile-content profile-agent  company create centralScroll ">
        <div class="row">
            <div class="content">
                <div class="agent-profil" data-img-src="/images/bg-agents.png" style="background-image: url(/images/bg-agents.png);">

                    <div class="agents">
<!--                        <input accept="image/*;capture=camera" style="display : none;" id="open_browse2" onchange="fileChange(this);" type="file">-->
<!--                            <a href="#" class="bg-profile company-bg" data-toggle="modal" data-target=".model-img"><i class="icon-camera"></i></a>-->
                            <a href="#" class="bg-profile company-bg"><i class="icon-camera"></i></a>
                        <div class="company-description">
<!--                            <input accept="image/*;capture=camera" style="display : none;" id="open_browse" onchange="fileChange(this);" type="file">-->
                            <div class="dropzone-disabled" id="disabled-input">disabled</div>
                            <form class="forma-input">

                                <div class="input-edit">
                                    <div class="input-container">
                                        <input class="text-input floating-label" name="company-name" type="text" value="">
                                        <label for="company-name"><?= Yii::t('account', 'company_name') ?></label>
                                    </div>
                                </div>
                                <div class="input-edit">
                                    <div class="input-container">
                                        <input class="text-input floating-label" name="brand-name" type="text" value="">
                                        <label for="brand-name"><?= Yii::t('account', 'brand_agency') ?></label>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="profile-edite">
                            <div class="profil-photo">
<!--                                <input accept="image/*;capture=camera" style="display : none;" id="open_browse1" onchange="fileChange(this);" type="file">-->
<!--                                <a href="#" id="profile-photo"><img src="" alt=""></a>-->
                                <a href="#"><img src="" alt=""></a>
                            </div>
                        </div>
                        <div class="create-profile">
                            <form class="forma-input">

                                <div class="input-edit position">
                                    <div class="input-container">
                                        <input class="text-input floating-label" name="position" type="text" value="<?= Yii::$app->user->identity->roles; ?>">
                                        <label for="position"><?= Yii::t('account', 'position') ?></label>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                                <div class="input-edit">
                                    <i class="icon-user168"></i>
                                    <div class="input-container">
                                        <input class="text-input floating-label" name="first-name" type="text" value="<?= Yii::$app->user->identity->first_name; ?>">
                                        <label for="first-name"><?= Yii::t('account', 'edit_first_name') ?></label>
                                    </div>
                                </div>
                                <div class="input-edit">
                                    <i class="icon-user168"></i>
                                    <div class="input-container">
                                        <input class="text-input floating-label" name="last-name" type="text" value="<?= Yii::$app->user->identity->last_name; ?>">
                                        <label for="last-name"><?= Yii::t('account', 'edit_last_name') ?></label>
                                    </div>
                                </div>
                                <div class="input-edit">
                                    <i class="icon-home"></i>
                                    <div class="input-container">
                                        <input class="text-input floating-label" name="country" type="text" value="<?= Yii::$app->user->identity->country; ?>">
                                        <label for="country"><?= Yii::t('account', 'country') ?></label>
                                    </div>
                                </div>
                                <div class="input-edit">
                                    <i class="icon-home"></i>
                                    <div class="input-container">
                                        <input class="text-input floating-label" name="city" type="text" value="<?= Yii::$app->user->identity->city; ?>">
                                        <label for="city"><?= Yii::t('account', 'edit_city') ?></label>
                                    </div>
                                </div>
                                <div class="input-edit">
                                    <i class="icon-telephone46"></i>
                                    <div class="input-container">
                                        <input class="text-input floating-label" name="phone" type="text" value="<?= Yii::$app->user->identity->phone; ?>">
                                        <label for="phone"><?= Yii::t('account', 'edit_phone') ?></label>
                                    </div>
                                </div>
                                <div class="input-edit">
                                    <i class="icon-location"></i>
                                    <div class="input-container">
                                        <input class="text-input floating-label" name="address" type="text" value="<?= Yii::$app->user->identity->address; ?>">
                                        <label for="address"><?= Yii::t('account', 'address') ?></label>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>


                    <div class="profil-edited">
                        <form class="forma-input" id="form_main_information">
                            <div class="input-edit-top sub-wrap">

                                <div class="input-edit">
                                    <i class="icon-webpage2"></i>
                                    <div class="input-container">
                                        <input class="text-input floating-label dirty" name="site" type="text" value="<?= Yii::$app->user->identity->link_user_site; ?>">
                                        <label for="site"><?= Yii::t('account', 'edit_site') ?></label>
                                    </div>
                                </div>

<!--                                <div class="input-edit">-->
<!--                                    <i class="icon-facebook55"></i>-->
<!--                                    <div class="input-container">-->
<!--                                        <input class="text-input floating-label dirty" name="facebook" type="text" value="">-->
<!--                                        <label for="facebook">Link for Facebook.com</label>-->
<!--                                    </div>-->
<!--                                </div>-->
<!---->
<!--                                <div class="input-edit">-->
<!--                                    <i class="icon-twitter1"></i>-->
<!--                                    <div class="input-container">-->
<!--                                        <input class="text-input floating-label dirty" name="twitter" type="text" value="">-->
<!--                                        <label for="twitter">Link for Twitter.com</label>-->
<!--                                    </div>-->
<!--                                </div>-->
<!---->
<!--                                <div class="input-edit">-->
<!--                                    <i class="icon-instagram12"></i>-->
<!--                                    <div class="input-container">-->
<!--                                        <input class="text-input floating-label dirty" name="instagram"  type="text" value="">-->
<!--                                        <label for="instagram">Link for Instagram.com</label>-->
<!--                                    </div>-->
<!--                                </div>-->
<!---->
<!--                                <div class="input-edit">-->
<!--                                    <i class="icon-behance2"></i>-->
<!--                                    <div class="input-container">-->
<!--                                        <input class="text-input floating-label dirty" name="behance" type="text" value="">-->
<!--                                        <label for="behance">Link for Behance.com</label>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                                <div class="input-edit">-->
<!--                                    <i class="icon-google116"></i>-->
<!--                                    <div class="input-container">-->
<!--                                        <input class="text-input floating-label dirty" name="google" type="text" value="">-->
<!--                                        <label for="google">Link for Google+</label>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                                <div class="input-edit">-->
<!--                                    <i class="icon-pinterest3"></i>-->
<!--                                    <div class="input-container">-->
<!--                                        <input class="text-input floating-label dirty" name="pinterest" type="text" value="">-->
<!--                                        <label for="pinterest">Link for Pinterest.com</label>-->
<!--                                    </div>-->
<!--                                </div>-->
<!---->
<!--                                <div class="input-edit">-->
<!--                                    <i class="icon-logotype1"></i>-->
<!--                                    <div class="input-container">-->
<!--                                        <input class="text-input floating-label dirty" name="tumblr" " type="text" value="">-->
<!--                                        <label for="tumblr">Link for Tumblr.com</label>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                                <div class="input-edit">-->
<!--                                    <i class="icon-linkedin11"></i>-->
<!--                                    <div class="input-container">-->
<!--                                        <input class="text-input floating-label dirty" name="linkedin" type="text" value="">-->
<!--                                        <label for="linkedin">Link for Linkedin.com</label>-->
<!--                                    </div>-->
<!--                                </div>-->
<!---->
<!---->
<!--                                <div class="input-edit">-->
<!--                                    <i class="icon-blogger8"></i>-->
<!--                                    <div class="input-container">-->
<!--                                        <input class="text-input floating-label dirty" name="blogger" type="text" value="">-->
<!--                                        <label for="blogger">Link for Blogger.com</label>-->
<!--                                    </div>-->
<!--                                </div>-->


                            </div>

                        </form>
                    </div>
                    <!--                <div class="mesages comp">-->
                    <!---->
                    <!--                    <div class="consultant">-->
                    <!--                        <div class="profile-edite">-->
                    <!--                            <div class="profil-photo">-->
                    <!--                                <input accept="image/*;capture=camera" style="display : none;" id="open_browse2" onchange="fileChange(this);" type="file">-->
                    <!--                                <a href="#" id="consultant-photo"><img src="" alt=""></a>-->
                    <!--                            </div>-->
                    <!--                        </div>-->
                    <!--                        <form class="forma-input ">-->
                    <!---->
                    <!--                            <div class="input-edit">-->
                    <!--                                <i class="icon-user168"></i>-->
                    <!--                                <div class="input-container">-->
                    <!--                                    <input class="text-input floating-label" name="consultant-first-name" type="text" value="">-->
                    <!--                                    <label for="consultant-first-name">First</label>-->
                    <!--                                </div>-->
                    <!--                            </div>-->
                    <!--                            <div class="input-edit">-->
                    <!--                                <i class="icon-user168"></i>-->
                    <!--                                <div class="input-container">-->
                    <!--                                    <input class="text-input floating-label" name="consultant-last-name" type="text" value="">-->
                    <!--                                    <label for="consultant-last-name">Last</label>-->
                    <!--                                </div>-->
                    <!--                            </div>-->
                    <!--                            <div class="input-edit">-->
                    <!--                                <i class="icon-telephone46"></i>-->
                    <!--                                <div class="input-container">-->
                    <!--                                    <input class="text-input floating-label" name="consultant-phone" type="text" value="">-->
                    <!--                                    <label for="consultant-phone">Phone</label>-->
                    <!--                                </div>-->
                    <!--                            </div>-->
                    <!--                            <div class="input-edit">-->
                    <!--                                <div class="input-container">-->
                    <!--                                    <input class="text-input floating-label" name="consultant-position" type="text" value="">-->
                    <!--                                    <label for="consultant-position">Position</label>-->
                    <!--                                </div>-->
                    <!--                            </div>-->
                    <!--                            <div class="radio-button">-->
                    <!--                                <p>Massages</p>-->
                    <!--                                <input id="radio-1" name="featured" checked="" type="radio"><label for="radio-1">Anyone</label>-->
                    <!--                            </div>-->
                    <!--                            <div class="radio-button">-->
                    <!--                                <input id="radio-2" name="featured" checked="" type="radio"><label for="radio-2">Everybody</label>-->
                    <!--                            </div>-->
                    <!--                            <button class="button" id="add-consultant">OK</button>-->
                    <!--                        </form>-->
                    <!--                        <p class="pass error-message" style="display:none;"><span class="text-danger"></span></p>-->
                    <!--                        <hr class="sline">-->
                    <!--                    </div>-->
                    <!--                    <ul class="consultants-list">-->
                    <!--                        <li>-->
                    <!--                            <a href="#"><img src="images/img-2.png" alt="#">Sam Clarintence <span>(consultant of bathrooms)</span></a><b>Give rights<i class="icon-black218 "></i></b>-->
                    <!--                        </li>-->
                    <!--                        <li>-->
                    <!--                            <a href="#"><img src="images/img-2.png" alt="#">Sam Clarintence <span>(consultant of bathrooms)</span></a><b>Give rights<i class="icon-black218 "></i></b>-->
                    <!--                        </li>-->
                    <!--                    </ul>-->
                    <!--                </div>-->
                    <div class="agent-text ">
                        <form class="forma-input">
                            <div class="input-container">
                                <input class="text-input floating-label " name="categories" type="text" value="">
                                <label for="categories"><?= Yii::t('account', 'order_category') ?></label>
                            </div>
                            <div class="textarea"><textarea name="message" id="#" cols="30" rows="10" placeholder="<?= Yii::t('account', 'signature') ?>..."></textarea></div>
                        </form>
                    </div>
                    <div class="profile-video">
                        <div class="clearfix"></div>
                        <div class="video">
                            <img class="video-img" src="/images/no-video.png" alt="" height="181" width="330">
                            <form class="forma-input">
                                <div class="input-container namb">
                                    <input class="text-input floating-label dirty" name="video-link" type="text" value="" disabled>
                                    <label for="video-link"><?= Yii::t('account', 'profile_video_link') ?></label>
                                </div>
                                <div class="input-container namb">
                                    <input class="text-input floating-label dirty" name="video-name" type="text" value="" disabled>
                                    <label for="video-name"><?= Yii::t('account', 'profile_video_name') ?></label>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
                <h4 class="promote promote-bottom"><?= Yii::t('account', 'edit_security') ?></h4>
                <form class="honours forma-input security security-info">
                    <div>
<!--                        <div class="input-edit">-->
<!--                            <i class="icon-symbol20"></i>-->
<!--                            <div class="input-container">-->
<!--                                <input class="text-input floating-label dirty" name="email"  type="text" value="">-->
<!--                                <label for="email">--><?//= Yii::t('account', 'edit_email') ?><!--</label>-->
<!--                            </div>-->
<!--                        </div>-->
                        <div class="input-edit">
                            <i class="icon-id16"></i>
                            <div class="input-container">
                                <input class="text-input floating-label dirty" name="login" type="text" value="">
                                <label for="login"><?= Yii::t('account', 'login') ?></label>
                            </div>

                        </div>
                        <div class="input-edit">
                            <i class="icon-locked59"></i>
                            <div class="input-container">
                                <input class="text-input floating-label" id="new_pass" name="password" type="text" value="">
                                <label for="password"><?= Yii::t('main', 'password') ?></label>
                            </div>

                        </div>
<!--                        <div class="input-edit">-->
<!--                            <i class="icon-locked59"></i>-->
<!--                            <div class="input-container">-->
<!--                                <input class="text-input floating-label" id="confirm_pass" name="confirm" type="text" value="">-->
<!--                                <label for="confirm">--><?//= Yii::t('account', 'edit_confirm') ?><!--</label>-->
<!--                            </div>-->
<!---->
<!--                        </div>-->
                        <p class="pass check-form-error-message" style="display: none;"><span class="text-danger"></span></p>
                        <p class="pass result-success" style="display: none;"><span class="text-success"></span></p>
                        <ul>
                            <li><a href="#" class="button" id="create-company">OK</a></li>
                        </ul>
                    </div>


                </form>
            </div>
        </div>
    </div>

<?php
$this->registerJsFile('js/bootstrap.min.js', ['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('agent_js/action_company.js', ['depends' => 'frontend\assets\AppAsset']);
$this->registerJsFile('js/common.js', ['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('scripts/main_profile.js', ['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('agent_js/action_agent.js', ['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('js/jquery_tokenizer.js', ['depends' => 'frontend\assets\AppAsset']);
//$this->registerJsFile('js/common.js', ['depends' => 'frontend\assets\AppAsset']);
