<?php

use yii\helpers\Url;
use yii\widgets\LinkPager;

?>

    <!--START CENTER-->
    <div class="col-lg-7 profile-content profile-agent company centralScroll">
        <div class="row">
            <div class="content">
                <?php if ($products) : ?>
                    <?php foreach ($products as $product) : ?>
                        <a href="<?= Url::to(['/about/product/', 'id' => $product['id_prod']]) ?>">
                            <div class="news"
                                 style="background-image: url(<?= Yii::getAlias('@products/' . $product['main_image']) ?>)">
                                <!--                        <div  src="" alt="">-->
                                <p><?= $product['product_name'] ?></p>
                            </div>
                        </a>
                    <?php endforeach; ?>
                <?php endif; ?>
                <div class="agent-profil"
                     style="background-image: url(/media/upload/<?= $manufacturer['background'] ?>);">

                    <div class="agents">
                        <p class="upload-portfolio download-portfolio edit">Edit
                            <a href="
                        <?= Yii::$app->urlManager->createUrl(['account/advertising-tools/edit']); ?>?id=<?= $manufacturer['id'] ?>"></a>
                            <!--                        <a href="#"></a>-->
                        </p>
                        <div class="company-description">
                            <img src="/media/upload/<?= $manufacturer['logo'] ?>" alt="#" class="logos">
                            <?php $company = explode("||", $manufacturer['company']); ?>
                            <h4><?= $company[0] ?> <span><?= $company[1] ?> </span><b>made
                                    in <?= $manufacturer['country'] ?></b></h4>
                            <a href="#"><i class="icon-black218"></i>Message</a>
                        </div>
                        <!--
                                                    <div class="company-description role">
                                                        <div class="descr">
                                                            <h4>Company name <span>brand agency</span><b>made in New Zealand</b></h4>
                                                        </div>
                                                        <a href="#"><i class="icon-black218"></i>Message</a>
                                                    </div>
                        -->
                        <div class="agent">
                            <img src="/media/upload/<?= $manufacturer['avatar'] ?>" alt="#" class="photo">
                            <h5 class="name"><?= $manufacturer['first_name'] . ' ' . $manufacturer['last_name'] ?></h5>
                            <p><?= $manufacturer['specialization'] ?></p>
                        </div>
                        <div class="description manufacturer-description">
                            <ul>
                                <li><a><i class="icon-follow1"></i>3859 Followers</a></li>
                                <li><a><i class="icon-home"></i><?= $manufacturer['city'] ?></a></li>
                                <li><a href="#"><i class="icon-follow"></i>3859 Following</a></li>
                                <li><a href="#"><i class="icon-location"></i><?= $manufacturer['address'] ?></a></li>
                                <li><a href="#"><i class="icon-telephone46"></i><?= $manufacturer['phone'] ?></a></li>
                                <li><a href="#"><i class="icon-webpage2"></i><?= $manufacturer['link_user_site'] ?></a>
                                </li>
                            </ul>
                        </div>
                        <!--                    <div class="view-buttons">-->
                        <!--                        <i class="icon-another_eye"></i>-->
                        <!--                        <a href="-->
                        <? //= Yii::$app->urlManager->createUrl(['manufacturers/profile', 'id' => $manufacturer['id']]) ?><!--" class="view-button">View my profile as others see it</a>-->
                        <!--                    </div>-->
                        <ul class="soc no-active">
                            <li><i
                                    class="icon-facebook55"></i></li>
                            <li><i class="icon-twitter1"></i>
                            </li>
                            <li><i class="icon-instagram12"></i>
                            </li>
                            <li><i class="icon-pinterest3"></i>
                            </li>
                            <li><i class="icon-logotype1"></i>
                            </li>
                            <li><i class="icon-id16"></i></li>
                            <li><i class="icon-blogger8"></i>
                            </li>
                        </ul>
                    </div>
                    <div class="agent-text ">
                        <ul class="categories">
                            <li><a href="#">Categories:</a></li>
                            <?php if ($categories) : ?>
                                <?php $collections = explode(',', $categories['collection']) ?>
                                <?php while ($categories) : ?>
                                    <?php $collection = array_shift($categories); ?>
                                    <li><a href="#"><?= $collection ?></a></li>
                                <?php endwhile; ?>
                            <?php endif; ?>
                        </ul>
                        <p><?= $manufacturer['message'] ?></p>
                    </div>
                    <div class="mesages comp">


                        <form class="forma-input">
                            <div class="search">
                                <div class="input-edit">
                                    <div class="input-container">
                                        <input class="text-input floating-label" name="sample1" type="text">
                                        <label for="sample1">Search people</label>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <ul>
                            <li>
                                <a href="#"><img src="/images/img-2.png" alt="#">Sam Clarintence <span>(consultant of bathrooms)</span></a><b>Give
                                    rights<i class="icon-black218 "></i></b></li>
                            <li>
                                <a href="#"><img src="/images/img-2.png" alt="#">Sam Clarintence <span>(consultant of bathrooms)</span></a><b>Give
                                    rights<i class="icon-black218 "></i></b></li>
                            <li>
                                <a href="#"><img src="/images/img-2.png" alt="#">Sam Clarintence <span>(consultant of bathrooms)</span></a><b>Give
                                    rights<i class="icon-black218 "></i></b>
                            </li>
                        </ul>
                    </div>
                    <div class="video">
                        <iframe width="466px" height="260" src="https://www.youtube.com/embed/LaP1H-JJroU"
                                frameborder="0"
                                allowfullscreen></iframe>
                    </div>
                </div>
                <div class="showrooms">
                    <h4 class="promote promote-bottom">Showrooms</h4>
                    <p class="upload-portfolio download-portfolio edit">Edit
                        <a href="#"></a>
                    </p>
                    <div class="clearfix"></div>

                    <div class="selects">
                        <i class="icon-home "></i>
                        <select name="#" id="#">
                            <option value="#">Town</option>
                            <option value="#">Town</option>
                            <option value="#">Town</option>
                            <option value="#">Town</option>
                        </select>
                    </div>
                    <div class="hashtag">
                        <a href="#">#lorem</a>
                        <a href="#"> #ipsum</a>
                        <a href="#"> #dolor</a>
                        <a href="#"> #sit</a>
                        <a href="#"> #consectetuer</a>
                        <a href="#"> #adipiscing</a>
                    </div>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris aliquet lacus lectus, a mollis
                        nunc
                        interdum eu. Proin vitae maximus elit, non molestie est. Aliquam at tortor aliquam, tincidunt
                        sem
                        sit amet, suscipit erat. Integer sed dapibus metus. Nullam fringilla velit ut porttitor...</p>
                    <div class="catalog-box">
                        <div class="item-box">
                            <div class="catalog-item">
                                <div class="catalog-img-box">
                                    <a href="#">
                                        <img src="/images/img-10.png" alt="">
                                    </a>
                                </div>
                                <div class="catalog-caption">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris aliquet lacus
                                        lectus,
                                        a
                                        mollis nunc interdum eu. Proin vitae maximus elit, non molestie est...</p>
                                </div>
                                <p class="town">London</p>
                            </div>
                            <div class="catalog-item">
                                <div class="catalog-img-box">
                                    <a href="#">
                                        <img src="/images/img-10.png" alt="">
                                    </a>
                                </div>
                                <div class="catalog-caption">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris aliquet lacus
                                        lectus,
                                        a
                                        mollis nunc interdum eu. Proin vitae maximus elit, non molestie est...</p>
                                </div>
                                <p class="town">Dublin</p>
                            </div>
                        </div>
                    </div>
                </div>

                <hr class="sline">
                <div class="catalog-box special-offers-catalog page-in-catalog">
                    <div class="clearfix"></div>
                    <h4 class="promote promote-bottom">Product</h4>
                    <p class="upload-portfolio download-portfolio join" data-toggle="modal"
                       data-target=".agent-modal-product-new">Add<a href="#"></a>
                    </p>
                    <div class="clearfix"></div>
                    <?php if ($products_list) : ?>
                    <div class="item-box">
                        <?php foreach ($products_list as $product) : ?>
                            <?php if ($product != null) : ?>
                                <div class="catalog-item">
                                    <div class="catalog-img-box">
                                        <a href="/about/product/<?= $product['id_prod'] ?>">
                                            <img src="/media/products/<?= $product['main_image'] ?>" alt="">
                                        </a>
                                        <?php if ($product['stock'] == 1) : ?>
                                            <div class="limited-offer">
                                                <p><b>Limited offer</b>
                                                    <br>save <?= $product['discount'] ?>%</p>
                                            </div>
                                            <div class="like-calc item-price">
                                                <i class="icon-label49"></i>
                                                <span><?= ($product['discount'] != 0) ? round($product['price'] - ((int)$product['discount'] / 100) * $product['price']) : $product['price'] ?>
                                                    $</span>
                                                <br>
                                                <p class="last-price"><?= $product['price'] ?>$</p>
                                            </div>
                                        <?php else : ?>
                                            <div class="limited-offer">

                                            </div>
                                            <div class="like-calc item-price">
                                                <i class="icon-label49"></i>
                                                <span><?= $product['price'] ?>$</span>
                                                <br>

                                            </div>
                                        <?php endif; ?>
                                    </div>
                                    <div class="catalog-caption">
                                        <?= $product['product_name'] ?>
                                    </div>
                                </div>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </div>
                </div>
                <div class="bread-crumbs">
                    <?= LinkPager::widget(['pagination' => $pagination_product,
                        'disabledPageCssClass' => false,
                        'nextPageLabel' => '',
                        'prevPageLabel' => '',
                        'options' => ['class' => 'hvr-radial-out products-pagination'],
                    ]); ?>
                </div>
                <?php else : ?>
            </div>
            <?php endif; ?>
            <hr class="sline">
            <div class="clearfix"></div>
            <h4 class="promote promote-bottom">Project catalog</h4>
            <p class="upload-portfolio download-portfolio join">Add<a href="#"></a></p>
            <div class="folder">
                <div class="name-folder ">
                    <div class="folder-img">
                        <img src="/images/img-9.png" alt="">
                        <img src="/images/img-9.png" alt="">
                        <img src="/images/img-9.png" alt="">
                        <img src="/images/img-9.png" alt="">
                        <p>Name folder</p>
                        <i class="icon-show8"></i>
                        <ul>
                            <li><a href="#"><i class="icon-edit45"></i>Rename</a></li>
                            <li><a href="#"><i class="icon-rubbish"></i>Delete</a></li>
                        </ul>
                    </div>
                </div>
                <div class="name-folder ">
                    <div class="folder-img">
                        <img src="/images/img-9.png" alt="">
                        <img src="/images/img-9.png" alt="">
                        <img src="/images/img-9.png" alt="">
                        <img src="/images/img-9.png" alt="">
                        <p>Name folder</p>
                        <i class="icon-show8"></i>
                        <ul>
                            <li><a href="#"><i class="icon-edit45"></i>Rename</a></li>
                            <li><a href="#"><i class="icon-rubbish"></i>Delete</a></li>
                        </ul>
                    </div>
                </div>
                <div class="name-folder ">
                    <div class="folder-img">
                        <img src="/images/img-9.png" alt="">
                        <img src="/images/img-9.png" alt="">
                        <img src="/images/img-9.png" alt="">
                        <img src="/images/img-9.png" alt="">
                        <p>Name folder</p>
                        <i class="icon-show8"></i>
                        <ul>
                            <li><a href="#"><i class="icon-edit45"></i>Rename</a></li>
                            <li><a href="#"><i class="icon-rubbish"></i>Delete</a></li>
                        </ul>
                    </div>
                </div>
                <div class="name-folder ">
                    <div class="folder-img">
                        <img src="/images/img-9.png" alt="">
                        <img src="/images/img-9.png" alt="">
                        <img src="/images/img-9.png" alt="">
                        <img src="/images/img-9.png" alt="">
                        <p>Name folder</p>
                        <i class="icon-show8"></i>
                        <ul>
                            <li><a href="#"><i class="icon-edit45"></i>Rename</a></li>
                            <li><a href="#"><i class="icon-rubbish"></i>Delete</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <!--END CENTER-->

<?php
$this->registerJsFile('js/jquery_tokenizer.js', ['depends' => 'frontend\assets\AppAsset']);
$this->registerJsFile('agent_js/action_agent.js', ['depends' => 'frontend\assets\AppAsset']);
?>