<?php


use yii\widgets\LinkPager;
use yii\helpers\Url;

$this->title = Yii::t('titles', 'account') . Yii::t('titles', 'account_profile');
$this->params['breadcrumbs'][] = $this->title;
?>
    <!--START CENTER-->
    <div
        class="col-lg-7 profile-content profile-agent central-content centralScroll adt profile-agent-comps agents archdes">
        <div class="row">
            <div class="content">
                <div class="analytics">
                    <h4 class="promote promote-bottom"><?= Yii::t('agent', 'analytics') ?></h4>
                    <p class="last-days"><?= Yii::t('agent', 'last_30_days') ?></p>
                    <div class="clearfix"></div>
                    <div class="graphic">
                        <p><?= Yii::t('agent', 'people') ?>,
                            <br><?= Yii::t('agent', 'who_have_visited_profile') ?></p>
                        <aside class="chart vert">
                            <canvas id="graphic" width="150" height="150" data-values="<?= $user_stat_to_string ?>">
                                This browser does not support HTML5 Canvas.
                            </canvas>
                        </aside>
                    </div>
                    <ul>
                        <li class="Guests"><i></i><?= Yii::t('agent', 'guests') ?>
                            <span><?= $user_stat_res['per_month']['guest'] ?></span></li>
                        <li class="Agent"><i></i><?= Yii::t('agent', 'agent') ?>
                            <span><?= $user_stat_res['per_month']['agent'] ?></span></li>
                        <li class="Homeowner">
                            <i></i><?= Yii::t('agent', 'homeowner') ?>
                            <span><?= $user_stat_res['per_month']['homeowner'] ?></span></li>
                        <li class="Manufacturer">
                            <i></i><?= Yii::t('agent', 'manufacturer') ?>
                            <span><?= $user_stat_res['per_month']['manufacturer'] ?></span></li>
                        <li class="Designers">
                            <i></i><?= Yii::t('agent', 'designers') ?>
                            <span><?= $user_stat_res['per_month']['designer'] ?></span></li>
                        <li class="Pros"><i></i>Pros<span><?= $user_stat_res['per_month']['professional'] ?></span></li>
                    </ul>
                    <a href="#" class="button"><?= Yii::t('agent', 'load_more_information') ?></a>
                </div>
                <div class="more-info">
                    <h4 align="center"><?= Yii::t('agent', 'block_in_development') ?></h4>
                    <div class="graphics">
                        <p><?= Yii::t('agent', 'previous') ?>
                            <br><?= Yii::t('agent', 'visited_page') ?></p>
                        <aside class="chart vert">
                            <canvas id="graphic-1" width="105" height="105" data-values="30, 30, 20, 60">
                                This browser does not support HTML5 Canvas.
                            </canvas>
                        </aside>
                        <ul>
                            <li class="project"><i
                                    style="background-color: #cbe4e4;"></i><?= Yii::t('agent', 'project') ?></li>
                            <li class="manufacturer"><i
                                    style="background-color: #cbe4e4;"></i><?= Yii::t('agent', 'manufacturer') ?></li>
                            <li class="pros"><i style="background-color: #cbe4e4;"></i><?= Yii::t('agent', 'pros') ?>
                            </li>
                            <li class="other"><i style="background-color: #cbe4e4;"></i><?= Yii::t('agent', 'other') ?>
                            </li>
                        </ul>
                    </div>
                    <div class="graphics">
                        <p><?= Yii::t('agent', 'next') ?>
                            <br> <?= Yii::t('agent', 'visited_page') ?></p>
                        <aside class="chart vert">
                            <canvas id="graphic-2" width="105" height="105" data-values="30, 30, 20, 60">
                                This browser does not support HTML5 Canvas.
                            </canvas>
                        </aside>
                        <ul>
                            <li class="project"><i
                                    style="background-color: #cbe4e4;"></i><?= Yii::t('agent', '1project') ?></li>
                            <li class="manufacturer"><i
                                    style="background-color: #cbe4e4;"></i><?= Yii::t('agent', 'manufacturer') ?></li>
                            <li class="pros"><i style="background-color: #cbe4e4;"></i><?= Yii::t('agent', 'pros') ?>
                            </li>
                            <li class="other"><i style="background-color: #cbe4e4;"></i><?= Yii::t('agent', 'other') ?>
                            </li>
                        </ul>
                    </div>
                    <div class="graphics-bottom">
                        <p><?= Yii::t('agent', 'avarage_visit') ?></p>
                        <div class="graphics">
                            <aside class="chart vert">
                                <canvas id="graphic-3" width="105" height="105" data-values="30, 30, 20, 60">
                                    This browser does not support HTML5 Canvas.
                                </canvas>
                            </aside>
                            <ul>
                                <li class="project"><i style="background-color: #cbe4e4;"></i>Project</li>
                                <li class="manufacturer"><i style="background-color: #cbe4e4;"></i>Manufacturers</li>
                                <li class="pros"><i style="background-color: #cbe4e4;"></i>Pros</li>
                                <li class="other"><i style="background-color: #cbe4e4;"></i>Other</li>
                            </ul>
                        </div>
                        <div class="graphics">

                            <aside class="chart vert">
                                <canvas id="graphic-4" width="105" height="105" data-values="30, 30, 20, 60">
                                    This browser does not support HTML5 Canvas.
                                </canvas>
                            </aside>
                            <ul>
                                <li class="project"><i style="background-color: #cbe4e4;"></i>Project</li>
                                <li class="manufacturer"><i style="background-color: #cbe4e4;"></i>Manufacturers</li>
                                <li class="pros"><i style="background-color: #cbe4e4;"> </i>Pros</li>
                                <li class="other"><i style="background-color: #cbe4e4;"></i>Other</li>
                            </ul>
                        </div>

                    </div>
                    <h5>WE DEVELOP STATISTIC DISPLAY AND CONNECT OT SOON</h5>
                </div>
                <hr class="sline">
                <div class="clearfix"></div>
                <div class="profile-agent-comps">
                    <div class="companys">
                        <h4 class="promote promote-bottom"><?= Yii::t('agent', 'available_manufacturers_pages') ?></h4>
                        <p class="upload-portfolio download-portfolio join"><?= Yii::t('agent', 'add') ?>
                            <a href="<?= Yii::$app->urlManager->createUrl(['account/advertising-tools/create']); ?>"
                               id="main-link-to-create-company"></a>
                        </p>
                        <div class="clearfix"></div>
                        <?php if ($companies) : ?>
                            <div class="companies-by-agent">
                                <?php while ($companies) :
                                    $company = array_shift($companies);
                                    $company_attr = explode("||", $company['company']); ?>
                                    <div class="company"
                                         style="background: url('/media/upload/<?= $company['background'] ?>')">
                                        <a href="<?= Yii::$app->urlManager->createUrl(['account/advertising-tools/manufacturer']) . '?id=' . $company['id'] ?>">
                                            <img class="company-img"
                                                 src="<?= '/media/upload/' . $company['logo'] ?>" alt="#">
                                        </a>
                                        <div class="name-company">
                                            <h5><?= $company_attr[0] ?> <span>(<?= $company_attr[1] ?>)</span></h5>
                                            <p>made in <span><?= $company['country'] ?></span></p>
                                        </div>
                                        
                                        <ul class="description">
                                            <li><?= Yii::t('agent', 'products') ?>:
                                                <span><?= $company['products_count'] ?></span></li>
                                            <li><?= Yii::t('agent', 'new') ?>:
                                                <span><?= $company['new_products'] ?></span></li>
                                            <li><?= Yii::t('agent', 'special_offers') ?>:
                                                <span><?= $company['so_count'] ?></span></li>
                                            <li>Sale:
                                                <span><?= $company['discounts']['discount'] ? $company['discounts']['discount'] : 0 ?>
                                                    %</span></li>
                                        </ul>
                                        <ul class="categories">
                                            <li><a href="#"><?= Yii::t('agent', 'categories') ?>:</a></li>
                                            <?php if ($company['collections']) : ?>
                                                <?php $collections = explode(',', $company['collections']['collection']) ?>
                                                <?php while ($collections) :
                                                    $collection = array_shift($collections); ?>
                                                    <li>
                                                        <a href="#"><?= $collection ?></a>
                                                    </li>
                                                <?php endwhile; ?>
                                            <?php endif; ?>
                                        </ul>
                                        <div class="author">
                                            <img src="<?= '/media/upload/' . $company['avatar'] ?>" alt="#">
                                            <p class="name"><?= $company['first_name'] . ' ' . $company['last_name'] ?></p>
                                        </div>
                                    </div>
                                <?php endwhile; ?>
                            </div>
                        <?php endif; ?>
                        <div class="bread-crumbs">
                            <?= LinkPager::widget(['pagination' => $pagination,
                                'disabledPageCssClass' => false,
                                'nextPageLabel' => '',
                                'prevPageLabel' => '',
                                'options' => ['class' => 'hvr-radial-out1, agent-comp-pagination'],
                            ]); ?>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <hr class="sline">
                    <div class="companys">
                        <h4 class="promote promote-bottom"><?= Yii::t('agent', 'manufacturers_special_offers') ?></h4>
                        <p class="upload-portfolio download-portfolio join"><?= Yii::t('agent', 'add') ?>
                            <a href="#"></a>
                        </p>
                        <div class="clearfix"></div>
                        <div>
                            <div class="company">
                                <img class="company-img" src="/images/company.png" alt="#">
                                <div class="name-company">
                                    <h5>TINY <span>(Brand studio)</span></h5>
                                    <p>made in <span>New Zealand</span></p>
                                </div>
                               
                                

                                 <ul class="categories">
                                    <li><a href="#">Categories:</a></li>
                                    <li><a href="#">Furniture</a></li>
                                    <li><a href="#">Furniture</a></li>
                                    <li><a href="#">Furniture</a></li>
                                    <li><a href="#">Furniture</a></li>
                                    <li><a href="#">Furniture</a></li>
                                </ul>
                                <ul class="description">
                                    <li>Products: <span>56</span></li>
                                    <li>New: <span>26</span></li>
                                    <li>Special offers: <span>16</span></li>
                                    <li>Sale: <span>20%</span></li>
                                </ul>
                                <div class="author">
                                    <img src="/images/img-2.png" alt="#">
                                    <p class="name">Sam Clarintence</p>
                                </div>
                            </div>


                            <div class="company" style="background-image: url(/images/bg-company-2.png);">
                                <img class="company-img" src="/images/company.png" alt="#">
                                <div class="name-company">
                                    <h5>TINY <span>(Brand studio)</span></h5>
                                    <p>made in <span>New Zealand</span></p>
                                </div>

                                <ul class="categories">
                                    <li><a href="#">Categories:</a></li>
                                    <li><a href="#">Furniture</a></li>
                                    <li><a href="#">Furniture</a></li>
                                    <li><a href="#">Furniture</a></li>
                                    <li><a href="#">Furniture</a></li>
                                    <li><a href="#">Furniture</a></li>
                                </ul>
                                <ul class="description">
                                    <li>Products: <span>56</span></li>
                                    <li>New: <span>26</span></li>
                                    <li>Special offers: <span>16</span></li>
                                    <li>Sale: <span>20%</span></li>
                                </ul>
                                <div class="author">
                                    <img src="/images/img-2.png" alt="#">
                                    <p class="name">Sam Clarintence</p>
                                </div>
                                
                            </div>
                        </div>
                        <div class="bread-crumbs">
                            <ul class="hvr-radial-out">
                                <li>
                                    <a href="#"></a>
                                </li>
                                <li class="active"><a href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li><a href="#">5</a></li>
                                <li><a href="#">6</a></li>
                                <li><a href="#">7</a></li>
                                <li>
                                    <a href="#"></a>
                                </li>
                            </ul>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <hr class="sline">
                <?= frontend\widgets\PGroups::widget(); ?>
                <hr class="sline">
                <div class="mesages ">
                    <h4 class="promote promote-bottom"><?= Yii::t('agent', 'architectors_designers') ?></h4>
                    <p id="more-designers" class="upload-portfolio download-portfolio join">Add more
                        <a href="#"></a>
                    </p>

                    <div class="clearfix"></div>
                    <?php if ($payment_users): ?>
                        <?php $paid_status = ['0' => 'Default', '1' => '2B', '2' => '3B']; ?>
                        <ul>
                            <li>
                                <a href="#" class="button"><?= Yii::t('agent', 'pay_for_tariff') ?></a>
                            </li>
                            <?php $count = 0; ?>
                            <?php foreach ($payment_users as $designer): ?>
                                <li <?= ($count > 1) ? "class='hide no-active'" : "" ?>>
                                    <div class="checkbox">
                                        <p>
                                            <input name="check<?= $count; ?>" type="checkbox" checked>
                                            <label for="check<?= $count; ?>"></label>
                                        </p>
                                    </div>
                                    <a href="<?= Yii::$app->urlManager->createUrl(['about/user', 'id' => $designer['id']]) ?>"><img
                                            src="<?= Yii::getAlias('@avatar/' . $designer['avatar']) ?>"
                                            alt="#"><?= $designer['first_name'] . ' ' . $designer['last_name'] ?>
                                        <span>(designer)</span></a><b><i
                                            class="icon-ascending24 "><?= $paid_status[$designer['paid_status']] ?></i></b>
                                </li>
                                <?php $count++; ?>
                            <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>
                </div>

                <hr class="sline">
                <div class="catalog-box special-offers-catalog page-in-catalog">
                    <div class="clearfix"></div>
                    <h4 class="promote promote-bottom"><?= Yii::t('agent', 'my_special_offers') ?></h4>
                    <!--                    <p class="upload-portfolio download-portfolio join" data-toggle="modal"-->
                    <!--                       data-target=".agent-modal-product">--><?//= Yii::t('agent', 'add') ?>
                    <!--                        <a href="#"></a>-->
                    <!--                    </p>-->

                    <div class="clearfix"></div>
                    <div class="products-area">
                        <?php foreach ($special_offers as $special_offer) : ?>
                            <div class="catalog-item">
                                <div class="catalog-img-box">
                                    <a href="<?= Url::to(['/about/product/', 'id' => $special_offer['id_prod']]) ?>">
                                        <img src="<?= Yii::getAlias('@products/' . $special_offer['main_image']) ?>"
                                             alt="">
                                    </a>
                                    <div class="limited-offer">
                                        <?php if ($special_offer['discount'] != 0): ?>
                                            <p><b><?= Yii::t('agent', 'limited_offer') ?></b>
                                                <br><?= Yii::t('agent', 'save') ?> <?= $special_offer['discount'] ?>%
                                            </p>
                                        <?php else : ?>
                                            <p><b><?= Yii::t('agent', 'limited_offer') ?></b></p>
                                        <?php endif; ?>
                                    </div>
                                    <div class="like-calc item-price">
                                        <?php if ($special_offer['discount'] != 0): ?>
                                            <i class="icon-label49"></i>
                                            <span><?= round($special_offer['price'] - (((int)$special_offer['discount'] / 100) * $special_offer['price'])) ?>
                                                $</span>
                                            <br>
                                            <p class="last-price"><?= $special_offer['price'] ?>$</p>
                                        <?php else : ?>
                                            <i class="icon-label49"></i>
                                            <span><?= $special_offer['price'] ?>$</span>
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <div class="catalog-caption">
                                    <p><?= $special_offer['product_name'] ?></p>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>

                </div>
                <div class="bread-crumbs">
                    <?= LinkPager::widget(['pagination' => $pagination_product,
                        'disabledPageCssClass' => false,
                        'nextPageLabel' => '',
                        'prevPageLabel' => '',
                        'options' => ['class' => 'hvr-radial-out1, products-pagination'],
                    ]); ?>
                </div>
                <?= frontend\widgets\Banner::widget(['position' => 'bottom']); ?>
            </div>
        </div>
    </div>
    <!--END CENTER-->

<?php
$this->registerJsFile('agent_js/action_agent.js', ['depends' => 'frontend\assets\AppAsset']);
$script_honours = <<< JS
    $('#more-designers a').click(function(e){
        e.preventDefault();
		$('.mesages .no-active').removeClass('hide');
	});
	
    $('#all-projects-view').change(function(){
        var all_view = $(this).val();
        var request = $.ajax({
			data : {all_view : all_view},
			url: '/account/projects-agent/viewed',
			type: 'post',
			dataType: 'html'
		});

		request.done(function(){
		    location.reload();
		});

    });

    $('body').delegate( ".agent-all-projects-pagination a", "click", function(e) {
          e.preventDefault();
          var link = $(this).attr('href');
          $('#agent-all-projects').load(link+' #agent-all-projects > *');
          $('.agent-all-projects-pagination').load(link+' .agent-all-projects-pagination > *');
    });

    var pieColors = [
		'rgb(51, 102, 102)',
		'rgb(40, 124, 122)',
		'rgb(51, 153, 153)',
		'rgb(84, 175, 172)',
		'rgb(153, 204, 204)',
		'rgb(203, 228, 228)'
	];
    function getTotal( arr ){
    var j,
        myTotal = 0;

    for( j = 0; j < arr.length; j++) {
        myTotal += ( typeof arr[j] === 'number' ) ? arr[j] : 0;
    }

    return myTotal;
}

    function drawPieChart( canvasId ) {
        var i,
            canvas = document.getElementById( canvasId ),
            pieData = canvas.dataset.values.split(',').map( function(x){ return parseInt( x, 10 )}),
            halfWidth = canvas.width * .5,
            halfHeight = canvas.height * .5,
            ctx = canvas.getContext( '2d' ),
            lastend = 0,
            myTotal = getTotal(pieData);

        ctx.clearRect( 0, 0, canvas.width, canvas.height );

        for( i = 0; i < pieData.length; i++) {
            ctx.fillStyle = pieColors[i];
            ctx.beginPath();
            ctx.moveTo( halfWidth, halfHeight );
            ctx.arc( halfWidth, halfHeight, halfHeight, lastend, lastend + ( Math.PI * 2 * ( pieData[i] / myTotal )), false );
            ctx.lineTo( halfWidth, halfHeight );
            ctx.fill();
            lastend += Math.PI * 2 * ( pieData[i] / myTotal );
        }
    }

    drawPieChart('graphic');
    drawPieChart('graphic-1');
    drawPieChart('graphic-2');
    drawPieChart('graphic-3');
    drawPieChart('graphic-4');

	$(document).ready(function(){
		$('.analytics .button').click(function(){
			$('.more-info').slideToggle();
		});
	});
JS;
$this->registerJs($script_honours, yii\web\View::POS_READY);
$this->registerJsFile('js/owl.carousel.min.js', ['depends' => 'frontend\assets\AppAsset']);
$this->registerJsFile('agent_js/action_company.js', ['depends' => 'frontend\assets\AppAsset']);





