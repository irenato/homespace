<?php

use yii\widgets\LinkPager;

$this->title = Yii::t('titles', 'account').Yii::t('titles', 'account_profile');
$this->params['breadcrumbs'][] = $this->title;

?>

<!--START CENTER-->
			<div class="col-lg-7 profile-content profile-conten-me central-content user-profile-edited centralScroll">
				<div class="content">
					<!--profile top-->
					<div class="profil pf-new" style="background-image: url('<?= Yii::getAlias('@background/'.$user_information['background']) ?>')">
						<p class="upload-portfolio download-portfolio edit"><?= Yii::t('account', 'edit') ?><a href="<?= Yii::$app->urlManager->createUrl(['account/edit']) ?>"></a></p>
						<div class="profile-info">
							<div class="left-side-prof central-prof">
								<div class="profil-photo photo-like">
									<img src="<?= Yii::getAlias('@avatar/'.$user_information['avatar']) ?>" alt="">
									<i class="icon-heart297 icon-heart2971"><p><?= $user_information['likes'] ?></p></i>
								</div>
								<div class="pr-name">
									<p class="pr-heading"><?= $user_information['first_name'] ?> <?= $user_information['last_name'] ?></p>
									<p><?= $user_information['specialization'] ?></p>

								</div>
							</div>
							<div class="left-side-prof">
								<div class="pr-name pr-follows">
									<span class="follow"><i class="icon-follow1"></i><?= $followers ?> <?= Yii::t('account', 'profile_followers') ?></span>
									<span class="follow"><i class="icon-follow"></i><?= $following ?> <?= Yii::t('account', 'Following') ?></span>
									<span class="follow"><i class="icon-telephone46"></i><?= $user_information['city'] ?></span>
									<span class="follow"><i class="icon-home"></i><?= $user_information['city'] ?></span>
									<span class="follow"><i class="icon-webpage2"></i><a href="//<?= $user_information['link_user_site'] ?>"><?= $user_information['link_user_site'] ?></a></span>
									<span class="follow"><i class="icon-ascending24"></i><?= $tariff ?></span>
									<a href="#" data-toggle="modal" data-target="#modal-pay" class="ch-tariff"><?= Yii::t('account', 'edit_pay_tariff') ?></a>
								</div>
							</div>
							<div class="right-side-prof">
								<div class="quote">
									<p><?= $user_information['message'] ?></p>
								</div>
								<div class="un-prof-line clearfix">
									<div class="soc-profile">
										<ul>
											<?php if($user_information['link_fb'] != ""){?>
												<li><a href="//<?= $user_information['link_fb'] ?>" target="_blank" ><i class="icon-facebook55"></i></a></li>
											<?php } ?>
											<?php if($user_information['link_tw'] != ""){?>
												<li><a href="//<?= $user_information['link_tw'] ?>" target="_blank" ><i class="icon-twitter1"></i></a></li>
											<?php } ?>
											<?php if($user_information['link_inst'] != ""){?>
												<li><a href="//<?= $user_information['link_inst'] ?>" target="_blank" ><i class="icon-instagram12"></i></a></li>
											<?php } ?>
											<?php if($user_information['link_behance'] != ""){?>
												<li><a href="//<?= $user_information['link_behance'] ?>" target="_blank" ><i class="icon-behance2"></i></a></li>
											<?php } ?>
											<?php if($user_information['link_google'] != ""){?>
												<li><a href="//<?= $user_information['link_google'] ?>" target="_blank" ><i class="icon-google116"></i></a></li>
											<?php } ?>
											<?php if($user_information['link_pint'] != ""){?>
												<li><a href="//<?= $user_information['link_pint'] ?>" target="_blank" ><i class="icon-pinterest3"></i></a></li>
											<?php } ?>
											<?php if($user_information['link_tumblr'] != ""){?>
												<li><a href="//<?= $user_information['link_tumblr'] ?>" target="_blank" ><i class="icon-logotype1"></i></a></li>
											<?php } ?>
											<?php if($user_information['link_linkedin'] != ""){?>
												<li><a href="//<?= $user_information['link_linkedin'] ?>" target="_blank" ><i class="icon-id16"></i></a></li>
											<?php } ?>
											<?php if($user_information['link_blg'] != ""){?>
												<li><a href="//<?= $user_information['link_blg'] ?>" target="_blank" ><i class="icon-blogger8"></i></a></li>
											<?php } ?>
										</ul>
									</div>
									<div class="view-buttons">
										<i class="icon-another_eye"></i>
										<a href="<?= Yii::$app->urlManager->createUrl(['about/user', 'id' => $user_information['id']]) ?>" class="view-button"><?= Yii::t('account', 'profile_view_profile') ?></a>
									</div>
								</div>

							</div>
						</div>
						<div class="clearfix"></div>
						<?php if($user_information['company'] != ""):?>
							<div class="members-comp">
								<div class="prof-comp-logo">
									<img src="<?= Yii::getAlias('@avatar/'.$user_information['logo']) ?>" alt="">
								</div>
								<div class="members-list">
									<p><?= $user_information['company'] ?></p>
									<?php $workers = $user_information['employees']; ?>
									<?php $workers = explode(',',$user_information['employees']); ?>
									<ul>
										<?php foreach($workers as $worker): ?>
											<?php if($worker != ""):?>
												<li><a href="#"><?= $worker ?></a></li>
											<?php endif; ?>
										<?php endforeach; ?>
									</ul>
								</div>
							</div>
						<?php endif; ?>
					</div>

					<!--END profile top-->


				<!-- Honours -->
				<?php if($honours){?>
					<hr class="sline">

					<div class="portfolio-slider profile-honours-slider">
						<h4 class="promote promote-bottom"><?= Yii::t('account', 'profile_honour') ?></h4>
						<p class="upload-portfolio download-portfolio edit"><?= Yii::t('account', 'edit') ?><a href="<?= Yii::$app->urlManager->createUrl(['account/edit']) ?>"></a></p>
							<div id="owl-demo-2" class="honours-slider">
								<?php foreach ($honours as $honour) {?>
								  <div class="item">
								  	<a href="<?= $honour['image'] ?>" ><img src="<?= $honour['image'] ?>" alt=""></a>
								  	<p><?= $honour['name'] ?></p>
								  </div>
								<?php } ?>
							</div>
						<br>
					</div>

				<?php } ?>
				<!-- End Honours -->


				<!-- Licenses -->

				<?php if($licenses){?>
						<div class="licenses">
	        	            <h4 class="promote promote-bottom"><?= Yii::t('account', 'profile_license') ?></h4>
	        	            <p class="upload-portfolio download-portfolio edit"><?= Yii::t('account', 'edit') ?><a href="<?= Yii::$app->urlManager->createUrl(['account/edit']) ?>"></a></p>
	        	            	<div class="cliearfix"></div>
								<?php foreach ($licenses as $license) {?>
	        	            	    <div class="licenses-item">
	        	            	    	<?php $images = explode('|', $license['images']); ?>

            	            	        <div class="licenses-imgs lb-container">
	        	            	            <?php foreach ($images as $image) {?>
	        	            	            	<?php if($image != ""){?>
            	            	                    <a href="<?= $image ?>"><img src="<?= $image ?>" alt=""></a>
	        	            	            	<?php } ?>
	        	            	            <?php } ?>
	        	            	        </div>
	        	            	        <div class="licenses-content">
	        	            	            <h5><b><?= Yii::t('account', 'profile_license_number') ?></b><?= $license['number'] ?></h5>
	        	            	            <p><b><?= Yii::t('account', 'profile_license_issued') ?></b><?= $license['issued_by'] ?></p>
	        	            	        </div>
	        	            	    </div>
	        	 				<?php } ?>
	        	        </div>

					<hr class="sline">

				<?php } ?>
				<!-- End Licenses -->

				<!-- Portfolio -->

				<?php if($portfolio){?>
					<div class="portfolio-top">
						<h4 class="promote promote-bottom"><?= Yii::t('account', 'my_items') ?></h4>
						<p class="upload-portfolio download-portfolio edit"><?= Yii::t('account', 'edit') ?><a href="<?= Yii::$app->urlManager->createUrl(['account/portfolio']) ?>"></a></p>
						<div class="clearfix"></div>
						<div class="img-item" id="current-item" >
							<a href="<?= $portfolio[0]['url'] ?>"><img style="height: 300px;" src="<?= $portfolio[0]['image'] ?>" alt="<?= $portfolio[0]['title'] ?>"></a>
							<p><?= $portfolio[0]['title'] ?></p>
						</div>
						<h5><i class="icon-1"></i><?= Yii::t('account', 'about') ?></h5>
						<p class="description-item" id="current-item-description"><?= $portfolio[0]['short_description'] ?></p>
					</div>
					<?php foreach ($portfolio as $item) {?>
						<div class="profil-img portfolio-items" style="cursor : pointer; height:150px;" data-link="<?= $item['url'] ?>" data-description="<?= $item['short_description'] ?>">
							<img style="height: 150px;" src="<?= $item['image'] ?>" alt="<?= $item['title'] ?>">
						</div>
					<?php } ?>

					<hr class="sline">

				<?php } ?>

				<!-- End Portfolio -->


				<!-- Reviews -->
				<?php if($reviews['reviews']){?>
					<div class="reviews">
						<h4 class="promote" id="insert-after"><?= Yii::t('account', 'profile_reviews') ?></h4>
						<div class="all-comments">
							<?php $first = array_shift($reviews['reviews']); ?>
							<div class="reviews-comment">
								<div class="reviews-photo">
									<a href="<?= $first['link'] ?>"><img src="<?= $first['image'] ?>" alt=""></a>
									<p class="name"><?= $first['author'] ?></p>
								</div>
								<p><?= $first['comment'] ?></p>
								<p class="data"><?= $first['date'] ?></p>
							</div>
							<i class="icon-arrow487 oppen-comments" style="cursor: pointer;"></i>
							<div class="all-reviews-comment">
								<?php foreach ($reviews['reviews'] as $review) {?>
									<div class="reviews-comment">
										<div class="reviews-photo">
											<a href="<?= $review['link'] ?>"><img src="<?= $review['image'] ?>" alt=""></a>
											<p class="name"><?= $review['author'] ?></p>
										</div>
										<p><?= $review['comment'] ?></p>
										<p class="data"><?= $review['date'] ?></p>
									</div>
								<?php } ?>
							</div>
						</div>
						<br>
						<div class="bread-crumbs">
						<?= LinkPager::widget([
							'pagination' => $reviews['pagination'],
							'disabledPageCssClass' => false,
							'maxButtonCount' => 10,
							'nextPageLabel' => '',
							'prevPageLabel' => '',
							'options'=>['class'=>'hvr-radial-out'], ]) ?>
						</div>
						<br>
					</div>
					<div class="clearfix"></div>
					<hr class="sline">
				<?php } ?>

				<!-- End Reviews -->


				<!-- Videos -->

				<?php if($videos){?>
					<div class="video">
						<p class="upload-portfolio download-portfolio edit"><?= Yii::t('account', 'edit') ?><a href="<?= Yii::$app->urlManager->createUrl(['account/edit']) ?>"></a></p>
						<h4 class="promote"><?= Yii::t('account', 'profile_video') ?></h4>
						<?php foreach ($videos as $video) {?>
							<p><i class="icon-videoplayer5"></i><?= $video['name'] ?></p>
							<iframe width="100%" height="360" src="<?= $video['link'] ?>" frameborder="0" allowfullscreen></iframe>
						<?php } ?>
					</div>
					<hr class="sline">
				<?php } ?>
				<!-- End Videos -->

				<!-- Groups -->
				<?= frontend\widgets\PGroups::widget();?>
				<!-- End Groups -->

				<?= frontend\widgets\Banner::widget(['position' => 'bottom']);?>
				
			</div>
		</div>
<!--END CENTER-->

<?php
$this->registerJsFile('js/bootstrap.min.js', ['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('js/owl.carousel.min.js', ['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('js/jquery.magnific-popup.min.js', ['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('js/jquery.cookie.js', ['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('js/common.js', ['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('scripts/common.js', ['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('scripts/payment_share.js', ['depends'=>'frontend\assets\AppAsset']);
$script_slider = <<< JS
      $("#owl-demo-2").owlCarousel({
            items: 3,
            itemsDesktop: [1199, 3],
            itemsDesktopSmall: [979, 3],
            navigationText: false,
            navigation: true,
            pagination: false

      });
  	  $('.lb-container').each(function(){
          $(this).magnificPopup({
              delegate: 'a',
              type: 'image',
              mainClass: 'mfp-img-mobile',
              gallery: {
                  enabled: true,
                  navigateByImgClick: true
              },
          });
      });

       $('.honours-slider').each(function(){
          $(this).magnificPopup({
              delegate: 'a',
              type: 'image',
              mainClass: 'mfp-img-mobile',
              gallery: {
                  enabled: true,
                  navigateByImgClick: true
              },
          });
      });


	$('.catalog-item').each(function () {
		var _this = $(this);

		_this.find('.button-comment').on({
			click: function() {
				_this.find('.form-comment').show();
			}
		});

	});


	$(document).on('submit', 'form.form-comment', function(){
		$('.form-comment').hide();
	})

      if (!$.cookie('profile_modal')) {
	  		setTimeout(function() { $('.how-it-word').modal('show'); }, 3000);
	  		$.cookie('profile_modal', 1);
      }

JS;
$this->registerJs($script_slider, yii\web\View::POS_READY);
?>

