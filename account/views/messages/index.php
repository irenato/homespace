<?php

use yii\widgets\LinkPager;
use yii\helpers\Url;

$this->title = Yii::t('titles', 'account').Yii::t('titles', 'messages');
$this->params['breadcrumbs'][] = $this->title;

?>

<!--START CENTER-->
<div class="col-lg-7 central-content centralScroll">


    <div class="content page-catalog page-in-catalog in-order applications message-from-moon">
        <div class="row">
            <div class="reviews aplication new-messages">
                <h4 class="promote  promote-messages">New message</h4>
                <div class="clearfix"></div>
                <form class="forma-input">
                    <div class="input-edit">
                        <div class="input-container">
                            <input class="text-input floating-label" type="text" name="sample1" />
                            <label for="sample1">Recipient</label>
                        </div>
                    </div>

                    <textarea class='message-to-moon' name="" id="" cols="30" rows="10" placeholder="Your message..."></textarea>
                    <div class="uploading-files-button button-for-change">
                        <input type="file" class="files-to-moon" name="file" accept=".txt, .pdf, .rtf, .doc, .jpg, .png" multiple="">
                        <i class="icon-symbols"></i>
                    </div>

                    <div class="block-button">
                        <input type="submit" id="message-to-moon" class="button" value="Send">
                    </div>

                </form>

                <div class="uploading-files-list">
                </div>
            </div>
        </div>
    </div>

    <div class="content page-catalog page-in-catalog in-order applications">
        <div class="row">
            <div class="reviews aplication">
                <h4 class="promote  promote-messages"><?= Yii::t('account', 'messages') ?></h4>
                <p class="upload-portfolio download-portfolio write"><?= Yii::t('account', 'write') ?><a href="#"></a></p>

                <form action="">
                    <div class="search search-messages">
                        <label for=""><input placeholder="Search" class="search-interlocutors" type="search"><input value="" type="submit"></label>
                    </div>
                </form>



                <div class="clearfix"></div>

                <?php if($messages) : ?>

                    <?php foreach($messages as $message): ?>

                        <?php if($message[0] == 'send' && $message['about']['id'] != NULL) : ?>

                            <div class="reviews-comment ">
                                <a href="<?= Url::to(['/account/message/index?'.$message['about']['id'] ]) ?>">
                                <div class="reviews-photo">
                                    <img src="<?= Yii::getAlias('@avatar/'.$message['about']['avatar']) ?>" alt="">
                                    <p class="name"><?= $message['about']['first_name']." ".$message['about']['last_name']?><span><?php if($message['about']['specialization'] != 'NULL') echo $message['about']['specialization']; ?></span></p>
                                </div>

                                    <div class="my-content-messages">

                                        <?php if(!empty($message['message'])) : ?>
                                              <p> <?= $message['message'] ?> </p>
                                            <?php endif; ?>

                                        <?php if ($message['files'][0] != 0) : ?>

                                                <ul class="uploading-files">

                                                    <?php foreach ($message['files'] as $file) : ?>

                                                        <li><?= $file ?></li>

                                                    <?php endforeach; ?>

                                                </ul>

                                        <?php endif; ?>

                                    </div>

                                <p class="data"><?= $message['created_at'] ?></p>
                                </a>
                            </div>
                            <hr class="sline">

                        <?php elseif ($message[0] == 'get' && $message['about']['id'] != NULL) : ?>

                            <div class="reviews-comment">
                                <a href="<?= Url::to(['/account/message/index?'.$message['about']['id'] ]) ?>">
                                <div class="reviews-photo">
                                    <img src="<?= Yii::getAlias('@avatar/'.$message['about']['avatar']) ?>" alt="">
                                    <p class="name"><?= $message['about']['first_name']." ".$message['about']['last_name']?><span><?php if($message['about']['specialization'] != 'NULL') echo $message['about']['specialization']; ?></span></p>
                                </div>

                                    <div class="my-content-messages">

                                        <?php if(!empty($message['message'])) : ?>

                                        <p> <?= $message['message'] ?></p>

                                        <?php endif; ?>

                                        <?php if ($message['files'][0] != 0) : ?>

                                            <ul class="uploading-files">

                                                <?php foreach ($message['files'] as $file) : ?>

                                                    <li><?= $file ?></li>

                                                <?php endforeach; ?>

                                            </ul>

                                    <?php endif; ?>

                                    </div>

                                    <p class="data"><?= $message['created_at'] ?></p>
                                </a>
                            </div>
                            <hr class="sline">

                        <?php endif; ?>

                    <?php endforeach; ?>

                <?php endif; ?>

                <?php if($pagination != false) : ?>

                    <div class="bread-crumbs">
                        <?= LinkPager::widget([
                            'pagination' => $pagination,
                            'disabledPageCssClass' => false,
                            'nextPageLabel' => '',
                            'prevPageLabel' => '',
                            'options'=>['class'=>'hvr-radial-out'], ]) ?>
                    </div>

                <?php endif; ?>

            </div>
        </div>
    </div>
    <div class="row">
        <?= frontend\widgets\Banner::widget(['position' => 'bottom']);?>
    </div>
</div>
<!--END CENTER-->



<?php
//    $this->registerJsFile('js/jquery-2.1.4.min.js', ['depends'=>'frontend\assets\AppAsset']);
//    $this->registerJsFile('js/owl.carousel.min.js', ['depends'=>'frontend\assets\AppAsset']);
//    $this->registerJsFile('js/bootstrap.min.js', ['depends'=>'frontend\assets\AppAsset']);
//    $this->registerJsFile('js/browser.js', ['depends'=>'frontend\assets\AppAsset']);
//    $this->registerJsFile('js/jquery.uploadThumbs.js', ['depends'=>'frontend\assets\AppAsset']);
//    $this->registerJsFile('js/common.js', ['depends'=>'frontend\assets\AppAsset']);
//    $this->registerJsFile('js/jscript.js', ['depends'=>'frontend\assets\AppAsset']);
//    $this->registerJsFile('js/new-js.js', ['depends'=>'frontend\assets\AppAsset']);
    $this->registerJsFile('js/messages.js', ['depends'=>'frontend\assets\AppAsset']);
?>
