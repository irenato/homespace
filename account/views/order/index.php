<?php
use yii\widgets\LinkPager;
use frontend\account\models\Application;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use frontend\models\Language;
use yii\helpers\Url;

$this->title = Yii::t('titles', 'account') . Yii::t('titles', 'order');
$this->params['breadcrumbs'][] = $this->title;

?>
    <!--START CENTER-->
    <div class="col-lg-7 central-content page-catalog page-in-catalog in-order comp-ord script centralScroll">
        <div class="content">
            <div class="row">
                <div class="col-lg-12 catalog-column">
                    <?php if (isset($model)): ?>
                        <h4 class="promote promote-bottom complete"><?= Yii::t('account', 'order_title') ?></h4>
                        <p class="upload-portfolio download-portfolio new-project"><?= Yii::t('account', 'new_project') ?>
                            <a href="#"></a></p>
                        <div class="clearfix"></div>
                        <div class="new_project">

                            <?php $form = ActiveForm::begin(['id' => 'application', 'options' => ['enctype' => 'multipart/form-data', 'class' => 'forma-input textarea']]) ?>
                            <div id="application-name" style="display: none"
                                 data-name="<?php if (Language::getCurrent()->url === 'en') {
                                     echo 'Name of application';
                                 } else echo 'Название проекта'; ?>">


                            </div>
                            <div class="input-container" style="display: block; width: 61%;">
                                <div class="form-group field-application-title_app required">
                                    <input type="text" id="application-title_app" class="floating-label text-input" name="Application[title_app]" value="" label="Name">
                                    <label for="application-title_app">Name</label>
                                    <div class="help-block">Name of application cannot be blank.</div>
                                </div>

<!--                                --><?//= $form->field($model, 'title_app')->textInput(['class' => 'floating-label text-input', 'value' => "",])->label(false) ?>

                            </div>
                            <div class="shared-upload myshare">
                                <a href="#" class="file">
                                    <?= $form->field($model, 'file[]')->fileInput(['multiple' => true])->label(false); ?>
                                    <div id="application-upload" style="display: none"
                                         data-upload="<?php if (Language::getCurrent()->url === 'en') {
                                             echo 'Upload files';
                                         } else echo 'Добавить файлы'; ?>">
                                        
                                    </div>
                                     Upload files<i class="icon-direction13"></i>
                                </a>
                                <!--                                   <div class="clearfix"></div>-->
                                <!--                                shared whith ( - - )-->
                                <!--                                   --><? //= $form->field($model, 'title_app')->textInput(['class' => 'floating-label text-input', 'value' => ""])->label(false) ?>
                                <!--                                    --><?php //$data12 = array('24' => 'Group 1', '121' => 'Group 2', '125' => 'Group 3', '126' => 'Group 4', '131' => 'Group 5','150' => 'Group 6'); ?>
                                <!--                                    --><? //= $form->field($model, 'recipients[]')->widget(Select2::classname(), [
                                //                                        'data' => $data12,
                                //                                        'options' => ['multiple' => true],
                                //                                        'showToggleAll' => false,
                                //                                        'pluginOptions' => [
                                //                                            'maximumSelectionLength' => 5,
                                //                                            'allowClear' => true,
                                //                                        ],
                                //                                    ])->label(false); ?>
                            </div>
                            <div class="sel">
                                <div class="selects prod">
                                    <p><?= Yii::t('account', 'order_category') ?></p>
                                    <!--                                    --><?php //$category = array( '2' => 'Furniture','3' => 'Sofa','4' => 'Bathroom','5' => 'Tables', '6' => 'Flowers');?>
                                    <?= $form->field($model, 'category_app')->dropDownList($collections, ['prompt' => ''])->label(false); ?>

                                </div>
                            </div>
                            <div class="sel">
                                <div class="selects prod">
                                    <p><?= Yii::t('account', 'order_choose_folder') ?></p>
                                    <?php $result = ArrayHelper::map($products_folders, 'id', ($lang == 'ru') ? 'name_ru' : 'name_en'); ?>
                                    <!--                            --><?php //$result = ArrayHelper::map($folder_list, 'id_folder', 'title'); ?>
                                    <?= $form->field($model, 'id_folder')->dropDownList($result, ['prompt' => ''])->label(false); ?>
                                </div>
                            </div>
                            <div class="sel">
                                <div class="selects prod">
                                    <p><?= Yii::t('account', 'order_choose_folder_projects') ?></p>
                                    <?php $result = ArrayHelper::map($projects_folders, 'id', ($lang == 'ru') ? 'name_ru' : 'name_en'); ?>
                                    <!--                            --><?php //$result = ArrayHelper::map($folder_list, 'id_folder', 'title'); ?>
                                    <?= $form->field($model, 'id_folder_projects')->dropDownList($result, ['prompt' => ''])->label(false); ?>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="shared-with share-this-order">
                                <i class="icon-invite"></i>
                            </div>
                            <?= $form->field($model, 'recipients')->hiddenInput()->label(false); ?>
                            <div class="input-container">
                                <input id="search-recipients" class="text-input floating-label" type="text"
                                       name="search-recipients"
                                       value=""/>
                                <label for="search-recipients"><?= Yii::t('account', 'order_share') ?></label>
                            </div>
                            <div id="search-results"></div>
                            <?= $form->field($model, 'description_app')->textarea(array('placeholder' => Yii::t('account', 'order_describe')), ['options' => ['id' => 'desc_app']])->label(false) ?>

                            <div class="block-button">
                                <div class="form-group">
                                    <?= Html::resetButton(Yii::t('account', 'cancel'), ['class' => 'button reset-app']) ?>
                                    <?= Html::submitButton(Yii::t('account', 'send'), ['class' => 'button']) ?>
                                </div>
                            </div>
                            <?php ActiveForm::end() ?>
                            <hr class="sline mt">
                        </div>
                    <?php else: ?>
                        <h4 class="promote promote-bottom complete"><?= $folder['title'] ?></h4>
                    <?php endif ?>

                    <div class="folder">
                        <div class="forma-input" style="display: none">
                            <div class="input-container">
                                <input class="text-input floating-label folder-name" type="text" name="title" value=""/>
                                <label for="sample"><?= Yii::t('account', 'name_folder') ?></label>
                                <input type="hidden" name="root" value="2">
                            </div>
                            <input type="submit" class="create-new-folder-button"
                                   value="<?= Yii::t('account', 'button_save') ?>">
                        </div>
                        <p class="upload-portfolio download-portfolio create-folder-for-order"><?= Yii::t('account', 'order_add') ?>
                            <a href="#"></a></p>
                        <div class="clearfix"></div>
                        <h4 class="promote promote-bottom">Products</h4>
                        <div class="clearfix"></div>
                        <div class="order-area">
                            <?php if ($products_folders) : ?>
                                <?php foreach ($products_folders as $folder) : ?>
                                    <div class="name-folder" data-id="<?= $folder['root']; ?>">

                                        <a href="<?= Url::to(['/account/like-it/root/', 'id' => $folder['id'], 'type' => 'products']); ?>"
                                           class="folder_link">
                                            <div class="folder-img " style="background-image: url('<?= Yii::getAlias('@basepath') ?>/images/folder_2.png')"></div>
                                        </a>
                                        <div class="folder-bottom change-folder forma-input">
                                            <p><?= ($lang == 'ru') ? $folder['name_ru'] : $folder['name_en']; ?></p>
                                            <div class="input-container" style="display: none">
                                                <input class="text-input floating-label title" type="text"
                                                       name="re-name"
                                                       value="<?= ($lang == 'ru') ? $folder['name_ru'] : $folder['name_en']; ?>"
                                                       data-id="<?= $folder['id'] ?>"/>
                                                <label for="re-name"></label>
                                            </div>
                                            <i class="icon-show8"></i>
                                            <ul>
<!-- -->

                                                <li>
                                                    <a class="del_folder" url="" href="#"
                                                       data-attr-id="<?= $folder['id'] ?>">
                                                        <i class="icon-rubbish"></i>Delete</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            <?php endif; ?>
                            <?php if ($products): ?>

                                <div class="col-lg-12 catalog-column">
                                    <div class="catalog-box">
                                        <?php foreach ($products as $item): ?>
                                            <div class="catalog-item">
                                                <div class="catalog-img-box">
                                                    <a href="<?= Url::to(['/about/product', 'id' => $item['id_prod']]); ?>">
                                                        <img
                                                            src="<?= Yii::getAlias('@products/' . $item['main_image']) ?>"
                                                            alt="" data-group="products"
                                                            data-id="<?= $item['id_prod'] ?>">
                                                    </a>
                                                    <div class="like-calc product-likes"><i
                                                            class="icon-heart2971"></i><span
                                                            likes_id="<?= $item['id_prod'] ?>"
                                                            class="prod-likes-count"><?= $item['likes'] ?></span>
                                                    </div>
                                                </div>
                                                <div class="catalog-caption">
                                                    <div class="social-cont">
                                                        <a data-id="<?= $item['id_prod'] ?>"
                                                           class="main-like icon-heart297"></a>
                                                    </div>
                                                    <p><?= $item['prod_name'] ?></p>
                                                </div>
                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>

            </div>
            <div class="folder">
                <div class="forma-input" style="display: none">
                    <div class="input-container">
                        <input class="text-input floating-label folder-name" type="text" name="title" value=""/>
                        <label for="sample"><?= Yii::t('account', 'name_folder') ?></label>
                        <input type="hidden" name="root" value="1">
                    </div>
                    <input type="submit" class="create-new-folder-button"
                           value="<?= Yii::t('account', 'button_save') ?>">
                </div>
                <p class="upload-portfolio download-portfolio create-folder-for-order"><?= Yii::t('account', 'order_add') ?>
                    <a href="#"></a></p>
                <div class="clearfix"></div>
                <h4 class="promote promote-bottom">Projects</h4>
                <div class="clearfix"></div>

                <div class="order-area">
                    <?php if ($projects_folders) : ?>
                        <?php foreach ($projects_folders as $folder) : ?>
                            <div class="name-folder" data-id="<?= $folder['root']; ?>">

                                <a href="<?= Url::to(['/account/like-it/root/', 'id' => $folder['id'], 'type' => 'projects']); ?>"
                                   class="folder_link">
                                    <div class="folder-img " style="background-image: url('<?= Yii::getAlias('@basepath') ?>/images/folder.png')"></div>
                                </a>


                                <div class="folder-bottom change-folder forma-input">
                                    <p><?= ($lang == 'ru') ? $folder['name_ru'] : $folder['name_en']; ?></p>
                                    <div class="input-container" style="display: none">
                                        <input class="text-input floating-label title" type="text"
                                               name="re-name"
                                               value="<?= ($lang == 'ru') ? $folder['name_ru'] : $folder['name_en']; ?>"/>
                                        <label for="re-name"></label>
                                    </div>
                                    <i class="icon-show8"></i>
                                    <ul>
<!--                                        <li>-->
<!--                                            <a class="re_folder" url="" href="#"-->
<!--                                               data-attr-id="--><?//= $folder['id'] ?><!--">-->
<!--                                                <i class="icon-edit45"></i>Rename</a>-->
<!--                                        </li>-->

                                        <li>
                                            <a class="del_folder" url="" href="#"
                                               data-attr-id="<?= $folder['id'] ?>">
                                                <i class="icon-rubbish"></i>Delete</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    <?php endif; ?>
                    <?php if ($projects): ?>

                        <div class="col-lg-12 catalog-column">
                            <div class="catalog-box">
                                <?php foreach ($projects as $item): ?>
                                    <div class="catalog-item">
                                        <div class="catalog-img-box">
                                            <?php
                                            $m_img = explode('|', $item['images']);
                                            ?>
                                            <a href="<?= Url::to(['/about/item', 'id' => $item['item_id']]); ?>">
                                                <img src="<?= Yii::getAlias('@portfolio/' . $m_img[0]) ?>" alt=""
                                                     data-group="projects"
                                                     data-id="<?= $item['item_id'] ?>">
                                            </a>
                                            <div class="like-calc product-likes"><i class="icon-heart2971"></i><span
                                                    likes_id="<?= $item['item_id'] ?>"
                                                    class="prod-likes-count"><?= $item['likes'] ?></span>
                                            </div>
                                        </div>
                                        <div class="catalog-caption">
                                            <div class="social-cont">
                                                <a data-id="<?= $item['item_id'] ?>"
                                                   class="main-like icon-heart297"></a>
                                            </div>
                                            <p><?= $item['title'] ?></p>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="row">
            <?= frontend\widgets\Banner::widget(['position' => 'bottom']); ?>
        </div>


    </div>


<?php $this->registerJsFile('js/common.js', ['depends' => 'frontend\assets\AppAsset']); ?>
<?php //$this->registerJsFile('js/setOptions.js', ['depends' => 'frontend\assets\AppAsset']); ?>
<?php $this->registerJsFile('js/new-js.js', ['depends' => 'frontend\assets\AppAsset']); ?>
<?php //$this->registerJsFile('scripts/like_it.js', ['depends' => 'frontend\assets\AppAsset']); ?>
<?php $this->registerJsFile('js/order.js', ['depends' => 'frontend\assets\AppAsset']); ?>
<?php $this->registerJsFile('https://code.jquery.com/ui/1.11.4/jquery-ui.js', ['depends' => 'frontend\assets\AppAsset']);


