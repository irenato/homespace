<?php


$this->title = Yii::t('titles', 'account').Yii::t('titles', 'account_edit');
$this->params['breadcrumbs'][] = $this->title;

?>
<!--START CENTER-->

<div class="col-lg-7 profile-content profile-edite central-content icon-edit prof-edit-changes centralScroll">
    <div class="content">
        <input type="file" accept="image/*;capture=camera" style="display : none;" id="open_browse" onchange="fileLogoChange(this);" />
        <input type="file" accept="image/*;capture=camera" style="display : none;" id="open_browse2" onchange="fileChange(this);" />
        <input type="file" accept="image/*;capture=camera" style="display : none;" id="open_browse3" onchange="userBackground(this)" />

        <!-- Main Information -->
        <div class="profil profil-edited" id="main-profil" style="background-image: url(/media/profile/background/<?= $user_information['background'] ?>);" data-url="<?= $user_information['background'] ?>">
            <div class="col-lg-4 clear">
                <div class="my-info">
                    <h5><?= Yii::t('account', 'edit_main_info') ?></h5>
                </div>
            </div>
            <div class="col-lg-4 clear">
                <div class="profil-photo">
                    <a href="#" id="profile-photo"><img src="<?= Yii::getAlias('@avatar/'.$user_information['avatar']) ?>" alt="#"></a>
                </div>
            </div>
            <div class="col-lg-4 clear">
                <a href="#" class="bg-profile" data-toggle="modal" data-target=".model-img"><i class="icon-camera"></i></a>
            </div>

            <div class="clearfix"></div>

            <form class="forma-input" id="form_main_information">
                <div class="sub-wrap">
                    <div class="selects" name="specialization">
                        <p><?= Yii::t('account', 'edit_specialization') ?></p>
                        <i class="icon-graduation-cap2"></i>
                        <select class="view">
                            <?php foreach($specialization as $value){?>
                                <option><?= $value ?></option>
                            <?php } ?>
                        </select>
                    </div>

                    <div class="input-edit">
                        <i class="icon-user168"></i>
                        <div class="input-container">
                            <input class="text-input floating-label" type="text" name="first_name" value="<?= $user_information['first_name'] ?>" />
                            <label for="first_name"><?= Yii::t('account', 'edit_first_name') ?></label>
                        </div>
                    </div>

                    <div class="input-edit">
                        <i class="icon-user168"></i>
                        <div class="input-container">
                            <input class="text-input floating-label" type="text" name="last_name" value="<?= $user_information['last_name'] ?>" />
                            <label for="last_name"><?= Yii::t('account', 'edit_last_name') ?></label>
                        </div>
                    </div>

                    <div class="input-edit">
                        <i class="icon-home"></i>
                        <div class="input-container">
                            <input class="text-input floating-label" type="text" name="city" value="<?= $user_information['city'] ?>" />
                            <label for="city"><?= Yii::t('account', 'edit_city') ?></label>
                        </div>
                    </div>

                    <div class="input-edit">
                        <i class="icon-telephone46"></i>
                        <div class="input-container">
                            <input class="text-input floating-label" type="text" name="phone" value="<?= $user_information['phone'] ?>" />
                            <label for="phone"><?= Yii::t('account', 'edit_phone') ?></label>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>

                <div class="edit-company">
                    <div class="prof-comp-logo">
                        <a href="#" id="company-logo">
                            <img src="<?= Yii::getAlias('@avatar/'.$user_information['logo']) ?>" alt="">
                        </a>
                    </div>
                    <?php $workers = $user_information['employees']; ?>
                    <?php $workers = explode(',',$user_information['employees']); ?>
                    <div class="input-comp-container sub-wrap">
                        <div class="input-edit">
                            <div class="input-container">
                                <input class="text-input floating-label dirty" name="company" value="<?= $user_information['company'] ?>" type="text">
                                <label for="company"><?= Yii::t('account', 'edit_company') ?></label>
                            </div>
                        </div>
                        <?php $workers = $user_information['employees']; ?>
                        <?php $workers = explode(',',$user_information['employees']); ?>
                        <div class="input-edit">
                            <div class="input-container">
                                <input class="text-input floating-label dirty" name="worker1" value="<?= $workers[0] ?>" type="text">
                                <label for="worker1"><?= Yii::t('account', 'edit_employees') ?></label>
                            </div>
                        </div>
                        <div class="input-edit">
                            <div class="input-container">
                                <input class="text-input floating-label dirty" name="worker2" value="<?= $workers[1] ?>" type="text">
                                <label for="worker2"><?= Yii::t('account', 'edit_employees') ?></label>
                            </div>
                        </div>
                        <div class="input-edit">
                            <div class="input-container">
                                <input class="text-input floating-label dirty" name="worker3" value="<?= $workers[2] ?>" type="text">
                                <label for="worker3"><?= Yii::t('account', 'edit_employees') ?></label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>

                <span class="textarea" ><textarea name="message" placeholder="<?= Yii::t('account', 'edit_message') ?>"><?= $user_information['message'] ?></textarea></span>

                <div class="input-edit-top sub-wrap">

                    <div class="input-edit">
                        <i class="icon-webpage2"></i>
                        <div class="input-container">
                            <input class="text-input floating-label" type="text" name="site" value="<?= $user_information['link_user_site'] ?>" />
                            <label for="site"><?= Yii::t('account', 'edit_site') ?></label>
                        </div>
                    </div>

                    <div class="input-edit">
                        <i class="icon-facebook55"></i>
                        <div class="input-container">
                            <input class="text-input floating-label" type="text" name="facebook" value="<?= $user_information['link_fb'] ?>" />
                            <label for="facebook"><?= Yii::t('account', 'edit_facebook') ?></label>
                        </div>
                    </div>

                    <div class="input-edit">
                        <i class="icon-twitter1"></i>
                        <div class="input-container">
                            <input class="text-input floating-label" type="text" name="twitter" value="<?= $user_information['link_tw'] ?>" />
                            <label for="twitter"><?= Yii::t('account', 'edit_twitter') ?></label>
                        </div>
                    </div>

                    <div class="input-edit">
                        <i class="icon-instagram12"></i>
                        <div class="input-container">
                            <input class="text-input floating-label" type="text" name="instagram" value="<?= $user_information['link_inst'] ?>"/>
                            <label for="instagram"><?= Yii::t('account', 'edit_instagram') ?></label>
                        </div>
                    </div>

                    <div class="input-edit">
                        <i class="icon-behance2"></i>
                        <div class="input-container">
                            <input class="text-input floating-label" type="text" name="behance" value="<?= $user_information['link_behance'] ?>"/>
                            <label for="behance"><?= Yii::t('account', 'edit_behance') ?></label>
                        </div>
                    </div>
                    <div class="input-edit">
                        <i class="icon-google116"></i>
                        <div class="input-container">
                            <input class="text-input floating-label" type="text" name="google" value="<?= $user_information['link_google'] ?>"/>
                            <label for="google"><?= Yii::t('account', 'edit_google') ?></label>
                        </div>
                    </div>
                    <div class="input-edit">
                        <i class="icon-pinterest3"></i>
                        <div class="input-container">
                            <input class="text-input floating-label" type="text" name="pinterest" value="<?= $user_information['link_pint'] ?>"/>
                            <label for="pinterest"><?= Yii::t('account', 'edit_pinterest') ?></label>
                        </div>
                    </div>

                    <div class="input-edit">
                        <i class="icon-logotype1"></i>
                        <div class="input-container">
                            <input class="text-input floating-label" type="text" name="tumblr" value="<?= $user_information['link_tumblr'] ?>" />
                            <label for="tumblr"><?= Yii::t('account', 'edit_tumblr') ?></label>
                        </div>
                    </div>
                    <div class="input-edit">
                        <i class="icon-linkedin11"></i>
                        <div class="input-container">
                            <input class="text-input floating-label" type="text" name="linkedin" value="<?= $user_information['link_linkedin'] ?>" />
                            <label for="linkedin"><?= Yii::t('account', 'edit_linkedin') ?></label>
                        </div>
                    </div>


                    <div class="input-edit">
                        <i class="icon-blogger8"></i>
                        <div class="input-container">
                            <input class="text-input floating-label" type="text" name="blogger" value="<?= $user_information['link_blg'] ?>" />
                            <label for="blogger"><?= Yii::t('account', 'edit_blogger') ?></label>
                        </div>
                    </div>


                </div>


                <div class="block-button">
                    <p class="valid" id="main-save-valid"></p>
                    <p class="error" id="main-save-error">Changes not saved correctly!</p>
                    <input type="submit" class="button" value="<?= Yii::t('account', 'button_save') ?>" id="main_save">
                </div>
            </form>

        </div>
        <!-- End Main Information -->

        <!-- Honours -->
        <input type="hidden" id="hidden-honours-text" data-del-text="<?= Yii::t('account', 'button_delete') ?>" data-honour-text="<?= Yii::t('account', 'profile_honour_name') ?>"/>
        <h4 class="promote promote-bottom"><?= Yii::t('account', 'profile_honour') ?></h4>
        <p class="upload-portfolio download-portfolio join"><?= Yii::t('account', 'profile_add_honour') ?><a href="#" id="add_honour"></a></p>
        <form class="honours forma-input">
            <div class="all-honours">
                <?php if($honours){?>
                    <?php foreach ($honours as $honour) {?>
                        <div class="input-edit count-honours" data-honour-id="<?= $honour['id'] ?>">
                            <i class="icon-trophy36"></i>
                            <div class="input-container">
                                <input class="text-input floating-label" type="text" name="sample" value="<?= $honour['name'] ?>" />
                                <label for="sample"><?= Yii::t('account', 'profile_honour_name') ?></label>
                            </div>
                            <a href="#" class="del del_honour"><?= Yii::t('account', 'button_delete') ?><i></i></a>
                        </div>
                    <?php } ?>
                <?php }else{ ?>
                    <div class="input-edit count-honours" data-honour-id="">
                        <i class="icon-trophy36"></i>
                        <div class="input-container">
                            <input class="text-input floating-label" type="text" name="sample" />
                            <label for="sample"><?= Yii::t('account', 'profile_honour_name') ?></label>
                        </div>
                        <a href="#" class="del del_honour"><?= Yii::t('account', 'button_delete') ?><i></i></a>
                    </div>
                <?php } ?>
            </div>
            <div class="block-button">
                <p class="valid" id="honours-save-valid"></p>
                <p class="error" id="honours-save-error">Changes not saved correctly!</p>
                <input type="submit" class="button" id="honours_save" value="<?= Yii::t('account', 'button_save') ?>">
            </div>
        </form>
        <!-- End Honours -->

        <!-- Licenses -->
        <input type="hidden" id="hidden-licenses-text" data-del-text="<?= Yii::t('account', 'button_delete') ?>" data-number-text="<?= Yii::t('account', 'profile_license_number') ?>" data-issued-text="<?= Yii::t('account', 'profile_license_issued') ?>"/>
        <div class="licenses">
            <h4 class="promote promote-bottom"><?= Yii::t('account', 'profile_license') ?></h4>
            <p class="upload-portfolio download-portfolio join"><?= Yii::t('account', 'profile_add_license') ?><a href="#" id="add_license"></a></p>
            <div class="all-licenses">
                <?php if($licenses){?>
                    <?php foreach ($licenses as $license) {?>
                        <?php $images = explode('|', $license['images'] ); ?>
                        <div class="licenses-item" data-license-id="<?= $license['id'] ?>">
                            <div class="clearfix"></div>

                            <div class="license-images">
                                <?php foreach ($images as $url) {?>
                                    <?php if($url != ""){?>
                                        <div class="licenses-img">
                                            <a href="#" class="del-licenses-img">X</a>
                                            <img src="<?= $url ?>" alt="">
                                        </div>
                                    <?php } ?>
                                <?php } ?>
                                <div class="licenses-img add add-licenses-img" ></div>
                            </div>

                            <a href="#" class="del del-license"><?= Yii::t('account', 'button_delete') ?><i></i></a>


                            <form class="forma-input">
                                <div class="input-container namb">
                                    <input class="text-input floating-label" type="text" name="number" value="<?= $license['number'] ?>" />
                                    <label for="number"><?= Yii::t('account', 'profile_license_number') ?></label>
                                </div>
                                <div class="input-container last">
                                    <input class="text-input floating-label" type="text" name="issued_by" value="<?= $license['issued_by'] ?>" />
                                    <label for="issued_by"><?= Yii::t('account', 'profile_license_issued') ?></label>
                                </div>
                            </form>
                        </div>
                    <?php } ?>
                <?php }else{ ?>

                    <div class="licenses-item" data-license-id="">
                        <div class="clearfix"></div>

                        <div class="license-images">
                            <div class="licenses-img add add-licenses-img" ></div>
                        </div>

                        <a href="#" class="del del-license"><?= Yii::t('account', 'button_delete') ?><i></i></a>

                        <form class="forma-input">
                            <div class="input-container namb">
                                <input class="text-input floating-label" type="text" name="number" value="" />
                                <label for="number"><?= Yii::t('account', 'profile_license_number') ?></label>
                            </div>
                            <div class="input-container last">
                                <input class="text-input floating-label" type="text" name="issued_by" value=""/>
                                <label for="issued_by"><?= Yii::t('account', 'profile_license_issued') ?></label>
                            </div>
                        </form>
                    </div>

                <?php } ?>
            </div>

            <div class="block-button">
                <p class="valid" id="licenses-save-valid"></p>
                <p class="error" id="licenses-save-error">Changes not saved correctly!</p>
                <input type="submit" class="button" value="<?= Yii::t('account', 'button_save') ?>" id="licenses_save">
            </div>


        </div>
        <!-- End Licenses -->

        <!-- Collections -->
        <input type="hidden" id="hidden-collections-text" data-del-text="<?= Yii::t('account', 'button_delete') ?>" data-name-text="<?= Yii::t('account', 'profile_collection_name') ?>" data-main-photo="<?= Yii::t('account', 'profile_collection_main') ?>"/>
        <?php if($collections){?>
            <div class="portfolio-edit">
                <h4 class="promote promote-bottom"><?= Yii::t('account', 'my_items') ?></h4>
                <p class="upload-portfolio download-portfolio join"><?= Yii::t('account', 'profile_add_collection') ?><a href="#" id="add_collection"></a></p>
                <?php $count = 1; ?>
                <?php $count_name = 1; ?>
                <div class="all-collections">
                    <?php foreach ($collections as $collection) {?>
                        <div class="one-collection" id="<?= $collection['id'] ?>">
                            <form class="forma-input">
                                <div class="input-container">
                                    <a href="#" class="del delete-collection"><?= Yii::t('account', 'button_delete') ?><i></i></a>
                                    <input class="text-input floating-label" type="text" name="collection-name" value="<?= $collection['name'] ?>" />
                                    <label for="collection-name"><?= Yii::t('account', 'profile_collection_name') ?></label>
                                </div>
                            </form>
                            <div class="portfolio_items">

                                <?php foreach ($collection['items'] as $item) {?>
                                    <div class="portfolio-items" id="<?= $item['id'] ?>">
                                        <a href="#" class="select-item" data-toggle="modal" data-target="#best_3_works"><img src="<?= $item['image'] ?>" alt=""></a>
                                        <h5 class="name"><?= $item['title'] ?></h5>
                                        <div class="checkbox">
                                            <p>
                                                <?php if($collection['main'] == $item['id']){?>
                                                    <input id="test<?= $count ?>" name="main<?= $count_name ?>"  type="radio" data-item="<?= $item['id'] ?>" checked/>
                                                <?php }else{ ?>
                                                    <input id="test<?= $count ?>" name="main<?= $count_name ?>"  type="radio" data-item="<?= $item['id'] ?>" />
                                                <?php } ?>
                                                <label for="test<?= $count ?>"><?= Yii::t('account', 'profile_collection_main') ?></label>
                                            </p>
                                            <div class="licenses-del">
                                                <a href="#" class="del delete-item"><?= Yii::t('account', 'button_delete') ?><i></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <?php $count++;?>
                                <?php } ?>


                            </div>
                            <form class="portfolio-textarea">
                                <textarea class="text-ar"><?= $collection['about'] ?></textarea>
                            </form>
                        </div>
                        <div class="clearfix"></div>
                        <?php $count_name++; ?>


                    <?php } ?>
                </div>

                <div class="clearfix"></div>
                <div class="block-button">
                    <p class="valid" id="collections-save-valid"></p>
                    <p class="error" id="collections-save-error">Changes not saved correctly!</p>
                    <input type="submit" class="button" value="<?= Yii::t('account', 'button_save') ?>" id="collections_save">
                </div>

            </div>

        <?php }else{ ?>
            <div class="portfolio-edit">
                <h4 class="promote promote-bottom"><?= Yii::t('account', 'my_items') ?></h4>
                <p class="upload-portfolio download-portfolio join"><?= Yii::t('account', 'profile_add_collection') ?><a href="#" id="add_collection"></a></p>

                <div class="all-collections">
                    <div class="one-collection" id="">
                        <form class="forma-input">
                            <div class="input-container">
                                <a href="#" class="del delete-collection"><?= Yii::t('account', 'button_delete') ?><i></i></a>
                                <input class="text-input floating-label" type="text" name="collection-name" />
                                <label for="collection-name"><?= Yii::t('account', 'profile_collection_name') ?></label>
                            </div>
                        </form>
                        <div class="portfolio_items">

                            <div class="portfolio-items" id="">
                                <a href="#" class="select-item" data-toggle="modal" data-target="#best_3_works"><img src="/images/no-img.png" alt=""></a>
                                <h5 class="name"></h5>
                                <div class="checkbox">
                                    <p>
                                        <input id="test1" name="main"  type="radio" item="" checked/>
                                        <label for="test1"><?= Yii::t('account', 'profile_collection_main') ?></label>
                                    </p>
                                    <div class="licenses-del">
                                        <a href="#" class="del delete-item"><?= Yii::t('account', 'button_delete') ?><i></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="portfolio-items" id="">
                                <a href="#" class="select-item" data-toggle="modal" data-target="#best_3_works"><img src="/images/no-img.png" alt=""></a>
                                <h5 class="name"></h5>
                                <div class="checkbox">
                                    <p>
                                        <input id="test2" name="main" type="radio" data-item=""/>
                                        <label for="test2"><?= Yii::t('account', 'profile_collection_main') ?></label>
                                    </p>
                                    <div class="licenses-del">
                                        <a href="#" class="del delete-item"><?= Yii::t('account', 'button_delete') ?><i></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="portfolio-items" id="">
                                <a href="#" class="select-item" data-toggle="modal" data-target="#best_3_works"><img src="/images/no-img.png" alt=""></a>
                                <h5 class="name"></h5>
                                <div class="checkbox">
                                    <p>
                                        <input id="test3" name="main" type="radio" data-item=""/>
                                        <label for="test3"><?= Yii::t('account', 'profile_collection_main') ?></label>
                                    </p>
                                    <div class="licenses-del">
                                        <a href="#" class="del delete-item"><?= Yii::t('account', 'button_delete') ?><i></i></a>
                                    </div>
                                </div>
                            </div>


                        </div>
                        <form class="portfolio-textarea">
                            <textarea class="text-ar"></textarea>
                        </form>
                    </div>
                </div>

                <div class="clearfix"></div>
                <div class="block-button">
                    <p class="valid" id="collections-save-valid"></p>
                    <p class="error" id="collections-save-error">Changes not saved correctly!</p>
                    <input type="submit" class="button" value="<?= Yii::t('account', 'button_save') ?>" id="collections_save">
                </div>

            </div>
        <?php } ?>

        <!-- End Collections -->

        <!-- Video -->
        <input type="hidden" id="hidden-videos-text" data-del-text="<?= Yii::t('account', 'button_delete') ?>" data-link-text="<?= Yii::t('account', 'profile_video_link') ?>" data-name-text="<?= Yii::t('account', 'profile_video_name') ?>"/>
        <div class="profile-video">
            <h4 class="promote promote-bottom"><?= Yii::t('account', 'profile_video') ?></h4>
            <p class="upload-portfolio download-portfolio join"><?= Yii::t('account', 'profile_add_video') ?><a href="#" id="add_video"></a></p>
            <div class="clearfix"></div>
            <div class="all-videos">
                <?php if($videos){?>
                    <?php foreach ($videos as $video) {?>
                        <div class="clearfix"></div>
                        <div class="video videos_count" data-video-id="<?= $video['id'] ?>">
                            <iframe width="330" height="181" src="<?= $video['link'] ?>" frameborder="0" allowfullscreen></iframe>
                            <form class="forma-input">
                                <div class="input-container namb">
                                    <input class="text-input floating-label video-input" type="text" name="link" value="<?= $video['link'] ?>">
                                    <label for="link"><?= Yii::t('account', 'profile_video_link') ?></label>
                                </div>
                                <div class="input-container namb">
                                    <input class="text-input floating-label video-input" type="text" name="name" value="<?= $video['name'] ?>" />
                                    <label for="name"><?= Yii::t('account', 'profile_video_name') ?></label>
                                </div>
                            </form>
                            <a href="#" class="del del_video"><?= Yii::t('account', 'button_delete') ?><i></i></a>
                        </div>
                    <?php } ?>
                <?php }else{ ?>
                    <div class="video videos_count" data-video-id="">
                        <img class="video-img" src="/images/no-video.png" alt="">
                        <form class="forma-input">
                            <div class="input-container namb">
                                <input class="text-input floating-label video-input" type="text" name="link" value=""/>
                                <label for="link"><?= Yii::t('account', 'profile_video_link') ?></label>
                            </div>
                            <div class="input-container namb">
                                <input class="text-input floating-label video-input" type="text" name="name" value=""/>
                                <label for="name"><?= Yii::t('account', 'profile_video_name') ?></label>
                            </div>
                        </form>
                        <a href="#" class="del del_video"><?= Yii::t('account', 'button_delete') ?><i></i></a>
                    </div>
                <?php } ?>
            </div>
            <div class="clearfix"></div>
            <div class="block-button">
                <p class="valid" id="videos-save-valid"></p>
                <p class="error" id="videos-save-error">Changes not saved correctly!</p>
                <input type="submit" class="button" value="<?= Yii::t('account', 'button_save') ?>" id="videos_save">
            </div>

        </div>
        <!--End Video -->

    </div>

    <?= frontend\widgets\Banner::widget(['position' => 'bottom']);?>
</div>
<!--END CENTER-->

<?php
$this->registerJsFile('js/bootstrap.min.js', ['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('js/common.js', ['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('scripts/main_profile.js', ['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('scripts/honours_profile.js', ['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('scripts/licenses_profile.js', ['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('scripts/collections_profile.js', ['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('scripts/videos_profile.js', ['depends'=>'frontend\assets\AppAsset']);
?>

