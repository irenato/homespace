<?php

$this->title = Yii::t('titles', 'account').Yii::t('titles', 'edit_honours');
$this->params['breadcrumbs'][] = $this->title;

?>
<!--START CENTER-->	
				
<div class="col-lg-7 profile-content profile-edite central-content icon-edit centralScroll">
	<div class="content">
		<input type="file" accept="image/*;capture=camera" style="display : none;" id="open_browse4" onchange="honourChange(this);" />
		<!-- Honours -->
		<input type="hidden" id="hidden-honours-text" data-del-text="<?= Yii::t('account', 'button_delete') ?>" data-honour-text="<?= Yii::t('account', 'profile_honour_name') ?>"/>
		<h4 class="promote promote-bottom"><?= Yii::t('account', 'profile_honour') ?></h4>
		<p class="upload-portfolio download-portfolio join"><?= Yii::t('account', 'profile_add_honour') ?><a href="#" id="add_honour"></a></p>
		<form class="honours forma-input">
			<div class="all-honours">
				<?php if($honours){?>
					<?php foreach ($honours as $honour) {?>
						<div class="input-edit count-honours" data-honour-id="<?= $honour['id'] ?>">
<!--							<i class="icon-trophy36"></i>-->
							<img class="honour-image" src="<?= $honour['image'] ?>" alt="">
							<div class="input-container">
								<input class="text-input floating-label" type="text" name="sample" value="<?= $honour['name'] ?>" />
								<label for="sample"><?= Yii::t('account', 'profile_honour_name') ?></label>
							</div>
							<a href="#" class="del del_honour"><?= Yii::t('account', 'button_delete') ?><i></i></a>
						</div>
					<?php } ?>
				<?php }else{ ?>
					<div class="input-edit count-honours" data-honour-id="">
<!--						<i class="icon-trophy36"></i>-->
						<img src="/images/trophy36.png" alt="">
						<div class="input-container">
							<input class="text-input floating-label" type="text" name="sample" />
							<label for="sample"><?= Yii::t('account', 'profile_honour_name') ?></label>
						</div>
						<a href="#" class="del del_honour"><?= Yii::t('account', 'button_delete') ?><i></i></a>
					</div>
				<?php } ?>
			</div>
			<div class="block-button">
				<p class="valid" id="honours-save-valid"></p>
				<p class="error" id="honours-save-error">Changes not saved correctly!</p>
				<input type="submit" class="button" id="honours_save" value="<?= Yii::t('account', 'button_save') ?>">
			</div>
		</form>
		<!-- End Honours -->

	</div>

</div>
<!--END CENTER-->	


<?php
$this->registerJsFile('js/bootstrap.min.js', ['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('js/common.js', ['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('scripts/honours_profile.js', ['depends'=>'frontend\assets\AppAsset']);

?><?php

