<?php

$this->title = Yii::t('titles', 'account').Yii::t('titles', 'edit_licenses');
$this->params['breadcrumbs'][] = $this->title;

?>
<!--START CENTER-->	
				
<div class="col-lg-7 profile-content profile-edite central-content icon-edit centralScroll">
	<div class="content">

		<input type="file" accept="image/*;capture=camera" style="display : none;" id="open_browse2" onchange="fileChange(this);" />

		<!-- Licenses -->
		<input type="hidden" id="hidden-licenses-text" data-del-text="<?= Yii::t('account', 'button_delete') ?>" data-number-text="<?= Yii::t('account', 'profile_license_number') ?>" data-issued-text="<?= Yii::t('account', 'profile_license_issued') ?>"/>
		<div class="licenses">
			<h4 class="promote promote-bottom"><?= Yii::t('account', 'profile_license') ?></h4>
			<p class="upload-portfolio download-portfolio join"><?= Yii::t('account', 'profile_add_license') ?><a href="#" id="add_license"></a></p>
			<div class="all-licenses">
				<?php if($licenses){?>
					<?php foreach ($licenses as $license) {?>
						<?php $images = explode('|', $license['images'] ); ?>
						<div class="licenses-item" data-license-id="<?= $license['id'] ?>">
							<div class="clearfix"></div>
							<a href="#" class="del del-license"><?= Yii::t('account', 'button_delete') ?><i></i></a>
							<div class="license-images">
								<?php foreach ($images as $url) {?>
									<?php if($url != ""){?>
										<div class="licenses-img">
											<a href="#" class="del-licenses-img">X</a>
											<img src="<?= $url ?>" alt="">
										</div>
									<?php } ?>
								<?php } ?>
								<div class="licenses-img add add-licenses-img" ></div>
							</div>


							<form class="forma-input">
								<div class="input-container namb">
									<input class="text-input floating-label" type="text" name="number" value="<?= $license['number'] ?>" />
									<label for="number"><?= Yii::t('account', 'profile_license_number') ?></label>
								</div>
								<div class="input-container last">
									<input class="text-input floating-label" type="text" name="issued_by" value="<?= $license['issued_by'] ?>" />
									<label for="issued_by"><?= Yii::t('account', 'profile_license_issued') ?></label>
								</div>
							</form>
						</div>
					<?php } ?>
				<?php }else{ ?>

					<div class="licenses-item" data-license-id="">
						<div class="clearfix"></div>
						<a href="#" class="del del-license"><?= Yii::t('account', 'button_delete') ?><i></i></a>
						<div class="license-images">
							<div class="licenses-img add add-licenses-img" ></div>
						</div>

						<form class="forma-input">
							<div class="input-container namb">
								<input class="text-input floating-label" type="text" name="number" value="" />
								<label for="number"><?= Yii::t('account', 'profile_license_number') ?></label>
							</div>
							<div class="input-container last">
								<input class="text-input floating-label" type="text" name="issued_by" value=""/>
								<label for="issued_by"><?= Yii::t('account', 'profile_license_issued') ?></label>
							</div>
						</form>
					</div>

				<?php } ?>
			</div>

			<div class="block-button">
				<p class="valid" id="licenses-save-valid"></p>
				<p class="error" id="licenses-save-error">Changes not saved correctly!</p>
				<input type="submit" class="button" value="<?= Yii::t('account', 'button_save') ?>" id="licenses_save">
			</div>


		</div>
		<!-- End Licenses -->


	</div>


</div>
<!--END CENTER-->	




<?php
$this->registerJsFile('js/bootstrap.min.js', ['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('js/common.js', ['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('scripts/licenses_profile.js', ['depends'=>'frontend\assets\AppAsset']);
?><?php

