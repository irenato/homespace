<?php

use yii\widgets\LinkPager;
use yii\helpers\Url;

$this->title = Yii::t('titles', 'account') . Yii::t('titles', 'special_offers');
$this->params['breadcrumbs'][] = $this->title;

?>

<!--START CENTER-->
<div class="col-lg-7 central-content page-catalog page-in-catalog special-offers-catalog centralScroll">
    <div class="content page-offer_content">
        <div class="row">
            <div class="col-lg-12 catalog-column">
                <div class="sidebar-caption">Special offers<br></div>
                <div class="clearfix"></div>
                <div class="sel">
                    <div class="selects">
                        <p>Sort by</p>
                        <i class="icon-sort"></i>
                        <select class="sort">
                            <option class="hiden"></option>
                            <option value="likes">Most liked</option>
                            <option value="price">Price high to low</option>
                            <option value="price1">Price low to high</option>
                        </select>
                    </div>
                    <div class="selects">
                        <p>View by</p>
                        <i class="icon-view"></i>
                        <select class="view">
                            <option value="12">12</option>
                            <option value="8">8</option>
                            <option value="4">4</option>
                        </select>
                    </div>
                </div>
                <div class="clearfix"></div>
                <form class="forma-input">
                    <i class="icon-tag"></i>
                    <div class="input-container">
                        <input class="text-input floating-label" type="text" name="tags" value=""/>
                        <label for="sample">Tags</label>
                    </div>
                </form>
                <!-- <p class="upload-portfolio download-portfolio join">Add folder<a href="#"></a></p> -->
                <div class="clearfix"></div>
                <div class="catalog-box">
                    <?php if ($special_offers) : ?>
                        <?php foreach ($special_offers as $special_offer) : ?>
                            <div class="catalog-item">
                                <div class="catalog-img-box">
                                    <a href="<?= Url::to(['/about/product/', 'id' => $special_offer['id_prod']]) ?>">
                                        <img src="<?= Yii::getAlias('@products/' . $special_offer['main_image']) ?>" alt="">
                                    </a>
                                    <div class="limited-offer">
                                        <p><b><?= Yii::t('agent', 'limited_offer') ?></b>
                                            <br><?= Yii::t('agent', 'save') ?> <?= $special_offer['discount'] ?>%
                                        </p>
                                    </div>
                                    <div class="like-calc"><i class="icon-heart2971"></i><span class="count-likes"><?= $special_offer['likes'] ?></span></div>
                                    <div class="like-calc item-price">
                                        <i class="icon-label49"></i>
                                        <span><?= round($special_offer['price'] - (((int)$special_offer['discount'] / 100) * $special_offer['price'])) ?>
                                            $</span>
                                        <br>
                                        <p class="last-price"><?= (int)$special_offer['price'] ?>$</p>
                                    </div>
                                </div>
                                <div class="catalog-caption">
                                    <div class="social-cont">
                                        <a class="main-like icon-heart297 product-like" data-id="<?= $special_offer['id_prod'] ?>"></a>
                                    </div>
                                    <p><?= $special_offer['product_name'] ?></p>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    <?php endif; ?>
                    <!--                    <div class="catalog-item">-->
                    <!--                        <div class="catalog-img-box">-->
                    <!--                            <a href="">-->
                    <!--                                <img src="/images/catalog-img-7.jpg" alt="">-->
                    <!--                            </a>-->
                    <!--                            <div class="limited-offer">-->
                    <!--                                <p><b>Limited offer</b> <br>save 30%</p>-->
                    <!--                            </div>-->
                    <!--                            <div class="like-calc"><i class="icon-heart2971"></i><span>2</span></div>-->
                    <!--                            <div class="like-calc item-price">-->
                    <!--                                <i class="icon-label49"></i>-->
                    <!--                                <span>1800$</span><br>-->
                    <!--                                <p class="last-price">3200$</p>-->
                    <!--                            </div>-->
                    <!--                        </div>-->
                    <!--                        <div class="catalog-caption">-->
                    <!--                            <div class="social-cont">-->
                    <!--                                <a class="main-like icon-heart297"></a>-->
                    <!--                            </div>-->
                    <!--                            <p>Lorem ipsum Lorem ipsum Lorem ipsum<br>-->
                    <!--                                Lorem ipsum Lorem ipsum Lorem ipsum</p>-->
                    <!--                        </div>-->
                    <!--                    </div>-->
                    <!--                    <div class="catalog-item">-->
                    <!--                        <div class="catalog-img-box">-->
                    <!--                            <a href="">-->
                    <!--                                <img src="/images/catalog-img-2.jpg" alt="">-->
                    <!--                            </a>-->
                    <!--                            <div class="limited-offer">-->
                    <!--                                <p><b>Limited offer</b> <br>save 30%</p>-->
                    <!--                            </div>-->
                    <!--                            <div class="like-calc"><i class="icon-heart2971"></i><span>2</span></div>-->
                    <!--                            <div class="like-calc item-price">-->
                    <!--                                <i class="icon-label49"></i>-->
                    <!--                                <span>1800$</span><br>-->
                    <!--                                <p class="last-price">3200$</p>-->
                    <!--                            </div>-->
                    <!--                        </div>-->
                    <!--                        <div class="catalog-caption">-->
                    <!--                            <div class="social-cont">-->
                    <!--                                <a class="main-like icon-heart297"></a>-->
                    <!--                            </div>-->
                    <!--                            <p>Lorem ipsum Lorem ipsum Lorem ipsum<br>-->
                    <!--                                Lorem ipsum Lorem ipsum Lorem ipsum</p>-->
                    <!--                        </div>-->
                    <!--                    </div>-->
                    <!--                    <div class="catalog-item">-->
                    <!--                        <div class="catalog-img-box">-->
                    <!--                            <a href="">-->
                    <!--                                <img src="/images/mainPage-column-32.jpg" alt="">-->
                    <!--                            </a>-->
                    <!--                            <div class="limited-offer">-->
                    <!--                                <p><b>Limited offer</b> <br>save 30%</p>-->
                    <!--                            </div>-->
                    <!--                            <div class="like-calc"><i class="icon-heart2971"></i><span>2</span></div>-->
                    <!--                            <div class="like-calc item-price">-->
                    <!--                                <i class="icon-label49"></i>-->
                    <!--                                <span>1800$</span><br>-->
                    <!--                                <p class="last-price">3200$</p>-->
                    <!--                            </div>-->
                    <!--                        </div>-->
                    <!--                        <div class="catalog-caption">-->
                    <!--                            <div class="social-cont">-->
                    <!--                                <a class="main-like icon-heart297"></a>-->
                    <!--                            </div>-->
                    <!--                            <p>Lorem ipsum Lorem ipsum Lorem ipsum<br>-->
                    <!--                                Lorem ipsum Lorem ipsum Lorem ipsum</p>-->
                    <!--                        </div>-->
                    <!--                    </div>-->
                    <!--                    <div class="catalog-item">-->
                    <!--                        <div class="catalog-img-box">-->
                    <!--                            <a href="">-->
                    <!--                                <img src="/images/catalog-img-4.jpg" alt="">-->
                    <!--                            </a>-->
                    <!--                            <div class="limited-offer">-->
                    <!--                                <p><b>Limited offer</b> <br>save 30%</p>-->
                    <!--                            </div>-->
                    <!--                            <div class="like-calc"><i class="icon-heart2971"></i><span>2</span></div>-->
                    <!--                            <div class="like-calc item-price">-->
                    <!--                                <i class="icon-label49"></i>-->
                    <!--                                <span>1800$</span><br>-->
                    <!--                                <p class="last-price">3200$</p>-->
                    <!--                            </div>-->
                    <!--                        </div>-->
                    <!--                        <div class="catalog-caption">-->
                    <!--                            <div class="social-cont">-->
                    <!--                                <a class="main-like icon-heart297"></a>-->
                    <!--                            </div>-->
                    <!--                            <p>Lorem ipsum Lorem ipsum Lorem ipsum<br>-->
                    <!--                                Lorem ipsum Lorem ipsum Lorem ipsum</p>-->
                    <!--                        </div>-->
                    <!--                    </div>-->
                    <!--                    <div class="catalog-item">-->
                    <!--                        <div class="catalog-img-box">-->
                    <!--                            <a href="">-->
                    <!--                                <img src="/images/catalog-img-5.jpg" alt="">-->
                    <!--                            </a>-->
                    <!--                            <div class="limited-offer">-->
                    <!--                                <p><b>Limited offer</b> <br>save 30%</p>-->
                    <!--                            </div>-->
                    <!--                            <div class="like-calc"><i class="icon-heart2971"></i><span>2</span></div>-->
                    <!--                            <div class="like-calc item-price">-->
                    <!--                                <i class="icon-label49"></i>-->
                    <!--                                <span>1800$</span><br>-->
                    <!--                                <p class="last-price">3200$</p>-->
                    <!--                            </div>-->
                    <!--                        </div>-->
                    <!--                        <div class="catalog-caption">-->
                    <!--                            <div class="social-cont">-->
                    <!--                                <a class="main-like icon-heart297"></a>-->
                    <!--                            </div>-->
                    <!--                            <p>Lorem ipsum Lorem ipsum Lorem ipsum<br>-->
                    <!--                                Lorem ipsum Lorem ipsum Lorem ipsum</p>-->
                    <!--                        </div>-->
                    <!--                    </div>-->
                    <!--                    <div class="catalog-item">-->
                    <!--                        <div class="catalog-img-box">-->
                    <!--                            <a href="">-->
                    <!--                                <img src="/images/mainPage-column-32.jpg" alt="">-->
                    <!--                            </a>-->
                    <!--                            <div class="limited-offer">-->
                    <!--                                <p><b>Limited offer</b> <br>save 30%</p>-->
                    <!--                            </div>-->
                    <!--                            <div class="like-calc"><i class="icon-heart2971"></i><span>2</span></div>-->
                    <!--                            <div class="like-calc item-price">-->
                    <!--                                <i class="icon-label49"></i>-->
                    <!--                                <span>1800$</span><br>-->
                    <!--                                <p class="last-price">3200$</p>-->
                    <!--                            </div>-->
                    <!--                        </div>-->
                    <!--                        <div class="catalog-caption">-->
                    <!--                            <div class="social-cont">-->
                    <!--                                <a class="main-like icon-heart297"></a>-->
                    <!--                            </div>-->
                    <!--                            <p>Lorem ipsum Lorem ipsum Lorem ipsum<br>-->
                    <!--                                Lorem ipsum Lorem ipsum Lorem ipsum</p>-->
                    <!--                        </div>-->
                    <!--                    </div>-->
                    <!--                    <div class="catalog-item">-->
                    <!--                        <div class="catalog-img-box">-->
                    <!--                            <a href="">-->
                    <!--                                <img src="/images/catalog-img-1.jpg" alt="">-->
                    <!--                            </a>-->
                    <!--                            <div class="limited-offer">-->
                    <!--                                <p><b>Limited offer</b> <br>save 30%</p>-->
                    <!--                            </div>-->
                    <!--                            <div class="like-calc"><i class="icon-heart2971"></i><span>2</span></div>-->
                    <!--                            <div class="like-calc item-price">-->
                    <!--                                <i class="icon-label49"></i>-->
                    <!--                                <span>1800$</span><br>-->
                    <!--                                <p class="last-price">3200$</p>-->
                    <!--                            </div>-->
                    <!--                        </div>-->
                    <!--                        <div class="catalog-caption">-->
                    <!--                            <div class="social-cont">-->
                    <!--                                <a class="main-like icon-heart297"></a>-->
                    <!--                            </div>-->
                    <!--                            <p>Lorem ipsum Lorem ipsum Lorem ipsum<br>-->
                    <!--                                Lorem ipsum Lorem ipsum Lorem ipsum</p>-->
                    <!--                        </div>-->
                    <!--                    </div>-->
                    <!--                    <div class="catalog-item">-->
                    <!--                        <div class="catalog-img-box">-->
                    <!--                            <a href="">-->
                    <!--                                <img src="/images/mainPage-column-31.jpg" alt="">-->
                    <!--                            </a>-->
                    <!--                            <div class="limited-offer">-->
                    <!--                                <p><b>Limited offer</b> <br>save 30%</p>-->
                    <!--                            </div>-->
                    <!--                            <div class="like-calc"><i class="icon-heart2971"></i><span>2</span></div>-->
                    <!--                            <div class="like-calc item-price">-->
                    <!--                                <i class="icon-label49"></i>-->
                    <!--                                <span>1800$</span><br>-->
                    <!--                                <p class="last-price">3200$</p>-->
                    <!--                            </div>-->
                    <!--                        </div>-->
                    <!--                        <div class="catalog-caption">-->
                    <!--                            <div class="social-cont">-->
                    <!--                                <a class="main-like icon-heart297"></a>-->
                    <!--                            </div>-->
                    <!--                            <p>Lorem ipsum Lorem ipsum Lorem ipsum<br>-->
                    <!--                                Lorem ipsum Lorem ipsum Lorem ipsum</p>-->
                    <!--                        </div>-->
                    <!--                    </div>-->
                    <!--                    <div class="catalog-item">-->
                    <!--                        <div class="catalog-img-box">-->
                    <!--                            <a href="">-->
                    <!--                                <img src="/images/catalog-img-6.jpg" alt="">-->
                    <!--                            </a>-->
                    <!--                            <div class="limited-offer">-->
                    <!--                                <p><b>Limited offer</b> <br>save 30%</p>-->
                    <!--                            </div>-->
                    <!--                            <div class="like-calc"><i class="icon-heart2971"></i><span>2</span></div>-->
                    <!--                            <div class="like-calc item-price">-->
                    <!--                                <i class="icon-label49"></i>-->
                    <!--                                <span>1800$</span><br>-->
                    <!--                                <p class="last-price">3200$</p>-->
                    <!--                            </div>-->
                    <!--                        </div>-->
                    <!--                        <div class="catalog-caption">-->
                    <!--                            <div class="social-cont">-->
                    <!--                                <a class="main-like icon-heart297"></a>-->
                    <!--                            </div>-->
                    <!--                            <p>Lorem ipsum Lorem ipsum Lorem ipsum<br>-->
                    <!--                                Lorem ipsum Lorem ipsum Lorem ipsum</p>-->
                    <!--                        </div>-->
                    <!--                    </div>-->
                    <div class="bread-crumbs">
                        <?= LinkPager::widget(['pagination' => $pagination,
                            'disabledPageCssClass' => false,
                            'nextPageLabel' => '',
                            'prevPageLabel' => '',
                            'options' => ['class' => 'hvr-radial-out1, products-pagination'],
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <?= frontend\widgets\Banner::widget(['position' => 'bottom']); ?>
    </div>
</div>
<!--END CENTER-->

<?php
$this->registerJsFile('js/bootstrap.min.js', ['depends' => 'frontend\assets\AppAsset']);
$this->registerJsFile('js/owl.carousel.min.js', ['depends' => 'frontend\assets\AppAsset']);
$this->registerJsFile('js/common.js', ['depends' => 'frontend\assets\AppAsset']);
$this->registerJsFile('agent_js/special_offers.js', ['depends' => 'frontend\assets\AppAsset']);
?>
