<?php
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;
use yii\widgets\LinkPager;

$this->title = Yii::t('titles', 'account').' - '.$catalog->title;
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="col-lg-7 central-content page-catalog page-in-catalog centralScroll">
        <div class="content script">
            <div class="row groups-instruments-column">
                <div class="col-lg-12 catalog-column">
                    <h4 class="sidebar-caption"><?= $catalog->title; ?><br></h4>
                    <div class="sel group-sort-instruments">
                        <div class="selects" name="sort">
                            <p><?= Yii::t('account', 'sort_by') ?></p>
                            <i class="icon-sort"></i>
                            <select class="sort sort-groups">
                                <option class="hiden"></option>
                                <option class="sort_opt" value="like" ><?= Yii::t('account', 'most_liked') ?></option>
                                <option class="sort_opt" value="expensive"><?= Yii::t('account', 'high_price') ?></option>
                                <option class="sort_opt" value="chip"><?= Yii::t('account', 'low_price') ?></option>
                            </select>
                        </div>
                        <div class="selects" name="count">
                            <p><?= Yii::t('account', 'view_by') ?></p>
                            <i class="icon-view"></i>
                            <select class="view">
                                <option value="12" >12</option>
                                <option value="8" >8</option>
                                <option value="4" >4</option>
                            </select>
                        </div>
                        <div class="forma-input catalog-in-tags">
                            <i class="icon-tag"></i>
                            <div class="input-container">
                                <input id="tag_field" class="text-input floating-label" type="text" name="sample" value=""/>
                                <label for="sample"><?= Yii::t('main', 'tags') ?></label>
                            </div>
                        </div>
                    </div>
                    <!--                <p class="upload-portfolio download-portfolio join">Add folder<a href="#"></a></p>-->
                    <div class="clearfix"></div>
                    <?php Pjax::begin(); ?>
                    <div class="catalog-box">
                        <?php $form = ActiveForm::begin([
                            'id' => 'productslist-form',
                            'action' => ['catalog', 'id' => $catalog->id_catalog],
                            'method' => 'get',
                            'options' => ['data-pjax' => TRUE]
                        ]); ?>
                        <?= $form->field($products_list, 'sort')->hiddenInput()->label(FALSE); ?>
                        <?= $form->field($products_list, 'count')->hiddenInput()->label(FALSE); ?>
                        <?= $form->field($products_list, 'meta_tags')->hiddenInput()->label(FALSE); ?>
                        <?php ActiveForm::end(); ?>
                        <?php
                        // print_r($prod);
                        foreach($data->models as $prod){
                            ?>
                            <div class="catalog-item">
                                <div class="catalog-img-box">
                                    <a href="">
                                        <img src="<?= Yii::getAlias('@products/'.$prod['main_image'] )?>" alt=""
                                             id_prod="<?= $prod['id_prod'] ?>">
                                    </a>
                                    <p id_prod="<?= $prod['id_prod'] ?>" class="add-order" data-toggle="modal" data-target=".add-order-model"><i class="icon-books72"></i><?= Yii::t('account', 'order') ?></p>
                                    <div class="like-calc product-likes"><i
                                            class="icon-heart2971"></i><span likes_id="<?= $prod['id_prod'] ?>" class="prod-likes-count"><?= $prod['likes'] ?></span></div>
                                    <div class="like-calc item-price"><i
                                            class="icon-label49"></i><span><?= $prod['price'] ?>$</span></div>
                                </div>
                                <div class="catalog-caption">
                                    <div class="social-cont">
                                        <a id="<?= $prod['id_prod'] ?>" class="main-like icon-heart297"></a>

                                    </div>
                                    <p><?= $prod['product_name'] ?><br>
                                        <?= $prod['meta_tags'] ?></p>
                                </div>
                            </div>
                        <?php } ?>

                    </div>
                    <div class="bread-crumbs">
                        <?= LinkPager::widget(['pagination' => $data->pagination,
                            'disabledPageCssClass' => false,
                            'nextPageLabel' => '',
                            'prevPageLabel' => '',
                            'options'=>['class'=>'hvr-radial-out'],
                        ]); ?>
                    </div>
                    <?php Pjax::end(); ?>
                </div>
            </div>
        </div>
        <div class="row">
            <?= frontend\widgets\Banner::widget(['position' => 'bottom']);?>
        </div>
    </div>
<?php $this->registerJsFile('js/setOptions.js', ['depends' => 'frontend\assets\AppAsset']); ?>
<?php $this->registerJsFile('js/common.js', ['depends' => 'frontend\assets\AppAsset']); ?>

