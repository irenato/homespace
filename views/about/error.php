<?php



use frontend\widgets\Banner;
use yii\bootstrap\ActiveForm;
use common\models\MessageForm;
use frontend\widgets\MessageWidget;
use yii\helpers\Html;
use \yii\bootstrap\Modal;
use yii\base\Event;

$this->title = Yii::t('titles', 'about');
$this->params['breadcrumbs'][] = $this->title;


?>

    <!--START CONTENT-->
    <section xmlns="http://www.w3.org/1999/html">
        <div class="container">
            <div class="row">
                <!--                		--><?php //var_dump($user_information)?>
                <!--START CENTER-->
                <div class="col-lg-10 profile-content b2 central-content">
                    <div class="content">
                        <h3><?= $error ?></h3>
                    </div>
                </div>
                <!--END CENTER-->

                <!--START RIGHT SIDEBAR-->
                <div class="col-lg-2 right-sidebar">
                    <span class="sidebar-caption">Widgets</span>
                    <div class="banners">
                        <div class="widgets">
                            <img src="/images/widgets.png" alt="">
                            <img src="/images/widgets-2.png" alt="">
                        </div>
                        <span class="sidebar-caption ">Advertising</span>
                        <?= Banner::widget();?>
                    </div>
                </div>
                <!--END RIGHT SIDEBAR-->
            </div>
        </div>
    </section>
    <!--END CONTENT-->
    <!--START CHAT-->
    <div class="chat-block">
        <div class="chat">
<!--            <div class="name" id="--><?//= $user_information['id'] ?><!--">--><?//= $user_information['first_name']." ".$user_information['last_name']?><!--</div>-->
<!--            <a href="#" class="roll-up"><i class="icon-11"></i></a>-->
<!--            <a href="#" class="close"><i class="icon-cancel30"></i></a>-->
<!--            <div class="message-content"  id="container">-->
<!--                <p class="data">--><?//= date("Y-m-d") ?><!--</p>-->
                <!--				<div class="player-1">-->
                <!--					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin</p>-->
                <!--					<i class="icon-user168"></i>-->
                <!--				</div>-->
                <!--				<div class="player-2">-->
                <!--					<img src="images/team1.png" alt="">-->
                <!--					<p>Lorem ipsum dolor sit amet, consectetur adi`piscing elit. Proin urna ipsum dolor sit</p>-->
                <!--				</div>-->
                <!--				<div class="player-1">-->
                <!--					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin</p>-->
                <!--					<i class="icon-user168"></i>-->
                <!--				</div>-->
                <!--				<p class="data">10. 01. 2016</p>-->
                <!--				<div class="player-2">-->
                <!--					<img src="images/team1.png" alt="">-->
                <!--					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin urna ipsum dolor sit</p>-->
                <!--				</div>-->
                <!--				<div class="player-1">-->
                <!--					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin</p>-->
                <!--					<img src="images/team1.png" alt="">-->
                <!--				</div>-->
                <!--				<div class="player-2">-->
                <!--					<img src="images/team1.png" alt="">-->
                <!--					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin urna ipsum dolor sit</p>-->
                <!--				</div>-->
            </div>
            <p class="send"><textarea class="message-input" placeholder="Type message..." ></textarea></p>
        </div>
    </div>
    <!--END CHAT-->
    <!--Вызов модального окна для переписки-->
<?//= frontend\widgets\MessageWidget::widget() ?>


<?php
$this->registerJsFile('js/bootstrap.min.js', ['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('js/owl.carousel.min.js', ['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('js/common.js', ['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('js/new-js.js', ['depends'=>'frontend\assets\AppAsset']);
//$this->registerJsFile('scripts/common.js', ['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('js/jscript.js', ['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('js/action.js', ['depends'=>'frontend\assets\AppAsset']);

?>