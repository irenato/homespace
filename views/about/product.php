<?php

use yii\widgets\LinkPager;
use frontend\widgets\Banner;
use yii\helpers\Url;
use frontend\widgets\SideBar;

$this->title = ' | ' . $product['product_name'];
$this->params['breadcrumbs'][] = $this->title;


?>

    <section class="catalog-id" data-id="2">
        <div class="container scrollSidebar">
            <div class="row">
                <!--START LEFT SIDEBAR-->
                <div class="col-lg-3 left-sidebar">
                    <span class="sidebar-caption"><?= Yii::t('account', 'manage_account') ?></span>
                    <?= SideBar::widget(); ?>

                </div>
                <!--END LEFT SIDEBAR-->
                <!--START CENTER-->
                <div class="col-lg-7 central-content page-catalog centralScroll">
                    <div class="content">
                        <div class="row">
                            <div class="col-lg-12 clear">
                                <?php if ($product): ?>
                                    <div class="img-gal">
                                        <div class="featured">
                                            <img src="<?= Yii::getAlias('@products/' . $product['main_image']) ?>"
                                                 alt="">
                                        </div>
                                        <?php $images = explode(",", $product['images']) ?>
                                        <div class="mini-photo thumbnails">
                                            <img src="<?= Yii::getAlias('@products/' . $product['main_image']) ?>"
                                                 alt=""
                                                 class="selected-img">
                                            <?php foreach ($images as $img): ?>
                                                <?php if ($img != ""): ?>
                                                    <img src="<?= Yii::getAlias('@products/' . $img) ?>" alt=""
                                                         class="selected-img">
                                                <?php endif; ?>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                    <div class="produkt-content">
                                        <img src="/media/upload/<?= $company['logo'] ?>" alt="" class="logo">
                                        <?php $company_name = explode('||', $company['company']); ?>
                                        <h4 class="name"><?= $company_name[0] ?></h4>
                                        <h5 class="collection">Collection: <span><?= $collection[$lang] ?></span></h5>
                                        <h4 class="name"><?= $product['product_name'] ?></h4>
                                        <i class="price icon-label49"><?= ($product['discount'] != 0) ? round($product['price'] - (((int)$product['discount'] / 100) * $product['price'])) : $product['price'] ?>
                                            $
                                            <?php if ($product['stock'] == 1) : ?>
                                                <span><?= $product['price'] ?>$</span>
                                            <?php endif; ?>
                                        </i>
                                        <div class="clearfix"></div>
                                        <div class="social-cont">
                                            <?php if (\Yii::$app->user->isGuest) : ?>
                                                <a class="main-like icon-heart297 product-like <?= $liked == 'like' ? 'active-social-button' : '' ?>"
                                                   data-toggle="modal" data-target="#login"></a>
                                            <?php else : ?>
                                                <a class="main-like icon-heart297 product-like <?= $liked == 'like' ? 'active-social-button' : '' ?>"
                                                   data-action="single-product" <?php if (\Yii::$app->user->isGuest) {
                                                    echo "data-toggle='modal' data-target='#login'";
                                                } ?>
                                                   data-id="<?= $product['id_prod'] ?>"></a>
                                            <?php endif; ?>
                                            <span><?= $product['likes'] ?></span>
                                            <a class="share-it-now-tw"
                                               href="https://twitter.com/intent/tweet?text=<?= $product['product_name'] ?>&url=<?= Yii::$app->urlManager->createAbsoluteUrl(['about/product/' . $product['id_prod']]) ?>"></a>
                                            <a class="share-it-now-fb" href="#" data-id="<?= $product['id_prod'] ?>"
                                               data-type="2"
                                               data-url="<?= Yii::$app->urlManager->createAbsoluteUrl(['about/product/' . $product['id_prod']]) ?>"
                                               data-image="<?= Url::home(true) . 'media/products/' . $product['main_image'] ?>"
                                               data-description="<?= $product['product_name'] ?>"></a>
                                        </div>

                                        <div class="hashtag">
                                            <?php $tags = explode(',', $product['meta_tags']); ?>
                                            <?php if ($tags) : ?>
                                                <?php foreach ($tags as $tag) : ?>
                                                    <a href="/catalog/products?tags=<?= $tag ?>"><?= $tag ?></a>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                        </div>
                                        <p class="text"><?= $product['prod_desc'] ?></p>
                                        <a href="#" class="button order add-to-my-projects"
                                           data-id=<?= $product['id_prod'] ?>><i class="icon-books72"></i>Add to
                                            order</a>

                                        <hr class="sline">
                                    </div>
                                    <div class="produkt-comment">
                                        <ul role="tablist">
                                            <li role="presentation" class="active"><a href="#Comments"
                                                                                      aria-controls="Comments"
                                                                                      role="tab" data-toggle="tab">Comments</a>
                                            </li>
                                            <li role="presentation"><a href="#Facebook" aria-controls="Facebook"
                                                                       role="tab"
                                                                       data-toggle="tab">Facebook</a></li>
                                        </ul>

                                        <div class="tab-content">
                                            <div role="tabpanel" class="tab-pane active" id="Comments">
                                                <textarea name="#" placeholder="Your comment..."></textarea>
                                                <input type="submit" class="review-for-products"
                                                       data-id="<?= $product['id_prod'] ?>" value="Sent">
                                                <hr class="sline">
                                                <?php if ($comments) : ?>
                                                    <?php foreach ($comments as $comment) : ?>
                                                        <div class="reviews-comment">
                                                            <div class="reviews-photo">
                                                                <img
                                                                    src="<?= Yii::getAlias('@avatar/' . $comment->user->avatar) ?>"
                                                                    alt="">
                                                                <p class="name"><?= $comment->user->first_name . ' ' . $comment->user->last_name ?>
                                                                    <span><?= ucfirst($comment->user->roles) ?></span>
                                                                </p>
                                                            </div>
                                                            <p><?= nl2br($comment->review_text); ?></p>
                                                            <p class="data"><?= date('d.m.Y', $comment->date) ?></p>
                                                        </div>
                                                    <?php endforeach; ?>
                                                <?php endif; ?>
                                                <?= LinkPager::widget(['pagination' => $pagination,
                                                    'disabledPageCssClass' => false,
                                                    'nextPageLabel' => '',
                                                    'prevPageLabel' => '',
                                                    'options' => ['class' => 'hvr-radial-out1, agent-comp-pagination'],
                                                ]); ?>
                                            </div>
                                            <div role="tabpanel" class="tab-pane" id="Facebook">
                                                <div class="fb-comments"
                                                     data-href="<?= Yii::$app->urlManager->createAbsoluteUrl(['about/product/' . $product['id_prod']]) ?>"
                                                     data-numposts="5" data-width="100%"></div>
                                            </div>

                                            <div class="showrooms">
                                                <h4>Suppliers & Showrooms</h4>
                                                <a href="#"
                                                   style="background-image: url(<?= Yii::getAlias('@products/2011-0705-moooi-amsterdam-store-02.jpg') ?>)"></a>
                                                <a href="#"
                                                   style="background-image: url(<?= Yii::getAlias('@products/Moooi+black+chairs.jpg') ?>)"></a>
                                                <a href="#"
                                                   style="background-image: url(<?= Yii::getAlias('@products/moooi_460_horiz_last.jpg') ?>)"></a>
                                            </div>

                                        </div>
                                    </div>
                                <?php else: ?>
                                    <h3>Not found</h3>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <?= frontend\widgets\Banner::widget(['position' => 'bottom']); ?>
                </div>
                <!--END CENTER-->
                <!--START RIGHT SIDEBAR-->
                <div class="col-lg-2 right-sidebar">
                    <?= Banner::widget(['position' => 'right']); ?>
                </div>
                <!--END RIGHT SIDEBAR-->
            </div>
        </div>
    </section>
    <!--END CONTENT-->

<?php
$this->registerJsFile('js/bootstrap.min.js', ['depends' => 'frontend\assets\AppAsset']);
$this->registerJsFile('js/owl.carousel.min.js', ['depends' => 'frontend\assets\AppAsset']);
$this->registerJsFile('js/jscript.js', ['depends' => 'frontend\assets\AppAsset']);
$this->registerJsFile('js/action.js', ['depends' => 'frontend\assets\AppAsset']);
$this->registerJsFile('js/fb_likes.js', ['depends' => 'frontend\assets\AppAsset']);
$this->registerJsFile('js/catalog.js', ['depends' => 'frontend\assets\AppAsset']);
$script_product = <<< JS
	$('.thumbnails img').click(function() {
			$(".featured img").fadeOut(0);
			$('.featured img').attr('src', $(this).attr('src')).fadeIn(500);
	});
JS;
$this->registerJs($script_product, yii\web\View::POS_READY);

?>