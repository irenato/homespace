<?php

/* @var $this yii\web\View */

$this->title = ' | Demo';
$this->params['breadcrumbs'][] = $this->title;
?>
<!--START CONTENT-->
<section>
    <div class="row">
        <img src="/images/lk_profile.png">
    </div>
</section>
<!--END CONTENT-->
<?php
$script_slider = <<< JS
    $("section").click(function(e){
       $('.how_it_words').modal('show');
    });
JS;
$this->registerJs($script_slider, yii\web\View::POS_READY);
?>