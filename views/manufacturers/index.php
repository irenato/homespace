<?php

use yii\widgets\LinkPager;
use frontend\widgets\Banner;
use frontend\widgets\SideBar;
use yii\helpers\Url;

$this->title = 'Manufacturers';
$this->params['breadcrumbs'][] = $this->title;
?>


    <!--START CONTENT-->
    <section class="catalog-id" data-id="1">
        <div class="container container-style-catalog scrollSidebar1">
            <div class="row">

                <!--START CENTER-->
                <?php if (!Yii::$app->user->isGuest): ?>
                <style>
                    .left-sidebar {
                        padding-left: 0;
                        padding-top: 0;

                    }
                </style>
                <div class="col-lg-3 left-sidebar">
                    <span class="sidebar-caption"><?= Yii::t('account', 'manage_account') ?></span>
                    <?= SideBar::widget(); ?>
                </div>
                <div class="col-lg-9 catalog-column profile-agent-comps centralScroll1">
                    <?php else: ?>
                    <div class="col-lg-12 catalog-column profile-agent-comps">
                        <?php endif; ?>

                        <div class="row">
                            <div class="content">
                                <div class="portfolio-top profile-conten-me">
                                    <div class="catalog-box special-offers-catalog page-in-catalog">
                                        <div class="clearfix"></div>
                                        <h4 class="promote promote-bottom">Manufacturers</h4>
                                        <?php if (Yii::$app->user->identity->roles == 'agent'): ?>
                                            <p class="upload-portfolio download-portfolio join">Add<a
                                                    href="<?= Yii::$app->urlManager->createUrl(['account/advertising-tools/create']) ?>"></a>
                                            </p>
                                        <?php endif; ?>
                                        <div class="clearfix"></div>
                                        <form class="forma-input">
                                            <div class="search">
                                                <div class="input-edit">
                                                    <div class="input-container">
                                                        <input id="manufacturer_name" class="text-input floating-label <?= $_SESSION['manufacturer_name'] ? 'dirty' : ''?>"
                                                               name="sample1"
                                                               type="text" value="<?= $_SESSION['manufacturer_name'] ? $_SESSION['manufacturer_name'] : ''?>">
                                                        <label for="sample1">Search</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="selects">
                                                <select name="choose-category" class="choose-category">
                                                    <option
                                                        value="All"
                                                        <?= !$current_category ? 'selected' : '' ?>
                                                    >All</option>
                                                    <?php foreach ($categories as $category) : ?>
                                                        <option
                                                            value="<?= $category[$language] ?>"
                                                            <?= $category[$language] == $current_category ? 'selected' : '' ?>
                                                        ><?= $category[$language] ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                            <div class="selects">
                                                <select name="view" class="view-by">
                                                    <?php $values = array(6, 9, 12, 24);
                                                    foreach ($values as $value) : ?>
                                                        <option <?= $value == $page_size ? 'selected' : ''; ?> ><?= $value ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </form>
                                        <?php if ($companies) : ?>
                                            <div class="companys">
                                                <?php foreach ($companies as $company): ?>
                                                    <div class="company"
                                                         style="background: url('/media/upload/<?= $company['background'] ?>')">
                                                        <?php $company_attr = explode("||", $company['company']); ?>
                                                        <a href="<?= Yii::$app->urlManager->createUrl(['manufacturers/profile', 'id' => $company['id']]) ?>">
                                                            <img class="company-img"
                                                                 src="<?= '/media/upload/' . $company['logo'] ?>"
                                                                 alt="#"></a>
                                                        <div class="name-company">
                                                            <h5><?= $company_attr[0] ?> <span>(<?= $company_attr[1] ?>
                                                                    )</span></h5>
                                                            <p>made in <span><?= $company['country'] ?></span></p>
                                                        </div>
                                                        <ul class="categories">
                                                            <li><a href="#">Categories:</a></li>
                                                            <?php if ($company['collections']) : ?>
                                                                <?php $collections = explode(',', $company['collections']['collection']) ?>
                                                                <?php while ($collections) :
                                                                    $collection = array_shift($collections); ?>
                                                                    <li>
                                                                        <a href="#"><?= $collection ?></a>
                                                                    </li>
                                                                <?php endwhile; ?>
                                                            <?php endif; ?>
                                                        </ul>
                                                        <ul class="description">
                                                            <li><?= Yii::t('agent', 'products') ?>:
                                                                <span><?= $company['products_count'] ?></span></li>
                                                            <li><?= Yii::t('agent', 'new') ?>: <span><?= $company['new_products'] ?></span></li>
                                                            <li><?= Yii::t('agent', 'special_offers') ?>: <span><?= $company['so_count'] ?></span></li>
                                                            <li>Sale: <span><?= $company['discounts']['discount'] ? $company['discounts']['discount'] : 0 ?>%</span></li>
                                                        </ul>
                                                        <div class="author">
                                                            <img src="<?= '/media/upload/' . $company['avatar'] ?>"
                                                                 alt="#">
                                                            <p class="name"><?= $company['first_name'] . ' ' . $company['last_name'] ?></p>
                                                        </div>
                                                    </div>

                                                <?php endforeach; ?>
                                            </div>

                                            <div class="clearfix"></div>
                                            <div class="bread-crumbs">
                                                <?php if ($pagination) : ?>
                                                    <?= LinkPager::widget(['pagination' => $pagination,
                                                        'disabledPageCssClass' => false,
                                                        'nextPageLabel' => '',
                                                        'prevPageLabel' => '',
                                                        'options' => ['class' => 'hvr-radial-out1, agent-comp-pagination'],
                                                    ]); ?>
                                                <?php endif; ?>
                                            </div>

                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--END CENTER-->
                </div>
    </section>
    <!--END CONTENT-->
<?php
$session = Yii::$app->session;
if ($session->has('manufacturer_name'))
    $session->remove('manufacturer_name'); ?>
<?php
$this->registerJsFile('js/manufacturers.js', ['depends' => 'frontend\assets\AppAsset']);