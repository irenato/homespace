<?php

use frontend\widgets\Banner;
use yii\widgets\LinkPager;
use frontend\widgets\SideBar;

$this->title = Yii::t('titles', 'profile').$user_information['first_name'].' '.$user_information['last_name'].')';
$this->params['breadcrumbs'][] = $this->title;


?>
<!--START CONTENT-->
<section>
    <div class="container scrollSidebar">
        <div class="row">
            <!--START CENTER-->
            <div class="col-lg-3 left-sidebar">
                <span class="sidebar-caption"><?= Yii::t('account', 'manage_account') ?></span>
                <?= SideBar::widget();?>
            </div>
            <div class="col-lg-9 profile-content b2 central-content centralScroll">
                <div class="content profile-agent overall-profile-agent pf-new" data-user="<?= $user_information['id'] ?>">
                    <div class="agent-profil" style="background: url('<?= Yii::getAlias('@background/' . $user_information['background']) ?>')">
                        <div class="clearfix"></div>
                        <div class="agents">
                            <img src="<?= Yii::getAlias('@avatar/' . $user_information['logo']) ?>" alt="logo" class="logos">
                            <div class="agent">
                                <img src="<?= Yii::getAlias('@avatar/' . $user_information['avatar']) ?>" alt="avatar" class="photo">
                                <h5 class="name"><?= $user_information['first_name'] ?> <?= $user_information['last_name'] ?></h5>
                                <p><?= $user_information['specialization'] ?></p>
                            </div>
                            <div class="description">
                                <ul>
                                    <li><a href="#"><i class="icon-follow1"></i> 3859 Followers </a></li>
                                    <li><a href="#"><i class="icon-home"></i><?= $user_information['country'] . ', ' . $user_information['city'] ?></a></li>
                                    <li><a href="#"><i class="icon-follow"></i> 3859 Following </a></li>
                                    <li><a href="#"><i class="icon-location"></i><?= $user_information['address'] ?></a></li>
                                    <li><a href="#"><i class="icon-telephone46"></i><?= $user_information['phone'] ?></a></li>
                                    <li><a href="#"><i class="icon-webpage2"></i><?= $user_information['link_user_site'] ?></a></li>
                                </ul>
                            </div>
                            <ul class="soc">
                                <?php if ($user_information['link_fb'] != "") { ?>
                                    <li><a href="//<?= $user_information['link_fb'] ?>" target="_blank"><i
                                                class="icon-facebook55"></i></a></li>
                                <?php } ?>
                                <?php if ($user_information['link_tw'] != "") { ?>
                                    <li><a href="//<?= $user_information['link_tw'] ?>" target="_blank"><i
                                                class="icon-twitter1"></i></a></li>
                                <?php } ?>
                                <?php if ($user_information['link_inst'] != "") { ?>
                                    <li><a href="//<?= $user_information['link_inst'] ?>" target="_blank"><i
                                                class="icon-instagram12"></i></a></li>
                                <?php } ?>
                                <?php if ($user_information['link_behance'] != "") { ?>
                                    <li><a href="//<?= $user_information['link_behance'] ?>" target="_blank"><i
                                                class="icon-behance2"></i></a></li>
                                <?php } ?>
                                <?php if ($user_information['link_google'] != "") { ?>
                                    <li><a href="//<?= $user_information['link_google'] ?>" target="_blank"><i
                                                class="icon-google116"></i></a></li>
                                <?php } ?>
                                <?php if ($user_information['link_pint'] != "") { ?>
                                    <li><a href="//<?= $user_information['link_pint'] ?>" target="_blank"><i
                                                class="icon-pinterest3"></i></a></li>
                                <?php } ?>
                                <?php if ($user_information['link_tumblr'] != "") { ?>
                                    <li><a href="//<?= $user_information['link_tumblr'] ?>" target="_blank"><i
                                                class="icon-logotype1"></i></a></li>
                                <?php } ?>
                                <?php if ($user_information['link_linkedin'] != "") { ?>
                                    <li><a href="//<?= $user_information['link_linkedin'] ?>" target="_blank"><i
                                                class="icon-id16"></i></a></li>
                                <?php } ?>
                                <?php if ($user_information['link_blg'] != "") { ?>
                                    <li><a href="//<?= $user_information['link_blg'] ?>" target="_blank"><i
                                                class="icon-blogger8"></i></a></li>
                                <?php } ?>
                            </ul>

                        </div>
                        <div class="agent-text">
                            <p><?= $user_information['message'] ?></p>
                        </div>
                        <?php if($click_action){ ?>
                            <div class="message send-chat-message">
                                <i class="icon-black218"></i><a href="#" class="message"><?= Yii::t('account', 'profile_message') ?></a>
                            </div>
                        <?php } ?>
                        <ul class="mesages">
                            <li>
                                <a href="#"><img src="/images/img-2.png" alt="#"> Sam Clarintence
                                    <span>(consultant of bathrooms)</span></a><i class="icon-black218"> Message</i></li>
                            <li>
                                <a href="#"><img src="/images/img-2.png" alt="#"> Sam Clarintence
                                    <span>(consultant of bathrooms)</span></a><i class="icon-black218"> Message</i></li>
                            <li>
                                <a href="#"><img src="/images/img-2.png" alt="#"> Sam Clarintence
                                    <span>(consultant of bathrooms)</span></a><i class="icon-black218 "> Message</i></li>
                        </ul>
                        <?php if ($videos): ?>
                            <div class="video">
                                <iframe width="466px" height="260" src="<?= $videos[0]['link'] ?>" frameborder="0"
                                        allowfullscreen></iframe>
                            </div>
                        <?php endif; ?>
                    </div>
                    <div class="showrooms">
                        <h4 class="promote promote-bottom"> Showrooms</h4>
                        <div class="hashtag">
                            <a href="">#lorem</a>
                            <a href=""> #ipsum</a>
                            <a href=""> #dolor</a>
                            <a href=""> #sit</a>
                            <a href=""> #consectetuer</a>
                            <a href=""> #adipiscing</a>
                        </div>
                        <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit . Mauris aliquet lacus lectus, a mollis nunc
                            interdum eu . Proin vitae maximus elit, non molestie est . Aliquam at tortor aliquam, tincidunt sem
                            sit amet, suscipit erat . Integer sed dapibus metus . Nullam fringilla velit ut porttitor...</p>
                        <div class="catalog-box">
                            <div class="clearfix"></div>
                            <div class="catalog-item">
                                <div class="catalog-img-box">
                                    <a href="">
                                        <img src="/images/mainPage-column-31.jpg" alt="">
                                    </a>
                                </div>
                                <div class="catalog-caption">
                                    <p> Lorem ipsum Lorem ipsum Lorem ipsum
                                        &lt; br&gt; Lorem ipsum Lorem ipsum Lorem ipsum </p>
                                </div>
                            </div>
                            <div class="catalog-item">
                                <div class="catalog-img-box">
                                    <a href="">
                                        <img src="/images/mainPage-column-32.jpg" alt="">
                                    </a>
                                </div>
                                <div class="catalog-caption">
                                    <p> Lorem ipsum Lorem ipsum Lorem ipsum
                                        &lt; br&gt; Lorem ipsum Lorem ipsum Lorem ipsum </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr class="sline">

                    <!-- Reviews -->
                    <div class="reviews">
                        <?php if($reviews['reviews'] || $click_action):?>
                            <h4 class="promote"><?= Yii::t('main', 'reviews') ?></h4>
                        <?php endif; ?>
                        <div class="reviews-block">
                            <?php if($reviews['reviews']){?>
                                <?php $first = array_shift($reviews['reviews']); ?>
                                <div class="reviews-comment">
                                    <div class="reviews-photo new-review-photo">
                                        <a href="<?= $first['link'] ?>"><img src="<?= Yii::getAlias('@avatar/'. $first['image']); ?>" alt=""></a>
                                        <p class="name"><?= $first['author'] ?></p>
                                    </div>
                                    <p><?= $first['comment'] ?></p>
                                    <p class="data"><?= $first['date'] ?></p>
                                </div>
                                <i class="icon-arrow487 oppen-comments" style="cursor: pointer;"></i>
                                <div class="all-reviews-comment">
                                    <?php foreach($reviews['reviews'] as $review) : ?>
                                        <div class="reviews-comment">
                                            <div class="reviews-photo new-review-photo">
                                                <a href="<?= $review['link'] ?>"><img src="<?= Yii::getAlias('@avatar/'. $review['image']); ?>" alt=""></a>
                                                <p class="name"><?= $review['author'] ?></p>
                                            </div>
                                            <p><?= $review['comment'] ?></p>
                                            <p class="data"><?= $review['date'] ?></p>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                                <br>
                                <div class="bread-crumbs">
                                    <?= LinkPager::widget([
                                        'pagination' => $reviews['pagination'],
                                        'disabledPageCssClass' => false,
                                        'nextPageLabel' => '',
                                        'prevPageLabel' => '',
                                        'options'=>['class'=>'hvr-radial-out'], ]) ?>
                                </div>
                                <br>
                            <?php } ?>
                        </div>
                        <div class="clearfix"></div>
                        <?php if($click_action){ ?>
                            <div class="reviews-comment">
                                <form action="">
                                    <textarea placeholder="<?= Yii::t('account', 'profile_reviews_place') ?>" id="comment"></textarea>
                                    <?php if(\Yii::$app->user->isGuest){ ?>
                                        <input type="submit" value="<?= Yii::t('account', 'send') ?>" class="button" id="guest-comment-send">
                                    <?php }else{ ?>
                                        <input type="submit" value="<?= Yii::t('account', 'send') ?>" class="button" id="comment-send">
                                    <?php } ?>
                                </form>
                            </div>
                        <?php } ?>
                    </div>

                    <!-- End Reviews -->

                    <div class="catalog-box special-offers-catalog page-in-catalog">
                        <div class="clearfix"></div>
                        <h4 class="promote promote-bottom"> Special offers </h4>
                        <div class="clearfix"></div>
                        <div class="catalog-item">
                            <div class="catalog-img-box">
                                <a href="">
                                    <img src="/images/mainPage-column-31.jpg" alt="">
                                </a>
                                <div class="limited-offer">
                                    <p><b> Limited offer </b>
                                        <br> save 30 %</p>
                                </div>
                                <div class="like-calc item-price">
                                    <i class="icon-label49"></i>
                                    <span> 1800$</span>
                                    <br>
                                    <p class="last-price"> 3200$</p>
                                </div>
                            </div>
                            <div class="catalog-caption">
                                <p> Lorem ipsum Lorem ipsum Lorem ipsum
                                    &lt; br&gt; Lorem ipsum Lorem ipsum Lorem ipsum </p>
                            </div>
                        </div>
                        <div class="catalog-item">
                            <div class="catalog-img-box">
                                <a href="">
                                    <img src="/images/mainPage-column-32.jpg" alt="">
                                </a>
                                <div class="limited-offer">
                                    <p><b> Limited offer </b>
                                        <br> save 30 %</p>
                                </div>

                                <div class="like-calc item-price">
                                    <i class="icon-label49"></i>
                                    <span> 1800$</span>
                                    <br>
                                    <p class="last-price"> 3200$</p>
                                </div>
                            </div>
                            <div class="catalog-caption">

                                <p> Lorem ipsum Lorem ipsum Lorem ipsum
                                    &lt; br&gt; Lorem ipsum Lorem ipsum Lorem ipsum </p>
                            </div>
                        </div>
                        <div class="catalog-item">
                            <div class="catalog-img-box">
                                <a href="">
                                    <img src="/images/mainPage-column-31.jpg" alt="">
                                </a>
                                <div class="limited-offer">
                                    <p><b> Limited offer </b>
                                        <br> save 30 %</p>
                                </div>
                                <div class="like-calc item-price">
                                    <i class="icon-label49"></i>
                                    <span> 1800$</span>
                                    <br>
                                    <p class="last-price"> 3200$</p>
                                </div>
                            </div>
                            <div class="catalog-caption">

                                <p> Lorem ipsum Lorem ipsum Lorem ipsum
                                    &lt; br&gt; Lorem ipsum Lorem ipsum Lorem ipsum </p>
                            </div>
                        </div>
                        <div class="catalog-item">
                            <div class="catalog-img-box">
                                <a href="">
                                    <img src="/images/mainPage-column-32.jpg" alt="">
                                </a>
                                <div class="limited-offer">
                                    <p><b> Limited offer </b>
                                        <br> save 30 %</p>
                                </div>
                                <div class="like-calc item-price">
                                    <i class="icon-label49"></i>
                                    <span> 1800$</span>
                                    <br>
                                    <p class="last-price"> 3200$</p>
                                </div>
                            </div>
                            <div class="catalog-caption">

                                <p> Lorem ipsum Lorem ipsum Lorem ipsum
                                    &lt; br&gt; Lorem ipsum Lorem ipsum Lorem ipsum </p>
                            </div>
                        </div>
                    </div>
                    <div class="bread-crumbs">
                        <ul class="hvr-radial-out">
                            <li>
                                <a href="#"></a>
                            </li>
                            <li class="active"><a href="#"> 1</a></li>
                            <li><a href="#"> 2</a></li>
                            <li><a href="#"> 3</a></li>
                            <li><a href="#"> 4</a></li>
                            <li><a href="#"> 5</a></li>
                            <li><a href="#"> 6</a></li>
                            <li><a href="#"> 7</a></li>
                            <li>
                                <a href="#"></a>
                            </li>
                        </ul>
                    </div>
                    <hr class="sline">
                    <div class="clearfix"></div>
                    <?php if ($approved): ?>
                        <h4 class="promote promote-bottom">Approved Projects</h4>
                        <div class="clearfix"></div>
                        <div id="approved-projects">
                            <?php foreach ($approved['projects'] as $project): ?>
                                <?php $images = explode("|", $project['images']); ?>
                                <div class="profil-img">
                                    <a href="<?= Yii::$app->urlManager->createUrl(['about/item', 'id' => $project['item_id']]) ?>"><img src="<?= Yii::getAlias('@portfolio/' . $images[0]) ?>" alt=""></a>
                                </div>
                            <?php endforeach; ?>
                        </div>
                        <div class="clearfix"></div>
                        <div class="bread-crumbs">
                            <?= LinkPager::widget(['pagination' => $approved['pagination'],
                                'disabledPageCssClass' => false,
                                'nextPageLabel' => '',
                                'prevPageLabel' => '',
                                'options' => ['class' => 'hvr-radial-out agent-approved-projects-pagination'],
                            ]); ?>
                        </div>
                        <hr class="sline">
                    <?php endif; ?>


                    <div class="video">
                        <h4 class="promote"> Video</h4>
                        <?php if ($videos): ?>
                            <?php array_shift($videos) ?>
                            <?php foreach ($videos as $video) : ?>
                                <p><i class="icon-videoplayer5"></i><?= $video['name'] ?></p>
                                <iframe width="100%" height="360" src="<?= $video['link'] ?>" frameborder="0"
                                        allowfullscreen></iframe>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </div>

                    <?= frontend\widgets\Banner::widget(['position' => 'bottom']); ?>
                </div>

            </div>
            <!--END CENTER-->

        </div>
    </div>
</section>
<!--END CONTENT-->

<?php
$this->registerJsFile('js/bootstrap.min.js', ['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('js/common.js', ['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('js/action.js', ['depends'=>'frontend\assets\AppAsset']);
?>


<!--Вызов модального окна для переписки-->


<?php if(\Yii::$app->user->isGuest): ?>
    <!--START CHAT-->
    <div class="chat-block">
        <div class="chat">
            <div class="name">Message</div>

            <a href="#" class="roll-up"><i class="icon-11"></i></a>
            <a href="#" class="close"><i class="icon-cancel30"></i></a>
            <div class="message-content"  id="container">
                <div class="correspondence-chat-list">

                </div>
                <p class="send"><textarea class="message-input" placeholder="Type message..." ></textarea></p>
            </div>

        </div>
    </div>
    <!--END CHAT-->
    <?php
    $script_chat = <<< JS
	$('.chat-block textarea').keypress(function(e){
    	if(e.keyCode == 13) {
    		$('.chat-block .message-input').val('');
			$('.how_it_words').modal('show')
    	}
    });

    $('.send-chat-message').click(function(e){
		e.preventDefault();
		$('.chat-block').css('display', 'block');
    });

    $('.chat-block .close').click(function(e){
		e.preventDefault();
		$('.chat-block').css('display', 'none');
    });


JS;
    $this->registerJs($script_chat, yii\web\View::POS_READY);

    ?>
<?php endif; ?>
