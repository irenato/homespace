<?php
use yii\helpers\Html;
use frontend\widgets\Chat;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use frontend\account\models\Likes;
use frontend\widgets\VideoForMain;

$this->title = '';

$this->params['breadcrumbs'][] = $this->title;
?>

<section>
    <div class="container container-full-catalog">
        <div class="row">
            <div class="col-lg-12">
<!--                <a href="#" class="multiple-share">Share</a>-->
                <div class="rubric-caption"><?= Yii::t('main', 'best_projects') ?></div>

                <div class="catalog-box">
                    <div class="catalog-row">

                    <?php if($all_works) : ?>
                        <?php foreach ($all_works as $item) : ?>

                                <?php $img = explode('|', $item['images']); ?>
                                <div class="catalog-item">
                                    <div class="catalog-img-box">
                                        <a href="<?= Url::to(['/about/user/', 'id' => $item['owner_id'], 'item' => $item['item_id']]); ?>"><img src="<?= Yii::getAlias('@portfolio/'.$img['0']); ?>" alt=""></a>
                                        <div class="like-calc"><i class="icon-heart2971"></i><span class="count-likes"><?= $item['likes'] ?></span></div>
                                    </div>
                                    <div class="catalog-caption">
                                        <div class="social-cont">
                                            <a class="main-like icon-heart297<?php if(isset($item['my_likes']) && $item['my_likes'] === '1'){echo ' active-social-button';} ?>" id="<?=$item['item_id']?>" <?php if(\Yii::$app->user->isGuest) {echo "data-toggle='modal' data-target='#login'";} ?>></a>
                                            <a class="main-share icon-share-2"></a>
                                        </div>
                                        <div class="share-container">
                                            <i class="twitter-share">
                                                <a class="share-it-now-tw" href="https://twitter.com/intent/tweet?text=<?= $item['title'] ?>&url=<?= Yii::$app->urlManager->createAbsoluteUrl(['about/item/'.$item['item_id']]) ?>"></a></i>
                                            <i class="facebook-share"><a class="share-it-now-fb" href="#" data-id="<?= $item['item_id'] ?>" data-type="1" data-url="<?= Yii::$app->urlManager->createAbsoluteUrl(['about/item/'.$item['item_id']]) ?>" data-image="<?= Url::home(true).'media/portfolio/'.$img[0] ?>" data-description="<?= $item['title'] ?>"></a></i>

                                        </div>
                                        <div class="clearfix"></div>
                                        <hr class="border-section">
                                        <p><?= $item['title'] ?></p>
                                    </div>
                                </div>

                        <?php endforeach; ?>

                        <div class="clearfix"></div>

                        <div class="bread-crumbs">
                            <?= LinkPager::widget(['pagination' => $pagination,
                                'disabledPageCssClass' => false,
                                'nextPageLabel' => '',
                                'prevPageLabel' => '',
                                'options'=>['class'=>'hvr-radial-out1'],
                            ]); ?>

                        </div>

                     <?php endif; ?>
                    </div>
                    <hr class="border-section">
                </div>
            </div>
        </div>
    </div>
</section>
<!--End full catalog section-->

<?php

$this->registerJsFile('js/bootstrap.min.js', ['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('js/jscript.js', ['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('js/lightbox.js', ['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('js/action.js', ['depends' => 'frontend\assets\AppAsset']);
$this->registerJsFile('js/fb_likes.js', ['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('scripts/test.js', ['depends'=>'frontend\assets\AppAsset']);


?>