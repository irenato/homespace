<?php

use frontend\widgets\Banner;



$this->title = Yii::t('titles', 'index');
$this->params['breadcrumbs'][] = $this->title;
?>


<!--START CENTER-->
<div class="container container-full-catalog">

            <div class="col-lg-12">
    <!--        --><?php //var_dump($work_description)?>
            <?php if($work_description){ ?>
                <div class="row">
                    <div class="sidebar-caption"><?= $work_description['title'] ?><br></div>
                </div>
                <div class="row row-portfolio">
                    <div class="about_portfolio-images">
                        <?php foreach ($img as $image) { ?>
                            <a href="/media/portfolio/<?= $image ?>" data-lightbox="roadtrip" style="background: url('<?= $image ?>') no-repeat;"></a>
                        <?php } ?>
                    </div>
                    <div class="clearfix"></div>
                    <div class="about_portfolio-caption">
                        <p class="about-date"><?= $date ?></p>
                        <div class="hashtag">
                            <?php foreach ($tags as $tag) { ?>
                                <a href="#"><?= $tag ?></a>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="portfolio-article">
                        <?= str_replace(['&lt;p&gt;','&lt;/p&gt;'], '',$work_description['description']) ?>
                    </div>
                </div>

            <?php }else{ ?>
                <div class="row">
                    <h2><?= Yii::t('account', 'not_found') ?></h2>
                    <div class="bottom-baner">
                        <img src="/images/banner.png" alt="">
                    </div>
                </div>
            <?php } ?>
        </div>

</div>
<!--END CENTER-->
<?php
$this->registerJsFile('js/fixed.js', ['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('js/lightbox.js', ['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('scripts/delete.js', ['depends'=>'frontend\assets\AppAsset']);
?>



