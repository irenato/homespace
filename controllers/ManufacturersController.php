<?php

namespace frontend\controllers;

use frontend\account\models\ProductsCollections;
use yii;
use yii\data\Pagination;
use yii\helpers\Url;
use yii\web\Controller;
use frontend\account\models\User;
use frontend\models\Language;
use frontend\account\models\ProductsList;
use frontend\account\models\UserStatistics;
use frontend\account\models\CompaniesCollections;


class ManufacturersController extends Controller
{
    private $role;
    private $language = "ru";

    public function init()
    {
        $this->language = Language::getCurrent()->url;
        $this->role = Yii::$app->user->identity->roles;
    }

    public function actionIndex()
    {
        $request = Yii::$app->request;
        $session = Yii::$app->session;
        $categories = ProductsCollections::selectAllCollections();
        if ($request->post('manufacturer_name') != null){
            $session->set('manufacturer_name', $request->post('manufacturer_name'));
            $session->remove('page_size');
            $session->remove('companies_category');
            die();
        }
        if ($request->post('page_size') != null)
            $session['page_size'] = $request->post('page_size');
        if ($request->post('companies_category') != null) {
            if ($request->post('companies_category') != 'All')
                $session['companies_category'] = $request->post('companies_category');
            else
                unset($session['companies_category']);
        }

        if ($session['manufacturer_name']) {
            $pagination = false;
            $companies = User::searchManufacturerByName($session['manufacturer_name']);
        } elseif ($session['companies_category']) {
            $companies_id = CompaniesCollections::selectCompaniesIdByCategory($session['companies_category']);
            $pagination = new Pagination([
                'defaultPageSize' => $session['page_size'] ? $session['page_size'] : 9,
                'totalCount' => User::find()
                    ->where(['roles' => 'manufacturer'])
                    ->andWhere(['in', 'id', $companies_id])
                    ->count(),
            ]);
            $companies = User::companiesListByCompaniesIds($pagination, $companies_id);
        } else {
            $pagination = new Pagination([
                'defaultPageSize' => $session['page_size'] ? $session['page_size'] : 9,
                'totalCount' => User::find()
                    ->where(['roles' => 'manufacturer'])
                    ->count(),
            ]);
            $companies = User::companiesList($pagination);
        }
        $i = -1;
        foreach ($companies as $company) {
            $i++;
            $companies[$i]['products_count'] = ProductsList::countProductsByCompanyId($company['id']);
            $companies[$i]['collections'] = CompaniesCollections::selectCollectionsByCompanyId($company['id']);
            $companies[$i]['so_count'] = ProductsList::countStockByCompanyId($company['id']);
            $companies[$i]['new_products'] = ProductsList::countNewProducts($company['id']);
            $companies[$i]['discounts'] = ProductsList::selectMaxDiscount($company['id']);
        }
        return $this->render('index', [
            'companies' => $companies,
            'pagination' => $pagination,
            'language' => $this->language,
            'categories' => $categories,
            'page_size' => $session['page_size'] ? $session['page_size'] : 9,
            'current_category' => $session['companies_category'] ? $session['companies_category'] : '',
        ]);
    }

    public
    function actionProfile()
    {
        $id = Yii::$app->request->get('id', false);
        $manufacturer = User::findOne(['id' => $id, 'roles' => 'manufacturer']);
        if ($id && $manufacturer) {
            $pagination = new Pagination([
                'defaultPageSize' => 4,
                'totalCount' => ProductsList::countProductsByCompanyId($manufacturer->id),
                ]);
            $products = ProductsList::selectProductsByCompanyId($manufacturer->id, $pagination, 0);
//            $categories = ProductsCollections::
            return $this->render('profile', [
                'manufacturer' => $manufacturer,
                'products' => $products,
                'pagination' => $pagination,
            ]);
        } else
            Yii::$app->getResponse()->redirect(array('manufacturers'));
    }


}
