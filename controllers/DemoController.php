<?php

namespace frontend\controllers;

use yii;
use yii\web\Controller;
use yii\helpers\Url;

class DemoController extends Controller
{

    public function init(){}
    public function actionIndex()
    {
        if(!Yii::$app->user->isGuest){
            if(Yii::$app->user->identity->roles == 'agent'){
                return $this->redirect(Url::to(['/account/profile-agent']));
            }
            return $this->redirect(Url::to(['/account/profile']));
        }
        return $this->render('index');
    }

}
