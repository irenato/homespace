<?php

namespace frontend\controllers;

use yii;
use yii\web\Controller;
use frontend\models\Search;
use frontend\models\Language;


class SearchController extends Controller
{
    public function init(){}
    public function actionIndex()
    {
        $q = Yii::$app->request->get('q', '');
        $lang = Language::getCurrent()->url;
        if($q != ''){
            $search_items = Search::searchPortfolio($q, $lang);
        }else $search_items = false;
        return $this->render('index',[
            'q' => $q,
            'search_items' => $search_items,
        ]);
    }

}
