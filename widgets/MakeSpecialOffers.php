<?php

namespace frontend\widgets;

use yii;
use frontend\models\Language;


class MakeSpecialOffers extends \yii\bootstrap\Widget
{

    public function init(){}

    public function run()
    {
        $lang = Language::getCurrent()->url;
        return $this->render('make_special_offers/view', [
            'lang' => $lang,
        ]);

    }

}