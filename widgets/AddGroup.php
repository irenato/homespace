<?php

namespace frontend\widgets;
use frontend\account\models\Groups;
use frontend\account\models\ProductsCollections;
use yii;
use frontend\models\Language;
use yii\helpers\ArrayHelper;
use frontend\models\CatalogCategories;


class AddGroup extends \yii\bootstrap\Widget
{

    public function init(){}

    public function run()
    {
        $lang = Language::getCurrent()->url;
        $collections =  ArrayHelper::map(ProductsCollections::selectAllCollections(), 'id', $lang);
        $categories = ArrayHelper::map(CatalogCategories::selectAllCategoriesByLanguage(), 'id', 'title_' . $lang);
        return $this->render('add_group/view', [
            'lang' => $lang,
            'model' => new Groups(),
            'categories' => $categories,
            'collections' => $collections,
        ]);

    }

}