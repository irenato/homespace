<?php
namespace frontend\widgets;

use common\models\LoginForm;

class LoginWidget extends \yii\bootstrap\Widget
{
    public function init(){}

    public function run() {
        $model = new LoginForm();
        return $this->render('login/view', [
            'model' => $model,
        ]);
    }
}