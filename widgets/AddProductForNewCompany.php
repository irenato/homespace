<?php

namespace frontend\widgets;

use yii;
use frontend\models\Language;
use frontend\account\models\User;
use frontend\account\models\ProductsCollections;


class AddProductForNewCompany extends \yii\bootstrap\Widget
{

    public function init()
    {
    }

    public function run()
    {
        $request = Yii::$app->request;
        $lang = Language::getCurrent()->url;
        $products_collections = ProductsCollections::find()->asArray()->all();
        if ($request->get('id')) {
            $company = User::selectCompanyById($request->get('id'));
            $company_name = explode('||', $company->company)[0];
            $company_id = $company->id;
        }else{
            $company_name = ' ';
            $company_id = '';
        }
        return $this->render('add_product_for_new_company/view', [
            'lang' => $lang,
            'company_id' => $company_id,
            'company_name' => $company_name,
            'products_collections' => $products_collections,
        ]);

    }

}
