<?php
namespace frontend\widgets;

use yii;
use frontend\account\models\ProductsList;
use frontend\models\Language;
use frontend\account\models\Portfolio;

class Modal extends \yii\bootstrap\Widget
{
    public $name;

    public function init(){}

    public function run() {
        $route = Yii::$app->controller->getRoute();
        if($this->name == 'best3works' ){
            if ($route == 'account/portfolio/index' || $route == 'account/edit/collections' || $route == 'account/edit/index') {
                return $this->render('modal/view', [
                    'name' => $this->name,
                ]);
            }
        }else if($this->name == 'style'){
            if ($route == 'account/portfolio/index') {
                return $this->render('modal/view', [
                    'name' => $this->name,
                ]);
            }
        } else if($this->name == 'add-order-model'){
            if ($route == 'account/catalog/catalog' || $route == 'account/order/index') {
                return $this->render('modal/view', [
                    'name' => $this->name,
                ]);
            }
        }
        else if($this->name == 'move-order-model'){
            if ($route == 'account/order/folder' || $route == 'account/order/index') {
                return $this->render('modal/view', [
                    'name' => $this->name,
                ]);
            }
        }else if($this->name == 'tariff-plan') {
            if ($route == 'account/profile/index' || $route == 'account/portfolio/index') {
                return $this->render('modal/view', [
                    'name' => $this->name,
                    'products' => ProductsList::find(['stock' => 1])->orderBy('RAND()')->limit(3)->all(),
//                    'projects' => Portfolio::itemForPaymentModal(Yii::$app->user->identity->id, Language::getCurrent()->url)
                ]);
            }
        }else{
            if(Yii::$app->user->isGuest) {
                return $this->render('modal/view', [
                    'name' => $this->name,
                    'route' => $route,
                    'lang' => Language::getCurrent()->url
                ]);
            }
        }
    }


}