<?php

?>

<!--START MODAL-->
<div class="modal make-special-offers" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="open-create-product">
                <div class="clearfix"></div>
                <form class="forma-input">
                    <div class="input-container">
                        <input class="text-input floating-label" name="discount" type="text" value="">
                        <label for="sample">Discount (%)</label>
                    </div>
                </form>
                <input type="button" id="make-s-o" class="button" value="Save"/>
                <div class="error-message" style="display: none">
                    <span class="text-danger"></span>
                </div>
                <div class="success-message" style="display: none">
                    <span class="text-success"><?= Yii::t('account', 'product_was_created_successfully') ?></span>
                </div>
            </div>
        </div>
    </div>
</div>
<!--END MODAL-->
