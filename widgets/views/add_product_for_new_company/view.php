<?php

?>

<!--START MODAL-->
<div class="modal fade agent-modal-product-new" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="open-create-product-new">
                <div class="clearfix"></div>
                <div class="google-search">
                    <input type="text" placeholder="search">
                </div>
                <div class="object-add">
                    <input type="file" accept="image/*;capture=camera" style="display : none;" id="open_browse"
                           onchange="fileAgentChange(this);"/>
                    <div class="dropzone" id="dropzone1"><?= Yii::t('account', 'drop_zone') ?></div>
                    <div class="dropzone" id="dropzone2"><?= Yii::t('account', 'drop_zone') ?></div>
                    <div class="dropzone" id="dropzone3"><?= Yii::t('account', 'drop_zone') ?></div>
                </div>
                <form class="forma-input">
                    <div class="input-container">
                        <input class="text-input floating-label" data-provide="tokenizer" name="tags" type="text" size="50">
                        <label for="add-tags">#tags</label>
                    </div>
                    <div class="input-container">
                        <input class="text-input floating-label dirty" name="company-name" data-id="<?= $company->id ?>" type="text" value="<?= $company_name ?>" disabled>
<!--                        <input class="text-input floating-label" name="company-name" type="text" value="" disabled>-->
                        <label for="sample">Company name</label>
                    </div>
                    <div class="input-container">
                        <input class="text-input floating-label" name="product-name" type="text" value="">
                        <label for="sample">Product Name</label>
                    </div>
                    <div class="selects">
                        <select class="sort" name="product-collection">
                            <?php foreach ($products_collections as $collection) : ?>
                                <option
                                    value="<?= $collection['id'] ?>"><?= $lang === 'ru' ? $collection['ru'] : $collection['en'] ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="input-container">
                        <input class="text-input floating-label" name="public-price" type="text" value="">
                        <label for="sample">Public price</label>
                    </div>
                    <div id="discount-block" class="input-container" style="display: none">
                        <input class="text-input floating-label" name="discount" type="text" value="">
                        <label for="sample">Discount (%)</label>
                    </div>
                    <div class="input-container">
                        <input id="special-offer" value="1" type="checkbox">
                        <label for="special-offer">special offer</label>
                    </div>
                </form>
                <form action="#">
                    <textarea name="description" id="#"></textarea>
                    <input type="button" id="save-product" class="button" value="Save"/>
                </form>
                <div class="error-message" style="display: none">
                    <span class="text-danger"></span>
                </div>
                <div class="success-message" style="display: none">
                    <span class="text-success"><?= Yii::t('account', 'product_was_created_successfully') ?></span>
                </div>
            </div>
        </div>
    </div>
</div>
<!--END MODAL-->
