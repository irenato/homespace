<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
?>
<!--<div class="modal fade" id="myChat" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">-->
<!--    <div class="modal-dialog">-->
<!--        <div class="modal-content">-->
<!--            <div class="modal-header">-->
<!--                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>-->
<!--                --><?//=$user_information['last_name']?>
<!--            </div>-->
<!---->
<!--            --><?php //$form = ActiveForm::begin(['id' => 'message-form']); ?>
<!---->
<!--            <div class="modal-body">-->
<!--                    <div id="message-list">-->
<!---->
<!--                    </div>-->
<!---->
<!--            </div>-->
<!--            --><?//= $form->field($model, 'message')->textArea(['rows' => '3']) ?>
<!--            <div class="modal-footer">-->
<!--                <div class="form-group">-->
<!--                    --><?//= Html::submitButton('Отправить', ['class' => 'btn btn-primary', 'name' => 'message-button', 'id' => 'message-button']) ?>
<!--                </div>-->
<!--            </div>-->
<!---->
<!--            --><?php //ActiveForm::end(); ?>
<!---->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->
    <div class="chat-block">
        <div class="chat">
            <div class="name">Sam Clarintence</div>
            <a href="#" class="roll-up"><i class="icon-11"></i></a>
            <a href="#" class="close"><i class="icon-cancel30"></i></a>
            <div class="message-content"  id="container">
                <p class="data">10. 01. 2016</p>
                <div class="player-1">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin</p>
                    <i class="icon-user168"></i>
                </div>
                <div class="player-2">
                    <img src="images/team1.png" alt="">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin urna ipsum dolor sit</p>
                </div>
                <div class="player-1">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin</p>
                    <i class="icon-user168"></i>
                </div>
                <p class="data">10. 01. 2016</p>
                <div class="player-2">
                    <img src="images/team1.png" alt="">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin urna ipsum dolor sit</p>
                </div>
                <div class="player-1">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin</p>
                    <img src="images/team1.png" alt="">
                </div>
                <div class="player-2">
                    <img src="images/team1.png" alt="">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin urna ipsum dolor sit</p>
                </div>
            </div>
            <p class="send"><textarea placeholder="Type message..." ></textarea></p>
        </div>
    </div>
    <!--END CHAT-->

<?php
    $this->registerJsFile('js/action.js', ['depends' => 'frontend\assets\AppAsset']);
?>