<?php
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
?>
    <!--START MODAL-->
    <div class="modal fade group-modal create-teaser" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
         aria-hidden="true">
    <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <h5 class="title">Add teaser</h5>
        <?php $form = ActiveForm::begin(['id' => 'group', 'options' => ['class' => 'forma-input',]]); ?>
        <!--        <input id="group-flag" name="Groups[flag]" value="1"-->
        <!--               type="checkbox">-->
        <!--        <label for="group-flag">to make major</label>-->

        <div class="cleafix"></div>




        <?php echo $form->field($model, 'cities_id')->hiddenInput()->label(false); ?>
        <div class="forma-input">
         <div class="form-group field-groups-title required ">
           <div class="input-container">
            <input type="text" id="groups-title" class="form-control text-input floating-label" name="Groups[title]" label="Title">
            </div>
        <div class="help-block"></div>
        </div>
        <div class="form-group">
           <div class="input-container google-search-cities">

<!--            <input id="google-search-cities"  class="text-input floating-label" type="search" value="">-->
<!--             <label for="google-search-cities ">Cities</label>-->
            </div>
            </div>
             <div class="search-result">
                <ul id="search-result-items"></ul>
            </div>
            <div class="bootom-form">


                <!--                --><?php //echo $form->field($model, 'tags')->dropDownList()->label(false); ?>

                <?= $form->field($model, 'colections[]')->widget(Select2::classname(), [
                    'data' => $collections,
                    'options' => ['multiple' => true],
                    'showToggleAll' => false,
                    'pluginOptions' => [
                        'maximumSelectionLength' => 4,
                        'allowClear' => true,
                    ],
                ])->label('Collections'); ?>

                <?= $form->field($model, 'categories[]')->widget(Select2::classname(), [
                    'data' => $categories,
                    'options' => ['multiple' => true],
                    'showToggleAll' => false,
                    'pluginOptions' => [
                        'maximumSelectionLength' => 4,
                        'allowClear' => true,
                    ],
                ])->label('Categories'); ?>

                <?php echo $form->field($model, 'text')->textarea()->label('Description'); ?>
                <div class="checkbox">
                    <p>
                        <input type="hidden" name="Groups[flag]" value="0">
                        <input id="groups-flag" name="Groups[flag]" value="1"
                               type="checkbox">
                        <label for="groups-flag">to make major</label>
                    </p>
                </div>
                <input type="submit" class="button" value="Add">

                <?php ActiveForm::end() ?>


            </div>
        </div>
    </div>
    <!--END MODAL-->
<?php

