<?php if($banners): ?>
<div id="owl-banner" class="owl-carousel owl-theme">
    <?php foreach($banners as $banner):?>
        <div class="item"><img src="<?= Yii::getAlias('@banners/'.$banner['image']) ?>" alt=""></div>
    <?php endforeach; ?>
</div>

<?php
$script_slider = <<< JS
   $("#owl-banner").owlCarousel({

          navigation : false, // Show next and prev buttons
          slideSpeed : 800,
          paginationSpeed : 1000,
          singleItem:true,
          autoPlay : 5000,
          transitionStyle : "fade"
      });
JS;
$this->registerJs($script_slider, yii\web\View::POS_READY);

?>

<?php endif; ?>
