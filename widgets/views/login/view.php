<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

?>
<?php if(\Yii::$app->user->isGuest) { ?>
    <div class="modal fade login-model" tabindex="-1" role="dialog" id="login" aria-labelledby="myLargeModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <h3><?= Yii::t('main','log_in')?></h3>

                <div class="form">
                    <?php $form = ActiveForm::begin([
                        'id' => 'login-form',
                        'options' => ['class' => 'forma-input',],
                        'action' => Url::to(['site/valid']),
                        'fieldConfig' => [
                            'template' => '{input}{label}',
                            'options' => [
                                'class' => 'input-container'
                            ],
                        ],
                        'enableAjaxValidation' => true
                    ]); ?>


                    <i class="icon-user168"></i>
                    <?= $form->field($model, 'username')->textInput(['class' => 'floating-label text-input', 'value' => ''])->label(Yii::t('main', 'login')) ?>

                    <?= Html::a(Yii::t('main','pas_forgot'), ['site/request-password-reset']) ?>
                    <div class="clearfix"></div>
                    <i class="icon-locked59"></i>

                    <?= $form->field($model, 'password')->passwordInput(['class' => 'floating-label text-input', 'value' => ''])->label(Yii::t('main', 'password')) ?>


                    <div class="checkbox">
                        <p>
                            <input type="hidden" name="LoginForm[rememberMe]" value="0">
                            <input id="loginform-rememberme" checked="" name="LoginForm[rememberMe]" value="1"
                                   type="checkbox">
                            <label for="loginform-rememberme"><?= Yii::t('main','remember') ?></label>
                        </p>
                    </div>
                    <div class="block-button">


                        <input class="button" value="<?= Yii::t('main','log_in_but') ?>" type="submit" name="login-button">

                    </div>
                    <p><span>*Password must contain at least 6 characters</span></p>
                    <h4><?= Yii::t('main','member')?> <a id="link-sign-up" href="#"><?= Yii::t('main','sign')?></a></h4>
                    <?php ActiveForm::end(); ?>

                </div>
                <div class="soc">
                    <ul>
                        <li><a href="/site/auth?authclient=twitter" class="auth-link twitter"><span
                                    class="auth-icon twitter"></span><i class="icon-twitter1"></i></a></li>
                        <li><a href="/site/auth?authclient=facebook" class="auth-link facebook" data-popup-width="860"
                               data-popup-height="480"><i class="icon-facebook55"></i></a></li>
                        <li><a href="#"><i class="icon-behance2"></i></a></li>
                        <li><a href="#"><i class="icon-pinterest3"></i></a></li>
                    </ul>
                    <p class="description">One-click sign in to HomeSpaceToday if your account is connected to Twitter,
                        Facebook, Behance or Pinterest. We'll walk you through connecting it if it isn't.</p>
                </div>
            </div>
        </div>
    </div>
    <?php
    $script_slider = <<< JS
      $('#login-form input').each(function() {
         $(this).bind( "change", function() {
            if ($(this).val() != '') {
                $(this).addClass('dirty');
            } else {
                $(this).removeClass('dirty')
            }
         });
      });

JS;
    $this->registerJs($script_slider, yii\web\View::POS_READY);
} ?>


