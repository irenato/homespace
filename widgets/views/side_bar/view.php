<?php if($side_bar){ ?>
    <ul class="category-menu">
        <?php foreach ($side_bar as $item){ ?>
            <?php $route_base = explode('/',Yii::$app->controller->getRoute()); ?>
            <?php $current_route = '/'.$route_base[0].'/'.$route_base[1]; ?>
            <?php if($item['sub_menu']){?>
                <?php if($current_route == $item['url'] || '/en'.$current_route == $item['url']){?>
                    <li class="li-dd active">
                        <a><i class="<?= $item['ico'] ?>"></i><?=$item['label'] ?></a>
                        <ul class="dropdown-sdb">
                            <?php foreach ($item['sub_menu'] as $sub_menu){ ?>
                                <li><a href="<?= $sub_menu['url'] ?>"><?= $sub_menu['label'] ?></a></li>
                            <?php } ?>
                        </ul>
                    </li>
                <?php }else{ ?>
                    <li class="li-dd">
                        <a><i class="<?= $item['ico'] ?>"></i><?=$item['label'] ?></a>
                        <ul class="dropdown-sdb">
                            <?php foreach ($item['sub_menu'] as $sub_menu){ ?>
                                <li><a href="<?= $sub_menu['url'] ?>"><?= $sub_menu['label'] ?></a></li>
                            <?php } ?>
                        </ul>
                    </li>
                <?php } ?>

            <?php }else{ ?>
                <?php if($current_route == $item['url'] || '/en'.$current_route == $item['url']){?>
                    <li class="active"><a href="<?= $item['url'] ?>"><i class="<?= $item['ico'] ?>"></i><?=$item['label'] ?></a></li>
                <?php }else{ ?>
                    <li><a href="<?= $item['url'] ?>"><i class="<?=$item['ico'] ?>"></i><?=$item['label'] ?></a></li>
                <?php } ?>
            <?php } ?>

        <?php } ?>
    </ul>

    <br>
    <?php if($video['video']): ?>
        <iframe id="play" width="262" height="150" src="<?= $video['video'] ?>" frameborder="0" allowfullscreen></iframe>
    <?php endif; ?>

    <?php
    $side_bar = <<< JS
    $( ".category-menu .li-dd" ).click(function() {
        $(this).toggleClass( "pressed" );
    });
JS;
    $this->registerJs($side_bar, yii\web\View::POS_LOAD);
    ?>

<?php }else{ ?>
    <ul class="category-menu">
        <li><a href="#" data-toggle="modal" data-target="#login"><i class="icon-1"></i>My projects</a></li>
        <li><a href="#" data-toggle="modal" data-target="#login"><i class="icon-catalog"></i>Application</a></li>
        <li><a href="#" data-toggle="modal" data-target="#login"><i class="icon-black218"></i>Messages</a></li>
        <li><a href="#" data-toggle="modal" data-target="#login"><i class="icon-3"></i>Agents</a></li>
        <li><a href="#" data-toggle="modal" data-target="#login"><i class="icon-7"></i>Special offers</a></li>
        <li><a href="#" data-toggle="modal" data-target="#login"><i class="icon-3"></i>My contractors</a></li>
        <li><a href="#" data-toggle="modal" data-target="#login"><i class="icon-catalog"></i>Press</a></li>
        <li><a href="#" data-toggle="modal" data-target="#login"><i class="icon-5"></i>Edit profile</a></li>
    </ul>
<?php } ?>