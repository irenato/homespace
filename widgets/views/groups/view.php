<!-- Groups -->
<?php //var_dump($designers); ?>
<?php //die(); ?>
<?php if($designers) : ?>
<!--        <div class="row">-->
<!--            <h4 class="promote promote-bottom">--><?//= Yii::t('account', 'group_to_promote') ?><!--</h4>-->
            <p class="upload-portfolio download-portfolio join"><?= Yii::t('account', 'join_group') ?><a href="<?= \Yii::$app->urlManager->createUrl(['account/designers-and-architects/all']) ?>"></a></p>
            <div class="clearfix"></div>
            <div class="companys">
                <?php foreach ($designers as $user): ?>
                    <div class="company agents pf-new new-groups-for-des"
                         style="background-image: url(<?= Yii::getAlias('@background/' . $user['background']) ?>);" data-user="<?= $user['id'] ?>">
                        <div class="author"><img
                                src="<?= Yii::getAlias('@avatar/' . $user['logo']) ?>" alt="#">
                            <p class="name"><?= $user['company'] ?></p>
                        </div>
                        <div class="author-description">
                            <img class="company-img"
                                 src="<?= Yii::getAlias('@avatar/' . $user['avatar']) ?>" alt="#">
                            <div class="name-company">
                                <h5><?= $user['first_name'] ?>
                                    <span><?= $user['last_name'] ?></span></h5>
                                <p><?= $user['country'] ?> <span><?= $user['city'] ?></span></p>
                                <a href="#" class="messages send-chat-message"><i
                                        class="icon-black218"></i>Message</a>
                            </div>
                        </div>
                        <p class="descriptions">
                            <?= substr($user['message'], 0, 80) ?>...
                        </p>

                        <ul class="description">
                            <li>Projects: <span><?= $user['portfolio'] ?></span></li>
                            <!--                                        <li class="new">New: <span>6</span></li>-->
                            <li>Products confirmed: <span><?= $user['confirmed'] ?></span></li>
                            <li><span class="dolor">$</span>: 8.01.15 - 10.03.15</li>
                        </ul>
                        <ul class="buttons">
                            <li>
                                <a href="<?= Yii::$app->urlManager->createUrl(['about/user', 'id' => $user['id']]) ?>"
                                   class="button">View</a></li>
                            <li><a href="#" class="button destroy-designer" data-id="<?= $user['id'] ?>">Unfollow</a></li>
                            <li><a href="#" data-id="<?= $user['id'] ?>" class="button">Be a
                                    sponsor</a></li>
                        </ul>
                    </div>
                <?php endforeach; ?>
                </div>
    <!--            --><?php //foreach ($groups as $group) {?>
    <!--                <div class="item">-->
    <!--                    <div class="portfolio-item white">-->
    <!--                        <a href="--><?//= $group['url'] ?><!--"><img src="--><?//= $group['image'] ?><!--" alt="--><?//= $group['title'] ?><!--"></a>-->
    <!--                        <div class="like-ico">-->
    <!--                            <i class="icon-eye110">--><?//= $group['views'] ?><!--</i>-->
    <!--                            <i class="icon-user168">--><?//= $group['followers'] ?><!--</i>-->
    <!--                            <i class="icon-heart297--><?//= $group['user_like'] ?><!--">--><?//= $group['likes'] ?><!--</i>-->
    <!--                        </div>-->
    <!--                        <a class="like"><i class="icon-heart297--><?//= $group['user_like'] ?><!--"></i></a>-->
    <!--                        <p>--><?//= $group['title'] ?><!--</p>-->
    <!--                    </div>-->
    <!--                </div>-->
    <!--            --><?php //} ?>
<!--        </div>-->
<?php elseif ($groups) : ?>

<div class="companys cfrj">
        <?php foreach ($groups as $group) : ?>
            <div class="company agents pf-new new-groups-for-des" style="background-image: url('/images/pic_50.png');" data-user="<?= $group['id'] ?>">
                <div class="bg teaser-block">
                    <h4><?= $group['teaser']['title'] ?></h4>
                    <div class="author"><img
                            src="<?= Yii::getAlias('@avatar/' . $group['logo']) ?>" alt="#">
                        <p class="name"><?= $group['company']; ?></p>
                    </div>
                    <div class="author-description">
                        <img class="company-img"
                             src="<?= Yii::getAlias('@avatar/' . $group['avatar']) ?>" alt="#">
                        <div class="name-company">
                            <h5><?= $group['first_name'] ?>
                                <span><?= $group['last_name'] ?></span>
                            </h5>
                        </div>
                    </div>
                    <div class="city">
                        <ul>
                            <?php $cities = explode(',', $group['teaser']['cities_names']);
                            $place_id = explode(',', $group['teaser']['cities_id']);
                            $i = -1;
                            foreach ($cities as $city) :
                                $i++;
                                ?>
                                <li data-id="<?= $place_id[$i] ?>"><?= $city ?></li>
                            <?php endforeach; ?>
                        </ul>
                        <div class="showRooms">
												<span class="showRoom">
													show <br> room
												</span>
												<span class="showRoom">
													show <br> room
												</span>
                        </div>
                        <p><?= $group['city'] ?></p>
                    </div>
                    <p class="descriptions">
                        <?= substr($group['teaser']['text'], 0, 120) . '...' ?>
                    </p>
                    <ul class="categories">
                        <li><a href="#">Categories:</a></li>
                        <?php foreach($group['teaser']['teaser_categories'] as $category) : ?>
                            <li><a data-id="<?= $group['id'] ?>" href="#"><?= $category['title_' . $language] ?></a></li>
                        <?php endforeach; ?>
                    </ul>
                    <ul class="description">
                        <li>Added products: <span>56</span></li>
                        <li>Identified objects: <span>16</span></li>
                        <li>Special offers: <span>16</span></li>

                    </ul>
                    <ul class="categories best">
                        <li><a href="#">Best seller for:</a></li>
                        <?php foreach($group['teaser']['teaser_collections'] as $collection) : ?>
                            <li><a href="#">-<?= $collection[$language] ?></a></li>
                        <?php endforeach; ?>
                    </ul>
                    <ul class="buttons">
                        <li><a href="<?= Yii::$app->urlManager->createUrl(['agents/profile', 'id' => $group['id']]) ?>" class="button">View</a></li>
                    </ul>
                    <a href="#" class="messages send-chat-message"><i class="icon-black218"></i>Message</a>
                </div>
            </div>
        <?php endforeach; ?>
    </div>

<?php endif; ?>
<?php //if($groups){ ?>
<!--    <div class="row">-->
<!--        <h4 class="promote promote-bottom">--><?//= Yii::t('account', 'group_to_promote') ?><!--</h4>-->
<!--        <p class="upload-portfolio download-portfolio join">--><?//= Yii::t('account', 'join_group') ?><!--<a href="--><?//= \Yii::$app->urlManager->createUrl(['account/join-group']) ?><!--"></a></p>-->
<!--        <div class="clearfix"></div>-->
<!--        <div id="owl-demo-3" class="slider-index">-->
<!--            --><?php //foreach ($groups as $group) {?>
<!--                <div class="item">-->
<!--                    <div class="portfolio-item white">-->
<!--                        <a href="--><?//= $group['url'] ?><!--"><img src="--><?//= $group['image'] ?><!--" alt="--><?//= $group['title'] ?><!--"></a>-->
<!--                        <div class="like-ico">-->
<!--                            <i class="icon-eye110">--><?//= $group['views'] ?><!--</i>-->
<!--                            <i class="icon-user168">--><?//= $group['followers'] ?><!--</i>-->
<!--                            <i class="icon-heart297--><?//= $group['user_like'] ?><!--">--><?//= $group['likes'] ?><!--</i>-->
<!--                        </div>-->
<!--                        <a class="like"><i class="icon-heart297--><?//= $group['user_like'] ?><!--"></i></a>-->
<!--                        <p>--><?//= $group['title'] ?><!--</p>-->
<!--                    </div>-->
<!--                </div>-->
<!--            --><?php //} ?>
<!--        </div>-->
<!--    </div>-->
<?php //} ?>
<!-- End Groups -->
<!---->
<?php
//$script_groups = <<< JS
//   $("#owl-demo-3").owlCarousel({
//        items : 2,
//        itemsDesktop : [1199,3],
//        itemsDesktopSmall : [979,3],
//        navigationText:false,
//        navigation:true,
//        pagination:false
//    });
//JS;
//$this->registerJs($script_groups, yii\web\View::POS_READY);
//?>

