<?php
namespace frontend\widgets;

use yii;
use frontend\account\models\Groups;
use frontend\account\models\Likes;
use frontend\models\CatalogCategories;
use frontend\account\models\AgentDesigners;
use frontend\account\models\User;
use frontend\account\models\ProductsCollections;
use frontend\models\Language;

class PGroups extends \yii\bootstrap\Widget
{

    private $user_id;

    public function init()
    {
        $this->user_id = Yii::$app->user->identity->id;
    }

    public function run()
    {
        if (Yii::$app->user->identity->roles == 'agent') {
            return $this->render('groups/view', [
                'designers' => $this->selectMyTwoLastDesigners(),
            ]);
        } elseif (Yii::$app->user->identity->roles == 'designer') {
            return $this->render('groups/view', [
                'language' => Language::getCurrent()->url,
                'groups' => $this->selectTwoLastGroups(User::selectAgentsById($this->selectTwoLastGroupsId())),
            ]);
        }
    }

    private function selectMyTwoLastDesignersId()
    {
        return AgentDesigners::getTwoDesignersIdByGroup($this->user_id);
    }

    private function selectMyTwoLastDesigners()
    {
        $result = User::myDesignersAndArchitects(false, false, false, false, implode(',', $this->selectMyTwoLastDesignersId()));
        if ($result)
            return $result;
        else
            return array();
    }

    private function selectTwoLastGroupsId()
    {
        return AgentDesigners::selectTwoLastGroupsForDesigner($this->user_id);
    }

    protected function selectTwoLastGroups($new_agents)
    {
        if (is_array($new_agents)) {
            $i = -1;
            foreach ($new_agents as $new_agent) {
                $i++;
                $new_agents[$i]['teaser'] = Groups::selectMainTeasersByAgentId($new_agent['id']);
                if ($new_agents[$i]['teaser']) {
                    $new_agents[$i]['teaser']['cities_names'] = $this->selectCityNameByPlaceId($new_agents[$i]['teaser']['cities_id']);
                    $new_agents[$i]['teaser']['teaser_collections'] = ProductsCollections::getCollectionsById(explode(',', $new_agents[$i]['teaser']['colections']), Language::getCurrent()->url);
                    $new_agents[$i]['teaser']['teaser_categories'] = CatalogCategories::selectCategoriesByCategoryId(explode(',', $new_agents[$i]['teaser']['categories']), Language::getCurrent()->url);
                }
            }
            return $new_agents;
        }
    }

    protected function selectCityNameByPlaceId($place_id)
    {
        $place_id = explode(',', $place_id);
        $city_name = [];
        $i = -1;
        foreach ($place_id as $single_place_id) {
            $i++;
            if ($single_place_id) {
                $data = json_decode(file_get_contents("https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyBZLWGV4klfZp4MjxvXtnljM-qsxyOdzrE&place_id=" . $single_place_id . "&sensor=false&language=" . Language::getCurrent()->url . ""));
                if ($i + 1 != count($place_id))
                    $city_name[] = $data->results[0]->address_components[0]->long_name;
            }
        }
        return implode(',', $city_name);
    }


//    public function run() {
//        return $this->render('groups/view', [
//            'groups' => $this->getGroups($this->user_id, 1),
//        ]);
//    }
//
//    private function getGroups($user_id, $object)
//    {
//        $temp_groups = Groups::find()->where(['paid_status' => 1])->all();
//        $data = array();
//        foreach ($temp_groups as $value) {
//            $my_like = Likes::find()->where(['user_id' => $user_id, 'object_id' => $value['id'], 'category' => $object])->one();
//            if ($my_like == null) {
//                $my_like = '';
//            } else $my_like = 1;
//
//            $data[] = [
//                'id' => $value['id'],
//                'image' => '/images/' . $value['images'],
//                'title' => $value['title'],
//                'views' => $value['views'],
//                'followers' => $value['followers_count'],
//                'likes' => Likes::find()->where(['object_id' => $value['id'], 'category' => $object])->count(),
//                'user_like' => $my_like,
//                'url' => Yii::$app->urlManager->createUrl(['account/join-group/group?' . $value['id']]),
//            ];
//
//        }
//        return $data;
//    }

}