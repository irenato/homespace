$(document).ready(function () {

    $('body').on('click', 'a.follow-designer', function (e) {
        e.preventDefault();
        var id_desiger = $(this).attr('data-id'),
            current_designer = $(this);
            add_designer = $.ajax({
                data: {
                    'id_designer': id_desiger,
                },
                url: '/account/designers-and-architects/add-designer',
                type: 'post',
                dataType: 'html',
            });
        add_designer.done(function(data){
            if(data == 'done!'){
                current_designer.text('Unfollow');
                current_designer.removeClass('follow-designer');
                current_designer.addClass('unfollow-designer');
            }
        })
    })

    $('body').on('click', 'a.unfollow-designer',function (e) {
        e.preventDefault();
        var id_desiger = $(this).attr('data-id'),
            current_designer = $(this);
        remove_designer = $.ajax({
            data: {
                'id_designer': id_desiger,
            },
            url: '/account/designers-and-architects/remove-designer',
            type: 'post',
            dataType: 'html',
        });
        remove_designer.done(function(data){
            if(data == 'done!'){
                current_designer.text('Follow');
                current_designer.removeClass('unfollow-designer');
                current_designer.addClass('follow-designer');
            }
        })
    })

    $('body').on('click', 'a.destroy-designer',function (e) {
        e.preventDefault();
        var id_desiger = $(this).attr('data-id'),
            current_designer = $(this);
        remove_designer = $.ajax({
            data: {
                'id_designer': id_desiger,
            },
            url: '/account/designers-and-architects/remove-designer',
            type: 'post',
            dataType: 'html',
        });
        remove_designer.done(function(data){
            if(data == 'done!'){
               var designer_box = current_designer.closest('div.company.agents');
                designer_box.detach();
            }
        })
    })

})
