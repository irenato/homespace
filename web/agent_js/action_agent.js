///-----------------------------------------------------------------
// Adding product. The modal window. agent Page
//-----------------------------------------------------------------
$(document).on("click", "a.create-product-card", function (e) {
    e.preventDefault();
    var company_name = $(this).closest('.modal-content').find("input[name='search-company-name']").val(),
        comp_id = $(this).closest('.modal-content').find("input[name='search-company-name']").attr('data-id');
    $(this).closest('.modal-content').find("input[name='company-name']").val(company_name).attr('data-id', comp_id);
    $('div.open-create-product').slideToggle();
})

var dropzone = $('#dropzone1');
var dropzone2 = $('#dropzone2');
var dropzone3 = $('#dropzone3');
var dropzone_id = '';
var img_url1 = "";
var img_url2 = "";
var img_url3 = "";
var check_before_save = 0;

//------------------------------
//upload files
//------------------------------
var upload = function (files) {
    var formData = new FormData();
    formData.append('file', files[0]);
    var request = $.ajax({
        data: formData,
        url: '/account/projects-agent/upload',
        type: 'post',
        dataType: 'html',
        contentType: false,
        processData: false,
    });

    request.done(function (url) {
        $(dropzone_id).css('background', 'url(' + url + ')');
        if (dropzone_id == "#dropzone1") {
            img_url1 = url;
        } else if (dropzone_id == "#dropzone2") {
            img_url2 = url;
        } else if (dropzone_id == "#dropzone3") {
            img_url3 = url;
        }
    });

    request.fail(function (jqXHR, textStatus) {
        alert(textStatus);
    });

};

dropzone.ondrop = function (e) {
    e.preventDefault();
    var img = $("#dropzone1").find("img");
    if (img.length != 1) {
        this.className = 'dropzone';
        dropzone_id = '#dropzone1';
        upload(e.dataTransfer.files);
    }
};

dropzone.ondragover = function () {
    this.className = 'dropzone dragover';
    return false;
};

dropzone.ondragleave = function () {
    this.className = 'dropzone';
    return false;
};


dropzone2.ondrop = function (e) {
    e.preventDefault();
    var img = $("#dropzone2").find("img");
    if (img.length != 1) {
        this.className = 'dropzone';
        dropzone_id = '#dropzone2';
        upload(e.dataTransfer.files);
    }
};

dropzone2.ondragover = function () {
    this.className = 'dropzone dragover';
    return false;
};

dropzone2.ondragleave = function () {
    this.className = 'dropzone';
    return false;
};

dropzone3.ondrop = function (e) {
    e.preventDefault();
    var img = $("#dropzone3").find("img");
    if (img.length != 1) {
        this.className = 'dropzone';
        dropzone_id = '#dropzone3';
        upload(e.dataTransfer.files);
    }
};

dropzone3.ondragover = function () {
    this.className = 'dropzone dragover';
    return false;
};

dropzone3.ondragleave = function () {
    this.className = 'dropzone';
    return false;
};


$(".dropzone").click(function () {
    $('#open_browse').trigger('click');
    dropzone_id = '#' + $(this).attr('id');
});


function fileAgentChange(files) {
    var file = files.files;
    if (file.length == 1)
        upload(file);
}

//--------------------------------
//search for tags (autocomplete)
//--------------------------------
// var tags_input = $("input#add-tags"),
//     tags_id = [];
//
// function split(val) {
//     return val.split(/,\s*/);
// }
//
// function extractLast(term) {
//     return split(term).pop();
// }
// $(document).on('keypress', tags_input, function (e) {
// // tags_input.keypress(function (e) {
//     var keycode = (e.keyCode ? e.keyCode : e.which);
//     var search_tags = extractLast($(this).val());
//     var search = $.ajax({
//         data: {
//             search_tags: search_tags,
//         },
//         url: '/account/projects-agent/search-tags',
//         type: 'post',
//         dataType: 'html',
//     });
//     search.done(function (action) {
//         action = JSON.parse(action || 'null');
//         var res = [];
//         var i;
//         for (i = 0; i < action.length; i++) {
//             if (action[i].tag != null) {
//                 res.push({value: action[i].tag, label: action[i].tag, data_id: action[i].id});
//             }
//         }
//
//         $(".agent-modal-product .modal-content #add-tags").autocomplete({
//             minLength: 0,
//             source: function (request, response) {
//                 response($.ui.autocomplete.filter(
//                     res, extractLast(request.term)));
//             },
//             focus: function () {
//                 return false;
//             },
//             select: function (e, ui) {
//                 var terms = split(this.value);
//                 terms.pop();
//                 terms.push('#' + ui.item.value);
//                 terms.push("");
//                 this.value = terms.join(", ");
//                 tags_id.push(ui.item.data_id);
//                 return false;
//             }
//         });
//     })
// })

//--------------------------------
//add company name for search (autocomplete)
//--------------------------------
var search_company_name = $("input[name='search-company-name']");
$(document).on('keyup', "input[name='search-company-name']", function (event) {
// company_name.keypress(function (event) {
    var keycode = (event.keyCode ? event.keyCode : event.which);
    var search_company = $(this).val();
    var search = $.ajax({
        data: {
            search_company: search_company,
        },
        url: '/account/projects-agent/search-company-name',
        type: 'post',
        dataType: 'html',
    });
    search.done(function (action) {
        action = JSON.parse(action || 'null');
        var companies = [],
            i;
        for (i = 0; i < action.length; i++) {
            if (action[i].company_name != null && action[i].id != null) {
                companies.push({value: action[i].company_name, label: action[i].company_name, id: action[i].id});
            }

        }
        $(".agent-modal-product .modal-content input[name='search-company-name']").autocomplete({
            minLength: 0,
            source: companies,
            focus: function (event, ui) {
                $(search_company_name).val(ui.item.label);
                $("input[name='search-company-name']").attr("data-id", ui.item.id);
                return false;
            },
            select: function (event, ui) {
                $("input[name='search-company-name']").val(ui.item.label);
                $("input[name='search-company-name']").attr("data-id", ui.item.id);
                return false;
            }
        });
    });
})

//--------------------------------
//add product name for search (autocomplete)
//--------------------------------
var search_product_name = $("input[name='search-product-name']");
$(document).on('keypress', "input[name='search-product-name']", function (event) {
// company_name.keypress(function (event) {
    var keycode = (event.keyCode ? event.keyCode : event.which);
    var search_product = $(this).val(),
        company_id = $(this).closest('form').find("input[name='search-company-name']").attr('data-id');
    var search = $.ajax({
        data: {
            search_product: search_product,
            company_id: company_id,
        },
        url: '/account/projects-agent/search-product-name',
        type: 'post',
        dataType: 'html',
    });
    search.done(function (action) {
        action = JSON.parse(action || 'null');
        var products = [],
            i;
        for (i = 0; i < action.length; i++) {
            if (action[i].product_name != null && action[i].id_prod != null) {
                products.push({value: action[i].product_name, label: action[i].product_name, id: action[i].id_prod});
            }

        }
        $(".agent-modal-product .modal-content input[name='search-product-name']").autocomplete({
            minLength: 0,
            source: products,
            focus: function (event, ui) {
                $(search_product_name).val(ui.item.label);
                return false;
            },
            select: function (event, ui) {
                $("input[name='search-product-name']").val(ui.item.label);
                $("input[name='search-product-name']").attr("data-id", ui.item.id);
                return false;
            }
        });
    });
})
//--------------------------------
//search products by name, company_id, collecions
//--------------------------------

$('body').on('click', '.button.search-product', function (e) {
    e.preventDefault();
    $('div.products').empty();
    var comp_id = $(this).closest('.modal-content').find("input[name='search-company-name']").attr('data-id');
    if (comp_id.length == 0){
        $('.no-object').show();
        return false;
    }
    $('.no-object').hide();
    $('#new-product').hide();
    // $('.search-results-not-found').hide();
    var collection_id = $(this).closest('form').find('select').val(),
        company = $(this).closest('form').find("input[name='search-company-name']").val(),
        product = $(this).closest('form').find("input[name='search-product-name']").val(),
        form = $(this).closest('form');
    var search = $.ajax({
        data: {
            collection_id: collection_id,
            company: company,
            product_name: product,
        },
        url: '/account/projects-agent/search-products',
        type: 'post',
        dataType: 'html',
    });
    search.done(function (result) {
        if (result == 'not found!') {
            form.removeClass('search-complete');
            $('.no-object').show();
        } else {
            result = JSON.parse(result || 'null');
            var products = '',
                i;
            if (result.length > 0) {
                form.addClass('search-complete');
                for (i = 0; i < result.length; i++) {
                    products += "<div class='product search-results'>";
                    products += "<div class='limited-offer'>";
                    products += "<p class='upload-portfolio download-portfolio join link-here' data-link='/about/product/" + result[i].id_prod + "' data-product-name='" + result[i].product_name + "'>";
                    products += "link here<a href=''></a></p>";
                    products += "</div>";
                    // products += "<a href='/about/product/" + result[i].id_prod + "'>";
                    products += "<img src='/media/products/" + result[i].main_image + "' alt='#'>";
                    products += "<p>" + result[i].product_name + "</p>";
                    products += "</div>";
                }
                $('div.products').append(products);
            } else {

                $('#new-product').show();
                form.removeClass('search-complete');
                // $('.search-results-not-found').show();
            }
        }

    })

})

//--------------------------------
//add product link into desription
//--------------------------------
$('body').on('click', '.link-here', function (e) {
    e.preventDefault();
    var description = $('#item-editor textarea').val();
    description += ' ' + "<a href='" + $(this).attr('data-link') + "'>" + $(this).attr('data-product-name') + "</a>"
    $('#item-editor textarea').val(description);
    $('.modal.fade.agent-modal-product.in').modal('hide');
})


//--------------------------------
//add company name (autocomplete)
//--------------------------------
var company_name = $("input[name='company-name']");
$(document).on('keypress', "input[name='company-name']", function (event) {
// company_name.keypress(function (event) {
    var keycode = (event.keyCode ? event.keyCode : event.which);
    var search_company = $(this).val();
    var search = $.ajax({
        data: {
            search_company: search_company,
        },
        url: '/account/projects-agent/search-company-name',
        type: 'post',
        dataType: 'html',
    });
    search.done(function (action) {
        action = JSON.parse(action || 'null');
        var companies = [],
            i;
        for (i = 0; i < action.length; i++) {
            if (action[i].company_name != null && action[i].id != null) {
                companies.push({value: action[i].company_name, label: action[i].company_name, id: action[i].id});
            }

        }
        $(".agent-modal-product .modal-content input[name='company-name']").autocomplete({
            minLength: 0,
            source: companies,
            focus: function (event, ui) {
                $(company_name).val(ui.item.label);
                return false;
            },
            select: function (event, ui) {
                $("input[name='company-name']").val(ui.item.label);
                $("input[name='company-name']").attr("data-id", ui.item.id);
                return false;
            }
        });
    });
})

//-------------------------------
//make special offer
//-------------------------------
$(document).on('change', '#special-offer', function () {
    $('div#discount-block').slideToggle();
})

//-------------------------------
//add new product
//-------------------------------
$(document).on('click', '#save-product', function () {
    $('div.error-message span').text('');
    $('div.error-message').hide();
    $('div.success-message').hide();
    var product_name = $("input[name='product-name']").val(),
        company_id = $(this).closest('.modal-content').find("input[name='company-name']").attr('data-id'),
        product_collection = $(this).closest('.modal-content').find("select[name='product-collection']").val(),
        prod_description = $(this).closest('.modal-content').find("textarea[name='description']").val(),
        price = $(this).closest('.modal-content').find("input[name='public-price']").val(),
        special_offer = $('#special-offer').prop('checked') == true ? 1 : 0,
        discount = $(this).closest('.modal-content').find("input[name='discount']").val(),
        tags_value = $(this).closest('.modal-content').find('div.tokenizer span.label'),
        tags = [],
        i;
    for (i = 0; i < tags_value.length; i++) {
        tags.push(tags_value[i].innerText);
    }

    send_data = $.ajax({
        data: {
            'company_id': company_id,
            'product_name': product_name,
            'product_collection': product_collection,
            'description': prod_description,
            'price': price,
            'image1': img_url1,
            'image2': img_url2,
            'image3': img_url3,
            // 'tags_id': tags_id,
            'special_offer': special_offer,
            'discount': discount,
            'tags': tags,
        },
        url: '/account/projects-agent/add-new-product',
        type: 'post',
        dataType: 'html',
    });
    send_data.done(function (result) {
        result = JSON.parse(result || 'null');
        if (result.response == 'done!') {
            var description = $('#item-editor textarea').val();
            description += ' ' + "<a href='/about/product/" + result.id_product + "'>" + product_name + "</a>"
            $('#item-editor textarea').val(description);
            $("input[name='product-name']").val('');
            $("input[name='company-name']").val('');
            $("textarea[name='description']").val('');
            $("input[name='public-price']").val('');
            $("input[name='discount']").val('');
            $('#special-offer').val('')
            $("input#add-tags").val('');
            dropzone.css('background', '');
            dropzone2.css('background', '');
            dropzone3.css('background', '');
            img_url1 = "";
            img_url2 = "";
            img_url3 = "";
            tags_id = "";
            tags_id = [];
            $('div.success-message').show();
            var str = localStorage.getItem('path') ? localStorage.getItem('path') : '',
                current_way = str.search('projects-agent/select'),
                current_page = document.URL,
                current_page_test = current_page.search('projects-agent/select');
            if (current_way > 0) {
                localStorage.setItem('description', localStorage.getItem('description') + ' ' + "<a href='/about/product/" + result.id_product + "'>" + product_name + "</a>");
                localStorage.removeItem('path');
                $(location).attr('href', str);
            } else if (current_page_test > 0) {
                setTimeout($('.modal.fade.agent-modal-product.in').modal('hide'), 3000);
            }

        } else {
            $('div.error-message span').text(result.response);
            $('div.error-message').show();
        }
    })
})

//-------------------------------
// pagination on Project page
//-------------------------------
$('body').delegate(".products-pagination a", "click", function (e) {
    e.preventDefault();
    var link = $(this).attr('href');
    $('.products-area').load(link + ' .products-area > *');
    $('.products-pagination').load(link + ' .products-pagination > *');
});

$("div.google-search input").keypress(function (event) {
    var keycode = (event.keyCode ? event.keyCode : event.which);
    //var tags = $(this).val();
    //
    //   console.log (tags);
    if (keycode == '13') {
        event.preventDefault();
        var q = $(this).val();
        if (q != '')
            window.open('https://www.google.com/#q=' + q, '_blank');
    }
})

//-------------------------------
// set default page-size on sp
//-------------------------------
$(document).on('change', '#sp-select-products', function () {
    var send_data = $.ajax({
        data: {
            'page_size': $(this).val()
        },
        url: '/account/projects-agent/change-page-size',
        type: 'post',
        dataType: 'html',
    });
    send_data.done(function () {
        location.reload();
    })
})

//-------------------------------
// sort by category on sp
//-------------------------------
$(document).on('change', '.categories-pg select', function () {
    var send_data = $.ajax({
        data: {
            'collection': $(this).val()
        },
        url: '/account/projects-agent/change-collection',
        type: 'post',
        dataType: 'html',
    });
    send_data.done(function () {
        location.reload();
    })
})

//-------------------------------
// save link into localstorage before redirect
//-------------------------------
$('body').on('click', '#link-to-create-company', function () {
    localStorage.removeItem('path');
    localStorage.setItem('path', document.URL);
    localStorage.setItem('description', $('#item-editor textarea').val());
})


$('body').on('click', '#main-link-to-create-company', function () {
    localStorage.removeItem('path');
    localStorage.setItem('path', document.URL);
})

$('#item-editor textarea').val(localStorage.getItem('description'));


//-------------------------------
//user statistic(graph)
//-------------------------------
//var pieColors = [
//    'rgb(51, 102, 102)',
//    'rgb(40, 124, 122)',
//    'rgb(51, 153, 153)',
//    'rgb(84, 175, 172)',
//    'rgb(153, 204, 204)',
//    'rgb(203, 228, 228)'
//];
//function getTotal(arr) {
//    var j,
//        myTotal = 0;
//
//    for (j = 0; j < arr.length; j++) {
//        myTotal += ( typeof arr[j] === 'number' ) ? arr[j] : 0;
//    }
//
//    return myTotal;
//}
//
//function drawPieChart(canvasId) {
//    var i,
//        canvas = document.getElementById(canvasId),
//        pieData = canvas.dataset.values.split(',').map(function (x) {
//            return parseInt(x, 10)
//        }),
//        halfWidth = canvas.width * .5,
//        halfHeight = canvas.height * .5,
//        ctx = canvas.getContext('2d'),
//        lastend = 0,
//        myTotal = getTotal(pieData);
//
//    ctx.clearRect(0, 0, canvas.width, canvas.height);
//
//    for (i = 0; i < pieData.length; i++) {
//        ctx.fillStyle = pieColors[i];
//        ctx.beginPath();
//        ctx.moveTo(halfWidth, halfHeight);
//        ctx.arc(halfWidth, halfHeight, halfHeight, lastend, lastend + ( Math.PI * 2 * ( pieData[i] / myTotal )), false);
//        ctx.lineTo(halfWidth, halfHeight);
//        ctx.fill();
//        lastend += Math.PI * 2 * ( pieData[i] / myTotal );
//    }
//}
//
//drawPieChart('canPie');
//


