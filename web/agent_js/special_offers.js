//-------------------------------------------
// Лайки
//-------------------------------------------
$('body').on('click', 'a.main-like', (function () {
        if ($(this).hasClass("active-social-button")) {
            $(this).removeClass("active-social-button");
        } else {
            $(this).addClass("active-social-button");
        }
        if ($(this).hasClass('product-like')) {
            send_url = '/site/productlikes';
        } else {
            send_url = '/site/likes';
        }
        if ($(this).attr('data-action') == 'single-product') {
            var text = $(this).next();
        } else {
            var text = $(this).parent().parent().parent().find("span.count-likes");
        }

        var like = $.ajax({
            data: {id: $(this).attr('data-id')},
            type: 'post',
            url: send_url,
            dataType: 'html',
        });
        like.done(function (action) {
            text.text(action);
        });
    })
)
$(".like-button").click(function () {
    var request = $.ajax({
        data: {id: this.id},
        url: '/account/join-group/likes',
        type: 'post',
        dataType: 'html',
    });
    request.done(function (action) {
        $(".like-button").text(action);
    });
})
if (typeof SortChangeLi != 'function') {
    function SortChangeLi(value, parent_class) {
        url = '/account/offers/sort';
        var request = $.ajax({
            data: {value: value,},
            url: url,
            type: 'post',
            dataType: 'html'
        });
        request.done(function () {
            location.reload();
        });
    }
}

