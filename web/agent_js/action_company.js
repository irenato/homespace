//-------------------------------
// error messages
//-------------------------------
var get = window.location;
var lang_param = get['href'].search(/\/en\//i);
if (lang_param != -1) {
    var agent_name_err = 'The name of the agent should not duplicate or contain the name of the company',
        error_message = 'Fill all fields!',
        check_fields_error1 = 'The field ',
        check_fields_error2 = 'can not be empty!',
        name_field_logo = 'Logo',
        name_field_avatar = 'Photo',
        password_error = 'Error when entering a password (the password is at least 6 characters or passwords are not the same)';
} else {
    var agent_name_err = 'Название агента не должно дублировать или содержать в себе название компании',
        error_message = 'Заполните все поля!',
        check_fields_error1 = 'Поле ',
        check_fields_error2 = 'не может быть пустым!',
        name_field_logo = 'Логотип',
        name_field_avatar = 'Фото',
        password_error = 'Ошибка при вводе пароля (длина пароля менее 6 символов либо пароли не совпадают)';
}

//-------------------------------
// upload logo(dropzone)
//-------------------------------
var dropzone = $("#dropzone1"),
    id_upload_el = '',
    company_logo = '',
    company_bg = '',
    company_avatar = '';

dropzone.ondrop = function (e) {
    e.preventDefault();
    var img = $("#dropzone1").find("img");
    if (img.length != 1) {
        this.className = 'dropzone';
        id_upload_el = 'logo';
        upload(e.dataTransfer.files);
    }
};

dropzone.ondragover = function () {
    this.className = 'dropzone dragover';
    return false;
};

dropzone.ondragleave = function () {
    this.className = 'dropzone';
    return false;
};

dropzone.click(function () {
    $('#open_browse').trigger('click');
    id_upload_el = 'logo';
});
//-------------------------------
// upload avatar
//-------------------------------
$('#profile-photo').click(function (e) {
    e.preventDefault();
    $('#open_browse1').trigger('click');
    id_upload_el = 'avatar';
    that_use = 1;
    action_img = 0;
});
//-------------------------------
// upload background
//-------------------------------
$('a.company-bg').click(function (e) {
    e.preventDefault();
    $('#open_browse2').trigger('click');
    id_upload_el = 'bg';
    that_use = 1;
    action_img = 0;
});
//-------------------------------
// upload avatar(consultant)
//-------------------------------

$('#consultant-photo').click(function (e) {
    e.preventDefault();
    $('#open_browse1').trigger('click');
    id_upload_el = 'consultant';
    that_use = 1;
    action_img = 0;
});

//-------------------------------
// upload image(main functions)
//-------------------------------

function fileChange(files) {
    var file = files.files;
    if (file.length == 1) {
        upload(file);
    }
}
var upload = function (files) {
    var formData = new FormData();
    formData.append('file', files[0]);
    var request = $.ajax({
        data: formData,
        url: '/account/projects-agent/upload',
        type: 'post',
        dataType: 'html',
        contentType: false,
        processData: false,
    });

    request.done(function (url) {
        switch (id_upload_el) {
            case 'logo':
                $("#dropzone1").css('background', 'url(' + url + ')');
                company_logo = url;
                break;
            case 'avatar':
                $("a#profile-photo img").attr("src", url);
                company_avatar = url;
                break;
            case 'consultant':
                $("a#consultant-photo img").attr("src", url);
                break;
            case 'bg':
                $("div.agent-profil").css('background-image', 'url(' + url + ')');
                company_bg = url;
                break;
        }
    });

    request.fail(function (jqXHR, textStatus) {
        alert(textStatus);
    });
};
//-------------------------------
// add consultants (deprecated)
//-------------------------------

$(document).on("click", "#add-consultant", function (e) {
    e.preventDefault();
    $("div.consultant p.pass span").text('');
    $("div.consultant p.pass").hide();
    var consultant_first_name = $("input[name='consultant-first-name']").val(),
        consultant_last_name = $("input[name='consultant-last-name']").val(),
        consultant_phone = $("input[name='consultant-phone']").val(),
        consultant_position = $("input[name='consultant-position']").val(),
        consultant_avatar = $("a#consultant-photo img").attr("src"),
        messages_status = $("div.consultant").find("input[type='radio']:checked");
    if (consultant_position == '' || consultant_first_name == '' || consultant_last_name == '' || consultant_phone == '' || consultant_avatar == '') {
        $("div.consultant p.pass span").text(error_message);
        $("div.consultant p.pass").show();
    } else {
        $("ul.consultants-list").append("<li data-id=" + messages_status.attr('id') + "><a href='#'><img src=" + consultant_avatar + " alt='#'>" + consultant_first_name + " " + consultant_last_name + " <span>(" + consultant_position + ")</span></a><b>Give rights<i class='icon-black218 '></i></b></li>");
        $("input[name='consultant-first-name']").val('');
        $("input[name='consultant-last-name']").val('');
        $("input[name='consultant-position']").val('');
        $("input[name='consultant-phone']").val('');
        $("a#consultant-photo img").attr("src", '');
    }
})

//-------------------------------
// add categories (autocomplete)
//-------------------------------
var categories_input = $("input[name='categories']");
//categories_id = [];

function split(val) {
    return val.split(/,\s*/);
}

function extractLast(term) {
    return split(term).pop();
}

categories_input.keypress(function (e) {
    var keycode = (e.keyCode ? e.keyCode : e.which);
    var search_categories = extractLast($(this).val());
    var search = $.ajax({
        data: {
            search_categories: search_categories,
        },
        url: '/account/company/search-categories',
        type: 'post',
        dataType: 'html',
    });
    search.done(function (action) {
        action = JSON.parse(action || 'null');
        var res = [];
        var i;
        for (i = 0; i < action.length; i++) {
            if (action[i].category != null) {
                res.push({value: action[i].category, label: action[i].category, id: action[i].id});
            }
        }

        categories_input.autocomplete({
            minLength: 0,
            source: function (request, response) {
                response($.ui.autocomplete.filter(
                    res, extractLast(request.term)));
            },
            focus: function () {
                return false;
            },
            select: function (e, ui) {
                var terms = split(this.value);
                terms.pop();
                terms.push(ui.item.value);
                terms.push("");
                this.value = terms.join(", ");
                //categories_id.push(ui.item.id);
                return false;
            }
        });
    })
})

//-------------------------------
// add video
//-------------------------------
$("input[name='video-link']").keypress(function (e) {
    if (e.keyCode == 13) {
        var link = $(this).val();
        if ($("div.video img").hasClass("video-img")) {
            $("div.video img").replaceWith('<iframe class="video-img" width="330" height="181" src="' + link + '" frameborder="0" allowfullscreen></iframe>');
        } else {
            $("iframe.video-img").attr("src", link);
        }
    }
});

//-------------------------------
// check fields error message
//-------------------------------
var show_error_message = function (elem) {
    $('p.check-form-error-message span').text(check_fields_error1 + elem + " " + check_fields_error2);
    $('p.check-form-error-message').show();
}

//-------------------------------
// check fields passwords error message
//-------------------------------
var show_error_password_message = function () {
    $('p.check-form-error-message span').text(password_error);
    $('p.check-form-error-message').show();
}

//-------------------------------
// create company
//-------------------------------
var company_name_val;
$('body').on("click", "a#create-company", function (e) {
    e.preventDefault();
    $('p.check-form-error-message span').text('');
    $('p.check-form-error-message').hide();
    $('p.result-success span').text('');
    $('p.result-success').hide();
    company_name_val = $("input[name='company-name']").val();
    var company_name = $("input[name='company-name']"),
        brand_name = $("input[name='brand-name']"),
        company_position = $("input[name='position']"),
        company_first_name = $("input[name='first-name']"),
        company_last_name = $("input[name='last-name']"),
        company_city = $("input[name='city']"),
        company_country = $("input[name='country']"),
        company_phone = $("input[name='phone']"),
        company_address = $("input[name='address']"),
        company_site = $("input[name='site']"),
        company_message = $("textarea[name='message']"),
        // company_email = $("input[name='email']"),
        company_login = $("input[name='login']"),
        company_password = $("input[name='password']"),
        // company_confirm_password = $("input[name='confirm']"),
        // company_video_link = $("input[name='video-link']"),
        // company_video_name = $("input[name='video-name']"),
        company_collections = $("input[name='categories']");
    if (company_name.val() == '') {
        show_error_message($("label[for='company-name']").text());
    } else if (brand_name.val() == '') {
        show_error_message($("label[for='brand-name']").text());
    } else if (company_position.val() == '') {
        show_error_message($("label[for='position']").text());
    } else if (company_first_name.val() == '') {
        show_error_message($("label[for='first-name']").text());
    } else if (company_last_name.val() == '') {
        show_error_message($("label[for='last-name']").text());
    } else if (company_city.val() == '') {
        show_error_message($("label[for='city']").text());
    } else if (company_phone.val() == '') {
        show_error_message($("label[for='phone']").text());
    } else if (company_address.val() == '') {
        show_error_message($("label[for='address']").text());
    } else if ((company_name.val()).indexOf(brand_name.val()) == 0) {
        show_error_message(agent_name_err);
    } else if ((brand_name.val()).indexOf(company_name.val()) == 0) {
        show_error_message(agent_name_err);
        // } else if (company_logo == '') {
        //     show_error_message(name_field_logo);
        // } else if (company_avatar == '') {
        //     show_error_message(name_field_avatar);
        // } else if (company_email.val() == '') {
        //     show_error_message($("label[for='email']").text());
    } else if (company_login.val() == '') {
        show_error_message($("label[for='login']").text());
    } else if (company_password.val() == '') {
        show_error_message($("label[for='password']").text());
        // } else if (company_password.val() != company_confirm_password.val() && company_password.val().length < 6) {
        //     show_error_password_message(password_error);
    } else {
        var send_data = $.ajax({
            data: {
                'id': $(this).attr('data-id'),
                'company_name': company_name.val(),
                'brand_name': brand_name.val(),
                'company_first_name': company_first_name.val(),
                'company_last_name': company_last_name.val(),
                'company_country': company_country.val(),
                'company_city': company_city.val(),
                'company_phone': company_phone.val(),
                'company_address': company_address.val(),
                'company_position': company_position.val(),
                'company_site': company_site.val(),
                'company_message': company_message.val(),
                // 'company_email': company_email.val(),
                'company_login': company_login.val(),
                'company_password': company_password.val(),
                // 'company_video_link': company_video_link.val(),
                // 'company_video_name': company_video_name.val(),
                // 'company_logo': company_logo,
                // 'company_bg': company_bg,
                // 'company_avatar': company_avatar,
                'company_collections': company_collections.val(),
            },
            url: '/account/company/create-new-company',
            type: 'post',
            dataType: 'html'
        });
        send_data.done(function (action) {
            action = JSON.parse(action || 'null');
            if (action.result === 'done!') {
                $('p.result-success span').text('Changes saved correctly!');
                $('p.result-success').show();
                $("div.company.create input[type='text']").val('');
                $("div.company.create textarea").val('');
                $('.agent-modal-product-new').find("input[name='company-name']").attr('data-id', action.company_id);
                $('.agent-modal-product-new').find("input[name='company-name']").val(action.company_name);
                $('.agent-modal-product-new').modal('show');
            } else {
                $('p.check-form-error-message span').text('Changes not saved correctly!');
                $('p.check-form-error-message').show();
            }
        })
        send_data.fail(function () {

            $('p.check-form-error-message span').text('Changes not saved correctly!');
            $('p.check-form-error-message').show();
        })
    }
})
//-------------------------------
// pagination on Advertising tools page
//-------------------------------
$('body').delegate(".agent-comp-pagination a", "click", function (e) {
    e.preventDefault();
    var link = $(this).attr('href');
    $('.companies-by-agent').load(link + ' .companies-by-agent > *');
    $('.agent-comp-pagination').load(link + ' .agent-comp-pagination > *');
});
//-------------------------------
// edit company
//-------------------------------
$(document).on("click", "a#edit-company", function (e) {
    e.preventDefault();
    $('p.check-form-error-message span').text('');
    $('p.check-form-error-message').hide();
    $('p.result-success span').text('');
    $('p.result-success').hide();
    company_name_val = $("input[name='company-name']").val();
    var company_name = $("input[name='company-name']"),
        brand_name = $("input[name='brand-name']"),
        company_position = $("input[name='position']"),
        company_first_name = $("input[name='first-name']"),
        company_last_name = $("input[name='last-name']"),
        company_country = $("input[name='country']"),
        company_city = $("input[name='city']"),
        company_phone = $("input[name='phone']"),
        company_address = $("input[name='address']"),
        company_site = $("input[name='site']"),
        company_message = $("textarea[name='message']"),
        // company_email = $("input[name='email']"),
        company_login = $("input[name='login']"),
        company_password = $("input[name='password']"),
        // company_confirm_password = $("input[name='confirm']"),
        // company_video_link = $("input[name='video-link']"),
        // company_video_name = $("input[name='video-name']"),
        company_collections = $("input[name='categories']");
    // if (company_password.val() != company_confirm_password.val()) {
    if (company_password.val().length < 6) {
        show_error_password_message(password_error);
    } else if ((company_name.val()).indexOf(brand_name.val()) == 0) {
        show_error_message(agent_name_err);
    } else if ((brand_name.val()).indexOf(company_name.val()) == 0) {
        show_error_message(agent_name_err);
    } else {
        var send_company_data = $.ajax({
            data: {
                'id': $(this).attr('data-id'),
                'company_name': company_name.val(),
                'brand_name': brand_name.val(),
                'company_first_name': company_first_name.val(),
                'company_last_name': company_last_name.val(),
                'company_country': company_country.val(),
                'company_city': company_city.val(),
                'company_phone': company_phone.val(),
                'company_address': company_address.val(),
                'company_position': company_position.val(),
                'company_site': company_site.val(),
                'company_message': company_message.val(),
                // 'company_email': company_email.val(),
                'company_login': company_login.val(),
                'company_password': company_password.val(),
                // 'company_video_link': company_video_link.val(),
                // 'company_video_name': company_video_name.val(),
                // 'company_logo': company_logo,
                // 'company_bg': company_bg,
                // 'company_avatar': company_avatar,
                'company_collections': company_collections.val(),
            },
            url: '/account/company/edit-company',
            type: 'post',
            dataType: 'html'
        });
        send_company_data.done(function (action) {
            action = JSON.parse(action || 'null');
            if (action.result === 'done') {
                $('p.result-success span').text('Changes saved correctly!');
                $('p.result-success').show();
                $("div.company.create input[type='text']").val('');
                $("div.company.create textarea").val('');
                $('.agent-modal-product-new').find("input[name='company-name']").attr('data-id', action.company_id);
                $('.agent-modal-product-new').find("input[name='company-name']").val(action.company_name);
                $('.agent-modal-product-new').modal('show');
            } else {
                $('p.check-form-error-message span').text(action.result);
                $('p.check-form-error-message').show();
            }
        })
        send_company_data.fail(function (action) {
            $('p.check-form-error-message span').text('Changes not saved correctly!');
            $('p.check-form-error-message').show();
        })
    }
})

// $('body').on('click', '#disabled-input', function(e){
//     e.stopPropagation();
// })

// console.log(localStorage.getItem('path'));

