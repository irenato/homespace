//-------------------------------
// pagination on Agents page
//-------------------------------
$('body').delegate(".agent-pagination a", "click", function (e) {
    e.preventDefault();
    var link = $(this).attr('href');
    $('.companys').load(link + ' .companys > *');
    $('.agent-pagination').load(link + ' .agent-pagination > *');
});

$('body').delegate(".teaser-pagination a", "click", function (e) {
    e.preventDefault();
    var link = $(this).attr('href');
    $('.companys').load(link + ' .companys > *');
    $('.preloader').show();
    $('.agent-pagination').load(link + ' .teaser-pagination > *', function () {
        $('.preloader').hide();
    });
});

$('body').delegate(".teaser-pagination a", "click", function (e) {
    e.preventDefault();
    var link = $(this).attr('href');
    $('.companys').load(link + ' .companys > *');
    $('.preloader').show();
    $('.agent-pagination').load(link + ' .teaser-pagination > *', function () {
        $('.preloader').hide();
    });
});

//-------------------------------
//add new teaser (show/hide block)
//-------------------------------
$('body').on('click', '.add-new-teaser', function (e) {
    e.preventDefault();
    $("span[data-id = 'categories']").detach();
    $('.select2-selection.select2-selection--multiple').append("<span class='select2-selection__choice' data-id='categories'>Categories</span>");
    $('div#add-new-teaser').slideToggle();
})

$('body').on('click', 'div.form-group.field-groups-colections.required', function () {
    $("span[data-id = 'categories']").detach();
})

// //-------------------------------
// //add new teaser
// //-------------------------------
// $('body').on('click', '#create-new-teaser', function (e) {
//     e.preventDefault();
//     $('#hidden-button').trigger('click');
//     var data = [];
//         data.push($('#groups-title').val());
//         data.push($('#pm-desc').val());
//         data.push($('#pm_collections').val());
//         data.push($('#pm_categories').val());
//         data.push($('#groups-cities_id').val());
//         data.push($('#pm-desc').val());
//     var send_data = $.ajax({
//         data: {
//             pt_data: JSON.stringify(data),
//         },
//         url: '/account/promotional-teaser/create-p-t',
//         type: 'post',
//         dataType: 'html',
//     });
// })

//-------------------------------
//find cities(google API)
//-------------------------------
jQuery(document).ready(function ($) {

    var options_city = {
            types: ['(cities)']
        },
        places = [];

    var input = document.getElementById('google-search-cities');
    autocomplete = new google.maps.places.Autocomplete(input, options_city);
    google.maps.event.addListener(autocomplete, 'place_changed', function () {
        var place = autocomplete.getPlace();
        places.push(place.place_id);
        $('#google-search-cities').val('');
        $("#search-result-items").append("<li><a href='#' class='del-before-upload' style='display : table;'><span class='city-name' data-id=" + place.place_id + ">" + place.name + " </span><span class='del'> (x)</span></a></li>");
        $('#groups-cities_id').val($('#groups-cities_id').val() + place.place_id + ',');
    });

    $(document).on('click', 'span.del', function (e) {
        e.preventDefault();

        var places_id = $('#groups-cities_id').val(),
            place_id = $(this).parent().find('span.city-name').attr('data-id');
        $('#groups-cities_id').val(places_id.replace(place_id + ',', ''));
        $(this).closest('li').detach();

    })

});

//-------------------------------
// edit teasers (show block)
//-------------------------------
var desc,
    company_name,
    teaser_first_name,
    teaser_last_name,
    cities_name,
    cities_id,
    categories_list,
    categories_list_id,
    collections_list,
    collections_list_id,
    teaser_avatar_img,
    teaser_logo,
    teaser_title,
    cities_name_arr,
    cities_id_arr,
    categories_input;
$('body').on('click', 'p.edit-teaser', function (e) {
    e.preventDefault();
    $(this).removeClass('edit-teaser');
    var parent_div = $(this).closest('div.company.agents'),
        teaser_title = parent_div.find('h4.teaser-title').text(),
        desc = parent_div.find('p.descriptions').text(),
        teaser_first_name = parent_div.find('h5.teaser-first-name').text(),
        teaser_last_name = parent_div.find('span.teaser-last-name').text(),
        cities_name = parent_div.find('div.city').attr('data-cities'),
        cities_id = parent_div.find('div.city').attr('data-cities-id'),
        categories_list = parent_div.find('ul.categories').attr('data-categories-list'),
        categories_list_id = parent_div.find('ul.categories').attr('data-categories-id'),
        collections_list = parent_div.find('ul.categories.best').attr('data-collections-list'),
        collections_list_id = parent_div.find('ul.categories.best').attr('data-collections-id'),
        teaser_avatar_img = parent_div.find('img.company-img').attr('src'),
        teaser_logo = parent_div.find('img.agent-teaser-logo-img').attr('src'),
        id = $(this).attr('data-id');

    parent_div.find('div.main-block').detach();
    parent_div.addClass('edite');
    $('div.use-it-for-edit-teaser').find('div.main-block').clone().appendTo(parent_div);
    var id_must_be_destroyed = $("input#google-search-cities-edit");
    id_must_be_destroyed.attr('id', '');
    parent_div.find('input.search-cities').attr('id', 'google-search-cities-edit');
    parent_div.find('input.teaser-title-edit').val(teaser_title);
    parent_div.find('p.company-name-edit').text(company_name);
    parent_div.find('img.company-img').attr('src', teaser_avatar_img);
    parent_div.find('img.company-img-edit').attr('src', teaser_logo);
    parent_div.find('img.company-avatar-edit').attr('src', teaser_avatar_img);
    parent_div.find('h5.teaser-first-name-edit').text(teaser_first_name);
    parent_div.find('span.teaser-last-name-edit').text(teaser_last_name);
    parent_div.find('textarea.description').val(desc);
    parent_div.find('input.teaser-categories-edit').val(categories_list + ',');
    parent_div.find('input.teaser-categories-edit').attr('data-id', categories_list_id);
    parent_div.find('textarea.teaser-collections-edit').val(collections_list);
    parent_div.find('textarea.teaser-collections-edit').attr('data-id', collections_list_id);
    parent_div.find('input.current-teaser-edit').attr('data-id', id);
    $(parent_div).find("input[type='checkbox']").attr('id', $(parent_div).find("input[type='checkbox']").attr('id') + id);
    $(parent_div).find("input[type='checkbox']").attr('name', $(parent_div).find("input[type='checkbox']").attr('name') + id);
    $(parent_div).find("input[type='checkbox']").next().attr('for', $(parent_div).find("input[type='checkbox']").next().attr('for') + id);
    var cities_list = '';
    cities_name_arr = cities_name.split(',');
    cities_id_arr = cities_id.split(',');
    for (i = 0; i < cities_name_arr.length; i++) {
        cities_list += "<li><a href='#' class='del-before-upload' style='display : table;'><span class='city-name' data-id=" + cities_id_arr[i] + ">" + cities_name_arr[i] + " </span><span class='del'> (x)</span></a></li>";
    }
    parent_div.find('ul.search-result-items').append(cities_list);

    var options = {
        types: ['(regions)']
    };
    var options_city = {
            types: ['(cities)']
        },
        places = [];

    var input = document.getElementById('google-search-cities-edit');
    autocomplete = new google.maps.places.Autocomplete(input, options_city);
    google.maps.event.addListener(autocomplete, 'place_changed', function () {
        var place = autocomplete.getPlace();
        places.push(place.place_id);
        $('#google-search-cities-edit').val('');
        parent_div.find("ul.search-result-items").append("<li><a href='#' class='del-before-upload' style='display : table;'><span class='city-name' data-id=" + place.place_id + ">" + place.name + " </span><span class='del'> (x)</span></a></li>");
        if (place.place_id != '')
            cities_id_arr.push(place.place_id);
    });
    console.log(cities_id_arr);
})


function split(val) {
    return val.split(/,\s*/);
}

function extractLast(term) {
    return split(term).pop();
}

$('body').on('keypress', 'input.teaser-categories-edit', function (e) {
    var categories_list = $(this);
    var categories_id = categories_list.attr('data-id').split(',');
    var keycode = (e.keyCode ? e.keyCode : e.which);
    var search_categories = extractLast($(this).val());
    var search = $.ajax({
        data: {
            search_categories: search_categories,
        },
        url: '/account/advertising-tools/search-categories',
        type: 'post',
        dataType: 'html',
    });
    search.done(function (action) {
        action = JSON.parse(action || 'null');
        var res = [];
        var i;
        for (i = 0; i < action.length; i++) {
            if (action[i].category != null) {
                res.push({value: action[i].category, label: action[i].category, id: action[i].id});
            }
        }

        categories_list.autocomplete({
            minLength: 0,
            source: function (request, response) {
                response($.ui.autocomplete.filter(
                    res, extractLast(request.term)));
            },
            focus: function () {
                return false;
            },
            select: function (e, ui) {
                var terms = split(this.value);
                terms.pop();
                terms.push(ui.item.value);
                terms.push("");
                this.value = terms.join(", ");
                categories_id.push(ui.item.id);
                console.log(categories_id);
                return false;
            }
        });
    })
})

//-------------------------------
// warning repairs
//-------------------------------
$('body').on('click', '.button.current-teaser-edit', function (e) {
    e.preventDefault();
    var parent_div = $(this).closest('.edite'),
        teaser_id = $(this).attr('data-id'),
        teaser_name = $(parent_div).find('.teaser-title-edit').val(),
        teaser_status = $(parent_div).find('.checkbox input'),
        teaser_description = $(parent_div).find('.description').val(),
        teaser_cities = cities_id_arr,
        teaser_categories = $(parent_div).find('.teaser-categories-edit').val(),
        teaser_collections = $(parent_div).find('.teaser-collections-edit').val(),
        send_data = $.ajax({
            data: {
                teaser_id: teaser_id,
                teaser_name: teaser_name,
                teaser_description: teaser_description,
                teaser_cities: teaser_cities,
                teaser_categories: teaser_categories,
                teaser_collections: teaser_collections,
                teaser_status: $(teaser_status).prop('checked') == true ? '1' : '0',
            },
            url: '/account/promotional-teaser/edit',
            type: 'post',
            dataType: 'html',
        });
    send_data.done(function (result) {
        if (result == 'Done!') {
            location.reload();
        }
    })
})

//-------------------------------
// edit teasers (send data)
//-------------------------------
//$('body').on('click', 'input.current-teaser-edit', function (e) {
//    e.preventDefault();
//    parent_div = $(this).closest('div.company.agents');
//    var teaser_title =  parent_div.find('input.teaser-title-edit').val();
//    var teaser_description =  parent_div.find('textarea.description').val();
//    var teaser_description =  parent_div.find('textarea.description').val();
//    console.log(cities_id_arr);
//})



