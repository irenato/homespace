$('body').on('change', 'select.view-by', function () {
    var page_size = $(this).val(),
        send_data = $.ajax({
            data: {
                'page_size': page_size,
            },
            url: '/manufacturers/index',
            type: 'post',
            dataType: 'html',
        });
    send_data.done(function () {
        location.reload();
    })
})

$('body').on('change', 'select.choose-category', function () {
    var companies_category = $(this).val(),
        send_data = $.ajax({
            data: {
                'companies_category': companies_category,
            },
            url: '/manufacturers/index',
            type: 'post',
            dataType: 'html',
        });
    send_data.done(function () {
        location.reload();
    })
})

$('body').on('keypress', '#manufacturer_name', function (event) {
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if (keycode == '13') {
        if ($(this).val() !== "" && $(this).val().length > 0) {
            event.preventDefault();
            var send = $.ajax({
                data: {
                    'manufacturer_name': $(this).val(),
                },
                url: '/manufacturers/index',
                type: 'post',
                dataType: 'html',
            });
            send.done(function () {
                location.reload();
            })
        }
    }
})
