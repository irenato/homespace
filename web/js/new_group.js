//-------------------------------------------
// подтверждение дизайнером вступления в группу
//-------------------------------------------

$('body').on('click', 'a.accept-invitation', function (e) {
    e.preventDefault();
    var id_agent = $(this).attr('data-id'),
        current_group = $(this),
        add_to_group = $.ajax({
            data: {
                'id_agent': id_agent,
            },
            url: '/account/join-group/add-to-group',
            type: 'post',
            dataType: 'html',
        });
    add_to_group.done(function (data) {
        if (data == 'done!')
            current_group.closest('.company').detach();
        location.reload();
    })
})

//-------------------------------------------
// отклонение дизайнером предложения
// о вступлении в группу
//-------------------------------------------
$('body').on('click', 'a.cancel-invitation', function (e) {
    e.preventDefault();
    var id_agent = $(this).attr('data-id'),
        current_group = $(this),
        escape_from_group = $.ajax({
            data: {
                'id_agent': id_agent,
            },
            url: '/account/join-group/escape-from-group',
            type: 'post',
            dataType: 'html',
        });
    escape_from_group.done(function (data) {
        if (data == 'done!')
            current_group.closest('.company').detach();
        location.reload();
    })
})
