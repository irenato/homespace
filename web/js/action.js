jQuery(document).ready(function ($) {
    //-------------------------------------------
    //Сортировка по тегам (не работает)
    //-------------------------------------------
    $("input.text-input.stworks-tags").keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        //var tags = $(this).val();
        //
        //   console.log (tags);
        if (keycode == '13') {
            event.preventDefault();
            ///**/
            var request = $.ajax({

                data: {value: $(this).val(), parent_class: 'st_tags'},
                url: '/site/index',
                type: 'post',
                dataType: 'html',
            });
            request.done(function (action) {
                location.reload();
            });
        }
    })
    $("input.text-input.products-tags").keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        //var tags = $(this).val();
        //
        //   console.log (tags);
        if (keycode == '13') {
            event.preventDefault();
            ///**/
            var request = $.ajax({
                data: {value: $(this).val(), parent_class: 'products_tags'},
                url: '/site/index',
                type: 'post',
                dataType: 'html',
            });
            request.done(function (action) {
                location.reload();
            });
        }
    })
    $("input.text-input.group-tags").keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        //   console.log (tags);
        if (keycode == '13') {
            event.preventDefault();
            console.log($(this).val());

            var request = $.ajax({

                data: {value: $(this).val(), parent_class: 'group_tags'},
                url: '/account/join-group/index',
                type: 'post',
                dataType: 'html',
            });
            request.done(function (action) {
                location.reload();
            });
        }
    })
});
//-------------------------------------------
//Сортировка + колличество вывода на страницу
//-------------------------------------------
if (typeof SortChangeLi != 'function') {
    function SortChangeLi(value, parent_class) {
        var url, str = document.URL,
            doom = str.search('account');
        if (doom > 0) {
            url = '/account/join-group/index';
            var request = $.ajax({
                data: {value: value, parent_class: parent_class},
                url: url,
                type: 'post',
                dataType: 'html'
            });
            request.done(function () {
                location.reload();
            });
        } else {
            url = '/site/index';
            var get = $.ajax({
                data: {value: value, parent_class: parent_class},
                url: url,
                type: 'post',
                dataType: 'html'
            });
            get.done(function () {
                location.reload();
            });
        }
    }
}
//-------------------------------------------
// Лайки
//-------------------------------------------
$('body').on('click', 'a.main-like', (function () {
        if ($(this).hasClass("active-social-button")) {
            $(this).removeClass("active-social-button");
        } else {
            $(this).addClass("active-social-button");
        }
        if ($(this).hasClass('product-like')) {
            send_url = '/site/productlikes';
        } else {
            send_url = '/site/likes';
        }
        if ($(this).attr('data-action') == 'single-product') {
            var text = $(this).next();
        } else {
            var text = $(this).parent().parent().parent().find("span.count-likes");
        }

        var like = $.ajax({
            data: {id: $(this).attr('data-id')},
            type: 'post',
            url: send_url,
            dataType: 'html',
        });
        like.done(function (action) {
            text.text(action);
        });
    })
)
$(".like-button").click(function () {
    var request = $.ajax({
        data: {id: this.id},
        url: '/account/join-group/likes',
        type: 'post',
        dataType: 'html',
    });
    request.done(function (action) {
        $(".like-button").text(action);
    });
})

////Корректировка DIV для работы формы входа
//$("<label class='control-label' for='loginform-password'>Password*</label>").insertAfter('input#loginform-password');
//$("<label class='control-label' for='loginform-username'>Username or e-mail</label>").insertAfter('input#loginform-username');
//-------------------------------------------
//Переключение между работами
//-------------------------------------------
$(".about-user-item img").click(function () {
    $("div.img-item a").attr('href', $(this).parent().attr('data-link'));
    $("div.img-item img").attr('src', $(this).attr('src'));
    $("div.img-item p").text($(this).attr('alt'));
    $("p.description-item").text($(this).parent().attr("data-description"));
    $('html, body').animate({scrollTop: $(".portfolio-top").offset().top - 100}, 1000);
});
$(".about-user-item2 img").click(function () {
    $("div.img-item2 a").attr('href', $(this).parent().attr('data-link'));
    $("div.img-item2 img").attr('src', $(this).attr('src'));
    $("div.img-item2 p").text($(this).attr('alt'));
    $("p.description-item2").text($(this).parent().attr("data-description"));
});
//-------------------------------------------
//Добавление отзывов
//-------------------------------------------
var get = window.location;
var lang_param = get['href'].search(/\/en\//i);
if (lang_param != -1) {
    var limit_warning = 'Exceeded the daily limit comments (20)';
} else {
    var limit_warning = 'Превышен суточный лимит комментариев (20)';
}
$("#comment-send").click(function (e) {
    e.preventDefault();
    var comment = $(this).prev().val();
    var recipient = $("div.pf-new").attr("data-user");
    if (comment.length > 0 && comment != "") {
        var request = $.ajax({
            data: {comment: comment, recipient: recipient},
            url: '/about/comment',
            type: 'post',
            dataType: 'html'
        });
        request.done(function (action) {
            action = JSON.parse(action);
            if (action['check'] == true) {
                $("div.reviews-block").prepend('<div class="reviews-comment active"><div class="reviews-photo new-review-photo"><a href=""><img src="' + action['avatar'] + '" alt=""></a><p class="name">' + action['author'] + '</p></div><p>' + action['comment'] + '</p> <p class="data">' + action['date'] + '</p> </div>');
                $('#comment').val('');
            } else {
                alert(limit_warning);
            }
        });
    }
});
$("#guest-comment-send").click(function (e) {
    e.preventDefault();
    $('.how_it_words').modal('show');
});
//-------------------------------------------
//Оправка по Enter
//-------------------------------------------
$("#comment").keypress(function (e) {
    if (e.keyCode == 13) {
        e.preventDefault();
        $("#comment-send").trigger("click");
    }
});
//-------------------------------------------
//отключаю действие по умолчанию на пагинацию
//на странице about
//-------------------------------------------
var str = document.URL,
    searchUrl = str.search('about');

if (searchUrl > 0) {
    $('body').delegate(".hvr-radial-out a", "click", function (e) {
        e.preventDefault();
        var link = $(this).attr('href');
        $('.reviews-block').load(link + ' .reviews-block > *');
        $('html, body').animate({scrollTop: $(".reviews").offset().top - 150}, 1000);
    });
}
//-------------------------------------------
// лайки на странице about/user
//-------------------------------------------
$("div.profil-photo").click(function () {
    var like = $.ajax({
        data: {id: $(this).find("i.icon-heart297").attr("id")},
        type: 'post',
        url: '/about/likes',
        dataType: 'html'
    });
    like.done(function (action) {
        $("div.profil-photo i.icon-heart297 p").text(action);
        if ($("div.profil-photo i").hasClass("icon-heart2971")) {
            $("div.profil-photo i").removeClass("icon-heart2971");
        } else {
            $("div.profil-photo i").addClass("icon-heart2971");
        }
    });
});

//-------------------------------------------
// make special offers on products page
//-------------------------------------------
$('body').on('click', 'p.make-so-btn', function () {
    $('.make-special-offers #make-s-o').attr('data-id', $(this).attr('data-id'));
});

$('body').on('click', '#make-s-o', function () {
    var discount = $(this).closest('.open-create-product').find("input[name='discount']"),
        s_message = $(this).closest('.open-create-product').find('div.success-message'),
        e_message = $(this).closest('.open-create-product').find('div.error-message');
    $(s_message).hide();
    $(e_message).hide();
    if ((discount.val()).length > 0) {
        var send_data = $.ajax({
            data: {
                id: $(this).attr('data-id'),
                discount: discount.val()
            },
            type: 'post',
            url: '/catalog/make-special-offer',
            dataType: 'html'
        });
        send_data.done(function (result) {
            if (result == 'Done!') {
                $(s_message).show();
                location.reload();
            } else {
                $(e_message).find('span.text-danger').text(result);
                $(e_message).show();
            }
        })
    }
});

//-------------------------------------------
// Add review for product
//-------------------------------------------
$('body').on('click', '.review-for-products', function () {
    var id = $(this).attr('data-id'),
        text = $(this).closest('div').find('textarea').val();
    if (text.length > 0) {
        var send_data = $.ajax({
            data: {
                id: id,
                text: text,
            },
            type: 'post',
            url: '/about/add-comment-for-product',
            dataType: 'html'
        });
        send_data.done(function (result) {
            if (result == 'Done!') {
                location.reload()
            }
        })
    }
})

//-------------------------------------------
// Autocomplete for search-tags form into producs
// ------------------------------------------

function split(val) {
    return val.split(/,\s*/);
}

function extractLast(term) {
    return split(term).pop();
}

$('body').on('keyup', '#products-tags-search-input', function (event) {
    var keycode = (event.keyCode ? event.keyCode : event.which),
        tag = extractLast($(this).val()),
        search_tags = $.ajax({
            data: {
                tag: tag,
            },
            url: '/catalog/search-tags-for-products',
            type: 'post',
            dataType: 'html',
        });
    search_tags.done(function (action) {
        action = JSON.parse(action || 'null');
        var tags = [],
            i;
        for (i = 0; i < action.length; i++) {
            if (action[i] != null) {
                tags.push({value: action[i], label: action[i]});
            }

        }
        $('#products-tags-search-input').autocomplete({
            minLength: 0,
            source: function (request, response) {
                response($.ui.autocomplete.filter(
                    tags, extractLast(request.term)));
            },
            focus: function (event, ui) {
                // $('#products-tags-search-input').val(ui.item.label);
                return false;
            },
            select: function (e, ui) {
                var terms = split(this.value);
                terms.pop();
                terms.push(ui.item.value + ', ');
                this.value = terms.join(", ");
                return false;
            // select: function (event, ui) {
            //     $('#products-tags-search-input').val(ui.item.label);
            //     return false;
            }
        });
    });
})

//-------------------------------------------
// Autocomplete for search-tags form into projects
// ------------------------------------------
$('body').on('keyup', '#search-project-tags-input', function (event) {
    var keycode = (event.keyCode ? event.keyCode : event.which),
        tag = extractLast($(this).val()),
        search_tags = $.ajax({
            data: {
                tag: tag,
            },
            url: '/catalog/search-tags-for-projects',
            type: 'post',
            dataType: 'html',
        });
    search_tags.done(function (action) {
        action = JSON.parse(action || 'null');
        var tags = [],
            i;
        for (i = 0; i < action.length; i++) {
            if (action[i] != null) {
                tags.push({value: action[i], label: action[i]});
            }

        }
        $('#search-project-tags-input').autocomplete({
            minLength: 0,
            source: function (request, response) {
                response($.ui.autocomplete.filter(
                    tags, extractLast(request.term)));
            },
            focus: function (event, ui) {
                // $('#products-tags-search-input').val(ui.item.label);
                return false;
            },
            select: function (e, ui) {
                var terms = split(this.value);
                terms.pop();
                terms.push(ui.item.value + ', ');
                this.value = terms.join(", ");
                return false;
                // select: function (event, ui) {
                //     $('#products-tags-search-input').val(ui.item.label);
                //     return false;
            }
        });
    });
})