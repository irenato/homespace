jQuery( document ).ready(function( $ ){
    $('body').on('click', 'a.share-it-now-fb', function(e) {
        e.preventDefault();
        var object_id = $(this).attr('data-id');
        var object_type = $(this).attr('data-type');
        FB.ui(
            {
                method: 'feed',
                link: $(this).attr('data-url'),
                picture: $(this).attr('data-image'),
                caption: 'Reference Documentation',
                description: $(this).attr('data-description'),
            },
            function(response) {
                if (response && response.post_id) {
                    $.ajax({
                        data: {id : object_id, type : object_type},
                        type: 'post',
                        url: '/site/share-counter',
                        dataType: 'html'
                    });
                    var product_id = $('.add-to-my-projects').attr('data-id');
                    var folder_id = $('.catalog-id').attr('data-id');
                    var send = $.ajax({
                        data: {id : product_id,
                            folder : folder_id},
                        url: '/catalog/add-to-like-it',
                        type: 'post',
                        dataType: 'html'
                    });
                }
            }
        );

    });

    $('body').on('click', 'a.share-it-now-pint', function(e) {
        e.preventDefault();
        alert('good');
    });
});

    // 1517240635237951
    //FB
    window.fbAsyncInit = function() {
        FB.init({
            appId      : '325312141156977',
            xfbml      : true,
            version    : 'v2.5'
        });

    };

    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_EN/sdk.js#xfbml=1&version=v2.5&appId=325312141156977";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    // TW
    window.twttr = (function (d,s,id) {
        var t, js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return; js=d.createElement(s); js.id=id;
        js.src="https://platform.twitter.com/widgets.js"; fjs.parentNode.insertBefore(js, fjs);
        return window.twttr || (t = { _e: [], ready: function(f){ t._e.push(f) } });
    }(document, "script", "twitter-wjs"));

    // On ready, register the callback...
    twttr.ready(function (twttr) {
        twttr.events.bind('tweet', function (event) {

        });
    });



