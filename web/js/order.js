$(document).on('keyup', "#search-recipients", function (event) {
    var recipients_id = $('#application-recipients').val();
        var keycode = (event.keyCode ? event.keyCode : event.which);
        var search_recipients = $(this).val();
        var search = $.ajax({
            data: {
                search_recipients: search_recipients,
            },
            url: '/account/order/search-recipients',
            type: 'post',
            dataType: 'html',
        });
        search.done(function (action) {
            action = JSON.parse(action || 'null');
            var all_recipients = [],
                ids = [],
                i;
            for (i = 0; i < action.length; i++) {
                if (action[i].user_data != null && action[i].id != null) {
                    all_recipients.push({value: action[i].user_data, label: action[i].user_data, id: action[i].id});
                }
            }
            $('#search-recipients').autocomplete({
                minLength: 0,
                source: all_recipients,
                focus: function (event, ui) {
                    $('#search-recipients').val('');
                    return false;
                },
                select: function (event, ui) {
                    $("#search-results").append("<li><a href='#' class='del-before-upload' style='display : table;'><span class='city-name' data-id=" + ui.item.id + ">" + ui.item.label + " </span><span class='del'> (x)</span></a></li>");
                    $('#application-recipients').val($('#application-recipients').val() + ui.item.id + ',');
                    // $('#search-recipients').val(ui.item.label);
                    // $('#search-recipients').attr("data-id", ui.item.id);
                    return false;
                }
            });
        });
});

$(document).on('click', 'span.del', function (e) {
    e.preventDefault();

    var recipients = $('#application-recipients').val(),
        recipient_id = $(this).parent().find('span.city-name').attr('data-id');
    $('#application-recipients').val(recipients.replace(recipient_id + ',', ''));
    $(this).closest('li').detach();
})

// Create new folder
$('.create-folder-for-order').click(function (e) {
    e.preventDefault();
    $(this).closest('.folder').find('div.forma-input').toggle();
});

$(".folder-name").keypress(function (event) {
    var key_code = (event.keyCode ? event.keyCode : event.which);
    if (key_code == 13) {
        $('.create-new-folder-button').trigger('click');
    }
});

$('.create-new-folder-button').on('click', function (e) {
    e.preventDefault();
    var name = $(this).closest('div.forma-input').find("input[name='title']").val(),
        root = $(this).closest('div.forma-input').find("input[name='root']").val();

    if (name != '') {
        var create = $.ajax({
            data: {
                name: name,
                root: root
            },
            url: '/account/like-it/folder-create',
            type: 'post',
            dataType: 'html'
        });

        create.done(function (response) {
            location.reload();
        })
    }

});

//End Create

var some_draggable_elementh,
    some_droppable_elementh,
    id_product,
    id_folder,
    d_area,
    d_group;

$(function() {
    $("div.catalog-item").mouseenter(function(){
        some_draggable_elementh = $(this);
        d_area = $(this).closest('.folder');
        some_draggable_elementh.draggable(
            {
                containment: d_area,
                cursor: "move",
                revert: true,
            }
        );
    })
    some_droppable_elementh = $("div.name-folder");
    some_droppable_elementh.droppable({
        drop: function (event, ui) {
            some_draggable_elementh.hide();
            id_product = some_draggable_elementh.find('img').attr('data-id');
            id_folder = $(this).find('a.re_folder').attr('data-attr-id');
            var add_product = $.ajax({
                data: {
                    id_product: id_product,
                    id_folder: id_folder,
                    group: d_group,
                },
                url: '/account/like-it/add-products-to-catalog',
                type: 'post',
                dataType: 'html'
            });
            add_product.done(function(response){
                //alert(response);
            })
        }
    })
})

$('.del_folder').on('click', function () {
    var id_folder = $(this).attr('data-attr-id');

    if (id_folder) {

        var delete_folder = $.ajax({
            data: {
                id_folder: id_folder
            },
            url: '/account/like-it/folder-delete',
            type: 'post',
            dataType: 'html'
        });

        delete_folder.done(function (response) {
            location.reload();
        })

    }

});


$('.re_folder').on('click', function(){
    var id_folder = $('.re_folder').attr('data-attr-id');
    var parent = $(this).parents('.folder-bottom');
    var input_container = $(parent).find('.input-container');
    var this_input = $(input_container).find('input');
    var base_text = $(this_input).val();
    $(input_container).toggle();

    $(this_input).keypress(function (event) {
        var key_code = (event.keyCode ? event.keyCode : event.which);
        var new_name = $(this).val();
        if(key_code == 13 && new_name != "" && new_name != base_text){
            var categories = $.ajax({
                data: {
                    id_folder: id_folder,
                    name: new_name
                },
                url: '/account/like-it/folder-rename',
                type: 'post',
                dataType: 'html'
            });

            categories.done(function (response) {
                location.reload();
            });
        }
    });

});
