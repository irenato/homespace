$(document).ready(function() {

	$('.portfolio-items').click(function(){
		$('#current-item').find('a').attr('href', $(this).attr('data-link'));
		$('#current-item').find('img').attr('src', $(this).find('img').attr('src'));
		$('#current-item').find('p').html($(this).find('img').attr('alt'));
		$('#current-item-description').html($(this).attr('data-description') );
        $('html, body').animate({scrollTop: $(".portfolio-top").offset().top-100}, 1000);
	});

    $('.collections-img').click(function(){
        var current_collection = $(this).parent();
        $(current_collection).find('.img-item img').attr('src', $(this).find('img').attr('src'));
        $(current_collection).find('.img-item a').attr('href', $(this).attr('data-link'));
        $(current_collection).find('.img-item p').html($(this).find('img').attr('alt'));

    });

	$('#work-2').addClass('active-carousel_item');

     $('#work-1').click(function() {
             $('#work-2').removeClass('active-carousel_item');
             $('#work-1').css('z-index','99');
             $('#work-1').css('transform','scale(1.25)');
             $('#work-1 .work-caption').delay(100).css('opacity','1');

             $('#work-2').css('transform','scale(1)');
             $('#work-2').css('z-index','1');
             $('#work-2 .work-caption').css('opacity','0');
             $('#work-3').css('transform','scale(1)');
             $('#work-3').css('z-index','1');
             $('#work-3 .work-caption').css('opacity','0');
     });

     $('#work-2').click(function() {
         $('#work-2').removeClass('active-carousel_item');
             $('#work-2').css('z-index','99');
             $('#work-2').css('transform','scale(1.25)');
             $('#work-2 .work-caption').delay(100).css('opacity','1');

             $('#work-1').css('transform','scale(1)');
             $('#work-1').css('z-index','1');
             $('#work-1 .work-caption').css('opacity','0');
             $('#work-3').css('transform','scale(1)');
             $('#work-3').css('z-index','1');
             $('#work-3 .work-caption').css('opacity','0');
     });

     $('#work-3').click(function() {
         $('#work-2').removeClass('active-carousel_item');
             $('#work-3').css('z-index','99');
             $('#work-3').css('transform','scale(1.25)');
             $('#work-3 .work-caption').delay(100).css('opacity','1');

             $('#work-1').css('transform','scale(1)');
             $('#work-1').css('z-index','1');
             $('#work-1 .work-caption').css('opacity','0');
             $('#work-2').css('transform','scale(1)');
             $('#work-2').css('z-index','1');
             $('#work-2 .work-caption').css('opacity','0');
     });

    $('form').submit(function(e) {
        e.preventDefault();
    });


    $('body').delegate( ".hvr-radial-out a", "click", function(e) {
        e.preventDefault();
        var link = $(this).attr('href');
        $('.reviews').load(link+' .reviews > *');
        $('html, body').animate({scrollTop: $(".reviews").offset().top-150}, 1000);
    });

});

//function myShare(){
//
//    FB.init({
//        appId      : '1746633468898413',
//        xfbml      : true,
//        version    : 'v2.5'
//    });
//
//    FB.ui(
//        {
//            method: 'feed',
//            name: 'Facebook Dialogs',
//            link: 'http://developers.facebook.com/docs/reference/dialogs/',
//            picture: 'http://fbrell.com/f8.jpg',
//            caption: 'Reference Documentation',
//            description: 'Dialogs provide a simple, consistent interface for applications to interface with users.',
//            message: 'Facebook Dialogs are easy!'
//        },
//        function(response) {
//            if (response && response.post_id) {
//                alert('Post was published.');
//            } else {
//                alert('Post was not published.');
//            }
//        }
//    );
//}

//$('.tw-button').click(function(e){
//    e.preventDefault();
//    var twWin = window.open(this.href, this.title, 'toolbar=0, status=0, width=548, height=325');
//    setTimeout(function(){twWin.document.body.innerHTML = "<b>Hello, stackoverflow!</b>";}, 3000);
//
//
//});

window.twttr = (function (d,s,id) {
    var t, js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return; js=d.createElement(s); js.id=id;
    js.src="https://platform.twitter.com/widgets.js"; fjs.parentNode.insertBefore(js, fjs);
    return window.twttr || (t = { _e: [], ready: function(f){ t._e.push(f) } });
}(document, "script", "twitter-wjs"));

// On ready, register the callback...
twttr.ready(function (twttr) {
    twttr.events.bind('tweet', function (event) {

    });
});