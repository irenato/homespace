var licenses_count = getCountLicenses('.licenses-item');
var licenses_chacked = false;
var current_license = '';

$('body').delegate( ".add-licenses-img", "click", function(e) {
	e.preventDefault();
	current_license = $(this).parent();
	$('#open_browse2').trigger('click');
	that_use = 2;
});

$('form').submit(function(e) {
	e.preventDefault();
});

$('#add_license').click(function(e) {
	e.preventDefault();
	licenses_count = getCountLicenses('.licenses-item');
	var del_text = $('#hidden-licenses-text').attr('data-del-text');
	var number_text = $('#hidden-licenses-text').attr('data-number-text');
	var issued_text = $('#hidden-licenses-text').attr('data-issued-text');

	if(licenses_count >= 0 && licenses_chacked != false){
		$('.all-licenses').append('<div class="licenses-item" data-license-id=""><a href="#" class="del del-license">'+del_text+'<i></i></a><div class="clearfix"></div><div class="license-images"><div class="licenses-img add add-licenses-img" ></div></div><form class="forma-input"> <div class="input-container namb"><input class="text-input floating-label" type="text" name="number" value="" onchange="changeInput(this)" /> <label for="number">'+number_text+'</label></div><div class="input-container last"><input class="text-input floating-label" type="text" name="issued_by" value="" onchange="changeInput(this)"/> <label for="issued_by">'+issued_text+'</label></div></form></div><div class="clearfix"></div>');
		licenses_count++;
	}
});

$('#licenses_save').click(function() {
	var licenses_data = getLicensesData('.licenses-item');
	if(licenses_chacked != false){
		var request = $.ajax({
			data : {licenses : licenses_data},
			url: '/account/edit/save-licenses',
			type: 'post',
			dataType: 'html'
		});

		request.done(function( message ) {

			$('#licenses-save-error').css('visibility', 'hidden');
			$('#licenses-save-valid').html(message);
			$('#licenses-save-valid').css('visibility', 'visible');
			setTimeout(funcTimer2, 5000);

		});
	}
});

$('.del-licenses-img').click(function(e) {
	e.preventDefault();
	$(this).parent().remove();
});

$('body').delegate( ".del-license", "click", function(e) {
	e.preventDefault();
	var license_id = $(this).parent().attr('data-license-id');
	if(license_id == ""){
		$(this).parent().remove();
		licenses_count--;
	}else{

		var request = $.ajax({
			data : {id : license_id},
			url: '/account/edit/delete-license',
			type: 'post',
			dataType: 'html'
		});

		request.done(function( message ) {

			$('#licenses-save-error').css('visibility', 'hidden');
			$('#licenses-save-valid').html(message);
			$('#licenses-save-valid').css('visibility', 'visible');
			setTimeout(funcTimer2, 5000);

		});

		$(this).parent().remove();
		licenses_count--;
	}
});


function fileChange(files)
{
	var file = files.files;
	if(file.length == 1){
			upload(file);
	}
}

var upload = function(files){
	var formData = new FormData();
	formData.append('file', files[0]);
	var request = $.ajax({
		data : formData,
        url: '/account/edit/loading-image',
        type: 'post',
        dataType: 'html',
		contentType: false, 
	    processData: false,
    });

    request.done(function( url ) {

		$('<a><div class="licenses-img"><a href="#" class="del-licenses-img">X</a><img src="'+url+'" alt=""></div></a>').insertBefore($(current_license).find('.add-licenses-img'));

    });

};


function getCountLicenses(object_class){

	var count = 0;
	licenses_chacked = true;
	var temp1, temp2;

	$(object_class).each(function() {
		temp1 = $(this).find('input[name=number]').val();
		temp2 = $(this).find('input[name=issued_by]').val();
		if(temp1 == "" || temp2 == ""){
			licenses_chacked = false;
		}
	    count++;
	});

	return count;
}


function getLicensesData(object_class){

	var data = [];
	var count = 0;
	var temp1, temp2, image = "";

	licenses_chacked = true;

	$(object_class).each(function() {
		image = "";
		temp1 = $(this).find('input[name=number]').val();
		temp2 = $(this).find('input[name=issued_by]').val();
		$('img',this).each(function() {
			image += $(this).attr('src')+'|';
		});
		if(temp1 == "" || temp2 == "" || image == ""){
			licenses_chacked = false;
		}else{
			data[count] = new Array($(this).attr('data-license-id'), temp1, temp2, image);
		}
	    count++;
	});
	return data;
}

function funcTimer2() {
	$('#licenses-save-valid').css('visibility', 'hidden');
	$('#licenses-save-error').css('visibility', 'hidden');
}
