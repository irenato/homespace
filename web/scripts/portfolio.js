var current_item = '';
var id = 'work-1';
var item_src = '';
var item_url = '';
var last_item = 'modal-item-1';
var item_id = 0;
var action = false;
var ajax_send = false;
var ajax_send1 = false;

$('.tariff-plan_block .work').click(function() {
	if(ajax_send == false) {
		var request = $.ajax({
			url: '/account/portfolio/modal-items',
			type: 'post',
			dataType: 'html'
		});

		request.done(function (data) {
			$('#best_3_works .modal-body').html(data);
			ajax_send = true;
		});
	}

	id = $(this).attr('id');
	//var url = $(this).find('img').attr('data-link');
	//if(url != "" && action == false){
	//	window.location = url;
	//}
	current_item = $(this).find('img').attr('data-item');
	item_src = '';
	item_url = '';
	last_item = 'modal-item-1';
	item_id = 0;	
});

$('#set-best-3-works').click(function(e) {
	e.preventDefault();
    if(action == false){
		$('.work').css('border', '1px solid #006666');
		action = true;
    }else{
		$('.work').css('border', 'none');
		action = false;
    }
});


$('#apply_this_item').click(function(e) {
	var priz = eachItems(item_id);
	if(item_src != '' && item_id != 0 && priz == 3){
		var request = $.ajax({
			data : { last : current_item, new_id : item_id},
	        url: 'portfolio/status',
	        type: 'post',
	        dataType: 'html',
	    });

	    request.done(function( action ) {

			$('#'+id).find('img').attr('src', item_src);
			$('#'+id).find('img').attr('data-link', item_url);
			$( ".close" ).trigger( "click" );

	    });

	    request.fail(function( jqXHR, textStatus ) {
	    	alert( textStatus );
	    });
	
	}else $( ".close" ).trigger( "click" );
});

function setBestItem(item){
	last_item = $(item).attr('id');
	item_src = $(item).find('img').attr('src');
	item_id = $(item).find('img').attr('data-item');
	item_url = $(item).find('img').attr('data-link');
	$(item).find('input').prop("checked", true);
}

function eachItems(item) {
	var cnt = 0;
	$('.row-2b .work').each(function() {
		var temp = $(this).find('img').attr('data-item');
		if(temp == item){
			cnt = 0;
		}else cnt++;
	});

	return cnt;
}


$('.like').click(function(e) {
	e.preventDefault();
});


// Set Style

$('#set-style').click(function(e) {
	e.preventDefault();
	var id = $(this).attr('data-item-id');
	var style_id = $('#style_id option:selected').val();
	var comment = $('textarea[name=style-comment]').val();
	if($("#checked-moderation").prop("checked") == true &&  id != "" && comment != ""){
		var request = $.ajax({
			data: {id : id, current_id : current_style_item_id, style_id : style_id, comment : comment},
			url: '/account/portfolio/set-style',
			type: 'post',
			dataType: 'html'
		});

		request.done(function (message) {
			location.reload();
		});
	}
});

$('#get-style').click(function() {
	if(ajax_send1 == false) {
		var request = $.ajax({
			url: '/account/portfolio/modal-style-items',
			type: 'post',
			dataType: 'html'
		});

		request.done(function (data) {
			$('#styleModal .modal-body').html(data);
			ajax_send1 = true;
		});
	}
});

$('#apply-style').click(function() {
	$( ".close" ).trigger( "click" );
	$('.download img').attr('src', $('#'+style_item_id).find('img').attr('src'));
	$('#set-style').attr('data-item-id', $('#'+style_item_id).find('img').attr('data-item'));
});

var style_item_id = 'style-1';
var current_style_item_id = $('#set-style').attr('data-item-id');

function setStyleItem(item){
	style_item_id = $(item).attr('id');
	$(item).find('input').prop("checked", true);
}

function SortChangeLi(){

}




