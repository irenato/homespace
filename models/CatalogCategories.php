<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "catalog_categories".
 *
 * @property integer $id
 * @property string $title_en
 * @property string $title_ru
 * @property integer $param
 */
class CatalogCategories extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'catalog_categories';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['param'], 'integer'],
            [['title_en', 'title_ru'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title_en' => 'Title En',
            'title_ru' => 'Title Ru',
            'param' => 'Param',
        ];
    }

    public static function selectAllCategoriesByLanguage()
    {
        return static::find()
            ->asArray()
            ->all();
    }

    public static function selectCategoriesByCategoryId($categories_id, $lang)
    {
        if (is_array($categories_id))
            $categories = static::find()
                ->where(['in', 'id', $categories_id])
                ->select(['id', 'title_' . $lang])
                ->orderBy('id DESC')
                ->asArray()
                ->all();
        else
            $categories = static::find()
                ->where(['id' => $categories_id])
                ->select(['id', 'title_' . $lang])
                ->orderBy('id DESC')
                ->asArray()
                ->all();
        return $categories;
        die();
    }
}
