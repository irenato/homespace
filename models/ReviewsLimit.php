<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "profile_reviews".
 *
 * @property integer $id
 * @property integer $to
 * @property integer $from
 * @property integer $date
 * @property string $review
 */
class ReviewsLimit extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'reviews_limit';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'reviews_count', 'category'], 'required'],
            [['user_id', 'reviews_count', 'category'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'reviews_count' => 'Reviews Count',
            'category' => 'Category',
            'date' => 'Date'
        ];
    }

    public static function checkReviews($user_id, $category){
        $get_reviews_by_category = ReviewsLimit::find()->select([ 'reviews_count','date'])->where(['user_id'=>$user_id, 'category' => $category])->asArray()->one();

        if(empty($get_reviews_by_category)){
            $reviews_limit = new ReviewsLimit();
            $reviews_limit->user_id = $user_id;
            $reviews_limit->reviews_count = 1;
            $reviews_limit->date = time();
            $reviews_limit->category = $category;
            $reviews_limit->save();
            return true;
        }else{

            $last_modified = date('Y-m-d', $get_reviews_by_category['date']);
            $current_date = date('Y-m-d', time());

            if($last_modified != $current_date){
                $update_reviews = ReviewsLimit::findOne(['user_id' => $user_id, 'category' => $category]);
                $update_reviews->reviews_count = 1;
                $update_reviews->date = time();
                $update_reviews->update();
                return true;
            }else if($last_modified == $current_date &&  $get_reviews_by_category['reviews_count']<20){
                $update_reviews = ReviewsLimit::findOne(['user_id' => $user_id, 'category' => $category]);
                $update_reviews->reviews_count = $update_reviews->reviews_count + 1;
                $update_reviews->date = time();
                $update_reviews->update();
                return true;
            }else{
                return false;
            }
        }
    }

}