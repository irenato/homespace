<?php

namespace frontend\models;

class Search{

    public static function searchPortfolio($q, $lang){
        $data = \Yii::$app->db->createCommand("SELECT portfolio.item_id, portfolio.owner_id, portfolio.images, item_description.title, portfolio.likes
                                               FROM  `portfolio`
                                               LEFT JOIN `item_description` ON (item_description.item_id = portfolio.item_id)
                                               WHERE item_description.language = '$lang'
                                               AND item_description.title LIKE '%".$q."%'")->queryAll();
        return $data;
    }



}