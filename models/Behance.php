<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\models;

use yii\authclient\OAuth2;

class Behance extends OAuth2
{
    /**
     * @inheritdoc
     */
    public $authUrl = 'https://www.behance.net/v2/oauth/authenticate';
    /**
     * @inheritdoc
     */
    public $tokenUrl = 'https://www.behance.net/v2/oauth/token';
    /**
     * @inheritdoc
     */
    public $apiBaseUrl = 'http://www.behance.net/dev/apps';
    /**
     * @inheritdoc
     */
    public $scope = 'email';

    /**
    * @var array list of attribute names, which should be requested from API to initialize user attributes.
    * @since 2.0.5
    */
   public $attributeNames = [
       'name',
       'email',
   ];

    /**
     * @inheritdoc
     */
    protected function initUserAttributes()
    {
        return $this->api('me', 'GET', [
            'fields' => implode(',', $this->attributeNames),
        ]);
    }

    /**
     * @inheritdoc
     */
    protected function defaultName()
    {
        return 'behance';
    }

    /**
     * @inheritdoc
     */
    protected function defaultTitle()
    {
        return 'Behance';
    }

    /**
     * @inheritdoc
     */
    protected function defaultViewOptions()
    {
        return [
            'popupWidth' => 860,
            'popupHeight' => 480,
        ];
    }
}

