<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "user_videos".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $link
 * @property string $name
 */
class UserVideos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_videos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'link', 'name'], 'required'],
            [['user_id'], 'integer'],
            [['link', 'name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'link' => 'Link',
            'name' => 'Name',
        ];
    }

    public function addVideo($id_user, $link, $name)
    {
        $this->user_id = (int)$id_user;
        $this->link = $link;
        $this->name = $name;
        return $this->save();
    }

    public static function selectUserVideo($user_id)
    {
        return static::findOne(['user_id'=>$user_id]);
    }

    public static function updateUserVideo($user_id, $link, $name){
        $video = static::findOne(['user_id'=>$user_id]);
        $video->link = ($link != null) ? $link : $video->link;
        $video->name = ($name != null) ? $name : $video->name;
        return $video->update();
    }
}
