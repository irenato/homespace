<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "portfolio_collections".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $items
 * @property integer $main
 * @property string $name
 * @property string $about
 */
class PortfolioCollections extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'portfolio_collections';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'items', 'main', 'name', 'about'], 'required'],
            [['user_id', 'main'], 'integer'],
            [['about'], 'string'],
            [['items', 'name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'items' => 'Items',
            'main' => 'Main',
            'name' => 'Name',
            'about' => 'About',
        ];
    }
}
